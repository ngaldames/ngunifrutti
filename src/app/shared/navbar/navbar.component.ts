import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ConfiUsuarioComponent } from '../dialogs/confi-usuario/confi-usuario.component';
import { ApiseguridadService } from '../../services/apiseguridad.service';
import { ApiCommonService } from '../../services/apicommonservice.service';
import { BuscardormenuComponent } from '../../pages/buscardormenu/buscardormenu.component';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {
  public version: string = environment.package.version;
  public nombre: string = environment.package.name;
  public publicacion: string = environment.publicacion;
  @Output()
  propagar = new EventEmitter<string>();
  propagarBusquedaMenu = new EventEmitter<any>();
  menuItem: any[] = [];
  titulo = 'Bienvenido';
  urlLogoEmpresa = 'assets/img/unifrutti.png';
  urlLogoPall = 'assets/img/logopallb.png';
  menuSuperior: any = [];
  NavExp = '';
  NavTemp = '';
  NavPlanta = '';
  NavUsuario: string = null;
  arr_buscador = [];
  txt_buscar = '';
  estadoMenu: string = 'Cargando menús...';

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private wsSeguridad: ApiseguridadService,
    private wsCommon: ApiCommonService
  ) {
    if (!localStorage.getItem('Nexportadora')) {
      setTimeout(() => {
        this.openDialog();
      }, 500);
    }
  }

  ngOnInit() {
    this.menuItem = [];
    this.NavExp = localStorage.getItem('Nexportadora');
    this.NavTemp = localStorage.getItem('Ntemporada');
    this.NavPlanta = localStorage.getItem('Nplanta');
    this.NavUsuario = localStorage.getItem('User');
    this.cargarMenus();
  }

  onNavigate() {
    window.open('http://www.unifrutti.com/', '_blank');
  }

  openDialog() {
    const dialogRef = this.dialog.open(ConfiUsuarioComponent, {
      width: '250px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.NavExp = localStorage.getItem('Nexportadora');
      this.NavTemp = localStorage.getItem('Ntemporada');
      this.NavPlanta = localStorage.getItem('Nplanta');
      this.NavUsuario = localStorage.getItem('User');
      console.log('Exportadora (ts): ' + this.NavExp + '/ Temporada: ' + this.NavTemp + '/ Planta: ' + this.NavPlanta);
      this.cargarMenus();
    });
  }

  cargarMenus() {
    setTimeout(() => {
      let idexportadora = localStorage.getItem('NidExportadora');
      let idPlanta = localStorage.getItem('NidPlanta');
      this.NavUsuario = localStorage.getItem('User');
      if (idexportadora && idPlanta) {
        this.wsSeguridad.MenuUsuario(this.NavUsuario, idPlanta, idexportadora)
          .subscribe((result_ws) => {
            console.log('MENUS', result_ws);
            this.menuSuperior = result_ws;
            this.estadoMenu = null;
          }, (error) => {
            this.estadoMenu = 'Error al cargar menú';
            console.error('ERROR CARGANDO MENU', error);
          });
      }
    }, 2000);
  }

  selectMenu(menu: any) {
    this.propagar.emit(menu);
  }

  verificarRuta(menu: any) {
    this.menuItem = [];
    console.log(JSON.stringify(menu));
    if (menu) {
      this.router.navigate([menu.path]).then((e) => {
        this.menuItem = menu.menus;
        this.titulo = menu.nombre;
      });
    }
  }

  abrirPantalla(item: any) {
    this.router.navigate([item.path]);
  }

  logout() {
    localStorage.removeItem('sesion');
    this.router.navigate(['login']);
  }

  buscarMenu(event: any) {
    // const url = this.router.url;
    // // localStorage.setItem('ultimaurl', url);
    // this.arr_buscador = [];
    // this.router.navigate(['buscador']);
    // if (event === '' ) {
    //       this.enviarBuscadorMenu(false);
    // } else {
    //     this.menuSuperior.forEach(element => {
    //       this.recorrerMenusBuscar(element.menus);
    //     });
    //     this.enviarBuscadorMenu(true);
    // }
  }

  buscarMenuEnter(event: any) {
    if (event.target.value === '') {
      return;
    }
    this.arr_buscador = [];
    if (event.code === 'Enter') {
      this.dialog.open(BuscardormenuComponent, {
        width: '1000px',
        data: {}
      });
      //  this.router.navigate(['buscador']);
      this.menuSuperior.forEach(element => {
        this.recorrerMenusBuscar(element.menus);
      });
      setTimeout(() => {
        this.enviarBuscadorMenu(true);
      }, 100);
      this.txt_buscar = '';
    }
  }

  recorrerMenusBuscar(menu: any) {
    menu.forEach(element => {
      if (element.menus.length > 0) {
        this.recorrerMenusBuscar(element.menus);
      } else {
        if (element.nombre.toUpperCase().indexOf(this.txt_buscar.toUpperCase()) >= 0) {
          this.arr_buscador.push(element);
        }
      }
    });
    this.enviarBuscadorMenu(true)
  }

  enviarBuscadorMenu(buscar) {
    this.wsCommon.enviarMenusBusqueda(this.arr_buscador, buscar);
  }
}