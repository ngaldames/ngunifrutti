import { AutenticacionService } from './../../services/autenticacion.service';
import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

const TREE_DATA: any[] = [
  {
    name: 'Fruit',
    children: [
      { name: 'Apple' },
      { name: 'Banana' },
      { name: 'Fruit loops' },
    ]
  }, {
    name: 'Vegetables',
    children: [
      {
        name: 'Green',
        children: [
          { name: 'Broccoli' },
          { name: 'Brussel sprouts' },
        ]
      }, {
        name: 'Orange',
        children: [
          { name: 'Pumpkins' },
          { name: 'Carrots' },
        ]
      },
    ]
  },
];
@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})

export class SidenavComponent implements OnInit {
  menuItem: any = [];
  panelOpenState = false;
  num_niv = 0;
  titulo = 'Bienvenido';
  nuevo_arr: any = [];
  contador = 0;
  nuevo: any = [];
  arr_parcial = [];
  menu = '';

  constructor(
    private router: Router,
    private authService: AutenticacionService,
  ) { }

  expandir(ind: any) {
    console.log('EXPANDIR', this.arr_parcial[ind]);
    if (this.arr_parcial[ind].url && this.arr_parcial[ind].url !== 'url') {
      console.log('tiene url');
      this.router.navigate([this.arr_parcial[ind].url], { queryParams: { mn: this.arr_parcial[ind].id } });
    }
    if (!this.arr_parcial[ind].menus) {
      return;
    }
    if (ind < this.arr_parcial.length - 1) {
      if (this.arr_parcial[ind + 1].left > this.arr_parcial[ind].left) {
        let ultimoAeliminar = 0;
        for (let index = ind + 1; index < this.arr_parcial.length; index++) {
          const element = this.arr_parcial[index];
          if (element.left > this.arr_parcial[ind].left) {
            ultimoAeliminar++;
          } else {
            break;
          }
        }
        this.arr_parcial.splice(ind + 1, ultimoAeliminar);
        return;
      }
    }

    this.arr_parcial[ind].menus.forEach(element => {
      element.left = this.arr_parcial[ind].left + 10;
      if (element.menus.length === 0) {
        element.ultimo_elemento = true;
        console.log('no hay mas', element);
      }
      this.arr_parcial.splice(ind + 1, 0, element);
    });

  }

  ngOnInit() { }

  favoritos() {
    this.router.navigate(['favoritos']);
  }

  abrirPantalla(item: any) { }

  logout() {
    localStorage.removeItem('Session')
    localStorage.clear();
    this.authService.change(false);
    this.router.navigate(['login']);
  }

  recibirMenu(mn: any) {
    this.arr_parcial = [];
    this.titulo = mn.nombre;
    console.log(mn);
    mn.menus.forEach(element => {
      element['left'] = 10;
      this.arr_parcial.push(element);
    });
  }
}