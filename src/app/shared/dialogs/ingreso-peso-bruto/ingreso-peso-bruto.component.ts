import { ApilogisticaService } from './../../../services/apilogistica.service';
import { ApiCommonService } from './../../../services/apicommonservice.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-ingreso-peso-bruto',
  templateUrl: './ingreso-peso-bruto.component.html',
  styleUrls: ['./ingreso-peso-bruto.component.css']
})
export class IngresoPesoBrutoComponent implements OnInit {
  /////////////////////////////////////////////////////////
  temporada: string = localStorage.getItem('temporada');
  usuario = localStorage.getItem('User');
  exportadora: string = localStorage.getItem('exportadora');
  planta: string = localStorage.getItem('planta');
  nombrePlanta: string = localStorage.getItem('Nplanta');
  zona: number = 0;
  //////////////////////////////////////////////////////////
  edita: boolean = false;
  cabecera: any = {
    id: null,
    idGuiaProductor: null,
    fechaCosecha: null,
    fechaRecepcion: null,
    fechaDestare: null,
    patenteCamion: '',
    patenteCarro: '',
    chofer: { id: null },
    idPesa: null,
    zona: { id: null },
    temporada: { id: this.temporada },
    empresa: { id: null, nombre: null },
    huerto: { id: null, codCSG: null },
    frigorifico: { id: null },
    pesoBrutoCamion: 0,
    pesoBrutoCarro: 0,
    pesoBrutoTotal: 0,
    pesoEnvasesIn: 0,
    pesoEnvasesOut: 0,
    pesoTaraCamion: 0,
    pesoTaraCarro: 0,
    pesoTaraTotal: 0,
    pesoNeto: 0,
    glosa: '',
    codSeccion: null,
    terminada: false,
    usuario: { id: this.usuario },
    exportadora: { id: this.exportadora },
    codTransaccionProceso: 'A',
    zonaProductor: { id: null, codProductor: null }
  };
  detalleAsignar: any = {
    tipoPesa: '1',
    especie: { id: null },
    variedad: { id: null },
    spd: { id: null },
    tipoContenedor: { id: null },
    impresora: { id: null, nombre: null },
    restoBins: 0,
    totalBins: 0,
    totalTara: 0,
    pesoBrutoBalanza: 0,
    promedioPesaje: 0,
    elementoPesado: '1',
    pesoVariedadRomanaCamionCarro: null,
    pesoBrutoRomana: null
  };
  // ARREGLOS GENERALES
  especies: any = [];
  variedades: any = [];
  envasesEntradaAgrega: any = [];
  envasesSalida: any = [];
  detalleRecepcion: any = [{
    numLinea: 0
  }];
  spds: any = [];
  contenedores: any = [];
  impresoras: any = [];
  detalleContenedores: any = [{
    cantidad: null,
    esContenedor: null,
    tara: null,
    ver: null,
    cantidadPesar: null,
    envaseCosecha: {
      id: null,
      nombre: null,
      tara: null,
      cantidadEmpresa: null,
      cantidadOtros: null,
      cantidadTotal: null,
      pesoMinimo: null,
      pesoMaximo: null,
      totalBins: null
    }
  }];
  tipoContenedores: any = [{
    cantidad: null,
    esContenedor: null,
    tara: null,
    ver: null,
    cantidadPesar: null,
    envaseCosecha: {
      id: null,
      check: null,
      nombre: null,
      tara: null,
      cantidadEmpresa: null,
      cantidadOtros: null,
      cantidadTotal: null,
      pesoMinimo: null,
      pesoMaximo: null,
      totalBins: null
    }
  }];
  dataSourceContenedores = new MatTableDataSource([]);
  dataSourceContenedores2 = new MatTableDataSource([]);
  columnasDet: string[] = ['contenedor', 'descripcion', 'cantidad', 'tara', 'empresa', 'otros', 'total'];
  columnasDet2: string[] = ['check', 'contenedor', 'descripcion', 'cantidad', 'tara', 'empresa', 'otros', 'total'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<IngresoPesoBrutoComponent>,
    private snackBar: MatSnackBar,
    private wsCommon: ApiCommonService,
    private wsLogistica: ApilogisticaService
  ) {
    if (data) {
      this.cabecera = data.cabecera;
      this.detalleRecepcion = data.detalles;
      this.envasesSalida = data.envasesSalida;
    }
  }

  ngOnInit() {
    this.cargarEspecies();
  }

  cargarEspecies() {
    this.especies = [];
    this.wsCommon.cargarEspecie(this.cabecera.huerto.id.trim(), this.cabecera.temporada.id, this.cabecera.zona.id, this.cabecera.zonaProductor.id)
      .subscribe((response) => {
        // console.log('RESPONSE ESPECIE', response);
        this.especies = response.items;
      }, (error) => {
        console.error('ERROR CARGANDO ESPECIES', error);
      });
  }

  finalizar() {
    console.log('FINALIZAR', this.detalleAsignar);
    if (this.detalleAsignar.tipoPesa === '1') {
      console.log('POR ROMANA');
      this.agregarDetallePorRomana();
    }
    if (this.detalleAsignar.tipoPesa === '2') {
      console.log('POR PESAS');
    }
  }

  agregarDetallePorRomana() {
    if (this.detalleRecepcion) {
      let ultimaLinea: number = this.detalleRecepcion[this.detalleRecepcion.length - 1].numLinea;
      let totalTara = 0.0;
      let env = {
        id: null, zona: { id: null }, temporada: { id: null }, frigorifico: { id: null }, linea: 0, envaseCosecha: { id: null },
        cantidadEmpresa: 0, cantidadOtros: 0, esContenedor: null, contenedor: { id: null }, exportadora: { id: null }
      };
      this.dataSourceContenedores.data.forEach((envase: any) => {
        console.log('ENVASE IT', envase);
        env.id = this.cabecera.id;
        env.zona.id = this.cabecera.zona.id;
        env.temporada.id = this.cabecera.temporada.id;
        env.frigorifico.id = this.cabecera.frigorifico.id;
        env.linea = ultimaLinea + 1;
        env.envaseCosecha = envase.envaseCosecha.id;
        env.cantidadEmpresa = envase.envaseCosecha.cantidadEmpresa;
        env.cantidadOtros = envase.envaseCosecha.cantidadOtros;
        env.esContenedor = envase.esContenedor;
        env.contenedor.id = this.detalleAsignar.tipoContenedor.id;
        env.exportadora.id = this.cabecera.exportadora.id;
        totalTara += envase.envaseCosecha.tara * (envase.envaseCosecha.cantidadEmpresa + envase.envaseCosecha.cantidadOtros);
        this.envasesEntradaAgrega.push(env);
      });
      console.log('TOTAL TARA', totalTara);
      this.detalleAsignar.pesoVariedadRomanaCamionCarro - totalTara;
      console.log('PESO IN', this.detalleAsignar.pesoVariedadRomanaCamionCarro - totalTara);
      let reqDetalle = {
        id: this.cabecera.id,
        zona: { id: this.cabecera.zona.id },
        temporada: { id: this.cabecera.temporada.id },
        frigorifico: { id: this.cabecera.frigorifico.id },
        codCuartel: '2',
        huerto: { id: this.cabecera.huerto.id },
        empresa: { id: this.cabecera.empresa.id },
        variedad: { idsnap: this.detalleAsignar.variedad.id },
        especie: { idsnap: this.detalleAsignar.especie.id },
        fechaCosecha: this.cabecera.fechaCosecha,
        pesoBruto: 0,
        pesoTara: '',
        pesoEnvasesIn: '',
        pesoEnvasesOut: 0,
        envaseCosecha: { id: this.dataSourceContenedores.data[0].envaseCosecha.id },
        numEnvasesCont: this.detalleAsignar.totalBins,
        numLinea: ultimaLinea + 1,
        exportadora: { id: this.cabecera.exportadora.id },
        cuentaCorriente: false,
        folioCKU: 0,
        tipoDucha: { id: '00' },
        tipoFrio: { id: '00' },
        preFrio: false,
        kilosNetosIn: '',
        KilosNetosOut: 0,
        snapAptitud: '00',
        contenedor: { id: this.detalleAsignar.tipoContenedor.id },
        condicion: { id: '00' }
      };

      console.log('RESULTADO ENVIO ENVASES', this.envasesEntradaAgrega);
      console.log('RESULTADO ENVIO ENVASES', JSON.stringify(this.envasesEntradaAgrega));

      console.log('REQUEST GUARDA LINEA', reqDetalle);
      console.log('REQUEST GUARDA LINEA', JSON.stringify(reqDetalle));
      // this.wsLogistica.agregarEnvasesEntrada(reqDetalle, this.envasesEntradaAgrega)
      //   .subscribe((response) => {
      //     console.log('RESPONSE GUARDAR DETALLE', response);
      //   }, (error) => {
      //     console.error('ERROR GUARDA DETALLE',error);
      //   });
    }
  }

  validarCampos() {
    if (this.detalleAsignar.tipoPesa === '1') {
      if (this.detalleAsignar.especie.id && this.detalleAsignar.variedad.id
        && this.cabecera.fechaCosecha && this.detalleAsignar.tipoContenedor.id
        && this.detalleAsignar.pesoVariedadRomanaCamionCarro && this.detalleAsignar.totalBins > 0) {
        return true;
      }
    }
    if (this.detalleAsignar.tipoPesa === '2') {
      if (this.detalleAsignar.especie.id && this.detalleAsignar.variedad.id
        && this.cabecera.fechaCosecha && this.detalleAsignar.tipoContenedor.id
        && this.detalleAsignar.pesoBrutoBalanza && this.detalleAsignar.totalBins > 0
        && this.detalleAsignar.promedioPesaje) {
        return true;
      }
    }
  }

  /////////////////
  // POR ESPECIE //
  /////////////////


  /////////////////
  // POR VARIDAD //
  /////////////////
  cambioTipoPesa(event: any) {
    if (event.checked) {
      this.detalleAsignar.tipoPesa = '2';
      this.cargarImpresoras();
    } else {
      this.detalleAsignar.tipoPesa = '1';
    }
    this.cargarEspecies();
  }

  // POR ROMANA
  sumarPeso(valor: any) {
    // console.log('PESO BRUTO TOTAL', this.cabecera.pesoBrutoTotal);
    if (!this.detalleAsignar.pesoVariedadRomanaCamionCarro) {
      this.detalleAsignar.pesoBrutoRomana = parseInt(this.cabecera.pesoBrutoTotal, 10);
    } else {
      this.detalleAsignar.pesoBrutoRomana = parseInt(this.cabecera.pesoBrutoTotal.toString(), 10) +
        parseInt(this.detalleAsignar.pesoVariedadRomanaCamionCarro.toString(), 10);
    }
  }

  // POR PESAS
  cargarImpresoras() {
    this.impresoras = [];
    this.wsCommon.cargarListadoImpresoras(this.cabecera.codSeccion, this.cabecera.zona.id, this.cabecera.frigorifico.id)
      .subscribe((result) => {
        // console.log('RESPONSE LISTADO IMPRESORAS', result);
        this.impresoras = result.items;
      }, (error) => {
        console.error('ERROR IMPRESORAS', error);
      });
  }

  seleccionEspecie(especie: any) {
    console.log('ESPECIE SELECCION', especie);
    this.variedades = [];
    this.spds = [];
    this.contenedores = [];
    this.detalleAsignar.variedad.id = null;
    this.detalleAsignar.spd.id = null;
    this.detalleAsignar.tipoContenedor.id = null;
    this.wsCommon.cargarVariedad(this.cabecera.huerto.id.trim(), this.cabecera.temporada.id, this.cabecera.zona.id, this.cabecera.zonaProductor.id, especie)
      .subscribe((response) => {
        // console.log('RESPONSE VARIEDADES', response);
        if (response) {
          this.variedades = response.items;
        }
      }, (error) => {
        console.error('ERROR VARIEDADES', error);
      });
  }

  seleccionVariedad(variedad: any) {
    this.spds = [];
    this.detalleContenedores = [];
    this.detalleAsignar.spd.id = null;
    this.detalleAsignar.tipoContenedor.id = null;
    this.cargarContenedores();
    this.wsCommon.cargarSDP(this.cabecera.huerto.id.trim(), this.cabecera.temporada.id, this.cabecera.zona.id, this.cabecera.huerto.id, this.detalleAsignar.especie.id, variedad)
      .subscribe((response) => {
        // console.log('RESPONSE SPD', response);
        if (response) {
          this.spds = response.items;
        }
      }, (error) => {
        console.error('ERROR CARGANDO SPD', error);
      });
  }

  cargarContenedores() {
    this.contenedores = [];
    this.wsCommon.cargarEnvasesCosechas(this.detalleAsignar.especie.id, this.detalleAsignar.variedad.id, this.cabecera.exportadora.id)
      .subscribe((response) => {
        // console.log('RESPONSE CONTENEDORES', response);
        this.contenedores = response;
        if (this.contenedores.length == 0) {
          this.detalleAsignar.variedad.id = null;
          this.snackBar.open('Variedad sin contenedores', null, { duration: 2000 });
        }
      }, (error) => {
        console.error('ERROR CARGANDO CONTENEDORES', error);
      });
  }

  seleccionContenedor(contenedor: any) {
    this.dataSourceContenedores = new MatTableDataSource([]);
    this.cargarTablaSecundaria(contenedor);
    this.wsCommon.cargarDetalleEnvasesCosechas(contenedor)
      .subscribe((response) => {
        // console.log('RESPONSE DETALLE ENVASE', response);
        this.detalleContenedores = response.items;
        let totalBins = 0;
        this.detalleContenedores.forEach((item: any, indice: number) => {
          this.wsCommon.cargarPesoZona(this.cabecera.zona.id, item.envaseCosecha.id)
            .subscribe((res) => {
              // console.log('RESPONSE PESOS', res);
              this.detalleContenedores[indice].envaseCosecha.tara = res.items[0].pesoEstado;
              this.detalleContenedores[indice].envaseCosecha.pesoMinimo = res.items[0].pesoMinimo;
              this.detalleContenedores[indice].envaseCosecha.pesoMaximo = res.items[0].pesoMaximo;
              this.detalleContenedores[indice].envaseCosecha.cantidadEmpresa = 0;
              this.detalleContenedores[indice].envaseCosecha.cantidadOtros = 0;
              this.detalleContenedores[indice].envaseCosecha.cantidadTotal = 0;
              totalBins = parseInt(this.detalleContenedores[indice].envaseCosecha.cantidadEmpresa, 10) + parseInt(this.detalleContenedores[indice].envaseCosecha.cantidadOtros, 10);
            }, (error) => {
              console.error('ERROR AL OBTENER PESOS', error);
            });
        });
        this.detalleAsignar.totalBins = totalBins;
        this.dataSourceContenedores = new MatTableDataSource(this.detalleContenedores);
      }, (error) => {
        console.error('ERROR DETALLE ENVASE', error);
      });
  }

  cargarTablaSecundaria(contenedor: any) {
    this.dataSourceContenedores2 = new MatTableDataSource([]);
    this.wsCommon.cargarDetalleEnvasesCosechas(contenedor)
      .subscribe((response) => {
        // console.log('RESPONSE DETALLE ENVASE', response);
        this.tipoContenedores = response.items;
        let totalBins = 0;
        this.tipoContenedores.forEach((item: any, indice: number) => {
          this.wsCommon.cargarPesoZona(this.cabecera.zona.id, item.envaseCosecha.id)
            .subscribe((res) => {
              // console.log('RESPONSE PESOS', res);
              this.tipoContenedores[indice].envaseCosecha.check = true;
              this.tipoContenedores[indice].envaseCosecha.tara = res.items[0].pesoEstado;
              this.tipoContenedores[indice].envaseCosecha.pesoMinimo = res.items[0].pesoMinimo;
              this.tipoContenedores[indice].envaseCosecha.pesoMaximo = res.items[0].pesoMaximo;
              this.tipoContenedores[indice].envaseCosecha.cantidadEmpresa = 0;
              this.tipoContenedores[indice].envaseCosecha.cantidadOtros = 0;
              this.tipoContenedores[indice].envaseCosecha.cantidadTotal = 0;
            }, (error) => {
              console.error('ERROR AL OBTENER PESOS', error);
            });
        });
        this.dataSourceContenedores2 = new MatTableDataSource(this.tipoContenedores);
        console.log('DETALLE 2',this.dataSourceContenedores2.data);
      }, (error) => {
        console.error('ERROR DETALLE ENVASE', error);
      });
  }

  validarTara(valor: any, detalle: any) {
    if (detalle) {
      if (parseFloat(valor) < detalle.envaseCosecha.pesoMinimo || parseFloat(valor) > detalle.envaseCosecha.pesoMaximo) {
        this.snackBar.open('La tara no está en el rango', null, { duration: 1000 });
        let index = this.detalleContenedores.findIndex((element: any) => {
          return element.envaseCosecha.id === detalle.envaseCosecha.id;
        });
        this.detalleContenedores[index].envaseCosecha.tara = null;
        return;
      }
    }
  }

  cantidadPesar(valor: any) {
    console.log('Cantidad pesar', valor);

  }

  agregarPesaje() {
    let data = {};
    let data2 = {};

    this.wsLogistica.agregarEnvasesEntrada(data, data2)
      .subscribe((response) => {

        console.log('AGREGA ENVASES ENTRADA', response);
      }, (error) => {
        console.error('ERROR AGREGAR ENVASES ENTRDADA', error);
      });
  }

  sumaTotalBines() {
    let suma = 0;
    this.dataSourceContenedores.data.forEach((det: any) => {
      suma += parseInt(det.envaseCosecha.cantidadEmpresa) + parseInt(det.envaseCosecha.cantidadOtros);
    });
    this.detalleAsignar.totalBins = suma;
  }

  validaAgregaPesaje() {

  }

  calcularPesoPromedio() {
    let sumaTaras = 0;
    let configuracionTotem = 0;
    this.dataSourceContenedores.data.forEach((envase: any, indice: number) => {
      console.log('indice', indice, 'DETALLE', envase);
      if (!envase.esContenedor) {

      }
      sumaTaras += parseInt(envase.cantidad) * parseFloat(envase.tara);
    });
    console.log('TOTAL TARA', sumaTaras);
    // this.detalleAsignar.cantidadPesar

  }

  cerrar() {
    this.dialogRef.close();
  }
}