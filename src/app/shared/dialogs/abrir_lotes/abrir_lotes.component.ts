import { Subscription } from 'rxjs';
import { ApiCommonService } from './../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../services/apilogistica.service';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-abrir_lotes',
  templateUrl: './abrir_lotes.component.html',
  styleUrls: ['./abrir_lotes.component.css']
})
export class Abrir_lotesComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['1', '2', '3', '4', '5', '6', '7'];
  muetraColumnas: string[] = ['1', '2', '4', '5', '6'];
  envasesSource = new MatTableDataSource([]);
  dataSource = new MatTableDataSource([]);
  lineaCKU: any = [];
  especies: any = [];
  variedades: any = [];
  detalleAsignar: any = {
    variedad: { id: null, nombre: null },
    especie: { id: null, nombre: null }
  };
  nro_linea: number;
  peso_bruto: number;
  registro_encontrado: boolean = false;
  sub: Subscription;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<Abrir_lotesComponent>,
    private snackBar: MatSnackBar,
    private serviceLogistica: ApilogisticaService,
    private serviceCommon: ApiCommonService
  ) { }

  ngOnInit() {
    this.lineaCKU = this.data;
    console.log('LLEGA ABRIR LOTES ', this.lineaCKU);
    this.cargarTablaCabecera();
    // this.cargarTablaDetalle();
    this.serviceCommon.cargarEspecies()
      .subscribe((data) => {
        // console.log('ESPECIES', data);
        this.especies = data.items;
      }, (error) => {
        console.log('Error al cargar especies');
      });
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  selecionaEspecie(especie: any) {
    this.variedades = [];
    this.serviceCommon.cargarVarieades(especie.id)
      .subscribe((data) => {
        this.variedades = data.items;
      }, (error) => {
        console.log('Error al cargar variedades');
      });
  }

  cargarTablaCabecera() {
    let req = {
      cod_Temp: this.lineaCKU.temporada.id,
      cod_Planta: this.lineaCKU.frigorifico.id,
      cod_Exportadora: this.lineaCKU.exportadora.id,
      cod_zona: this.lineaCKU.zona.id,
      num_recep: this.lineaCKU.id
    };
    this.sub = this.serviceLogistica.cargarListadoCabeceraLotes(req)
      .subscribe((data) => {
        // console.log('DATA CABECERA', data);
        let valido = data.valido;
        if (valido) {
          console.log('RESPUESTA CABECERA', data);
          this.nro_linea = data.lista.items[0].linea;
          this.envasesSource = data.lista.items;
          this.cargarTablaDetalle();
        } else {
          this.snackBar.open('Sin lote', null, { duration: 1000 });
        }
      }, (error) => {
        console.log('Error cargar cabecera', error);
      });
  }

  cargarTablaDetalle() {
    if (this.lineaCKU.exportadora.id != undefined && this.lineaCKU.frigorifico.id != undefined
      && this.lineaCKU.temporada.id != undefined && this.lineaCKU.zona.id != undefined
      && this.lineaCKU.id != undefined
    ) {
      let req = {
        cod_Exportadora: this.lineaCKU.exportadora.id,
        cod_Planta: this.lineaCKU.frigorifico.id,
        cod_Temp: this.lineaCKU.temporada.id,
        cod_Zona: this.lineaCKU.zona.id,
        num_Recepcion: this.lineaCKU.id
      };
      this.sub = this.serviceLogistica.listadoDetalleCku(req)
        .subscribe((data) => {
          console.log('RESPUESTA DETALLE', data);
          let valido = data.valido;
          if (valido) {
            this.dataSource = data.lista.items;
          } else {
            this.snackBar.open('Sin detalles', null, { duration: 1000 });
          }
        }, (error) => {
          this.snackBar.open('Error al cargar detalles', null, { duration: 1000 });
        });
    }
  }

  editarLinea(linea: any) {
    console.log('Linea', linea);
    this.detalleAsignar = linea;
  }

  cancelarLinea() {
    this.registro_encontrado = false;
    this.detalleAsignar = {
      variedad: { id: null, nombre: null },
      especie: { id: null, nombre: null }
    };
  }

  actualizarLinea() {
    let req = {

    };
    this.serviceLogistica.agregaLineaLotes(req)
      .subscribe((data) => {
        console.log('RESPONSE AGREGA ')
      }, (error) => {

      });
  }

  cerrar() {
    this.dialogRef.close({ data: this.lineaCKU });
  }

  guardarLoteEditado() {
    this.snackBar.open('Lote editado correctamente', null, { duration: 1000 });
    this.cerrar();
  }
}