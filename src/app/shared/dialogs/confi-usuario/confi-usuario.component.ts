import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ApiCommonService } from '../../../services/apicommonservice.service';
import { AutenticacionService } from '../../../services/autenticacion.service';

@Component({
  selector: 'app-confi-usuario',
  templateUrl: './confi-usuario.component.html',
  styleUrls: ['./confi-usuario.component.css']
})
export class ConfiUsuarioComponent implements OnInit {
  _usr = localStorage.getItem('User');
  cargando: boolean = true;

  //ARREGLOS
  exportadoras: any = [];
  temporadas: any = [];
  plantas: any = [];
  validaPlantas: boolean = false;

  //GUARDADOS
  exportadora: string = null;
  temporada: string = null;
  planta: string = null;
  nombrePlanta: string = null;
  nombreTemporada: string = null;
  nombreExportadora: string = null;

  constructor(
    private dialogRef: MatDialogRef<ConfiUsuarioComponent>,
    private _router: Router,
    private apicommonService: ApiCommonService,
    private authService: AutenticacionService,
    private snackBar: MatSnackBar
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.cargarExportadoras();
    this.cargarTemporadas();
    this.cargarGuardados();
  }

  cargarGuardados() {
    this.exportadora = null;
    this.temporada = null;
    this.planta = null;
    if (localStorage.getItem('exportadora')) {
      this.exportadora = localStorage.getItem('exportadora');
    }
    if (localStorage.getItem('exportadora')) {
      this.temporada = localStorage.getItem('temporada');
    }
    if (localStorage.getItem('planta')) {
      this.planta = localStorage.getItem('planta');
    }
  }

  guardar() {
    if (this.planta == null || this.temporada == null || this.exportadora == null) {
      this.snackBar.open('Por favor, seleccione todos los parámetros', null, { duration: 800 });
      return;
    }
    console.log('datos ok');
    localStorage.setItem('planta', this.planta);
    localStorage.setItem('exportadora', this.exportadora);
    localStorage.setItem('temporada', this.temporada);
    localStorage.setItem('Nplanta', this.nombrePlanta);
    localStorage.setItem('Nexportadora', this.nombreExportadora);
    localStorage.setItem('Ntemporada', this.nombreTemporada);
    localStorage.setItem('NidPlanta', this.planta);
    localStorage.setItem('NidExportadora', this.exportadora);
    this.snackBar.open('Guardado correctamente', null, { duration: 800 });
    console.log('Temporada Nombre:' + this.nombreTemporada + ' / ' + this.nombreExportadora + ' / ' + this.nombrePlanta);
    this.dialogRef.close();
  }

  cargarExportadoras() {
    this.exportadoras = [];
    this.apicommonService.cargarExportadoras(this._usr)
      .subscribe((data: any) => {
        console.log('RESPONSE EXPORADORAS', data);
        this.exportadoras = data.items;
      }, (error) => {
        this.cargando = false;
        console.error('ERROR CARGA EXPORTADORAS', error);
        this.exportadora = null;
      });
  }

  cargarTemporadas() {
    this.apicommonService.cargarTemporadas(this._usr)
      .subscribe((data: any) => {
        // console.log('TEMPORADAS', data);
        this.temporadas = data.items;
        this.cargando = false;
      }, (error) => {
        this.cargando = false;
        console.log('ERROR OBTENER TEMPORADAS', error);
        this.temporada = null;
      });
  }

  cargarPlantas() {
    this.plantas = [];
    if (this.exportadora) {
      this.apicommonService.cargarPlantas(this._usr, this.exportadora)
        .subscribe((data: any) => {
          console.log('RESPONSE PLANTAS', data);
          this.plantas = data.items;
          this.validaPlantas = true;
        }, (error) => {
          console.log('ERROR OBTENER PLANTAS', error);
          this.cargando = false;
          this.validaPlantas = false;
          this.planta = null;
        });
    } else {
      this.snackBar.open('Debe seleccionar la exportadora', null, { duration: 3000 });
    }
  }

  seleccionaExportadora(expor: any) {
    this.exportadora = null;
    this.nombreExportadora = null;
    if (expor.id) {
      this.exportadora = expor.id;
      this.nombreExportadora = expor.nombre;
      this.cargarPlantas();
    }
  }

  seleccionaTemporada(temp: any) {
    this.temporada = null;
    this.nombreTemporada = null;
    this.plantas = [];
    if (temp.id) {
      this.temporada = temp.id;
      this.nombreTemporada = temp.nombre;
      this.cargarPlantas();
    }
  }

  seleccionaPlanta(plan: any) {
    this.planta = null;
    this.nombrePlanta = null;
    if (plan.id) {
      this.planta = plan.id;
      this.nombrePlanta = plan.nombreFantasia;
    }
  }

  seleccPlanta() {
    let result = this.plantas.find((element) => element.id == this.planta);
    this.nombrePlanta = result.nombreFantasia;
  }

  cerrarSesion() {
    localStorage.removeItem('Session')
    localStorage.clear();
    this.authService.change(false);
    this._router.navigate(['login']);
    this.dialogRef.close();
  }
}