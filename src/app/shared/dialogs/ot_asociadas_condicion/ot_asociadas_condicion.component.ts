import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource } from '@angular/material';

const ELEMENT_DATA = [
  {
    linea: '1', ot: '3 (CALIBRAJE)', fecha: '13/05/08', variedad: 'GOLDEN SMOOTH', frio:'F/C2', asignados: '1', despachados: ''
  },
  {
    linea: '1', ot: '379 (CALIBRAJE)', fecha: '9/03/09', variedad: 'GOLDEN SMOOTH', frio: 'F/C2', asignados: '23', despachados: ''
  },
  {
    linea: '1', ot: '22316 (GLOBAL)', fecha: '31/03/03', variedad: 'GOLDEN SMOOTH', frio: 'F/C2', asignados: '42', despachados: ''
  },
  {
    linea: '26298', ot: '(TPTERC)', fecha: '3/05/06', variedad: 'GOLDEN SMOOTH', frio: 'F/C2', asignados: '10', despachados: '10'
  }
];
@Component({
  selector: 'app-ot_asociadas_condicion',
  templateUrl: './ot_asociadas_condicion.component.html',
  styleUrls: ['./ot_asociadas_condicion.component.css']
})

export class Ot_asociadas_condicionComponent implements OnInit {
  displayedColumns: string[] = ['linea', 'ot', 'fecha', 'variedad', 'frio', 'asignados', 'despachados'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  constructor(public dialogRef: MatDialogRef<Ot_asociadas_condicionComponent>) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }
}
