import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-importar_parametros',
  templateUrl: './importar_parametros.component.html',
  styleUrls: ['./importar_parametros.component.css']
})
export class Importar_parametrosComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<Importar_parametrosComponent>) { }

  ngOnInit() {
  }

  close(){
    this.dialogRef.close();
  }
}
