import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ApiseguridadService } from '../../../services/apiseguridad.service';
import { UserInterface } from '../../../model/UsuarioInterface';
import { FormGroup, FormBuilder, Validators, MaxLengthValidator } from '@angular/forms';
import { max } from 'rxjs/operators';

@Component({
  selector: 'app-usuario-agregar',
  templateUrl: './usuario-agregar.component.html',
  styleUrls: ['./usuario-agregar.component.css']
})
export class UsuarioAgregarComponent  {
  hide = true;
  _codigo = '';
  _nombre = '';
  _fecini = '';
  _fecfin = '';
  _activo = '1';
  _editando = false;
  _clave:string = '';


  CLAVE : FormGroup;

  constructor(public dialogRef: MatDialogRef<UsuarioAgregarComponent>, 
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private apiSeguridadService: ApiseguridadService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);
    if (data.editar) {
      this._editando = true;
      this._codigo = data.data.id;
      this._nombre = data.data.nombre;
      this._fecini = data.data.desde;
      this._fecfin = data.data.hasta;
      this._activo = data.data.activo === true ? '1' : '2';
    }

    this.CLAVE = formBuilder.group({
      __clave: ['', Validators.required]
      
    });

  }
  caracteres(){
    if (this._clave.length === 8) {      
      this.snackBar.open('Maxímo 8 caracteres', null, { duration: 3000 });
    }
  }

  onClick(): void {
    const data = {
      id: this._codigo,
      nombre: this._nombre,
      password : this._clave,
      // desde: this._fecini,
      // hasta: this._fecfin,
      desde: '2019-01-08T10:39:56.9775521-03:00',
      hasta: '2030-01-01T10:39:56.9775521-03:00',
      activo: this._activo === '1' ? true : false,
    };

    console.log(data);

    this.dialogRef.close({data , editando : this._editando, close : false});
    this.snackBar.open('Usuario guardado correctamente', null, { duration: 3000 });
  }

  close(): void {
    this.dialogRef.close({ close : true });
  }
}
