import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar, MatDialogRef, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { element } from '@angular/core/src/render3';
import { ApiCommonService } from '../../../services/apicommonservice.service';
import { ApilogisticaService } from '../../../services/apilogistica.service';
@Component({
  selector: 'app-peso-bruto',
  templateUrl: './peso-bruto.component.html',
  styleUrls: ['./peso-bruto.component.css']
})
export class PesoBrutoComponent implements OnInit {
  @ViewChild('refVariedad') refVariedad: any;
  @ViewChild('refEspecie') refEspecie: any;
  isActive: boolean;
  controldepeso = true;
  mostrarPeso = '0';
  variedades = [];
  contenedores: any = [];
  detContenedores: any = [];
  detContenedores_aux = [];
  sdp: any = '';
  especie = '';
  variedad = '';
  elementoPesado = '1';
  especies = [];
  tipoContenedores = [];
  tipoContenedores_ = [];
  tipoContenedores_detalle = [];
  arr_lineas = [];
  arr_lineas_uni = [];
  pesoVariedadRomanaCamionCarro = 0;
  pesoBrutoVariedadRomana = 0;
  sumatemporal = 0;
  pesoTemporal = 0;
  pesoBrutoBalanza = 0;
  promedioPesaje = 0;
  cantidadPesar = 0;
  saldoBins = 0;
  totalBinsPesar = 0;
  restoPesajeBins = 0;
  envasesSobrantesFaltantes = 0;
  sobranteFaltante = true;
  productor = ''
  temporada = ''
  zona = ''
  zonaSelecionada = ''
  tipoContenedor = ''
  tipoContenedorEspecie = '';
  fechaCosecha = '26/03/2018';
  fechaCosechaEspecie = '26/03/2018';
  huerto = '';
  arr_sdp = [];
  exportadora = localStorage.getItem('exportadora');
  rutEmpresa = '';
  idGuia = 0;
  frigorifico = '';
  dataLinea = {};
  arr_envases_entrada = [];
  arr_envases_detalle = [];
  linea = 1;
  productorNombre = '';
  habilitarBoton = true;
  editando = false;
  listaBins = [];
  seccion = 0;
  zpl_cargado: any = [];
  csg = '';
  nombreHuero = '';
  impresoras = [];
  impresoraSeleccion: any = [];
  guiaProductor = '';
  patenteCamion = '';
  patenteCarro = '';
  chofer = '';
  fechaRecepcion = '';
  fechaDespacho = '';
  taraIngresada: any;

  constructor(public dialogRef: MatDialogRef<PesoBrutoComponent>,
    private wsCommon: ApiCommonService,
    private wsLogistica: ApilogisticaService,
    public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log('LLEGA A PESO BRUTO', data);
    this.mostrarPeso = data.mostrarPeso;
    this.elementoPesado = data.elementoPesado === '1' ? '2' : '1';
    this.pesoBrutoVariedadRomana = parseInt(data.pesoVariedadRomanaCamionCarro, 10);
    this.pesoTemporal = parseInt(data.pesoVariedadRomanaCamionCarro, 10);
    this.productor = data.productor
    this.zona = data.ambiente.zona
    this.productor = data.ambiente.productor
    this.linea = data.ambiente.linea
    this.huerto = data.huerto,
      this.idGuia = data.ambiente.idGuia
    this.guiaProductor = data.ambiente.guiaProductor
    this.fechaCosecha = new Date(data.ambiente.fechaCosecha).toISOString()
    this.productorNombre = data.ambiente.productorNombre
    this.controldepeso = data.ambiente.controldepeso
    this.frigorifico = data.ambiente.frigorifico
    this.rutEmpresa = data.ambiente.rutEmpresa
    this.zonaSelecionada = data.ambiente.zonaSeleccionada
    this.temporada = data.ambiente.temporada
    this.isActive = data.ambiente.isActive
    this.seccion = data.ambiente.seccion
    this.csg = data.ambiente.csg
    this.nombreHuero = data.ambiente.nombreHuerto
    this.patenteCamion = data.ambiente.patenteCamion
    this.patenteCarro = data.ambiente.patenteCarro
    this.chofer = data.ambiente.chofer
    this.fechaRecepcion = data.ambiente.fechaRecepcion
    this.fechaDespacho = data.ambiente.fechaDespacho
    if (data.editando) {
      this.editando = true
      this.saldoBins = data.dataVariedaPesa.totalBinsPesar - data.dataVariedaPesa.restoPesajeBins
      this.detContenedores = data.dataVariedaPesa.detContenedores
      this.tipoContenedores_detalle = data.dataVariedaPesa.tipoContenedores_detalle
      this.restoPesajeBins = data.dataVariedaPesa.restoPesajeBins
      this.totalBinsPesar = data.dataVariedaPesa.totalBinsPesar
      this.detContenedores_aux = this.detContenedores,
        this.variedad = data.dataVariedaPesa.variedadId
      this.especie = data.dataVariedaPesa.especieId
      this.sdp = data.dataVariedaPesa.sdp
      this.arr_envases_detalle = data.dataVariedaPesa.arr_envases_detalle
      this.arr_envases_entrada = data.dataVariedaPesa.arr_envases_entrada
      this.selEspecie(this.especie)
      this.calculoContenedores()

    }

    this.wsCommon.cargarEspecie(this.productor, this.temporada, this.zona, this.zonaSelecionada)
      .subscribe((result) => {
        console.log('RESPONSE ESPECIE', result)
        this.especies = result.items;
      });
  }

  ngOnInit() {
    this.cargarImpresoras();
  }

  cargarImpresoras() {
    this.wsCommon.cargarListadoImpresoras(this.seccion.toString(), this.zona, this.frigorifico)
      .subscribe((result) => {
        console.log('RESPONSE LISTADO IMPRESORAS', result);
        this.impresoras = result.items;
      });
  }

  selEspecie(especie: any) {
    this.wsCommon.cargarVariedad(this.productor, this.temporada, this.zona, this.zonaSelecionada, especie)
      .subscribe((result) => {
        this.variedades = result.items;
      });
  }

  selVariedad(variedad: any) {
    this.wsCommon.cargarSDP(this.productor, this.temporada, this.zona, this.huerto, this.especie, this.variedad)
      .subscribe((result) => {
        this.arr_sdp = result.items;
      }, (error) => {
        this.arr_sdp = [];
      });
    this.cargarEnvases(this.especie, variedad, this.exportadora);
  }

  cargarEnvases(especie: any, variedad: any, exportadora: any) {
    console.log(especie+',', variedad+',', exportadora+',');
    this.wsCommon.cargarEnvasesCosechas(especie, variedad, exportadora)
      .subscribe((result) => {
        console.log(result);
        this.tipoContenedores = result;
      }, (error) => {
        this.tipoContenedores = [];
      });
  }

  selContenedor(contendor) {
    this.wsCommon.cargarDetalleEnvasesCosechas(contendor)
      .subscribe((result) => {
        this.detContenedores = result.items;
        const detCont = this.detContenedores;
        detCont.forEach(async element => {
          let tara = await this.wsCommon.cargarPesoZona(this.zona, element.envaseCosecha.id).toPromise();
          let taraAsignada = 0;
          let taraMaxima = 0;
          let taraMinima = 0;
          if (tara.items.length > 0) {
            taraAsignada = tara.items[0].pesoEstado;
            taraMaxima = tara.items[0].pesoMaximo;
            taraMinima = tara.items[0].pesoMinimo;
          }
          element['check'] = true;
          element['tara'] = taraAsignada;
          element['rangos'] = { taraEstado: taraAsignada, taraMaxima: taraMaxima, taraMinima: taraMinima }
          element['cantEnvasesUni'] = 0;
          element['cantEnvasesOtros'] = 0;
          element['cantEnvases'] = parseInt(element['cantEnvasesUni'], 10) + parseInt(element['cantEnvasesOtros'], 10);;
          element['total'] = parseInt(element['cantEnvasesUni'], 10) + parseInt(element['cantEnvasesOtros'], 10);
          this.totalBinsPesar = element['total'];
          if (!this.sobranteFaltante) {
            this.totalBinsPesar++;
          }
          element['sdp'] = this.sdp;
          element['especie'] = this.especie;
          element['especieNombre'] = this.refEspecie.triggerValue;
          element['variedad'] = this.variedad;
          element['variedadNombre'] = this.refVariedad.triggerValue;
        });

        
        this.detContenedores = detCont;
        this.tipoContenedores_detalle = JSON.parse(JSON.stringify(detCont));
        this.tipoContenedores_detalle.forEach(async element => {
          let tara = await this.wsCommon.cargarPesoZona(this.zona, element.envaseCosecha.id).toPromise();
          let taraAsignada = 0;
          let taraMaxima = 0;
          let taraMinima = 0;
          if (tara.items.length > 0) {
            taraAsignada = tara.items[0].pesoEstado;
            taraMaxima = tara.items[0].pesoMaximo;
            taraMinima = tara.items[0].pesoMinimo;
          }
          element['check'] = true;
          element['tara'] = taraAsignada;
          element['rangos'] = { taraEstado: taraAsignada, taraMaxima: taraMaxima, taraMinima: taraMinima };
          element['cantEnvasesUni'] = 0;
          element['cantEnvasesOtros'] = 0;
          element['cantEnvases'] = parseInt(element['cantEnvasesUni'], 10) + parseInt(element['cantEnvasesOtros'], 10);;
          element['total'] = parseInt(element['cantEnvasesUni'], 10) + parseInt(element['cantEnvasesOtros'], 10);
          this.totalBinsPesar = element['total'];
          if (!this.sobranteFaltante) {
            this.totalBinsPesar++;
          }
          element['sdp'] = this.sdp;
          element['especie'] = this.especie;
          element['especieNombre'] = this.refEspecie.triggerValue;
          element['variedad'] = this.variedad;
          element['variedadNombre'] = this.refVariedad.triggerValue;
        });
        this.detContenedores_aux = detCont;
        this.saldoBins = parseInt(this.detContenedores[0]['cantEnvasesUni'], 10) +
          parseInt(this.detContenedores[0]['cantEnvasesOtros'], 10);
        this.calculoContenedores();
      });
  }

  actualizar_total() {
    this.calculoContenedores();
  }

  validarTara($data: any, detCont: any) {
    console.log('VALIDAR TARA', $data.target.value, detCont);
    if (parseFloat(detCont.tara) > detCont.rangos.taraMaxima || parseFloat(detCont.tara) < detCont.rangos.taraMinima) {
      console.log('PASADO');
      detCont.tara = detCont.rangos.taraEstado;
      this.taraIngresada = detCont.tara;
    }
    this.calculoContenedores();
  }

  actualizarTotal(detCont: any) {
    this.totalBinsPesar = detCont['total'] = parseInt(detCont['cantEnvasesUni'], 10) +
      parseInt(detCont['cantEnvasesOtros'], 10);
    this.saldoBins = this.totalBinsPesar;
    this.calculoContenedores();
    if (!this.sobranteFaltante) {
      this.totalBinsPesar++;
    }
  }

  actualizarTotalDetalle(data: any) {
    let cantidadEsContenedor = 0;
    this.tipoContenedores_detalle.forEach(element => {
      if (element.esContenedor) {
        cantidadEsContenedor = parseInt(element['cantEnvasesUni'], 10) + parseInt(element['cantEnvasesOtros'], 10);
      } else {
        element['cantEnvasesUni'] = element['cantidad'] * cantidadEsContenedor;
      }
    });
  }

  sumaPeso(valor: any) {
    if (!this.pesoVariedadRomanaCamionCarro) {
      this.pesoBrutoVariedadRomana = parseInt(this.pesoTemporal.toString(), 10);
    } else {
      this.pesoBrutoVariedadRomana = parseInt(this.pesoTemporal.toString(), 10) +
        parseInt(this.pesoVariedadRomanaCamionCarro.toString(), 10);
    }
  }

  guardar() {
    let elemento = [];
    if (this.detContenedores.length === 0) {
      const data = {
        tara: 0,
        cantEnvasesUni: 0,
        cantEnvasesOtros: 0,
        total: 0,
        sdp: this.sdp,
        especie: this.especie,
        especieNombre: this.refEspecie.triggerValue,
        variedad: this.variedad,
        variedadNombre: this.refVariedad.triggerValue,
      };
      elemento.push(data);
      this.detContenedores = elemento;
    }
    let sumaEnvases = 0;
    this.detContenedores.forEach(element => {
      sumaEnvases += (element.cantEnvasesUni + element.cantEnvasesOtros) * element.tara
      element['cantEnvases'] = element.cantEnvasesUni + element.cantEnvasesOtros
    });
    console.log('SUMA ENVASES', sumaEnvases);
    this.dialogRef.close({
      resultado: this.detContenedores,
      totalPesoTaraIN: sumaEnvases,
      idContendor: this.tipoContenedor,
      resultadoVariedadPesa: this.arr_lineas,
      variedadPesa: {
        envases: this.arr_envases_entrada,
        detalle: this.arr_envases_detalle
      },
      dataVariedadPesa: {
        detContenedores: this.detContenedores,
        tipoContenedores_detalle: this.tipoContenedores_detalle,
        restoPesajeBins: this.restoPesajeBins,
        totalBinsPesar: this.totalBinsPesar,
        linea: this.linea,
        variedadId: this.variedad,
        variedadNombre: this.refVariedad.triggerValue,
        especieId: this.especie,
        especieNombre: this.refEspecie.triggerValue,
        sdp: this.sdp,
        arr_envases_detalle: this.arr_envases_detalle,
        arr_envases_entrada: this.arr_envases_entrada
      },
      tipoPeso: this.controldepeso,
      otros: {
        elementoPesado: this.elementoPesado,
        pesoVariedadRomanaCamionCarro: this.pesoBrutoVariedadRomana,
      }
    });
  }

  onToggleChange() {
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.controldepeso = false;
    } else {
      this.controldepeso = true;
    }
  }

  close() {
    this.dialogRef.close();
  }

  addPesaje() {
    this.habilitarBoton = false;
    const { tara, neto, promedio } = this.calculoPesoPromedio();
    if (this.arr_envases_detalle.length === 0) {
      console.log('AGREGA PESAJE');
      // this.totalBinsPesar = this.detContenedores[0].total;
      this.saldoBins = this.detContenedores[0].total - this.cantidadPesar;
      let index = this.tipoContenedores_detalle.findIndex(element => {
        return element.esContenedor;
      });
      let data_detalle = {
        linea: this.linea,
        especie: this.especie,
        variedad: this.variedad,
        pesoBruto: parseFloat(this.pesoBrutoBalanza.toString()),
        tara: tara,
        pesoEnvasesIn: tara * (this.tipoContenedores_detalle[index]['cantEnvasesUni'] + this.tipoContenedores_detalle[index]['cantEnvasesOtros']),
        pesoEnvasesOut: 0,
        codigoEnvase: this.tipoContenedores_detalle[index].envaseCosecha.id,
        cantidadEnvases: (this.tipoContenedores_detalle[index]['cantEnvasesUni'] + this.tipoContenedores_detalle[index]['cantEnvasesOtros']),
        pesoNeto: neto,
        promedio: promedio,
        variedadNombre: this.refVariedad.triggerValue,
        especieNombre: this.refEspecie.triggerValue,
        envaseNombre: this.tipoContenedores_detalle[index].envaseCosecha.nombre,
        envaseId: this.tipoContenedores_detalle[index].envaseCosecha.id,
      }
      const f_cosecha = new Date(this.fechaCosecha).toISOString();
      this.dataLinea = {
        id: this.idGuia,
        zona: { id: this.zona },
        temporada: { id: localStorage.getItem('temporada') },
        frigorifico: { id: this.frigorifico },
        codCuartel: '2',// SDP
        huerto: { id: this.huerto },
        empresa: { id: this.rutEmpresa },
        variedad: { idsnap: this.variedad },
        especie: { idsnap: this.especie },
        fechaCosecha: f_cosecha,
        pesoBruto: parseFloat(this.pesoBrutoBalanza.toString()),
        pesoTara: tara,
        pesoEnvasesIn: data_detalle.pesoEnvasesIn,
        pesoEnvasesOut: data_detalle.pesoEnvasesIn,
        envaseCosecha: { id: data_detalle.codigoEnvase },
        numEnvasesCont: data_detalle.cantidadEnvases,
        numLinea: data_detalle.linea,
        exportadora: { id: localStorage.getItem('exportadora') },
        cuentaCorriente: false,
        folioCKU: 0,
        tipoDucha: { id: '00' },
        tipoFrio: { id: '00' },
        preFrio: false,
        kilosNetosIn: data_detalle.pesoNeto, ///// ???????
        KilosNetosOut: 0, ///// ?????
        snapAptitud: '00', //// ?????
        contenedor: { id: this.tipoContenedor },
      };
      this.arr_envases_detalle.push(data_detalle)
      let indice_contenedor = this.tipoContenedores_detalle.findIndex(element => {
        return element.esContenedor;
      })
      this.tipoContenedores_detalle.forEach(element => {
        let data = {
          id: this.idGuia,
          zona: { id: this.zona },
          temporada: { id: localStorage.getItem('temporada') },
          frigorifico: { id: this.frigorifico },
          linea: this.linea,
          envaseCosecha: { id: element.envaseCosecha.id },
          cantidadEmpresa: parseFloat(element.cantEnvasesUni.toString()),
          cantidadOtros: parseFloat(element.cantEnvasesOtros.toString()),
          esContenedor: element.esContenedor,
          contenedor: { id: this.tipoContenedor },
          exportadora: { id: localStorage.getItem('exportadora') },
          tara: element.tara,
          envaseNombre: element.envaseCosecha.nombre,
          envaseId: element.envaseCosecha.id,
          pesoEnvase: this.taraIngresada
        }
        if (element.check || element.esContenedor) {
          this.arr_envases_entrada.push(data);
        }
      });
      console.log('ENVIO DE ENVASE ENTRADA',this.arr_envases_entrada);
      let load = this.snackBar.open(' Agregando...', null, { duration: 0 });
      this.wsLogistica.agregarEnvasesEntrada(this.dataLinea, this.arr_envases_entrada)
        .subscribe((result) => {
          console.log('AGREGA ENVASES ENTRADA', result);
          load.dismiss();
          this.habilitarBoton = false;
          this.restoPesajeBins = this.totalBinsPesar - this.saldoBins;
          this.pesoBrutoBalanza = null;
          this.snackBar.open(' ✔  linea creada', null, { duration: 2000 });
          this.crearFolios(this.linea, data_detalle.pesoNeto, this.cantidadPesar);
        }, (error) => {
          load.dismiss();
          this.arr_envases_entrada = [];
          this.habilitarBoton = false;
          this.snackBar.open(' Error al crear Linea', null, { duration: 2000 });
        });
    } else {
      const index = this.existeVariedadPesada(this.variedad);
      const index_ = this.existeVariedadPesadaDetalle(this.variedad);
      let index_cont_det = this.tipoContenedores_detalle.findIndex(element => {
        return element.esContenedor;
      });
      if (index_ >= 0) {
        let data_detalle = {
          linea: this.arr_envases_detalle[index_].linea,
          especie: this.arr_envases_detalle[index_].especie,
          variedad: this.arr_envases_detalle[index_].variedad,
          pesoBruto: this.arr_envases_detalle[index_].pesoBruto + parseFloat(this.pesoBrutoBalanza.toString()),
          tara: this.arr_envases_detalle[index_].tara + tara,
          pesoEnvasesIn: this.arr_envases_detalle[index_].pesoEnvasesIn + (tara * (this.tipoContenedores_detalle[index_cont_det]['cantEnvasesUni'] + this.tipoContenedores_detalle[index_cont_det]['cantEnvasesOtros'])),
          pesoEnvasesOut: 0,
          codigoEnvase: this.tipoContenedores_detalle[index].envaseCosecha.id,
          cantidadEnvases: this.arr_envases_detalle[index_].cantidadEnvases + ((this.tipoContenedores_detalle[index_cont_det]['cantEnvasesUni'] + this.tipoContenedores_detalle[index_cont_det]['cantEnvasesOtros'])),
          pesoNeto: this.arr_envases_detalle[index_].pesoNeto + neto,
          promedio: this.arr_envases_detalle[index_].promedio + promedio,
          envaseNombre: this.arr_envases_detalle[index_].envaseNombre,
          envaseId: this.arr_envases_detalle[index_].envaseId,
          variedadNombre: this.refVariedad.triggerValue,
          especieNombre: this.refEspecie.triggerValue,
        }
        console.log('REQUEST DETALLE', data_detalle);
        this.arr_envases_detalle[index_] = data_detalle
        const f_cosecha = new Date(this.fechaCosecha).toISOString();
        let data_detalle_envio = {
          id: this.idGuia,
          zona: { id: this.zona },
          temporada: { id: localStorage.getItem('temporada') },
          frigorifico: { id: this.frigorifico },
          codCuartel: '2',// SDP
          huerto: { id: this.huerto },
          empresa: { id: this.rutEmpresa },
          variedad: { idsnap: this.variedad },
          especie: { idsnap: this.especie },
          fechaCosecha: f_cosecha,
          pesoBruto: data_detalle.pesoBruto,
          pesoTara: data_detalle.tara,
          pesoEnvasesIn: data_detalle.pesoEnvasesIn,
          pesoEnvasesOut: data_detalle.pesoEnvasesOut,
          envaseCosecha: { id: data_detalle.codigoEnvase },
          numEnvasesCont: data_detalle.cantidadEnvases,
          numLinea: data_detalle.linea,
          exportadora: { id: localStorage.getItem('exportadora') },
          cuentaCorriente: false,
          folioCKU: 0,
          tipoDucha: { id: '00' },
          tipoFrio: { id: '00' },
          preFrio: false,
          kilosNetosIn: data_detalle.pesoNeto, ///// ???????
          KilosNetosOut: 0, ///// ?????
          snapAptitud: '00', //// ?????
          contenedor: { id: this.tipoContenedor }
        };
        let arr_envases_entrada_aux = JSON.parse(JSON.stringify(this.arr_envases_entrada))
        this.tipoContenedores_detalle.forEach(element => {
          let indice_envase = this.encontrarContenedor(element.envaseCosecha.id)
          console.log('INDICE ENVASES ENTRADA', this.arr_envases_entrada[indice_envase])
          let data = {
            id: this.idGuia,
            zona: { id: this.zona },
            temporada: { id: localStorage.getItem('temporada') },
            frigorifico: { id: this.frigorifico },
            linea: this.linea,
            envaseCosecha: { id: element.envaseCosecha.id },
            cantidadEmpresa: parseFloat(this.arr_envases_entrada[indice_envase].cantidadEmpresa) + parseFloat(element.cantEnvasesUni.toString()),
            cantidadOtros: parseFloat(this.arr_envases_entrada[indice_envase].cantidadOtros) + parseFloat(element.cantEnvasesOtros.toString()),
            esContenedor: element.esContenedor,
            contenedor: { id: data_detalle_envio.envaseCosecha.id },
            exportadora: { id: localStorage.getItem('exportadora') },
            tara: element.tara,
            envaseNombre: this.arr_envases_entrada[indice_envase].envaseNombre,
            envaseId: this.arr_envases_entrada[indice_envase].envaseId
          }
          this.arr_envases_entrada[indice_envase] = data;
        });
        console.log('ENVASES ENTRADA', this.arr_envases_entrada)
        let load = this.snackBar.open('Actualizando...', null, { duration: 2000 });
        this.wsLogistica.actualizarDetalleEncabezado(data_detalle_envio)
          .subscribe((result) => {
            console.log('REPSONSE DETALLE', result);
            load.dismiss()
            this.habilitarBoton = false
            this.pesoBrutoBalanza = null
            this.snackBar.open(' ✔  Recepcion Actualizada', null, { duration: 2000 });
            this.crearFolios(this.linea, data_detalle.pesoNeto, this.cantidadPesar)
          }, (err) => {
            console.error('ERROR CARGANDO DETALLE', err);
            load.dismiss();
            this.habilitarBoton = false;
            this.snackBar.open(' ✔  Error Al Actualizada Detalle', null, { duration: 2000 });
          });
        this.wsLogistica.actualizarEnvasesEntrada(this.arr_envases_entrada)
          .subscribe((result) => {
            load.dismiss();
            this.snackBar.open(' ✔  Envases Actualizados', null, { duration: 2000 });
            this.habilitarBoton = false;
            this.saldoBins = this.saldoBins - this.cantidadPesar;
            this.restoPesajeBins = this.totalBinsPesar - this.saldoBins;
            this.pesoBrutoBalanza = null;
            console.log('RESPONSE ACTUALIZA ENVASES ENTRADAS', result);
          }, (err) => {
            load.dismiss();
            this.saldoBins = this.saldoBins - this.cantidadPesar;
            this.restoPesajeBins = this.totalBinsPesar - this.saldoBins;
            // this.arr_envases_entrada = arr_envases_entrada_aux
            this.habilitarBoton = false;
            this.snackBar.open(' ✔  Error Al Actualizada Envases', null, { duration: 2000 });
            console.error('ERROR ACTUALIZA ENVASES ENTRADA', err);
          });
      }
    }
    console.log('ENVASES ENTRADA', this.arr_envases_entrada); /// envases de entrada
    console.log('ENVASES SALIDA', this.arr_envases_detalle); /// detalle
    if (this.saldoBins < this.cantidadPesar) { }
  }

  calculoContenedores() {
    this.detContenedores = this.detContenedores_aux;
    let contenedor = this.detContenedores.findIndex(cont => {
      return cont.esContenedor;
    })
    this.detContenedores.forEach(env => {
      // console.log(env)
      env['total'] = env.cantEnvasesOtros + env.cantEnvasesUni;
      env['totalTara'] = env.tara * parseFloat(env.tara);
      env['etiquetas'] = env.total / env.cantidad;
      if (!env.esContenedor) {
        const redondeo = Math.ceil(env['etiquetas']);
        env['etiquetaJusta'] = redondeo === this.detContenedores[contenedor].total ? true : false;
        // console.log(env['etiquetaJusta'])
        this.sobranteFaltante = env['etiquetaJusta'];
        env['envasesRestantes'] = env.total - parseInt(env['etiquetas'], 10) * env.cantidad;
        this.envasesSobrantesFaltantes = env['envasesRestantes'];
        let aux = this.totalBinsPesar
        if (this.sobranteFaltante || env['envasesRestantes'] == 0) {
          this.totalBinsPesar = this.detContenedores[contenedor].cantEnvasesOtros + this.detContenedores[contenedor].cantEnvasesUni
        } else {
          aux++;
          this.totalBinsPesar = aux
        }
      } else {
      }
    });
  }

  calculoPesoPromedio() {
    let sumaTaras = 0;
    let configuracionTotem = 0;
    this.detContenedores = this.detContenedores_aux;
    this.detContenedores.forEach(env => {
      if (!env.esContenedor) {
        configuracionTotem = env.cantidad;
      }
      if ((this.cantidadPesar) + (this.restoPesajeBins) === (this.totalBinsPesar) && !env.esContenedor) {
        sumaTaras += this.envasesSobrantesFaltantes * parseFloat(env.tara);
      } else {
        sumaTaras += env.cantidad * parseFloat(env.tara);
      }
    });
    console.log('DETALLE CONTENEDORES', this.detContenedores);
    sumaTaras = sumaTaras * this.cantidadPesar;
    console.log('SUMA TARAS', sumaTaras);
    const sumaNeto = this.pesoBrutoBalanza - sumaTaras;
    console.log('SUMA NETO', sumaNeto)
    console.log('CONFIG TOTEM', configuracionTotem);
    let indexVer = this.detContenedores.findIndex(element => {
      return element.ver;
    });
    configuracionTotem = this.detContenedores[indexVer].cantidad
    const pm = (sumaNeto / configuracionTotem);
    console.log('PROMEDIO', pm);
    this.promedioPesaje = pm / this.cantidadPesar;
    return { tara: sumaTaras, neto: sumaNeto, promedio: this.promedioPesaje }
  }

  existeVariedadPesada(variedad: any) {
    const ind = this.tipoContenedores_detalle.findIndex(linea => {
      return linea.variedad === variedad;
    });
    return ind;
  }

  existeVariedadPesadaDetalle(variedad: any) {
    const ind = this.tipoContenedores_detalle.findIndex(env => {
      return env.variedad === variedad;
    });
    return ind;
  }

  encontrarContenedor(idContenedor: any) {
    const ind = this.arr_envases_entrada.findIndex(env => {
      return env.envaseCosecha.id === idContenedor;
    });
    return ind;
  }

  cantidadPesarChange(data: any) {
    console.log('SALDO BINS', this.saldoBins)
    console.log('CANTIDAD A PESAR', this.cantidadPesar)
    if (this.saldoBins < parseInt(this.cantidadPesar.toString(), 10)) {
      this.cantidadPesar = this.saldoBins;
    }
    let cantidadEsContenedor = 0
    this.tipoContenedores_detalle.forEach(element => {
      if (element.esContenedor) {
        element['cantEnvasesUni'] = parseFloat(data);
        element['cantEnvasesOtros'] = 0;
        element['cantEnvases'] = parseInt(element['cantEnvasesUni'], 10) + parseInt(element['cantEnvasesOtros'], 10);;
        cantidadEsContenedor = element['cantEnvases']
        console.log('CANTIDAD ES CONTENEDOR', cantidadEsContenedor)
      } else {
        element['cantEnvasesUni'] = element['cantidad'] * cantidadEsContenedor;
      }
    });
  }

  validarCampos() {
    if (this.variedad && this.tipoContenedor && this.fechaCosecha
      && this.pesoBrutoVariedadRomana > 0 && this.controldepeso
      && this.validarContenedores() && this.mostrarPeso === '2') {
      return true;
    }
    if (this.variedad && this.tipoContenedor && this.fechaCosechaEspecie
      && this.mostrarPeso === '1'
      && this.validarContenedores()) {
      return true;
    }
    if (this.variedad && this.tipoContenedor && this.fechaCosecha
      && !this.controldepeso
      && this.mostrarPeso === '2') {
      return true;
    }
  }

  validarContenedores() {
    let checkeados = false;
    this.detContenedores.forEach(detCont => {
      if (detCont.check && (detCont.cantEnvasesUni + detCont.cantEnvasesOtros) > 0) {
        checkeados = true;
      }
    });
    return checkeados;
  }

  validadorAddPesaje() {
    let checkeados = false;
    if (this.cantidadPesar > 0 && this.pesoBrutoBalanza > 0) {
      checkeados = true;
    }
    return checkeados;
  }

  guardarDetalle(dataDetalle: any) {
    console.log('GUARDAR DETALLE', dataDetalle);
    const f_cosecha = new Date(this.fechaCosecha).toISOString();
    this.dataLinea = {
      id: this.idGuia,
      zona: { id: this.zona },
      temporada: { id: localStorage.getItem('temporada') },
      frigorifico: { id: this.frigorifico },
      codCuartel: '2',// SDP
      huerto: { id: this.huerto },
      empresa: { id: this.rutEmpresa },
      variedad: { idsnap: dataDetalle.variedad },
      especie: { idsnap: dataDetalle.especie },
      fechaCosecha: f_cosecha,
      pesoBruto: dataDetalle.pesoBruto,
      pesoTara: dataDetalle.pesoTara,
      pesoEnvasesIn: dataDetalle.pesoEnvasesIn,
      pesoEnvasesOut: dataDetalle.pesoEnvasesOut,
      envaseCosecha: { id: dataDetalle.codigoEnvase },
      numEnvasesCont: dataDetalle.cantidadEnvases,
      numLinea: 18,
      exportadora: { id: localStorage.getItem('exportadora') },
      cuentaCorriente: false,
      folioCKU: 0,
      tipoDucha: { id: '   1' },
      tipoFrio: { id: '  10' },
      preFrio: false,
      kilosNetosIn: dataDetalle.pesoNeto, ///// ???????
      KilosNetosOut: 50, ///// ?????
      snapAptitud: '  17', //// ?????
      contenedor: { id: dataDetalle.codigoEnvase },
    };
  }

  crearFolios(linea: any, KilosNetos: any, cantidad: any) {
    this.listaBins = [];
    let reqImpFolio = {
      numRecepcion: this.idGuia,
      zona: { id: this.zona },
      temporada: { id: this.temporada },
      frigorifico: { id: this.frigorifico },
      linea: linea,
      folio: 0,
      kilosNetos: KilosNetos,
      cantidad: cantidad,
      vaciado: false,
      exportadora: { id: this.exportadora },
      seccionPlanta: { id: this.seccion },
      fecha: this.fechaCosecha
    };
    console.log('REQUEST IMPRIMIR FOLIO', reqImpFolio);
    this.wsLogistica.imprimirFolio(reqImpFolio)
      .subscribe((data) => {
        console.log('REPSONSE IMPRIMIR FOLIO', data)
        let valido = data['valido'];
        if (valido) {
          this.listaBins = data['lista'];
          this.cargarFormatoEtiqueta();
        } else {
          this.snackBar.open('Sin bins', null, { duration: 2000 });
        }
        console.log('RESPONSE IMPRIME FOLIOS', data);
      }, (error) => {
        console.error('ERROR IMPRIMIR FOLIO', error);
        this.snackBar.open('Error al cargar listado', null, { duration: 2000 })
      });
  }

  cargarFormatoEtiqueta() {
    let data = {
      id: 1,
      seccion: this.seccion,
      zona: this.zona,
      frigorifico: this.frigorifico
    }
    this.wsCommon.cargarEtiquetasZpl(data).subscribe(result => {
      console.log('REQUEST ETIQUETAS ZPL', result);
      this.zpl_cargado = result
      this.listaBins.forEach(element => {
        this.imprimir(element.folio);
      });
    });
  }

  imprimir(nro_bin: any) {
    // CONSUMIER APIS PARA LLENAR LAS VARIABLES
    let csg = this.csg;
    let fechaFormateada = new Date(this.fechaCosecha).toLocaleDateString();
    var rem_cod_prod = this.zpl_cargado.detalle.replace(/#cod_prod#/gi, this.huerto);
    var rem_des_prod = rem_cod_prod.replace(/#des_prod#/gi, this.nombreHuero);
    var rem_csg = rem_des_prod.replace(/#csg#/gi, csg);
    var rem_huerto = rem_csg.replace(/#huerto#/gi, this.nombreHuero);
    var rem_cod_var = rem_huerto.replace(/#cod_var#/gi, this.variedad);
    var rem_variedad = rem_cod_var.replace(/#des_var#/gi, this.refVariedad.triggerValue);
    var rem_aptitud = rem_variedad.replace(/#aptitud#/gi, '');
    var rem_fecha = rem_aptitud.replace(/#fecha#/gi, fechaFormateada);
    var rem_id_bins = rem_fecha.replace(/#id_bins#/gi, nro_bin);
    var rem_nro_bins = rem_id_bins.replace(/#nro_bins#/gi, nro_bin);
    let impresoraSeleccionada = this.impresoras[this.impresoraSeleccion]
    console.log('IP', impresoraSeleccionada.IP);
    if (impresoraSeleccionada.IP) {
      let req = {
        zpl: rem_nro_bins,
        ip: impresoraSeleccionada.IP.trim(),
        puerto: impresoraSeleccionada.PORT.trim()
      };
      this.wsLogistica.imprimirZpl(req)
        .subscribe((data) => {
          let valido = data.realizado;
          if (valido) {
            this.snackBar.open(data.mensaje, null, { duration: 2000 })
          } else {
            this.snackBar.open(data.mensaje, null, { duration: 2000 })
          }
          console.log('RESPONSE IMPRIMIR ZPL', data);
        }, (error) => {
          console.error('ERROR IMPRIMIR ZPL', error);
        });
    } else {
      this.snackBar.open('Debe seleccionar una impresora', null, { duration: 2000 })
    }
  }
}