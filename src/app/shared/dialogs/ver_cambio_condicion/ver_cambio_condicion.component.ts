import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource } from '@angular/material';

const ELEMENT_DATA = [
  {
    origen: '3 MEDIANO PLAZO', destino: '2 CORTO PLAZO', usuario: 'Oscar Vargas', fecha: '23/11/2002', hora: '18:06'
  }
];

@Component({
  selector: 'app-ver_cambio_condicion',
  templateUrl: './ver_cambio_condicion.component.html',
  styleUrls: ['./ver_cambio_condicion.component.css']
})

export class Ver_cambio_condicionComponent implements OnInit {
  displayedColumns: string[] = ['origen', 'destino', 'usuario', 'fecha', 'hora'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  constructor(public dialogRef: MatDialogRef<Ver_cambio_condicionComponent>) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }
}
