import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-nuevo_cambio_condicion',
  templateUrl: './nuevo_cambio_condicion.component.html',
  styleUrls: ['./nuevo_cambio_condicion.component.css']
})

export class Nuevo_cambio_condicionComponent implements OnInit {
  mostrar: boolean = true;
  isActive: boolean;
  constructor(public dialogRef: MatDialogRef<Nuevo_cambio_condicionComponent>, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }

  guardarCondicion(){
    this.dialogRef.close();
    this.snackBar.open('Cambio de condición realizado correctamente', null, { duration: 3000 });
  }

  verFolios(event) {
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.mostrar = false;
    }
    else {
      this.mostrar = true;
    }
  }
}
