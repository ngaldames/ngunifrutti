import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.css']
})
export class ConfirmacionComponent implements OnInit {


  constructor(
    @Inject(MAT_DIALOG_DATA) public datos: any,
    private dialogRef: MatDialogRef<ConfirmacionComponent>,
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {}

  clickSi() {
    this.dialogRef.close({confirmacion: true});
  }

  clickNo() {
    this.dialogRef.close({confirmacion: false});
  }

  ngOnDestroy() {}
}