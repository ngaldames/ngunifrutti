import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-añadir-pantalla',
  templateUrl: './añadir-pantalla.component.html',
  styleUrls: ['./añadir-pantalla.component.css']
})
export class AñadirPantallaComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AñadirPantallaComponent>) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }

  onClick() {

  }
}
