import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-subir_documento',
  templateUrl: './subir_documento.component.html',
  styleUrls: ['./subir_documento.component.css']
})
export class Subir_documentoComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<Subir_documentoComponent>) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }
}
