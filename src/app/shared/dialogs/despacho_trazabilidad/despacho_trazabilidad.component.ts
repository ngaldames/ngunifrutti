import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource } from '@angular/material';

const ELEMENT_DATA4 = [
  {
    nro_despacho: '145', fecha: '11/01/2019', tipo_venta: 'Nacional', mercado: 'interno', etiqueta: 'Unifrutti', caja: '10', kg: '44'
  },
  {
    nro_despacho: '65', fecha: '11/01/2019', tipo_venta: 'Nacional', mercado: 'interno', etiqueta: 'Unifrutti', caja: '11', kg: '48'
  },
  {
    nro_despacho: '112', fecha: '11/01/2019', tipo_venta: 'Nacional', mercado: 'interno', etiqueta: 'Unifrutti', caja: '12', kg: '120'
  }
];

@Component({
  selector: 'app-despacho_trazabilidad',
  templateUrl: './despacho_trazabilidad.component.html',
  styleUrls: ['./despacho_trazabilidad.component.css']
})
export class Despacho_trazabilidadComponent implements OnInit {
  nro_despacho: any;
  fecha: any;
  tipo_venta: string;
  mercado: string;
  etiqueta: any;
  caja: any;
  kg: any;
  displayedColumns4: string[] = ['nro_despacho', 'fecha', 'tipo_venta', 'mercado', 'etiqueta', 'caja', 'kg'];
  dataSource4 = new MatTableDataSource(ELEMENT_DATA4);

  constructor(public dialogRef: MatDialogRef<Despacho_trazabilidadComponent>) { }

  
  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }
}
