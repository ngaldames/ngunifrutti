import { Tabla_foliosComponent } from './pages/logistica/fruta_embalada/despacho/componentes_compartidos/tabla_folios/tabla_folios.component';
import { Documentos_electronicosComponent } from './pages/general/documentos_electronicos/documentos_electronicos.component';
import { Folios_productor_a_recibidorComponent } from './pages/logistica/fruta_embalada/despacho/vistas/folios_productor_a_recibidor/folios_productor_a_recibidor.component';
import { Carga_camionComponent } from './pages/logistica/fruta_embalada/despacho/dialogs/carga_camion/carga_camion.component';
import { Recepcion_camionComponent } from './pages/logistica/fruta_embalada/despacho/vistas/recepcion_camion/recepcion_camion.component';
import { Listado_despacho_puertoComponent } from './pages/logistica/fruta_embalada/despacho/vistas/listado_despacho_puerto/listado_despacho_puerto.component';
import { Asignar_folios_produccion_a_recibidorComponent } from './pages/logistica/fruta_embalada/despacho/dialogs/asignar_folios_produccion_a_recibidor/asignar_folios_produccion_a_recibidor.component';
import { Listado_asignacion_prod_a_recibidorComponent } from './pages/logistica/fruta_embalada/despacho/vistas/listado_asignacion_prod_a_recibidor/listado_asignacion_prod_a_recibidor.component';
import { Agregar_folios_opComponent } from './pages/logistica/fruta_embalada/despacho/dialogs/agregar_folios_op/agregar_folios_op.component';
import { Nueva_asignacion_opComponent } from './pages/logistica/fruta_embalada/despacho/vistas/nueva_asignacion_op/nueva_asignacion_op.component';
import { Listado_principal_folios_opComponent } from './pages/logistica/fruta_embalada/despacho/vistas/listado_principal_folios_op/listado_principal_folios_op.component';
import { Importar_parametrosComponent } from './shared/dialogs/importar_parametros/importar_parametros.component';
import { Nuevo_registro_de_cambioComponent } from './pages/logistica/fruta_embalada/control_de_calidad/vistas/nuevo_registro_de_cambio/nuevo_registro_de_cambio.component';
import { Asignar_folios_controldecalidadComponent } from './pages/logistica/fruta_embalada/control_de_calidad/dialogs/asignar_folios_controldecalidad/asignar_folios_controldecalidad.component';
import { Asignar_productoComponent } from './pages/logistica/fruta_granel/tratamientos/dialogs/asignar_producto/asignar_producto.component';
import { Despacho_productores_por_otComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_productores_por_ot/despacho_productores_por_ot.component';
import { Asignar_folios_productoresComponent } from './pages/logistica/fruta_granel/despacho/dialogs/asignar_folios_productores/asignar_folios_productores.component';
import { Asignar_folios_a_tercerosComponent } from './pages/logistica/fruta_granel/despacho/dialogs/asignar_folios_a_terceros/asignar_folios_a_terceros.component';
import { Asignar_folios_interplantaComponent } from './pages/logistica/fruta_granel/despacho/dialogs/asignar_folios_interplanta/asignar_folios_interplanta.component';
import { Factura_asociadaComponent } from './pages/logistica/fruta_granel/tratamientos/dialogs/factura_asociada/factura_asociada.component';
import { Listado_tratamientoComponent } from './pages/logistica/fruta_granel/tratamientos/vistas/listado_tratamiento/listado_tratamiento.component';
import { TratamientosComponent } from './pages/logistica/fruta_granel/tratamientos/vistas/tratamientos/tratamientos.component';
import { IngresoPesoBrutoComponent } from './shared/dialogs/ingreso-peso-bruto/ingreso-peso-bruto.component';
import { PesoBrutoVariedadComponent } from './shared/dialogs/peso-bruto-variedad/peso-bruto-variedad.component';
import { Despacho_a_productoresComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_a_productores/despacho_a_productores.component';
import { RecFrutaGranelVariedadComponent } from './pages/logistica/fruta_granel/recepcion/rec-fruta-granel-variedad/rec-fruta-granel-variedad.component';
import { ConfirmacionComponent } from './shared/dialogs/confirmacion/confirmacion.component';
import { RecepcionFrutaGranelComponent } from './pages/logistica/fruta_granel/recepcion/recepcion-fruta-granel/recepcion-fruta-granel.component';
import { AppInitService } from './services/AppInit.service';
import { Ot_asociadas_condicionComponent } from './shared/dialogs/ot_asociadas_condicion/ot_asociadas_condicion.component';
import { Nuevo_cambio_condicionComponent } from './shared/dialogs/nuevo_cambio_condicion/nuevo_cambio_condicion.component';

import { Consulta_cambio_condicionComponent } from './pages/logistica/fruta_granel/recepcion/consulta_cambio_condicion/consulta_cambio_condicion.component';
import { Emision_guia_despachoComponent } from './pages/logistica/fruta_granel/despacho/dialogs/emision_guia_despacho/emision_guia_despacho.component';
import { Despacho_interplanta_por_OTComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_interplanta_por_OT/despacho_interplanta_por_OT.component';
 
import { Anular_despacho_packingComponent } from './pages/logistica/fruta_granel/despacho/vistas/anular_despacho_packing/anular_despacho_packing.component';
import { Devolver_packing_por_otComponent } from './pages/logistica/fruta_granel/despacho/vistas/devolver_packing_por_ot/devolver_packing_por_ot.component';
import { Asignar_folios_adespacharComponent } from './pages/logistica/fruta_granel/despacho/dialogs/asignar_folios_adespachar/asignar_folios_adespachar.component';
import { Despacho_packing_por_otComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_packing_por_ot/despacho_packing_por_ot.component';
import { Despacho_a_packingComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_a_packing/despacho_a_packing.component';
import { Modificar_peso_brutoComponent } from './pages/logistica/fruta_granel/recepcion/dialogs/modificar_peso_bruto/modificar_peso_bruto.component';
import { Nuevo_contenedorComponent } from './pages/logistica/parametros/dialogs/nuevo_contenedor/nuevo_contenedor.component';
import { Lista_parametrosComponent } from './pages/logistica/parametros/vistas/lista_parametros/lista_parametros.component';
import { Lista_contenedoresComponent } from './pages/logistica/parametros/vistas/lista_contenedores/lista_contenedores.component';
import { Subir_documentoComponent } from './shared/dialogs/subir_documento/subir_documento.component';
import { DocumentosComponent } from './pages/documentos/documentos.component';
import { Despacho_trazabilidadComponent } from './shared/dialogs/despacho_trazabilidad/despacho_trazabilidad.component';

import { DefFuncionalidadesGlobalesComponent } from './pages/seguridad/def-funcionalidades-globales/def-funcionalidades-globales.component';
import { DefFolioComponent } from './shared/dialogs/def-folio/def-folio.component';
import { Abrir_lotesComponent } from './shared/dialogs/abrir_lotes/abrir_lotes.component';
import { CkuConTablaComponent } from './pages/logistica/fruta_granel/recepcion/cku-con-tabla/cku-con-tabla.component';
import { EncabezadoComponent } from './pages/logistica/fruta_granel/recepcion/componentes_compartidos/encabezado/encabezado.component';
import { DestareComponent } from './pages/logistica/fruta_granel/recepcion/destare/destare.component';
import { Env_salidaComponent } from './pages/logistica/fruta_granel/recepcion/env_salida/env_salida.component';
import { PantallaCkuComponent } from './pages/logistica/fruta_granel/recepcion/pantalla-cku/pantalla-cku.component';
import { PesoBrutoComponent } from './shared/dialogs/peso-bruto/peso-bruto.component';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { DragScrollModule } from 'ngx-drag-scroll';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import {
  MAT_DATE_LOCALE, MatRippleModule, MatAutocompleteModule,
  MatBadgeModule, MatBottomSheetModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule,
  MatChipsModule, MatStepperModule, MatDialogModule, MatDividerModule, MatGridListModule, MatListModule,
  MatMenuModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatSelectModule,
  MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule,
  MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule
} from '@angular/material';

import { NavbarComponent } from './shared/navbar/navbar.component';
import { SidenavComponent } from './shared/sidenav/sidenav.component';
import { ConfignavComponent } from './shared/confignav/confignav.component';
import { RolAgregarComponent } from './shared/dialogs/rol-agregar/rol-agregar.component';
import { UsuarioAgregarComponent } from './shared/dialogs/usuario-agregar/usuario-agregar.component';
import { FavoritosComponent } from './pages/favoritos/favoritos.component';

// Http
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPrintModule } from 'ngx-print';

// Angular Material
import {
  MatIconModule, MatFormFieldModule, MatInputModule, MatButtonModule,
  MatDatepickerModule, MatNativeDateModule, MatRadioModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LoginComponent } from './login/login.component';
import { ConfiUsuarioComponent } from './shared/dialogs/confi-usuario/confi-usuario.component';
import { RecFrutaGranelComponent } from './pages/logistica/fruta_granel/recepcion/rec-fruta-granel/rec-fruta-granel.component';
import { DesEnvvaciosComponent } from './pages/logistica/fruta_granel/recepcion/des-envvacios/des-envvacios.component';
import { RecCkuComponent } from './pages/logistica/fruta_granel/recepcion/rec-cku/rec-cku.component';
import { CtacteEnvcosComponent } from './pages/logistica/fruta_granel/recepcion/ctacte-envcos/ctacte-envcos.component';
import { RecTrazabilidadComponent } from './pages/logistica/fruta_granel/recepcion/rec-trazabilidad/rec-trazabilidad.component';
import { RecFrioComponent } from './pages/logistica/fruta_granel/recepcion/rec-frio/rec-frio.component';
import { RolesListadoComponent } from './pages/seguridad/roles-listado/roles-listado.component';
import { RolesCrearComponent } from './pages/seguridad/roles-crear/roles-crear.component';
import { UsuarioCrearComponent } from './pages/seguridad/usuario-crear/usuario-crear.component';
import { UsuarioListadoComponent } from './pages/seguridad/usuario-listado/usuario-listado.component';
import { AñadirPantallaComponent } from './shared/dialogs/añadir-pantalla/añadir-pantalla.component';
import { BuscardormenuComponent } from './pages/buscardormenu/buscardormenu.component';
import { Ot_relacionada_trazabilidadComponent } from './shared/dialogs/ot_relacionada_trazabilidad/ot_relacionada_trazabilidad.component';
import { Imp_folio_binsComponent } from './pages/logistica/fruta_granel/recepcion/dialogs/imp_folio_bins/imp_folio_bins.component';
import { Despacho_interplantaComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_interplanta/despacho_interplanta.component';
import { Despacho_planta_tercerosComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_planta_terceros/despacho_planta_terceros.component';
import { Ver_cambio_condicionComponent } from './shared/dialogs/ver_cambio_condicion/ver_cambio_condicion.component';
import { AñadirGuiasComponent } from './pages/logistica/fruta_granel/despacho/vistas/añadir-guias/añadir-guias.component';
import { Despacho_terceros_por_otComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_terceros_por_ot/despacho_terceros_por_ot.component';
import { Eliminar_folios_adespacharComponent } from './pages/logistica/fruta_granel/despacho/dialogs/eliminar_folios_adespachar/eliminar_folios_adespachar.component';
import { CerrarOTDespachoComponent } from './pages/logistica/fruta_granel/despacho/dialogs/cerrar-otdespacho/cerrar-otdespacho.component';
import { Control_de_calidadComponent } from './pages/logistica/fruta_embalada/control_de_calidad/vistas/control_de_calidad/control_de_calidad.component';
import { EliminarProductoComponent } from './pages/logistica/fruta_granel/tratamientos/dialogs/eliminar-producto/eliminar-producto.component';
import { Listado_recepcionComponent } from './pages/logistica/fruta_embalada/recepcion/vistas/listado_recepcion/listado_recepcion.component';
import { Nueva_recepcion_fruta_embaladaComponent } from './pages/logistica/fruta_embalada/recepcion/vistas/nueva_recepcion_fruta_embalada/nueva_recepcion_fruta_embalada.component';
import { VerDetalleCondicionComponent } from './pages/logistica/fruta_embalada/control_de_calidad/dialogs/ver-detalle-condicion/ver-detalle-condicion.component';

export function init_app(appLoadService: AppInitService) {
  return () => appLoadService.obtenerConfig()
}
@NgModule({
  declarations: [
    ConfirmacionComponent,
    AppComponent,
    NavbarComponent,
    SidenavComponent,
    ConfignavComponent,
    RolAgregarComponent,
    UsuarioAgregarComponent,
    LoginComponent,
    FavoritosComponent,
    ConfiUsuarioComponent,
    PesoBrutoComponent,
    PesoBrutoVariedadComponent,
    IngresoPesoBrutoComponent,
    RecFrutaGranelComponent,
    RecFrutaGranelVariedadComponent,
    RecepcionFrutaGranelComponent,
    DesEnvvaciosComponent,
    RecCkuComponent,
    CtacteEnvcosComponent,
    RecTrazabilidadComponent,
    RecFrioComponent,
    RolesListadoComponent,
    RolesCrearComponent,
    UsuarioCrearComponent,
    UsuarioListadoComponent,
    PantallaCkuComponent,
    Env_salidaComponent,
    DestareComponent,
    EncabezadoComponent,
    CkuConTablaComponent,
    Abrir_lotesComponent,
    DefFolioComponent,
    DefFuncionalidadesGlobalesComponent,
    AñadirPantallaComponent,
    BuscardormenuComponent,
    Ot_relacionada_trazabilidadComponent,
    Despacho_trazabilidadComponent,
    DocumentosComponent,
    Subir_documentoComponent,
    Imp_folio_binsComponent,
    Lista_contenedoresComponent,
    Lista_parametrosComponent,
    Nuevo_contenedorComponent,
    Modificar_peso_brutoComponent,
    Despacho_a_packingComponent,
    Despacho_packing_por_otComponent,
    Asignar_folios_adespacharComponent,
    Eliminar_folios_adespacharComponent,
    Devolver_packing_por_otComponent,
    Anular_despacho_packingComponent,
    Despacho_interplantaComponent,
    Despacho_interplanta_por_OTComponent,
    Despacho_planta_tercerosComponent,
    Emision_guia_despachoComponent,
    Consulta_cambio_condicionComponent,
    Ver_cambio_condicionComponent,
    Nuevo_cambio_condicionComponent,
    Ot_asociadas_condicionComponent,
    AñadirGuiasComponent,
    Despacho_a_productoresComponent,
    TratamientosComponent,
    Listado_tratamientoComponent,
    Factura_asociadaComponent,
    Asignar_folios_interplantaComponent,
    Asignar_folios_a_tercerosComponent,
    Despacho_terceros_por_otComponent,
    Asignar_folios_productoresComponent,
    Despacho_productores_por_otComponent,
    CerrarOTDespachoComponent,
    Asignar_productoComponent,
    Control_de_calidadComponent,
    Asignar_folios_controldecalidadComponent,
    VerDetalleCondicionComponent,
    Nuevo_registro_de_cambioComponent,
    EliminarProductoComponent,
    Listado_recepcionComponent,
    Nueva_recepcion_fruta_embaladaComponent,
    Importar_parametrosComponent,
    Listado_principal_folios_opComponent,
    Nueva_asignacion_opComponent,
    Agregar_folios_opComponent,
    Listado_asignacion_prod_a_recibidorComponent,
    Asignar_folios_produccion_a_recibidorComponent,
    Listado_despacho_puertoComponent,
    Recepcion_camionComponent,
    Carga_camionComponent,
    Folios_productor_a_recibidorComponent,
    Documentos_electronicosComponent,
    Tabla_foliosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatRippleModule,
    MatExpansionModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatGridListModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    ScrollingModule,
    ReactiveFormsModule,
    DragScrollModule,
    NgxPrintModule
  ],
  entryComponents: [
    ConfirmacionComponent,
    RolAgregarComponent,
    UsuarioAgregarComponent,
    ConfiUsuarioComponent,
    PesoBrutoComponent,
    PesoBrutoVariedadComponent,
    IngresoPesoBrutoComponent,
    Abrir_lotesComponent,
    DefFolioComponent,
    AñadirPantallaComponent,
    Ot_relacionada_trazabilidadComponent,
    Despacho_trazabilidadComponent,
    Subir_documentoComponent,
    Imp_folio_binsComponent,
    Nuevo_contenedorComponent,
    Modificar_peso_brutoComponent,
    Asignar_folios_adespacharComponent,
    Emision_guia_despachoComponent,
    Ver_cambio_condicionComponent,
    Nuevo_cambio_condicionComponent,
    Ot_asociadas_condicionComponent,
    Factura_asociadaComponent,
    Asignar_folios_interplantaComponent,
    Asignar_folios_a_tercerosComponent,
    Ot_asociadas_condicionComponent,
    Eliminar_folios_adespacharComponent,
    Asignar_folios_productoresComponent,
    CerrarOTDespachoComponent,
    Asignar_productoComponent,
    Asignar_folios_controldecalidadComponent,
    VerDetalleCondicionComponent,
    EliminarProductoComponent,
    Importar_parametrosComponent,
    Agregar_folios_opComponent,
    Asignar_folios_produccion_a_recibidorComponent,
    Carga_camionComponent
  ],
  providers: [
    AppInitService,
    {
      provide: APP_INITIALIZER,
      useFactory: init_app,
      deps: [AppInitService],
      multi: true
    },
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
