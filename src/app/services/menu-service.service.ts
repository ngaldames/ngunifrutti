import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuServiceService {
  // menu: any[] = [{ nombre: null, path: null, icon: null }];
  private ipServer = 'http://172.18.8.139';
  private apiURL_menu = this.ipServer + '/apiSeguridad/api/Menu';
  private cabeceras: any;
  menu = [];

  constructor(
    private http: HttpClient
  ) {
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  private apiURL_DSCommon = '/apiSeguridad/api/';

  cargarMenus(): any {
    this.menu = [];
    return this.menu;
  }

  listar() {
    return this.http.get<any>(this.apiURL_DSCommon + 'menu', { headers: this.cabeceras });
  }

  obtenerMenus() {
    return this.http.get<any>(this.apiURL_menu, { headers: this.cabeceras });
  }
  // cargarMenus(): any {
  //   this.menu = [];
  //   return this.menu;
  // }
}