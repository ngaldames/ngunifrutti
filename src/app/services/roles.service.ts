import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  private cabeceras: any;
  // private ipServer = 'http://192.168.0.100';
  private ipServer = '';
  private ListadoROL = this.ipServer + '/Servicios/Seguridad.svc/rest/ListadoRol';
  private ListadoMenu = this.ipServer + '/Servicios/Seguridad.svc/rest/ListadoMenu';
  private ListadoFuncionalidadeMenuRol = this.ipServer + '/Servicios/Seguridad.svc/rest/ListadoFuncionalidadMenuRol';

  constructor(
    private http: HttpClient
  ) {
    this.cabeceras = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  getListaRoles() {
    const cuerpo = { ListadoRolrequest: { cod_rol_rol: '' } };
    return this.http.post<any>(this.ListadoROL, cuerpo, { headers: this.cabeceras });
  }
  getListadoMenu() {
    const cuerpo = { ListadoMenurequest: { cod_menu_menu: '' } };
    return this.http.post<any>(this.ListadoMenu, cuerpo, { headers: this.cabeceras });
  }
  getListadoFuncionalidadMenu(codRol: string, codMenu: string) {
    const cuerpo = { ListadoFuncionalidadMenuRolrequest: { cod_rol_rol: codRol, cod_menu_menu: codMenu } };
    return this.http.post<any>(this.ListadoFuncionalidadeMenuRol, cuerpo, { headers: this.cabeceras });
  }
}