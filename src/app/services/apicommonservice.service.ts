import { AppInitService } from './AppInit.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ApiCommonService {
  private cabeceras: any;
  private subject = new Subject<any>();
  private ipServer: string = null;
  private apiCommon: string = null;
  private apiCKU: string = null;
  private apiRecepcionGranel: string = null;
  private apiCambioCondicion: string = null;
  private apiFrutaEmbalada: string = null;

  constructor(
    private http: HttpClient,
    private initService: AppInitService
  ) {
    this.leerConfiguracion();
    this.cabeceras = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  leerConfiguracion() {
    this.initService.obtenerConfig()
      .subscribe((config) => {
        // console.log('OBTENER CONFIG', config);
        // console.log('TIPO ENTORNO AUTH (' + config.prod + ')');
        let entorno = null;
        switch (config.prod) {
          case "local":
            entorno = 'apis_local';
            this.ipServer = config.ips.ip_local;
            break;
          case "dev":
            entorno = 'apis_dev';
            this.ipServer = config.ips.ip_dev;
            break;
          case "prod":
            this.ipServer = config.ips.ip_prod;
            entorno = 'apis_prod';
            break;
          default:
            this.ipServer = config.ips.ip_prod;
            entorno = 'apis_prod';
            break;
        }
        this.apiCommon = this.ipServer + config[entorno].apiCommon;
        this.apiRecepcionGranel = this.ipServer + config[entorno].apiRecepcionGranel;
        this.apiCKU = this.ipServer + config[entorno].apiCKU;
        this.apiCambioCondicion = this.ipServer + config[entorno].apiCambioCondicion;
        this.apiFrutaEmbalada = this.ipServer + config[entorno].apiFrutaEmbalada;
        // console.log('IP', this.ipServer);
      }, (error) => {
        console.error('ERROR AL LEER ARCHIVO DE CONFIGURACION', error);
        return;
      });
  }

  ///////////////////
  // ETIQUETAS ZPL //
  ///////////////////
  cargarEtiquetasZpl(req: any) {
    // console.log('REQUEST ETIQUETA ZPL', req);
    // console.log('REQUEST ETIQUETA ZPL', JSON.stringify(req));
    return this.http.get<any>(this.apiCommon + 'EtiquetasZpl/' + req.id + '?seccion=' + req.seccion + '&zona=' + req.zona + '&frigorifico=' + req.frigorifico, { headers: this.cabeceras });
  }

  cargarCSG(req: any) {
    // console.log('REQUEST CSG', req);
    return this.http.get<any>(this.apiCommon + 'CSG?productor=' + req.productor + '&temporada=' + req.temporada + '&zona=' + req.zona + '&huerto=' + req.huerto);
  }

  ///////////////
  // GENERALES //
  ///////////////
  cargarListadoImpresoras(seccion: string, zona: string, frigorifico: string) {
    return this.http.get<any>(this.apiCommon + `ImpresoraEtiquetas?seccion=${seccion}&zona=${zona}&frigorifico=${frigorifico}`,
      { headers: this.cabeceras });
  }

  cargarGlosas(transaccion: string) {
    // console.log('REQUEST GLOSAS', transaccion);
    return this.http.get<any>(this.apiCommon + 'TipoDocumento/Leyenda?transaccion=' + transaccion, { headers: this.cabeceras });
  }

  cargarEnvasesCosecha() {
    return this.http.get<any>(this.apiCommon + 'EnvaseCosecha', { headers: this.cabeceras });
  }

  cargarExportadoras(usuario: string) {
    return this.http.get<any>(this.apiCommon + 'Exportadora/usuario/' + usuario, { headers: this.cabeceras });
  }

  cargarTemporadas(usuario: string) {
    return this.http.get<any>(this.apiCommon + 'Temporada', { headers: this.cabeceras });
  }

  cargarPlantas(usuario: string, exp: string) {
    return this.http.get<any>(this.apiCommon + 'Planta/Usuario/' + usuario + "/" + exp, { headers: this.cabeceras });
  }

  enviarMenusBusqueda(menus, buscar) {
    this.subject.next({ menus, buscar });
  }

  recibirMenusBusqueda(): Observable<any> {
    return this.subject.asObservable();
  }

  cargarTodasExportadoras() {
    return this.http.get<any>(this.apiCommon + "Exportadora", { headers: this.cabeceras });
  }

  cargarTodasPlantas() {
    return this.http.get<any>(this.apiCommon + "Planta/", { headers: this.cabeceras });
  }

  listarGuiaRecepcion(data: any) {
    // console.log('REQUEST LISTADO RECPCIONES', data);
    let url = this.apiRecepcionGranel + `ListaRecepcionPrincipal?CodTemp=${data.class_medio_ambiente.CodTemp}&CodPlanta=${data.class_medio_ambiente.CodPlanta}&CodExportadora=${data.class_medio_ambiente.CodExportadora}&Pendientes=${data.bit_pendientes}&CantRegistro=${data.class_paginacion.CantiReg}&Pagin=${data.class_paginacion.Pagina}&CodZona=${data.class_medio_ambiente.CodZona}`
    // let url = `http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/ListaRecepcionPrincipal?CodTemp=${data.class_medio_ambiente.CodTemp}&CodPlanta=${data.class_medio_ambiente.CodPlanta}&CodExportadora=${data.class_medio_ambiente.CodExportadora}&Pendientes=${data.bit_pendientes}&CantRegistro=${data.class_paginacion.CantiReg}&Pagin=${data.class_paginacion.Pagina}&CodZona=${data.class_medio_ambiente.CodZona}`
    return this.http.get<any>(url, { headers: this.cabeceras });
  }

  cargarProductor(req: any) {
    // console.log('REQUEST CARGA PRODUCTOR', req);
    return this.http.get<any>(
      this.apiCommon + 'Productor?exportadora=' + req.exportadora + '&huerto=' + req.huerto + '&empresa=' + req.empresa + '&zona=' + req.zona,
      { headers: this.cabeceras });
  }

  cargarTipo(especie: string, exportadora: string) {
    return this.http.get<any>(this.apiCommon + 'Condicion/Especie?especie=' + especie + '&exportadora=' + exportadora, { headers: this.cabeceras });
  }

  /////////////
  // EMPRESA //
  /////////////
  obtenerProductor(rut: string) {
    return this.http.get<any>(this.apiCommon + 'Empresa/' + rut, { headers: this.cabeceras });
  }

  ////////////
  // HUERTO //
  ////////////
  listadoHuerto(rut_empresa: string) {
    return this.http.get<any>(this.apiCommon + 'Huerto/Empresa/' + rut_empresa, { headers: this.cabeceras });
  }

  ///////////////////
  // APIS PARA CKU //
  ///////////////////
  cargarSeccionPlanta(zona: any, frigorifico: any) {
    return this.http.get<any>(this.apiCommon + 'SeccionPlanta?zona=' + zona + '&frigorifico=' + frigorifico);
  }

  cargarEspecies() {
    return this.http.get<any>(this.apiCommon + 'Especie/', { headers: this.cabeceras });
  }

  cargarTipoFrio(especie: string, exportadora: string) {
    // console.log('REQUEST TIPO FRIO', especie + ' ' + exportadora);
    return this.http.get<any>(this.apiCommon +
      'TipoFrio/Especie?especie=' + especie + '&exportadora=' + exportadora, { headers: this.cabeceras });
  }

  cargarVarieades(id_especie: string) {
    return this.http.get<any>(this.apiCommon + 'Variedad/Especie?id=' + id_especie, { headers: this.cabeceras });
  }

  cargarEnvases() {
    return this.http.get<any>(this.apiCommon + 'Envases/', { headers: this.cabeceras });
  }

  cargarDuchas() {
    return this.http.get<any>(this.apiCommon + 'TipoDucha/', { headers: this.cabeceras });
  }

  cargarCondicionesCKU(especie: string, exportadora: string) {
    // console.log('REQUEST CONDICIONES: ' + especie + '  ' + exportadora);
    return this.http.get<any>(this.apiCommon + 'Condicion/Especie?especie=' + especie + '&exportadora=' + exportadora, { headers: this.cabeceras });
  }

  cargarEmpresa(rut: string) {
    return this.http.get<any>(this.apiCommon + 'Empresa/' + rut, { headers: this.cabeceras });
  }

  cargarHuertos(productor: any, temporada: any, zona: any) {
    // console.log('URL HUERTO', this.apiCommon + `HuertoContratado/Productor?productor=${productor}&temporada=${temporada}&zona=${zona}`);
    return this.http.get<any>(this.apiCommon + `HuertoContratado/Productor?productor=${productor}&temporada=${temporada}&zona=${zona}`, { headers: this.cabeceras });
    // return this.http.get<any>(this.apiURL_DSCommonDEV + `` + rut, { headers: this.cabeceras });
  }

  cargarZona(temporada: any, planta: any) {
    return this.http.get<any>(this.apiCommon +
      `Zona/Planta?planta=${planta}`, { headers: this.cabeceras });
  }

  cargarFrigorifico(exportadora: any, zona: any) {
    // console.log(this.apiCommon + `Frigorifico?empresa=${exportadora}&zona=${zona}`);
    return this.http.get<any>(this.apiCommon +
      `Frigorifico?empresa=${exportadora}&zona=${zona}`, { headers: this.cabeceras });
  }

  cargarChofer(rut: string) {
    return this.http.get<any>(this.apiCommon +
      `Chofer/${rut}`, { headers: this.cabeceras });
  }

  agregarChofer(data: any) {
    return this.http.post<any>(this.apiCommon +
      `Chofer/`, data, { headers: this.cabeceras });
  }

  cargarSeccion(temporada: any, zona: any, frigorifico: any) {
    // console.log(this.apiCommon + `SeccionPlanta?zona=${zona}&frigorifico=${frigorifico}`, { headers: this.cabeceras });

    // console.log('REQUEST SECCION', this.apiCommon + `SeccionPlanta?zona=${zona}&frigorifico=${frigorifico}`);
    return this.http.get<any>(this.apiCommon + `SeccionPlanta?zona=${zona}&frigorifico=${frigorifico}`, { headers: this.cabeceras });
  }

  cargarBalanzas(seccion: any, zona: any, frigorifico: any) {
    // temporada: any, &temporada=${temporada}
    return this.http.get<any>(this.apiCommon +
      `frigorifico/Balanzas?seccion=${seccion}&zona=${zona}&frigorifico=${frigorifico}`, { headers: this.cabeceras });
  }

  cargarCorrelativo(tipo: any, temporada: any) {
    return this.http.get<any>(this.apiCommon +
      `Correlativo?tipo=${tipo}&temporada=${temporada}`, { headers: this.cabeceras });
  }

  cargarEspecie(productor: any, temporada: any, zona: any, zonaSelecionada: any) {
    // console.log('REQUEST ESPECIES', productor, temporada, zona, zonaSelecionada);
    // console.log(this.apiURL_DSCommonDEV +`EspecieContratada?productor=${productor}&temporada=${temporada}&zona=${zona}&zonaSelecionada=${zonaSelecionada}`)
    return this.http.get<any>(this.apiCommon +
      `EspecieContratada?productor=${productor}&temporada=${temporada}&zona=${zona}&zonaSeleccionada=${zonaSelecionada}`, { headers: this.cabeceras });
  }

  cargarVariedad(productor: any, temporada: any, zona: any, zonaSelecionada: any, especie: any) {
    // console.log(`VariedadContratada?productor=${productor}&temporada=${temporada}&zona=${zona}&zonaSelecionada=${zonaSelecionada}&especie=${especie}`)
    return this.http.get<any>(this.apiCommon +
      `VariedadContratada?productor=${productor}&temporada=${temporada}&zona=${zona}&zonaSelecionada=${zonaSelecionada}&especie=${especie}`, { headers: this.cabeceras });
  }

  cargarSDP(productor: any, temporada: any, zona: any, huerto: any, especie: any, variedad: any) {
    return this.http.get<any>(this.apiCommon +
      `SDP?productor=${productor}&temporada=${temporada}&zona=${zona}&huerto=${huerto}&especie=${especie}&variedad=${variedad}`, { headers: this.cabeceras });
  }

  cargarEnvasesCosechas(especie: any, variedad: any, exportadora: any) {
    return this.http.get<any>(this.apiCommon +
      `ContenedorEnvaseCosecha/Especie?especie=${especie}&variedad=${variedad}&exportadora=${exportadora}`, { headers: this.cabeceras });
  }

  cargarDetalleEnvasesCosechas(idContenedor: any) {
    return this.http.get<any>(this.apiCommon + `DetalleContenedorEnvCosecha?contenedor=${idContenedor}`, { headers: this.cabeceras });
  }

  cargarEnvasesLista() {
    return this.http.get<any>(this.apiCommon + 'EnvaseCosecha/', { headers: this.cabeceras });
  }

  cargarPesoZona(zona: any, envaseCosecha: any) {
    return this.http.get<any>(this.apiCommon +
      `PesoZona?zona=${zona}&envaseCosecha=${envaseCosecha}`, { headers: this.cabeceras });
  }

  validadorRut(rut: string) {
    return this.http.get<any>(this.apiCKU +
      `ValidarRut?rut=${rut}`, { headers: this.cabeceras });
  }
  ////////////////////// --TRATAMIENTO-- //////////////////////

  getPais() {
    return this.http.get<any>(this.apiCommon +
      `Pais/all`, { headers: this.cabeceras });
  }
  getTipoTratamientoAll() {
    return this.http.get<any>(this.apiCommon +
      `TipoTratamiento/all`, { headers: this.cabeceras });
  }
  getTipoTratamientoIngreActivo(codtratamiento: string) {
    return this.http.get<any>(this.apiCommon +
      `TipoTratamiento/IngredientesActivos?codtratamiento=${codtratamiento}`, { headers: this.cabeceras });
  }
  getProveedor() {
    return this.http.get<any>(this.apiCommon +
      `Proveedor/all`, { headers: this.cabeceras });
  }
  getCamaraSeccion(data: any) {
    return this.http.get<any>(this.apiCommon +
      `Camara/Seccion?seccion=${data.seccion}&zona=${data.zona}&frigorifico=${data.frigorifico}`, { headers: this.cabeceras });
  }
  getCamara(data: any) {
    // console.log(JSON.stringify(data));
    return this.http.get<any>(this.apiCommon +
      `Camara?camara=${data.camara}&seccion=${data.seccion}&zona=${data.zona}&frigorifico=${data.frigorifico}`, { headers: this.cabeceras });
  }
  getAnalisisTratamiento(data: any) {
    // console.log(this.apiCommon +
      // `Camara/AnalisisTratamiento?temporada=${data.temporada}&camara=${data.camara}&zona=${data.zona}&frigorifico=${data.frigorifico}`);
    return this.http.get<any>(this.apiCommon +
      `Camara/AnalisisTratamiento?temporada=${data.temporada}&camara=${data.camara}&zona=${data.zona}&frigorifico=${data.frigorifico}`, { headers: this.cabeceras });
  }
  getTipoFruta(granel: boolean) {
    return this.http.get<any>(this.apiCommon +
      `TipoFruta/tipo?granel=` + granel, { headers: this.cabeceras });
  }

  getIngredienteActivoProducto(data: any) {
    const URL = this.apiCommon + 'IngredienteActivo/Productos?codIngredienteActivo=' + data.codIngredienteActivo;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  getespecieTratamiento(codEspecie: string) {
    const URL = this.apiCommon + `Especie/Tratamientos?codEspecie=${codEspecie}`;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  getAnalisisTratamientoGroup(data: any) {
    const URL = this.apiCommon + `Camara/AnalisisTratamiento/Group?temporada=${data.temporada}&camara=${data.camara}&zona=${data.zona}&frigorifico=${data.frigorifico}`;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }


  ////////////////////// -- FIN TRATAMIENTO-- //////////////////////

  ////////////////////// -- CAMBIO CONDICION-- //////////////////////

  getListadoCondicion() {
    const URL = this.apiCommon + `Condicion/all`;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  cargarZonas() {
    const URL = this.apiCommon + `Zona`;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  filtrar(data: any) {
    const URL = `http://172.18.8.139/NegApiCambioCondicion/api/CambioCondicion/ListadoPalletCambiarCondicion?CodTemp=${data.CodTemp}&CodZona=${data.CodZona}&CodExportadora=${data.CodExportadora}&CodPlanta=${data.CodPlanta}&folioPallet=${data.folioPallet}&guia=${data.guia}&planilla=${data.planilla}&CodEspecie=${data.CodEspecie}&CodVaridad=${data.CodVaridad}&CodProductor=${data.CodProductor}&CodZonaProductor=${data.CodZonaProductor}&CantRegistro=${data.CantRegistro}&Pagin=${data.Pagin}`;
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }

  conteo(data: any) {
    const URL = `http://172.18.8.139/NegApiCambioCondicion/api/CambioCondicion/ListadoCountCondicion?CodTemp=${data.CodTemp}&CodZona=${data.CodZona}&CodExportadora=${data.CodExportadora}&CodPlanta=${data.CodPlanta}&folioPallet=${data.folioPallet}&guia=${data.guia}&planilla=${data.planilla}&CodEspecie=${data.CodEspecie}&CodVaridad=${data.CodVaridad}&CodProductor=${data.CodProductor}&CodZonaProductor=${data.CodZonaProductor}&CantRegistro=${data.CantRegistro}&Pagin=${data.Pagin}`;
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  getListadoProductores(data: any) {
    const URL = this.apiCommon + `Productor/zona?exportadora=${data.exportadora}&zona=${data.zona}`;
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }

  patchEncabezadoPalletFolio(data: any) {
    // console.log(data);
    return this.http.patch<any>(this.apiCambioCondicion + 'EncabezadoPallet/Folio', data, { headers: this.cabeceras })
  }

  postEncabezadoCondicion(data: any) {
    return this.http.post<any>(this.apiCambioCondicion + `EncabezadoCambioCondicion`, data, { headers: this.cabeceras });
  }
  postDetalleCambioCondicion(data: any) {
    return this.http.post<any>('http://172.18.8.139/NegApiCambioCondicion/api/CambioCondicion/DetalleCondicion', data, { headers: this.cabeceras });
    // return this.http.post<any>('http://192.168.0.101/NegCambioCondicion/api/CambioCondicion/DetalleCondicion', data, { headers: this.cabeceras });
  }
  getListadoPrincipalCambioCondicion(data:any){
    const URL = this.apiCambioCondicion + `EncabezadoCambioCondicion/search?temporada=${data.temporada}&zona=${data.zona}&exportadora=${data.exportadora}
    &frigorifico=${data.frigorifico}&pagina=${data.pagina}&filas=${data.filas}`;
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  getDetalleCambioCondicion(data:any){
    const URL = `http://172.18.8.139/NegApiCambioCondicion/api/CambioCondicion/ListadoDetalleMIXProductor?CodTemp=${data.CodTemp}&CodZona=${data.CodZona}&CodExportadora=${data.CodExportadora}&CodPlanta=${data.CodPlanta}&NumeroCambio=${data.NumeroCambio}`;
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }

  getDetallePorFolio(data:any){
    const URL = `http://172.18.8.139/NegApiCambioCondicion/api/CambioCondicion/ListadoDetallePorPallet?CodTemp=${data.CodTemp}&CodZona=${data.CodZona}&CodExportadora=${data.CodExportadora}&CodPlanta=${data.CodPlanta}&folioPallet=${data.folioPallet}&NumeroCambio=${data.NumeroCambio}`;
    // const URL = `http://192.168.0.101/NegCambioCondicion/api/CambioCondicion/ListadoDetallePorPallet?CodTemp=${data.CodTemp}&CodZona=${data.CodZona}&CodExportadora=${data.CodExportadora}&CodPlanta=${data.CodPlanta}&folioPallet=${data.folioPallet}&NumeroCambio=${data.NumeroCambio}`;
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }


  ////////////////////// -- FIN CAMBIO CONDICION-- //////////////////////


  ////////////////////// -- INICIO FRUTA EMBALADA-- //////////////////////

  getPackingSatelite(data: any){
    const URL = this.apiCommon + `PackingSatelite/search?productor=${data.productor}&temporada=${data.temporada}&zona=${data.zona}&zonaProductor=${data.zonaProductor}`;
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }

  postEncabezadoPackingSatelite(data: any) {
    return this.http.post<any>(this.apiFrutaEmbalada + 'EncabezadoPackingSatelite', data, { headers: this.cabeceras });
  }


  ////////////////////// -- FIN FRUTA EMBALADA-- //////////////////////


}