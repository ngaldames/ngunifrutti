import { AppInitService } from './AppInit.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApilogisticaService {
  cabeceras: any;
  private ipServer: string = null;

  private apiSeguridad: string = null;
  private apiCKU: string = null;
  private apiEnvases: string = null;
  private apiRecepcionGranel = null;
  private apiLogistica = null;
  private apiDespachoPacking = null;
  private apiDespachoSGP = null;
  private apiDespachoBinsSGP = null;
  private apiTratamientosSGP = null;
// 76510190: RUT TENO;
// 76224944: RUT LINDEROS;
  constructor(
    private http: HttpClient,
    private initService: AppInitService
  ) {
    this.leerConfiguracion();
    this.cabeceras = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  leerConfiguracion() {
    this.initService.obtenerConfig()
      .subscribe((config) => {
        let entorno = null;
        switch (config.prod) {
          case "local":
            entorno = 'apis_local';
            this.ipServer = config.ips.ip_local;
            break;
          case "dev":
            entorno = 'apis_dev';
            this.ipServer = config.ips.ip_dev;
            break;
          case "prod":
            this.ipServer = config.ips.ip_prod;
            entorno = 'apis_prod';
            break;
          default:
            this.ipServer = config.ips.ip_prod;
            entorno = 'apis_prod';
            break;
        }
        this.apiSeguridad = this.ipServer + config[entorno].apiSeguridad;
        this.apiCKU = this.ipServer + config[entorno].apiCKU;
        this.apiRecepcionGranel = this.ipServer + config[entorno].apiRecepcionGranel;
        this.apiDespachoPacking = this.ipServer + config[entorno].apiDespachoPacking;
        this.apiEnvases = this.ipServer + config[entorno].apiEnvases;
        this.apiLogistica = this.ipServer + config[entorno].apiLogistica;
        this.apiDespachoSGP = this.ipServer + config[entorno].apiDespachoSGP;
        this.apiDespachoBinsSGP = this.ipServer + config[entorno].apiDespachoBinsSGP;
        this.apiTratamientosSGP = this.ipServer + config[entorno].apiTratamientosSGP;
      }, (error) => {
        console.error('ERROR AL LEER ARCHIVO DE CONFIGURACION', error);
        return;
      });
  }

  //////////////
  // INFORMES //
  //////////////
  obtenerInformeSII(req: any) {
    // console.log('REQUEST INFORME SII', req);
    return this.http.get<any>(this.apiRecepcionGranel + 'ImprimirGuiaEnvases?' +
      'CodExportadora=' + req.CodExportadora + '&CodPlanta=' + req.CodPlanta + '&' +
      'CodTemp=' + req.CodTemp + '&CodZona=' + req.CodZona + '&NumRecep=' + req.NumRecep,
      { headers: this.cabeceras });
  }

  /////////
  // ZPL //
  /////////
  imprimirZpl(req: any) {
    // console.log('REQ IMPRIME ZPL', req);
    // let url = 'http://192.168.100.135/Pall.API.Neg.Logistica.CKU/api/ImprimirZPL'
    // return this.http.post<any>(url, req, { headers: this.cabeceras });
    return this.http.post<any>(this.apiCKU + 'ImprimirZPL', req, { headers: this.cabeceras });
  }

  /////////
  // CKU //
  /////////
  listadoCkuPrincipal(req: any) {
    // console.log('REQUEST CKU PRINCIPAL 1', req);
    // return this.http.get<any>(`http://192.168.100.135/Pall.API.Neg.Logistica.CKU/api/ListadoRecepcionCKUPrincipal?bit_pendientes=${req.bit_pendientes}&cod_Temp=${req.class_medio_ambiente.cod_Temp}&cod_Planta=${req.class_medio_ambiente.cod_Planta}&cod_Exportadora=${req.class_medio_ambiente.cod_Exportadora}&cod_Usuario=${req.class_medio_ambiente.cod_Usuario}&num_CantiReg=${req.class_paginacion.num_CantiReg}&num_Pagina=${req.class_paginacion.num_Pagina}&cod_zona=${req.class_medio_ambiente.cod_zona}`, { headers: this.cabeceras });
    return this.http.get<any>(this.apiCKU + `ListadoRecepcionCKUPrincipal?bit_pendientes=${req.bit_pendientes}&cod_Temp=${req.class_medio_ambiente.cod_Temp}&cod_Planta=${req.class_medio_ambiente.cod_Planta}&cod_Exportadora=${req.class_medio_ambiente.cod_Exportadora}&cod_Usuario=${req.class_medio_ambiente.cod_Usuario}&num_CantiReg=${req.class_paginacion.num_CantiReg}&num_Pagina=${req.class_paginacion.num_Pagina}&cod_zona=${req.class_medio_ambiente.cod_zona}`, { headers: this.cabeceras });
  }

  guardarDestalleLineaCKU(req: any) {
    // console.log('REQUEST ACTUALIZA LINEA ', JSON.stringify(req));
    return this.http.put<any>(this.apiCKU + 'UpdateCKUDetalleRecepcion', req, { headers: this.cabeceras });
  }

  listadoEncabezadoCKU(req: any) {
    // console.log('REQUEST ENCABEZADO CKU ', req);
    // return this.http.get<any>(`http://192.168.100.135/Pall.API.Neg.Logistica.CKU/ListadoEncabezadoCKU?cod_temp=${req.cod_temp}&cod_planta=${req.cod_planta}&cod_exportadora=${req.cod_exportadora}&cod_zona=${req.cod_zona}&num_recepcion=${req.num_recepcion}`, { headers: this.cabeceras });
    return this.http.get<any>(this.apiCKU + `ListadoEncabezadoCKU?cod_temp=${req.cod_temp}&cod_planta=${req.cod_planta}&cod_exportadora=${req.cod_exportadora}&cod_zona=${req.cod_zona}&num_recepcion=${req.num_recepcion}`, { headers: this.cabeceras });
  }

  listadoDetalleCku(req: any) {
    // console.log('REQUEST DETALLE CKU', req);
    return this.http.get<any>(this.apiCKU + `ListadoDetalleCKURecepcion?cod_temp=${req.cod_Temp}&cod_planta=${req.cod_Planta}&cod_exportadora=${req.cod_Exportadora}&cod_zona=${req.cod_Zona}&num_recepcion=${req.num_Recepcion}`, { headers: this.cabeceras });
  }

  //////////////////////
  // RECEPCION GRANEL //
  //////////////////////
  obtenerCalculosGenerales(data: any) {
    return this.http.get<any>(this.apiRecepcionGranel + 'TotalesRecepcionGranel?'
      + 'CodExportadora=' + data.CodExportadora
      + '&CodPlanta=' + data.CodPlanta
      + '&CodTemp=' + data.CodTemp
      + '&CodZona=' + data.CodZona
      + '&NumRecep=' + data.NumRecep,
      { headers: this.cabeceras });
  }

  validaCamionEnPlanta(req: any) {
    // console.log('REQUEST VALIDA PATENTE', req);
    return this.http.get<any>(this.apiRecepcionGranel + 'ValidarCamionEnPlanta?'
      + 'CodTemp=' + req.CodTemp
      + '&CodPlanta=' + req.CodPlanta
      + '&CodExportadora=' + req.CodExportadora
      + '&CantRegistro=' + req.CantRegistro
      + '&Pagin=' + req.Pagin
      + '&CodZona=' + req.CodZona
      + '&Patente=' + req.Patente
      , { headers: this.cabeceras });
  }

  guardarEncabezado(data: any) {
    // console.log('REQUEST CABECERA', JSON.stringify(data));
    // let URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/EncabezadoRecepcion';
    // return this.http.post<any>(URL, data, { headers: this.cabeceras });
    return this.http.post<any>(this.apiRecepcionGranel + 'EncabezadoRecepcion/', data, { headers: this.cabeceras });
  }

  actualizarEncabezado(data: any) {
    // console.log('REQUEST EDITA CABECERA', data);
    // console.log('REQUEST EDITA CABECERA', JSON.stringify(data));
    // const URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/EncabezadoRecepcion';
    // return this.http.put<any>(URL, data, { headers: this.cabeceras });
    // return this.http.put<any>(this.apiLogistica + 'GuiaRecepcion/', data, { headers: this.cabeceras }); //DIRECTA
    return this.http.put<any>(this.apiRecepcionGranel + 'EncabezadoRecepcion/', data, { headers: this.cabeceras });
  }

  guardarDetalleEncabezado(data: any) {
    let cuerpo = { itemGuiaRecepcion: data, itemGuiaRecepcionLineaEnvase: data };
    // console.log('REQUEST GUARDA DETALLE ENCABEZADO', cuerpo);
    // console.log('REQUEST GUARDA DETALLE ENCABEZADO', JSON.stringify(cuerpo));
    // const URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/DetalleRecepcionGuia';
    // return this.http.post<any>(URL, cuerpo, { headers: this.cabeceras });
    return this.http.post<any>(this.apiRecepcionGranel + 'DetalleRecepcionGuia/', cuerpo, { headers: this.cabeceras });
  }

  actualizarDetalleEncabezado(data: any) {
    const d = { itemGuiaRecepcion: data };
    // console.log('REQUEST DETALLE RECEPCION DESTARE', data);
    // console.log('REQUEST DETALLE RECEPCION DESTARE', JSON.stringify(data));
    // const URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/DetalleRecepcionDestare';
    // return this.http.put<any>(URL, data, { headers: this.cabeceras });
    return this.http.put<any>(this.apiRecepcionGranel + 'DetalleRecepcionDestare', data, { headers: this.cabeceras });
  }

  actualizaDetallePatch(data: any) { // ACTUALIZA EL DETALLE SIN MODIFICAR EL FOLIO CKU
    // console.log('REQUEST METODO PATCH', data);
    // console.log('REQUEST METODO PATCH', JSON.stringify(data));
    return this.http.patch<any>(this.apiLogistica + 'DetalleGuiaRecepcion', data, { headers: this.cabeceras });
  }

  actualizarTarjas(req: any) { // ACTUALIZA LAS TARJAS PARA EL DESTARE
    // console.log('REQUEST ACTUALIZA TARJAS', req);
    return this.http.post<any>(this.apiRecepcionGranel + 'ActualizarPesoLinea', req, { headers: this.cabeceras });
  }

  // ENVASE SALIDA FRUTA GRANEL //
  obtenerCabeceraEnvaseSalida(req: any) {
    // console.log('REQUEST CABECERA ENVASES SALIDA', req);
    return this.http.get<any>(this.apiLogistica + 'EnvasesInOut' +
      '?exportadora=' + req.exportadora +
      '&planta=' + req.planta +
      '&temporada=' + req.temporada +
      '&zona=' + req.zona +
      '&num_recep=' + req.num_recep, { headers: this.cabeceras });
  }

  agregarEnvaseSalida(data: any) { // AGREGA CABECERA DE ENVASES SALIDA
    // console.log('AGREGAR CABECERA ENVASES SALIDA',this.apiLogistica + 'EnvasesInOut', JSON.stringify(data));
    // console.log('AGREGAR CABECERA ENVASES SALIDA', JSON.stringify(data));
    // console.log('AGREGAR CABECERA ENVASES SALIDA', JSON.stringify(data));
    // const URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/EncabezadoRecepcionEnvasesInOut';
    
    return this.http.post<any>(this.apiLogistica + 'EnvasesInOut', data, { headers: this.cabeceras });
    // return this.http.post<any>(this.apiRecepcionGranel + 'EncabezadoRecepcionEnvasesInOut', data, { headers: this.cabeceras });
    // return this.http.post<any>(URL, data, { headers: this.cabeceras });
  }

  agregarEnvaseSalidaDetalle(data: any) { // AGREGA DETALLE DE ENVASES SALIDA
    // console.log('REQUEST AGREGA ENVASE SALIDA', data);
    // console.log('REQUEST AGREGA ENVASE SALIDA', JSON.stringify(data));
    // const URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/DetalleRecepcionEnvasesInOut';
    return this.http.post<any>(this.apiRecepcionGranel + 'DetalleRecepcionEnvasesInOut', data, { headers: this.cabeceras });
    // return this.http.post<any>(URL, data, { headers: this.cabeceras });
  }

  detalleGuiaRecepcionEnvaseSalida(req: any) {
    // console.log('PATCH RECALCULA REQ', req);
    // console.log('PATCH RECALCULA REQ', JSON.stringify(req));
    return this.http.patch<any>(this.apiLogistica + 'DetalleGuiaRecepcionPesoEnvaseSalida', req, { headers: this.cabeceras });
  }

  listadoProductor(id_productor: string) {
    // return this.http.get<any>(this.apiProductor + '/' + id_productor, { headers: this.cabeceras });
    return this.http.get<any>(this.apiCKU + 'Productor/' + id_productor, { headers: this.cabeceras });
    // return this.http.post<any>(this.apiURL_DSCommon_Local + 'GuardarEncabezadoRecepcion/',
    //   data, { headers: this.cabeceras });
  }

  ///////////////////////
  // CKU - ABRIR LOTES //
  ///////////////////////
  cargarListadoCabeceraLotes(req: any) {
    // console.log('REQUEST LOTE CABECERA CKU', req);
    return this.http.get<any>(this.apiCKU + `ListadoEnvasesCKUDetalle?cod_temp=${req.cod_Temp}&cod_planta=${req.cod_Planta}&cod_exportadora=${req.cod_Exportadora}&cod_zona=${req.cod_zona}&num_recepcion=${req.num_recep}`, { headers: this.cabeceras });
  }

  guardarLineaDetalleLote(req: any) {
    // console.log('REQUEST GUARDAR LINEA LOTE', req);
    return this.http.put(this.apiCKU + 'DetalleGuiaRecepcion', req, { headers: this.cabeceras });
  }

  agregaLineaLotes(req: any) {
    // apiAgregaLineaLotesCKU
    // console.log('REQUEST AGREGA LINEA LOTE', req);
    return this.http.post(this.apiCKU + 'DetalleLoteCKU', req, { headers: this.cabeceras });
  }

  agregaLineaDetalle(req: any) {
    // console.log('REQUEST AGREGA LINEA DETALLE LOTE', req);
    return this.http.post(this.apiCKU + 'DetalleLoteLineaCKU', req, { headers: this.cabeceras });
  }

  actualizaLineaLotes(req: any) {
    // console.log('REQUEST ACTUALIZA LINEA LOTE', req);
    return this.http.put(this.apiCKU + 'DetalleGuiaRecepcion', req, { headers: this.cabeceras });
  }

  actualizaLineaDetalleLotes(req: any) {
    // console.log('REQUEST ACTUALIZA DETALLE LINEA LOTE', req);
    return this.http.put(this.apiCKU + 'DetalleGuiaRecepcion', req, { headers: this.cabeceras });
  }

  ////////////////////////
  // CKU IMPRIMIR FOLIO //
  ////////////////////////
  imprimirFolio(req: any) {
    // console.log('REQUEST IMPRIMIR FOLIO', req);
    // let ip = 'http://192.168.100.135/Pall.API.Neg.Logistica.CKU/api/ImprimirFolio'
    return this.http.post(this.apiCKU + 'ImprimirFolio', req, { headers: this.cabeceras });
    // return this.http.post(this.apiImprimirFolio, req, { headers: this.cabeceras });
  }

  obtenerListadoBines(req: any) {
    // console.log('REQUEST LISTADO BINES', req);
    // console.log('REQUEST LISTADO BINES', JSON.stringify(req));
    // console.log('URL : ' + this.apiBines + '?CodExportadora=' + req.exportadora + '&CodTemp=' + req.temporada + '&cod_zona=' + req.zona + '&num_recep=' + req.num_recep + '&codFrigorifico=' + req.frigorifico + '&codLinea=' + req.cod_linea);
    return this.http.get(this.apiCKU + 'DetalleFoliosCKU?CodExportadora=' + req.exportadora + '&CodTemp=' + req.temporada + '&cod_zona=' + req.zona + '&num_recep=' + req.num_recep + '&codFrigorifico=' + req.frigorifico + '&codLinea=' + req.cod_linea, { headers: this.cabeceras });
  }

  ////////////////////////////////
  // ENVASES DESPACHO/RECEPCION //
  ////////////////////////////////
  listadoEnvases(req: any) {
    let cuerpo = {
      zona: req.zona,
      num_recep: req.num_recep,
      class_medio_ambiente: { cod_Temp: req.cod_Temp, cod_Planta: req.cod_Planta, cod_Exportadora: req.cod_Exportadora }
    };
    // console.log('REQUEST ENVASES', cuerpo);
    return this.http.post<any>(this.apiLogistica + 'DetalleEnvasesInOut/Cabecera', cuerpo, { headers: this.cabeceras });
  }

  guardarCabeceraEnvases(req: any) {
    // console.log('REQUEST GUARDA CABECERA ENV', JSON.stringify(req));
    return this.http.post<any>(this.apiEnvases + 'GuardarEncabezadoEnvases', req, { headers: this.cabeceras });
  }

  listadoEnvasesVacios(req: any) {
    // console.log('REQUEST ENVASES LISTADO', req);
    return this.http.get<any>(this.apiEnvases + `DetalleEnvasesInOut/Cabecera?exportadora=${req.class_medio_ambiente.cod_Exportadora}&planta=${req.class_medio_ambiente.cod_Planta}&temporada=${req.class_medio_ambiente.cod_Temp}&zona=${req.zona}&num_cabecera=${req.num_cabecera}`, { headers: this.cabeceras });
  }

  editaLineaEnvase(req: any) {
    // console.log('REQUEST EDITA ENVASES', req);
    return this.http.put<any>(this.apiEnvases + 'DetalleEnvasesInOut', req, { headers: this.cabeceras });
  }

  guardarLineaEnvase(req: any) {
    // console.log('REQUEST GUARDA ENVASES', req);
    return this.http.post<any>(this.apiEnvases + 'Envases', req, { headers: this.cabeceras });
  }

  cargarZonaProductor(rut: string, temporada: any) {
    // console.log(this.apiLogistica + `ZonaProductor?rut_productor=${rut}&temporada=${temporada}`);
    return this.http.get<any>(this.apiLogistica +
      `ZonaProductor?rut_productor=${rut}&temporada=${temporada}`, { headers: this.cabeceras });
  }

  agregarEnvasesEntrada(itemGuiaRecepcion: any, itemGuiaRecepcionLineaEnvase: any) {
    let cuerpo = { itemGuiaRecepcion: itemGuiaRecepcion, itemGuiaRecepcionLineaEnvase: { items: itemGuiaRecepcionLineaEnvase } };
    // console.log('REQUEST AGREGA ENVASE ENTRADA Y DEALLE', cuerpo);
    // console.log('REQUEST AGREGA ENVASE ENTRADA Y DEALLE', JSON.stringify(cuerpo));
    // const URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/DetalleRecepcionGuia';
    return this.http.post<any>(this.apiRecepcionGranel + 'DetalleRecepcionGuia', cuerpo, { headers: this.cabeceras });
    // return this.http.post<any>(URL, cuerpo, { headers: this.cabeceras });
  }

  actualizarEnvasesEntrada(itemGuiaRecepcion: any) {
    let data = { items: itemGuiaRecepcion };
    // console.log('REQUEST ENVASES ENTRADA', data);
    // const URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/DetalleRecepcionGuia';
    return this.http.put<any>(this.apiRecepcionGranel + 'DetalleGuiaRecepcionEnvasesLinea', data, { headers: this.cabeceras });
    // return this.http.post<any>(URL, data, { headers: this.cabeceras });
  }

  actualizaLineaGuiaRecepcion(data: any) {
    // console.log('REQUEST LINEA RECEPCION', data);
    return this.http.put<any>(this.apiLogistica + 'DetalleRecepcionGranelTarja', data, { headers: this.cabeceras });
  }

  obtenerTarjas(req: any) {
    // console.log('REQUEST OBTENER TARJA', req);
    // return this.http.get<any>(this.apiURL_DSCommon_Local + `DetalleRecepcionGranelTarja/Detalle?num_recep=${req.num_recep}&zona=${req.zona}&frigorifico=${req.frigorifico}&exportadora=${req.exportadora}&temporada=${req.temporada}&linea=${req.linea}`, { headers: this.cabeceras });
    return this.http.get<any>(this.apiLogistica + `DetalleRecepcionGranelTarja/Detalle?num_recep=${req.num_recep}&zona=${req.zona}&frigorifico=${req.frigorifico}&exportadora=${req.exportadora}&temporada=${req.temporada}&linea=${req.linea}`, { headers: this.cabeceras });
  }

  agregarEnvasesEntradaIndividual(envase: any) {
    // console.log('REQUEST ENVASE ENTRADA AGREGA', envase);
    // const URL = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/DetalleRecepcionEnvaseEntrada';
    // return this.http.post<any>(URL, envase, { headers: this.cabeceras });
    return this.http.post<any>(this.apiRecepcionGranel + 'DetalleRecepcionEnvaseEntrada', envase, { headers: this.cabeceras });
    // return this.http.post<any>(this.apiURL_DSLogisticaDEV,data, { headers: this.cabeceras });
  }

  eliminarLinea(data: any) {
    // console.log('REQUEST ELIMINAR', JSON.stringify(data));
    // let URL = 'http://192.168.0.127/Pall.API.Neg.Logistica.FrutaGranel/api/DetalleRecepcionGuia';
    // return this.http.delete<any>(URL + `?NumRecep=${data.NumRecep}&CodZona=${data.CodZona}&CodFrigorifico=${data.CodFrigorifico}&NumLinea=${data.NumLinea}&CodExportadora=${data.CodExportadora}&CodTemp=${data.CodTemp}&PesoEnvaseEntrada=${data.PesoEnvaseEntrada}`, { headers: this.cabeceras });
    return this.http.delete<any>(this.apiRecepcionGranel + `DetalleRecepcionGuia?NumRecep=${data.NumRecep}&CodZona=${data.CodZona}&CodFrigorifico=${data.CodFrigorifico}&NumLinea=${data.NumLinea}&CodExportadora=${data.CodExportadora}&CodTemp=${data.CodTemp}&PesoEnvaseEntrada=${data.PesoEnvaseEntrada}`, { headers: this.cabeceras });
  }

  recepcionFinal(data: any) {
    // console.log('RECEPCION FINAL REQUEST', data);
    return this.http.post<any>(this.apiLogistica + `GuiaRecepcion/Exportar`, data, { headers: this.cabeceras });
  }

  eliminarEnvaseEntrada(id: string, env_cosecha: string, exportadora: string, zona: number, temporada: string) {
    // let env = 'http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/'
    // return this.http.delete<any>(env + `DetalleRecepcionEnvasesInOut/${id}?env_cosecha=${env_cosecha}&exportadora=${exportadora}&zona=${zona}&temporada=${temporada}`, { headers: this.cabeceras });
    console.log(this.apiLogistica + `DetalleEnvasesInOut/${id}?env_cosecha=${env_cosecha}&exportadora=${exportadora}&zona=${zona}&temporada=${temporada}`);
    return this.http.delete<any>(this.apiLogistica + `DetalleEnvasesInOut/${id}?env_cosecha=${env_cosecha}&exportadora=${exportadora}&zona=${zona}&temporada=${temporada}`, { headers: this.cabeceras });
  }

  obtenerEncabezado(exportadora: string, planta: any, temporada: string, zona: number, recepcion: any) {
    //  const URL = `http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/EncabezadoRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}` ;
    // console.log(this.apiRecepcionGranel + `EncabezadoRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`)
    return this.http.get<any>(this.apiRecepcionGranel + `EncabezadoRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    // return this.http.get<any>( URL , { headers: this.cabeceras });
  }

  obtenerListaRecepcionlinea(exportadora: string, planta: any, temporada: string, zona: number, recepcion: any) {
    // const URL = `http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/ListaRecepGranelDetalleGuia?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}` ;
    return this.http.get<any>(this.apiRecepcionGranel + `ListaRecepGranelDetalleGuia?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    // return this.http.get<any>( URL , { headers: this.cabeceras });
  }

  obtenerListaEnvaseEntrada(exportadora: any, planta: any, temporada: string, zona: number, recepcion: any) {
    // const URL = `http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/ListaDetalleEnvaseEntradaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}` ;
    return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleEnvaseEntradaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    // return this.http.get<any>( URL , { headers: this.cabeceras });
  }

  obtenerListaEnvaseSalida(exportadora: string, planta: string, temporada: string, zona: number, recepcion: string) {
    // const URL = `http://192.168.100.197/Pall.API.Neg.Logistica.FrutaGranel/api/ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}` ;
    return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    // return this.http.get<any>( URL , { headers: this.cabeceras });
  }

  anularGuia(data: any) {
    // let URL = 'NegRecepcionDespacioEnvasesVacios/api/DetalleEnvases'
    // console.log('REQUEST ANULAR GUIA', data);
    // console.log('REQUEST ANULAR GUIA', JSON.stringify(data));
    return this.http.put<any>(this.apiLogistica + 'EnvasesInOut', data, { headers: this.cabeceras });
  }

  //////////////////DESPACHO-PACKING//////////////////////////

  obtenerListaPrincipalOt(data: any) {
    const URL = this.apiDespachoSGP + 'OrdenDeTrabajo/DespacharAPacking?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora + '&cerradas=' + data.cerradas + '&pagina=' + data.pagina + '&registros=' + data.registros;
    // return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  obtenerEncabezadoOt(data: any) {
    const URL = this.apiDespachoSGP + 'OrdenDeTrabajo?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora + '&nro_ot=' + data.nro_ot;
    // return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }

  obtenerDetelleOTOt(data: any) {
    const URL = this.apiDespachoPacking + 'DetalleOT?CodTemp=' + data.CodTemp + '&CodZonaProd=' + data.CodZonaProd + '&CodExportadora=' + data.CodExportadora + '&NumeroOt=' + data.NumeroOt;
    // return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }

  obtenerEncabezadoDespachoAPacking(data: any) {
    const URL = this.apiDespachoPacking + 'PopUpAsignarBinsOT?CodTemp=' + data.CodTemp + '&CodZonaProd=' + data.CodZonaProd + '&CodExportadora=' + data.CodExportadora + '&NumGuia=' + data.NumGuia + '&NumLinea=' + data.NumLinea + '&NumeroOt=' + data.NumeroOt;
    // return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  getAsignadosBins(data: any) {
    // console.log('REQUEST ASIGNADOS BINS', data);
    const URL = this.apiDespachoPacking + 'PopUpAsignadosBinsOT?CodTemp=' + data.CodTemp + '&CodZonaProd=' + data.CodZonaProd + '&CodExportadora=' + data.CodExportadora + '&NumGuia=' + data.NumGuia + '&NumLinea=' + data.NumLinea + '&NumeroOt=' + data.NumeroOt;
    // return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  postAsignarBins(data: any) {
    // console.log('POST ASIGNAR BINS', data);
    return this.http.post<any>(this.apiDespachoBinsSGP + 'Bins/AsignarPacking', data, { headers: this.cabeceras });
  }

  EliminarAsignadosBins(data: any) {
    // console.log('POST ELIMINAR ASIGNADOS BINS', JSON.stringify(data));
    return this.http.delete<any>(this.apiDespachoBinsSGP + `Bins?temporada=${data.temporada}&zona_prod=${data.zona_prod}&exportadora=${data.exportadora}&nro_ot=${data.nro_ot}&bins=${data.bins}`, { headers: this.cabeceras });
  }
  patchCerrar(data: any) {
    // console.log('POST CERRAR ASIGNADOS BINS', JSON.stringify(data));
    return this.http.patch<any>(this.apiDespachoSGP + 'OrdenDeTrabajo', data, { headers: this.cabeceras });
    // return this.http.patch<any>('http://172.18.8.165/apiDespachogranelSGP/api/OrdenDeTrabajo?temporada='+data.temporada+'&zona='+data.zona+'&id='+data.id+'&cierre='+data.cierre, { headers: this.cabeceras });
  }

  listadoPrincipalDespachoATercero(data: any) {
    // console.log('GET ENCABEZADO DESPACHO INTERPLANTA POR OT', data);
    const URL = this.apiDespachoSGP + 'OrdenDeTrabajo/DespacharTerceros?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  listadoPrincipalDevolucionProductor(data: any) {
    // console.log('GET LISTADO DEVOLUCION PRODUCTOR', data);
    const URL = this.apiDespachoSGP + 'OrdenDeTrabajo/DevolverProductor?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora + '&concepto=' + data.concepto;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }

  ////////////////////// DESPACHO INTERPLANTA ////////////////////////////
  listadoPrincipalDespachoInterplanta(data: any) {
    // http://172.18.8.165/apiDespachogranelSGP/api/OrdenDeTrabajo/DespacharInterplanta?temporada=17/18&zona_prod=20&exportadora=100
    const URL = this.apiDespachoSGP + 'OrdenDeTrabajo/DespacharInterplanta?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora;
    // return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  getEncabezadodespachoInterplanta(data: any) {
    // console.log('GET ENCABEZADO DESPACHO INTERPLANTA POR OT', data);
    const URL = this.apiDespachoSGP + 'OrdenDeTrabajoOtros?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora + '&nro_ot=' + data.nro_ot;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  getDetalledespachoInterplanta(data: any) {
    // console.log('GET DETALLE DESPACHO INTERPLANTA POR OT', data);
    const URL = this.apiDespachoSGP + 'OrdenDeTrabajoOtros/Detalle?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora + '&ot=' + data.ot;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  getDisponiblesADespacharOtros(data) {
    // console.log('GET DETALLE DESPACHO INTERPLANTA OTROS POPUP', data);
    const URL = this.apiDespachoSGP + 'BinsOtros/DisponiblesDespachar?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora + '&nro_ot=' + data.nro_ot + '&movimiento=' + data.movimiento;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  postPopUpDisponiblesDespacharInterplanta(data: any) {
    // console.log('POST GUARDAR FOLIOS D INTERPLANTAS', JSON.stringify(data));
    return this.http.post<any>(this.apiDespachoSGP + `Bins/AsignarOtros`, data, { headers: this.cabeceras });
  }
  postDisponiblesDespacharCopia(data: any) {
    // console.log('POST GUARDAR FOLIOS D INTERPLANTAS OTRA TABLA', JSON.stringify(data));
    return this.http.post<any>(this.apiDespachoSGP + `OrdenDeTrabajoOtros/CopiaADetalle`, data, { headers: this.cabeceras });
  }
  patchGuardarObservacionDespacharInterplantaTerceros(data: any) {
    // console.log('PATCH PARA GUARDAR OBSERVACION', JSON.stringify(data));
    return this.http.patch<any>(this.apiDespachoSGP + 'OrdenDeTrabajoOtros', data, { headers: this.cabeceras });
  }

  ////////////////////// TRATAMIENTOS ////////////////////////////

  getListadoPrincipalTratamientos(data: any) {
    // http://172.18.8.165/apiDespachogranelSGP/api/OrdenDeTrabajo/DespacharInterplanta?temporada=17/18&zona_prod=20&exportadora=100
    const URL = this.apiTratamientosSGP + 'Tratamiento/search?temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora='
      + data.exportadora + '&frigorifico=' + data.frigorifico + '&estado='
      + data.estado + '&pagina=' + data.pagina + '&filas=' + data.filas;
    // console.log(URL);
    // return this.http.get<any>(this.apiRecepcionGranel + `ListaDetalleSalidaRecepcion?CodExportadora=${exportadora}&CodPlanta=${planta}&CodTemp=${temporada}&CodZona=${zona}&NumRecep=${recepcion}`, { headers: this.cabeceras });
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  postTratamiento(data: any) {
    // console.log(JSON.stringify(data));
    return this.http.post<any>(this.apiTratamientosSGP + `Tratamiento`, data, { headers: this.cabeceras });
  }
  getTratamientoEditar(data: any) {
    const URL = this.apiTratamientosSGP + 'Tratamiento?nrotratamiento=' + data.nrotratamiento + '&temporada=' + data.temporada + '&exportadora='
      + data.exportadora + '&zona_prod=' + data.zona_prod + '&frigorifico=' + data.frigorifico;
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  patchTratamiento(data: any) {

    return this.http.patch<any>(this.apiTratamientosSGP + 'Tratamiento', data, { headers: this.cabeceras });
  }
  getAplicadortratamiento(rut: string) {
    const URL = this.apiTratamientosSGP + 'AplicadorTratamiento?rut=' + rut;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  postAplicadorTratamiento(data:any) {
    return this.http.post<any>(this.apiTratamientosSGP + 'AplicadorTratamiento', data, { headers: this.cabeceras });
  }
  getDetalletratamientoEjecutar(data:any){
    const URL = this.apiTratamientosSGP + 'DetalleTratamiento/Productos?nrotratamiento=' + data.nrotratamiento + '&temporada=' + data.temporada + '&exportadora='
      + data.exportadora + '&zona_prod=' + data.zona_prod + '&frigorifico=' + data.frigorifico;
    // console.log(URL);
    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  postDetalleTratamiento(data:any){
    return this.http.post<any>(this.apiTratamientosSGP + 'DetalleTratamiento',data, { headers: this.cabeceras });
  }
  putDetalleTratamiento(data:any){
    return this.http.put<any>(this.apiTratamientosSGP + 'DetalleTratamiento',data, { headers: this.cabeceras });
  }
  deleteProductoDetalleTratamiento(data:any){
    // console.log('POST ELIMINAR ASIGNADOS BINS', JSON.stringify(data));
    return this.http.delete<any>(this.apiTratamientosSGP + `DetalleTratamiento?nrotratamiento=${data.nrotratamiento}&temporada=${data.temporada}&exportadora=${data.exportadora}&zona_prod=${data.zona_prod}&frigorifico=${data.frigorifico}&producto=${data.producto}`, { headers: this.cabeceras });
  }
  getProveedorTratamiento(data:any){
    const URL = this.apiTratamientosSGP + 'Proveedor/Tratamientos?temporada=' + data.temporada + '&zona=' + data.zona + '&exportadora=' + data.exportadora + '&rutProveedor=' + data.rutProveedor + '&condicion=' + data.condicion;// + '&condicion=' + data.condicion

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  postTratamientoFactura(data:any){
    // console.log(this.apiTratamientosSGP + 'Tratamiento/Factura', JSON.stringify(data));
    return this.http.post<any>(this.apiTratamientosSGP + 'Tratamiento/Factura', data, { headers: this.cabeceras });
  }
  getFacturados(data:any){
    const URL = this.apiTratamientosSGP + 'Proveedor/Facturados?nroFactura=' + data.nroFactura + '&proveedor=' + data.proveedor + '&temporada=' + data.temporada + '&zona_prod=' + data.zona_prod + '&exportadora=' + data.exportadora + '&frigorifico=' + data.frigorifico + '&condicion=' + data.condicion;

    return this.http.get<any>(URL, { headers: this.cabeceras });
  }
  postBinsCopia(data){
    return this.http.post<any>(this.apiTratamientosSGP + 'BinsTratamiento/Copia', data, { headers: this.cabeceras });
  }
}