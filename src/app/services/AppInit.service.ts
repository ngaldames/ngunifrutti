import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { resolve } from 'url';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class AppInitService {

  constructor(
    private http: HttpClient,
  ) { }

  public obtenerConfig(): Observable<any> {
    return this.http.get('./assets/config/app-config.json');
    // return this.http.get<any>('./assets/config/app-config.json');
  }

  public cargarConfiguracion(){
    
  }
}