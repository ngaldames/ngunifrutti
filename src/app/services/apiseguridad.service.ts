import { AppInitService } from './AppInit.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { stringify } from 'querystring';
@Injectable({
  providedIn: 'root'
})

export class ApiseguridadService {
  cabeceras: any;
  private ipServer = null;

  private apiSeguridad: string = 'apiSeguridadSGP/api/';
  private apiCKU: string = null;
  private apiEnvases: string = null;
  private apiRecepcionGranel = null;
  private apiLogistica = null;

  constructor(
    private http: HttpClient,
    private initService: AppInitService
  ) {
    this.cabeceras = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.leerConfiguracion();
  }

  leerConfiguracion() {
    this.initService.obtenerConfig()
      .subscribe((config) => {
        // console.log('TIPO ENTORNO SEGURIDAD (' + config.prod + ')');
        let entorno = null;
        switch (config.prod) {
          case "local":
            entorno = 'apis_local';
            this.ipServer = config.ips.ip_local;
            break;
          case "dev":
            entorno = 'apis_dev';
            this.ipServer = config.ips.ip_dev;
            break;
          case "prod":
            this.ipServer = config.ips.ip_prod;
            entorno = 'apis_prod';
            break;
          default:
            this.ipServer = config.ips.ip_prod;
            entorno = 'apis_prod';
            break;
        }
        this.apiSeguridad = this.ipServer + config[entorno].apiSeguridad;
        this.apiCKU = this.ipServer + config[entorno].apiCKU;
        this.apiRecepcionGranel = this.ipServer + config[entorno].apiRecepcionGranel;
        this.apiEnvases = this.ipServer + config[entorno].apiEnvases;
        this.apiLogistica = this.ipServer + config[entorno].apiLogistica;
        // console.log('IP SERVER', this.ipServer);
        // console.log('SEGURIDAD', this.apiSeguridad);
      }, (error) => {
        console.error('ERROR AL LEER ARCHIVO DE CONFIGURACION', error);
        return;
      });
  }

  ///////////////
  // FAVORITOS //
  ///////////////
  obtenerFavoritos(usuario: string) {
    console.log('REQUEST FAVORITOS', usuario);
    return this.http.get<any>(this.apiSeguridad + 'Favorito?usuario=' + usuario, { headers: this.cabeceras });
  }

  guardarFavorito(favorito: any) {
    console.log('REQUEST GUARDA FAVORITOS', favorito);
    return this.http.post<any>(this.apiSeguridad + 'Favorito', favorito, { headers: this.cabeceras });
  }

  //////////////
  // USUARIOS //
  //////////////
  cargarListadoUsuarios(user: string) {
    return this.http.get<any>(this.apiSeguridad + 'usuario', { headers: this.cabeceras });
  }

  AddUsuarios(id: string, nombre: string, password: string, activo: any, desde: any, hasta: any) {
    // console.log('Datos a grabar: ' + id + ' - ' + nombre);
    const data = { id: id, nombre: nombre, password: password, activo: activo, desde: desde, hasta: hasta };
    return this.http.post<any>(this.apiSeguridad + 'Usuario', data, { headers: this.cabeceras });
  }

  actualizarUsuario(id: string, data: string) {
    console.log(JSON.stringify(id));
    console.log(JSON.stringify(data));
    return this.http.put<any>(this.apiSeguridad + 'Usuario/' + id, data, { headers: this.cabeceras });
  }

  DelUsuarios(id: string) {
    console.log('Usuario borrado', id);
    return this.http.delete<any>(this.apiSeguridad + 'Usuario/' + id, { headers: this.cabeceras });
  }

  //MENU - USUARIO
  MenuUsuario(id: string, planta: string, exportadora: string) {
    return this.http.get<any>(this.apiSeguridad + `Usuario/Pantallas/${id}/${planta}/${exportadora}`,
      { headers: this.cabeceras });
  }

  cargarMenu() {
    return this.http.get<any>(this.apiSeguridad + 'Menu', { headers: this.cabeceras });
  }

  ///////////
  // ROLES //
  ///////////
  obtenerRoles() {
    return this.http.get<any>(this.apiSeguridad + 'rol', { headers: this.cabeceras });
  }

  agregarRol(id: string, rol: string) {
    const cuerpo = { 'id': id, 'nombre': rol };
    return this.http.post<any>(this.apiSeguridad + 'rol', cuerpo, { headers: this.cabeceras });
  }

  editarRol(id: string, rol: string) {
    const cuerpo = { 'id': id, 'nombre': rol };
    return this.http.put<any>(this.apiSeguridad + 'rol/' + id, cuerpo, { headers: this.cabeceras });
  }

  eliminarRol(id: string) {
    return this.http.delete<any>(this.apiSeguridad + 'rol/' + id, { headers: this.cabeceras });
  }

  //////////////
  // PANTALLA //
  //////////////
  obtenerPantallas() {
    return this.http.get<any>(this.apiSeguridad + 'pantalla', { headers: this.cabeceras });
  }

  ///////////
  // MENUS //
  ///////////
  obtenerFuncionalidadesMenus(menu: string) {
    console.log('RQUEST MENU', menu);
    return this.http.get<any>(this.apiSeguridad + '/Pantalla/Funcionalidad?id=' + menu, { headers: this.cabeceras });
  }

  obtenerFuncionalidadesUsuarios(usuario: string) {
    return this.http.get<any>(this.apiSeguridad + 'Usuario/Funcionalidades/' + usuario, { headers: this.cabeceras });
  }

  agregarFuncionalidadesUsuarios(data: any) {
    console.log(JSON.stringify(data));
    console.log(this.apiSeguridad + 'Usuario/Funcionalidad/', data);
    return this.http.post<any>(this.apiSeguridad + 'Usuario/Funcionalidad/', data, { headers: this.cabeceras });
  }

  copiarFuncionalidadesUsuarios(data: any) {
    return this.http.post<any>(this.apiSeguridad + '/Usuario/CopiaFuncionalidades/', data, { headers: this.cabeceras });
  }

  eliminarFuncionalidadesUsuarios(usuario: string, planta: string, exportadora: string, funcionalidad: string, pantalla: string) {
    return this.http.delete<any>(this.apiSeguridad +
      `Usuario/Funcionalidad/${usuario}/${planta}/${exportadora}?funcionalidad=${funcionalidad}&pantalla=${pantalla}`,
      { headers: this.cabeceras });
  }
  ////////////////////TRATAMIENTO////////////////////

  getUsuarioSearch(nombre:string){
    return this.http.get<any>(this.apiSeguridad + 'Usuario/search?nombre='+nombre, { headers: this.cabeceras });
  }

}