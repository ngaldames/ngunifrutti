import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { AppInitService } from './AppInit.service';

@Injectable({
  providedIn: 'root'
})

export class AutenticacionService {
  @Output() fire: EventEmitter<any> = new EventEmitter();
  private cabeceras: any;
  private logueado: boolean = false;
  private ipServer = null;
  private apiSeguridadNeg: string = null;

  constructor(
    private http: HttpClient,
    private initService: AppInitService
  ) {
    this.leerConfiguracion();
    this.cabeceras = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  leerConfiguracion() {
    this.initService.obtenerConfig()
      .subscribe((config) => {
        let entorno = null;
        switch (config.prod) {
          case "local":
            entorno = 'apis_local';
            this.ipServer = config.ips.ip_local;
            break;
          case "dev":
            entorno = 'apis_dev';
            this.ipServer = config.ips.ip_dev;
            break;
          case "prod":
            this.ipServer = config.ips.ip_prod;
            entorno = 'apis_prod';
            break;
          default:
            this.ipServer = config.ips.ip_prod;
            entorno = 'apis_prod';
            break;
        }
        this.apiSeguridadNeg = this.ipServer + config[entorno].apiSeguridadNeg;
      }, (error) => {
        console.error('ERROR AL LEER ARCHIVO DE CONFIGURACION', error);
        return;
      });
  }

  // iniciarSesion(usuario: string, password: string, nombre: string, activo: boolean, desde:any, hasta:any) {
  iniciarSesion(usuario: string, password: string) {
    const cuerpo = { id: usuario, password: password};
    console.log(this.apiSeguridadNeg + 'login', JSON.stringify(cuerpo))
    return this.http.post<any>(this.apiSeguridadNeg + 'login', cuerpo, { headers: this.cabeceras });
    // return this.http.post<any>('http://192.168.0.102/Pall.API.Neg.Seguridad/api/Login', cuerpo, { headers: this.cabeceras });
  }

  change(status: boolean) {
    this.fire.emit(status);
  }

  getEmitteedValue() {
    return this.fire;
  }
}