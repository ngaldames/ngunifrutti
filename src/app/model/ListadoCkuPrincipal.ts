export interface ICKUPrincipalRequest {
    'Pendientes': boolean;
    'medioambiente': {
        'CodTemp': null;
        'CodPlanta': null;
        'CodExportadora': null;
        'CodUsuario': null
    };
    'paginacion': {
        'CantiReg': 0;
        'Pagina': 0
    }
}