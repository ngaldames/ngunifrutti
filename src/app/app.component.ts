import { ApiseguridadService } from './services/apiseguridad.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AutenticacionService } from './services/autenticacion.service';
import { Component, ViewChild, OnChanges, OnInit, enableProdMode } from '@angular/core';
import { SidenavComponent } from './shared/sidenav/sidenav.component';
import { environment } from '../environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnChanges {

  @ViewChild(SidenavComponent) sidenav: SidenavComponent;
  public home: boolean;
  public config = false;
  public title = 'Unifrutti';
  public _Subscription: Subscription;
  key: string;
  exportadora: string;
  temporada: string;
  planta: string;
  esFavorito: boolean;

  constructor(
    private authService: AutenticacionService,
    private _router: Router,
    private apiSeguridad: ApiseguridadService
  ) {
    this._Subscription = this.authService.getEmitteedValue()
      .subscribe((item: any) => {
        this.home = item;
      });
    this.key = localStorage.getItem('Session');
    if (this.key !== 'true') {
      this._router.navigate(['login']);
    } else {
      this.authService.change(true);
    }
    this.exportadora = localStorage.getItem('Exportadora');
    this.temporada = localStorage.getItem('Temporada');
    this.planta = localStorage.getItem('Planta');
    if (this.exportadora !== '' && this.temporada !== '' && this.planta !== '') {
      this._router.navigate(['favoritos']);
    } else {
      this.authService.change(true);
    }
    // ESTE METODO ELIMINA TODOS LOS CONSOLE LOGS DE LA APLICACION | ng build --prod
    // if (environment.production) {
    //   if (window) {
    //     window.console.log = function () { };
    //   }
    // }
  }

  ngOnInit() { }

  ngOnChanges() {
    this.apiSeguridad.leerConfiguracion();
  }

  cerrarSesion() {
    localStorage.removeItem('Session');
    localStorage.removeItem('User');
    this.authService.change(false);
    this._router.navigate(['login']);
  }

  procesaPropagar(event: any) {
    this.sidenav.recibirMenu(event);
  }

  busquedaMenu(event: any) {
    console.log('BUSCA MENU', event);
  }
}