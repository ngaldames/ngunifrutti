import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { AutenticacionService } from '../services/autenticacion.service';
import { Md5 } from 'md5-typescript';
import { Subscription } from 'rxjs';
import { MatDialog, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    trigger('show', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(500, style({ opacity: 0 }))
      ])
    ])
  ]
})

export class LoginComponent implements OnInit, OnDestroy {
  hide = true;
  sesion: boolean = false;
  urlLogoEmpresa: string = 'assets/img/unifrutti.png';
  urlLogoPall: string = 'assets/img/logopallb.png';
  key: string;
  Loginkey: string;
  config: string[];
  private _Subscription: Subscription;
  user: any[] = [];
  usuario: string = null;
  password: string = null;
  msgError: string;
  cargando: boolean = false;

  constructor(
    private _router: Router,
    private authService: AutenticacionService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {
    this.key = localStorage.getItem('Session');
    if (this.key === 'true') {
      this._router.navigate(['favoritos']);
    }
  }

  ngOnInit() { }

  ngOnDestroy() { }

  iniciarSesion() {
    console.log('ENVIO', this.usuario, this.password);
    this.cargando = true;
    this._Subscription = this.authService.iniciarSesion(this.usuario, this.password)
      .subscribe((data: any) => {
        console.log('USUARIO LOGIN', data);
        this.user = data;
        if (this.user['valido']) {
          this.Loginkey = 'true';
          localStorage.setItem('Session', this.Loginkey);
          localStorage.setItem('User', this.usuario);
          this.authService.change(true);
        } else {
          this.snackBar.open(this.user['mensaje'], null, { duration: 3000 });
        }
        this.cargando = false;
      }, (error) => {
        this.cargando = false;
        console.error('ERROR EN EL SERVIDOR', error);
        // this.snackBar.open('Usuario/contraseña no son válidos', null, { duration: 3000 });
        this.snackBar.open(error.error.mensaje, null, { duration: 3000 });
      });
  }
}