import { Importar_parametrosComponent } from './../../shared/dialogs/importar_parametros/importar_parametros.component';
import { ApiseguridadService } from './../../services/apiseguridad.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCommonService } from '../../services/apicommonservice.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.css']
})
export class FavoritosComponent implements OnInit {
  menuFavoritos: any[] = [];
  message: any;
  subscription: Subscription;
  menuBusqueda = [];
  buscando = false;
  favoritos: any = [];

  constructor(
    private router: Router,
    private apiSeguridadService: ApiseguridadService,
    public apiCommon: ApiCommonService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.obtenerFavoritos();
  }

  obtenerFavoritos() {
    setTimeout(() => {
      let usuario = localStorage.getItem('User');
      this.apiSeguridadService.obtenerFavoritos(usuario)
        .subscribe((data) => {
          this.favoritos = data.pantallas;
          console.log('FAVORITOS', data);
        }, (error) => {
          console.error('ERROR CARGA FAVORITOS', error);
        });
    }, 1000);
  }

  frutagranel() {
    this.router.navigate(['/pantallacku'], { queryParams: { mn: '3.1.1.1.' } });
  }

  cku() {
    this.router.navigate(['/rec_cku']);
  }

  frio() {
    this.router.navigate(['/rec_frio']);
  }

  roles() {
    this.router.navigate(['/roleslistado']);
  }

  usuarios() {
    this.router.navigate(['/usuarioslistado']);
  }

  envvacios() {
    this.router.navigate(['/Des_env_vacios']);
  }

  funcionalidades() {
    this.router.navigate(['/def-funcionalidades-globales']);
  }

  documentos_globales() {
    this.router.navigate(['/documentos_globales']);
  }

  parametros_logistica() {
    this.router.navigate(['/parametros_logistica']);
  }

  despacho_packing() {
    this.router.navigate(['/despacho_packing']);
  }

  despacho_interplanta() {
    this.router.navigate(['/despacho_interplanta']);
  }
  despacho_terceros() {
    this.router.navigate(['/despacho_planta_terceros']);
  }
  despacho_productores() {
    this.router.navigate(['/despacho_productores']);
  }
  consulta_cambio_condicion() {
    this.router.navigate(['/consulta_cambio_condicion']);
  }

  cta_cte() {
    this.router.navigate(['/ctacte_envcos']);
  }
  tratamientos() {
    this.router.navigate(['/listado_tratamientos']);
  }
  cku_fruta_embalada(){
    this.router.navigate(['/control_de_calidad_fruta_embalada']);
  }

  frutaembalada(){
    this.router.navigate(['/listado_recepcion_fruta_embalada']);
  }
  op_fruta_embalada() {
    this.router.navigate(['/listado_principal_folios_op']);
  }
  asignacion_prod_a_recibidor(){
    this.router.navigate(['/listado_principal_asignacion_prod']);
  }
  listado_despacho_puerto_aeropuerto(){
    this.router.navigate(['/listado_despacho_puerto_aeropuerto']);
  }
  documentos_electronicos() {
    this.router.navigate(['/documentos_electronicos']);
  }


  importar_parametros(){
    const dialogRef = this.dialog.open(Importar_parametrosComponent, {
      width: '700px',
    });
  }
}