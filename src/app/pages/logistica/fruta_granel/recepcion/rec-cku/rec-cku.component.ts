import { Subscription, Subject } from 'rxjs';
import { ApiCommonService } from './../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../services/apilogistica.service';
import { Component, OnInit, ViewChild, Output, EventEmitter, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router, NavigationExtras } from '@angular/router';
import { Imp_folio_binsComponent } from '../dialogs/imp_folio_bins/imp_folio_bins.component';
import { MatSlideToggleChange } from '@angular/material';
import { map, takeUntil } from 'rxjs/operators';
export interface PeriodicElement {
  planta: string;
  lote: string;
  fecha: any;
  nombreproductor: string;
  cod_productor: any;
  variedad: string;
  bandejas: any;
  totalkilos: any;
  hrllegada: any;
  icons: any;
}
@Component({
  selector: 'app-rec-cku',
  templateUrl: './rec-cku.component.html',
  styleUrls: ['./rec-cku.component.css']
})

export class RecCkuComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  private sub = new Subject();
  displayedColumns: string[] = ['num_Guia_Recepcion', 'fecha', 'cod_productor', 'huerto', 'guia_productor', 'cod_especie', 'patente', 'icons'];
  dataSource = new MatTableDataSource([]);
  mostrarTabla: boolean = false;
  @Output()
  change: EventEmitter<MatSlideToggleChange>;
  frigorificos: any = [];
  frigoSel: any;
  pendiente: boolean = false;
  temporada: string = null;
  planta: string = null;
  exportadora: string = null;
  zona: string = null;
  usuario: string = null;
  tituloTabla: string;
  esFavorito: boolean;
  public cargando = true;
  public cargandoFrigo = false;
  tablaError: boolean = false;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.cargando = false;
    this.obtieneZona();
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnDestroy() { }

  ngAfterViewInit() { }

  obtieneZona() {
    this.temporada = localStorage.getItem('temporada');
    this.planta = localStorage.getItem('planta');
    this.exportadora = localStorage.getItem('exportadora');
    this.usuario = localStorage.getItem('User');
    this.commonService.cargarZona(this.temporada, this.planta)
      .subscribe((result) => {
        // console.log('ZONAS', result);
        this.zona = result.items[0].id;
        this.obtieneFrigorificos();
      }, (error) => {
        console.error('ERROR ZONAS', error);
        this.snackBar.open('Error al cargar zonas', null, { duration: 3000 });
      });
  }

  obtieneFrigorificos() {
    this.frigorificos = [];
    this.commonService.cargarFrigorifico(localStorage.getItem('exportadora'), localStorage.getItem('planta'))
      .subscribe((data) => {
        console.log('FRIGORIFICOS', data);
        this.frigorificos = data.items;
        this.frigoSel = this.frigorificos[0].id;
        this.selFrigorifico(this.frigorificos[0].id);
        this.cargandoFrigo = true;
      }, (error) => {
        console.error('ERROR FRIGORIFICOS', error);
        this.snackBar.open('Error al cargar frigorificos', null, { duration: 3000 });
      });
  }

  selFrigorifico(event: any) {
    if (event.value > 0) {
      this.frigoSel = event.value;
    } else {
      this.frigoSel = event;
    }
    this.cargaListadoCKU();
  }

  recargar(){
    this.obtieneFrigorificos();
    this.cargaListadoCKU();
  }

  cargaListadoCKU() {
    this.cargando = true;
    
    if (this.pendiente) {
      this.tituloTabla = ' Listado recepciones con CKU';
    } else {
      this.tituloTabla = ' Listado recepciones sin CKU';
    }
    let req = {
      bit_pendientes: this.pendiente,
      class_medio_ambiente: {
        cod_Temp: this.temporada,
        cod_Planta: this.frigoSel,
        cod_Exportadora: this.exportadora,
        cod_Usuario: this.usuario,
        cod_zona: this.zona
      }, class_paginacion: { num_CantiReg: 50, num_Pagina: 1 }
    };
    console.log('ZONA', this.zona);
    this.logisticaService.listadoCkuPrincipal(req)
      .subscribe((data) => {
        if (data.lista.length > 0) {
          this.mostrarTabla = true;
          console.log('LISTADO PRINCIPAL CKU', data.lista);
          this.dataSource = new MatTableDataSource(data.lista);
          this.cargando = false;
          this.tablaError = false;
        } else {
          this.tablaError = true;
          this.mostrarTabla = false;
        }
      }, (error) => {
        console.error('ERROR TABLA CKU', error);
        this.snackBar.open('Error al cargar tabla CKU', null, { duration: 3000 });
        this.mostrarTabla = false;
        this.cargando = false;
        this.tablaError = true;
      });
  }

  imprimirFolio() {
    const dialogRef = this.dialog.open(Imp_folio_binsComponent, {
      disableClose: true,
      width: '500px'
    });
    dialogRef.afterClosed()
      .subscribe((data) => {
      }, (error) => {
        console.log('Error cerrando folio');
      });
  }

  filtrar(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  cambioEstado(event: MatSlideToggleChange) {
    this.pendiente = event.checked;
    this.cargaListadoCKU();
  }

  ckuTabla(item: any) {
    // console.log('ITEM ', item);
    let extras: NavigationExtras = {
      queryParams: {
        'bit_Pendientes': this.pendiente,
        'temporada': this.temporada,
        'planta': this.frigoSel,
        'zona': this.planta,
        'exportadora': this.exportadora,
        'usuario': this.usuario,
        'num_recepcion': item['num_Guia_Recepcion']
      }
    };
    this.router.navigate(['/cku-tabla'], extras);
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  anterior() {

  }

  siguiente() {

  }
}