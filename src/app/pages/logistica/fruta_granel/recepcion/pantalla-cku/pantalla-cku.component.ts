import { ApilogisticaService } from './../../../../../services/apilogistica.service';
import { Component, OnInit, } from '@angular/core';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ApiCommonService } from '../../../../../services/apicommonservice.service';
import { ApiseguridadService } from '../../../../../services/apiseguridad.service';

@Component({
  selector: 'app-pantalla-cku',
  templateUrl: './pantalla-cku.component.html',
  styleUrls: ['./pantalla-cku.component.css']
})
export class PantallaCkuComponent implements OnInit {
  guia: string;
  fecha: any;
  cod_productor: any;
  huerto: string;
  guia_productor: any;
  cod_especie: any;
  patente: any;
  icons: any;
  isActive: boolean = false;
  esFavorito: boolean;
  displayedColumns: string[] = ['guia', 'fecha', 'cod_productor', 'huerto', 'guia_productor', 'cod_especie', 'patente', 'icons'];
  dataSource = new MatTableDataSource([]);
  dataSource2 = new MatTableDataSource([]);
  mostrarTabla = true;
  menus_usuario_funcionalidad = [];
  public cargando = true;
  public cargandoFrigo = false;
  menu = '';
  arregloPermisos: any = [];
  usuario = localStorage.getItem('User');
  temporada = localStorage.getItem('temporada');
  zona = 0;
  frigorifico = 0;
  frigorificos = [];
  pagina = 1
  sinDatos = false;
  cod_zona: string = null;
  horaActual: any;

  arrEnvasesEntrada: any = [];
  arrEnvasesSalida = [];
  //REPORTE
  totalUniReporte = 0
  totalOtrosReporte = 0
  pesoEnvasesReporte = 0
  guiaProductor = '';

  detalle: any = [{}];
  cabecera: any = {
    id: null,
    fechaCosecha: null,
    empresa: { id: null, nombre: null },
    huerto: { id: null, nombre: null },
    pesoBrutoCamion: null,
    pesoBrutoTotal: null,
    chofer: { id: null, nombre: null },
    frigorifico: { id: null, nombre: null },
    glosa: null,
    pesoEnvasesIn: null,
    pesoEnvasesOut: null,
  };
  especieInforme: string;
  nombrePlanta: string;
  totalEnvases: any;
  totalPesoEnvase: any;

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private wsCommon: ApiCommonService,
    private wsSeguridad: ApiseguridadService,
    private activareRoute: ActivatedRoute,
    private apiLogisticaService: ApilogisticaService
  ) {
    const m = this.activareRoute.queryParams.forEach(el => {
      this.menu = el.mn;
    });
  }

  ngOnInit() {
    this.cargando = false;
    this.rescatarZona();
    // this.frigorifico = '  22';
    this.horaActual = new Date();
    let idexportadora = localStorage.getItem('NidExportadora');
    let idPlanta = localStorage.getItem('NidPlanta');
    this.usuario = localStorage.getItem('User');
    this.wsSeguridad.obtenerFuncionalidadesUsuarios(this.usuario)
      .subscribe(result => {
        // console.log('FUNCIONALIDAD USUARIO', result);
        this.menus_usuario_funcionalidad = result.items.filter(res => {
          return res.exportadora.id === idexportadora && res.planta.id === idPlanta && res.pantalla_funcionalidad.pantalla.id === this.menu;
        });
        // console.log('MENUS USUARIO', this.menus_usuario_funcionalidad);
        this.menus_usuario_funcionalidad.forEach(ele => {
          this.arregloPermisos[ele.pantalla_funcionalidad.funcionalidad.id] = true;
        });
        console.log('PERMISOS', this.arregloPermisos);
      });
    this.cargarZona();
  }

  cargarZona() {
    this.wsCommon.cargarZona(localStorage.getItem('temporada'), localStorage.getItem('NidPlanta'))
      .subscribe((data) => {
        if (data.items[0].id) {
          this.cod_zona = data.items[0].id;
        }
      }, (error) => {
        console.error('ERROR AL OBTENER ZONA', error);
        this.cod_zona = null;
      });
  }

  sumaBultos() {
    let suma = 0;
    this.detalle.forEach(ln => {
      suma += ln.numEnvasesCont;
    });
    return suma;
  }

  sumaPesoNeto() {
    let suma = 0
    this.detalle.forEach(ln => {
      suma += ln.kilosNetosIn;
    });
    return suma;
  }

  rescatarZona() {
    const temporada = localStorage.getItem('temporada');
    const planta = localStorage.getItem('planta');
    this.wsCommon.cargarZona(temporada, planta)
      .subscribe((result) => {
        this.zona = result.items[0].id;
        console.log('ZONA', this.zona);
        this.rescatarFrigorifico();
      });
  }

  rescatarFrigorifico() {
    const exportadora = localStorage.getItem('exportadora');
    this.wsCommon.cargarFrigorifico(exportadora, this.zona)
      .subscribe((result) => {
        console.log('FRIGORIFICOS', result);
        this.frigorificos = result.items;
        this.frigorifico = this.frigorificos[0].id;
        this.selFrigorifico(this.frigorificos[0]);
        this.cargandoFrigo = true;
      }, (error) => {
        console.error('ERROR AL OBTENER FRIGORIFICO', error);
      });
  }

  recepcionFinal(item: any) {
    console.log('SELEECCION', item);
    this.arrEnvasesEntrada = [];
    this.arrEnvasesSalida = [];
    // this.apiLogisticaService.obtenerEncabezado(localStorage.getItem('NidExportadora'), localStorage.getItem('NidPlanta'), localStorage.getItem('temporada'), this.zona, item.num_guia_recepcion)
    this.apiLogisticaService.obtenerEncabezado(localStorage.getItem('NidExportadora'), this.frigorifico, localStorage.getItem('temporada'), this.zona, item.num_guia_recepcion)
      .subscribe((data) => {
        console.log('OBTENER ENCABEZADO', data);
        this.cabecera = data.enc;
        // console.log('CEBECERA EN DESTARE', this.cabecera);
      }, (error) => {
        console.error('ERROR CARGANDO CABECERA', error);
      });
    // this.apiLogisticaService.obtenerListaEnvaseEntrada(localStorage.getItem('NidExportadora'), localStorage.getItem('NidPlanta'), localStorage.getItem('temporada'), this.zona, item.num_guia_recepcion)
    this.apiLogisticaService.obtenerListaEnvaseEntrada(localStorage.getItem('NidExportadora'), this.frigorifico, localStorage.getItem('temporada'), this.zona, item.num_guia_recepcion)
      .subscribe(data => {
        // console.log('ENVASES ENTRADA', data)
        this.arrEnvasesEntrada = data.lista.items;
      }, (error) => {
        console.error('ERROR CARGANDO ENVASES ENTRADA', error);
      });
    // this.apiLogisticaService.obtenerListaEnvaseSalida(localStorage.getItem('NidExportadora'), localStorage.getItem('NidPlanta'), localStorage.getItem('temporada'), this.zona, item.num_guia_recepcion)
    this.apiLogisticaService.obtenerListaEnvaseSalida(localStorage.getItem('NidExportadora'), this.frigorifico.toString(), localStorage.getItem('temporada'), this.zona, item.num_guia_recepcion)
      .subscribe(data => {
        // console.log('ENVASES SALIDA', data);
        this.arrEnvasesSalida = data.lista.items;
      }, (error) => {
        // this.snackBar.open(error.error.mensaje, null, { duration: 3000 });
        console.error('ERROR CARGANDO ENVASES SALIDA', error);
      });
    this.cargarListadoDetalle(item.num_guia_recepcion);
    this.reporte();
    setTimeout(() => {
      var printContents = document.getElementById('print-section-final').innerHTML;
      let w = window.open();
      w.document.write(printContents);
      w.document.close(); // necessary for IE >= 10
      w.focus(); // necessary for IE >= 10
      w.print();
      // w.close();
      return true;
    }, 200);
  }

  async cargarListadoDetalle(num_guia_recepcion: any) {
    // this.pesoBrutoIngresado = '';
    this.detalle = [];
    try {
      // const data = await this.apiLogisticaService.obtenerListaRecepcionlinea(localStorage.getItem('NidExportadora'), localStorage.getItem('NidPlanta'), localStorage.getItem('temporada'), this.zona, num_guia_recepcion).toPromise();
      const data = await this.apiLogisticaService.obtenerListaRecepcionlinea(localStorage.getItem('NidExportadora'), this.frigorifico, localStorage.getItem('temporada'), this.zona, num_guia_recepcion).toPromise();
      console.log('LISTADO DETALLE', data);
      this.dataSource = data.lista.items;
      let total_bines = 0;
      let total_peso = 0;
      data.lista.items.forEach(element => {
        // console.log('ELEMENT', element);
        total_bines += element.numEnvasesCont;
        total_peso += element.pesoEnvasesIn;
      });
      this.totalEnvases = total_bines;
      this.totalPesoEnvase = total_peso;
      this.detalle = data.lista.items;
      this.especieInforme = data.lista.items[0].especie.nombre;
    } catch (error) {
      console.error('ERROR LISTADO DETALLE', error);
    }
  }

  reporte() {
    let totalEmpresa = 0;
    let totalOtro = 0;
    let pesoEnvasesReporte = 0;
    let nombreEnvaseLinea = '';
    let nombreEnvaseSalida = '';
    if (this.arrEnvasesEntrada) {
      this.arrEnvasesEntrada.forEach(ln => {
        nombreEnvaseLinea = ln.envaseCosecha.nombre;
        totalEmpresa += ln.cantidadEmpresa;
        totalOtro += ln.cantidadOtros;
        pesoEnvasesReporte += ln.pesoEnvases;
      });
    } else {
      pesoEnvasesReporte = 0;
    }
    this.totalUniReporte = totalEmpresa;
    this.totalOtrosReporte = totalOtro;
    this.pesoEnvasesReporte = pesoEnvasesReporte;
    ///// Envases desalida
    let totalUniSalida = 0;
    let totalOtroSalida = 0;
    let pesoEnvasesReporteSalida = 0;
    if (this.arrEnvasesSalida.length) {
      this.arrEnvasesSalida.forEach(ln => {
        nombreEnvaseSalida = ln.nombre;
        totalUniSalida += ln.cantEnvasesUni;
        totalOtroSalida += ln.cantEnvasesOtros;
        pesoEnvasesReporteSalida += (ln.cantEnvasesUni + ln.cantEnvasesOtros) * parseFloat(ln.tara);
      });
    } else {
      pesoEnvasesReporteSalida = 0;
    }
    return {
      totalUniReporte: totalEmpresa,
      totalOtrosReporte: totalOtro,
      pesoEnvasesReporte: pesoEnvasesReporte,
      totalUniSalida: totalUniSalida,
      totalOtroSalida: totalOtroSalida,
      pesoEnvasesReporteSalida: pesoEnvasesReporteSalida,
      nombreEnvaseLinea: nombreEnvaseLinea,
      nombreEnvaseSalida: nombreEnvaseSalida
    }
  }

  selFrigorifico(data: any) {
    this.cargarZona();
    let data_ = {
      bit_pendientes: false,
      class_medio_ambiente: {
        CodTemp: this.temporada,
        CodPlanta: this.frigorifico,
        CodExportadora: localStorage.getItem('exportadora'),
        CodUsuario: this.usuario,
        CodZona: this.cod_zona
      },
      class_paginacion: { CantiReg: 50, Pagina: this.pagina }
    };
    console.log('REQUEST GUIA RECEPCION', data_);
    this.cargando = true;
    this.dataSource = new MatTableDataSource([]);
    this.wsCommon.listarGuiaRecepcion(data_)
      .subscribe((result) => {
        console.log('RESPONSE GUIA RECEPCION', result);
        this.dataSource = new MatTableDataSource(result.lista);
        this.cargando = false;
      }, (error) => {
        this.cargando = false;
        console.error('ERROR RECEPCIONES', error);
        this.snackBar.open(error.error.mensaje, null, { duration: 5000 });
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  applyFilter2(filterValue: string) {
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  onToggleChange2() {
    this.isActive = !this.isActive;
    console.log('ES ACTIVO', this.isActive);
    if (this.isActive) {
      this.mostrarTabla = false;
      this.pagina = 1;
      this.listarCerradas();
    }
    else {
      this.pagina = 1;
      this.mostrarTabla = true;
      this.listarPendientes();
    }
  }

  listarPendientes() {
    const data_ = {
      bit_pendientes: false,
      class_medio_ambiente: {
        CodTemp: this.temporada,
        CodPlanta: this.frigorifico,
        CodExportadora: localStorage.getItem('exportadora'),
        CodUsuario: this.usuario,
        CodZona: this.cod_zona
      },
      class_paginacion: { CantiReg: 50, Pagina: this.pagina }
    }
    this.wsCommon.listarGuiaRecepcion(data_)
      .subscribe((result) => {
        // console.log('RESPONSE LISTA GUIA RECEPCION', result);
        if (result.lista.length > 0) {
          if (result.lista.length < 50) {
            this.sinDatos = true;
          } else {
            this.sinDatos = false;
          }
          // console.log(result.lista.length);
          this.dataSource = new MatTableDataSource(result.lista);
          this.cargando = false;
        } else {
          this.sinDatos = true;
          this.snackBar.open(' ✖  No existen mas datos para visualizar', null, { duration: 800 })
        }
      }, error => {
        this.sinDatos = true
        this.snackBar.open(' ✖  No existen mas datos para visualizar', null, { duration: 800 });
        this.cargando = false;
      });
  }

  listarCerradas() {
    const data_ = {
      bit_pendientes: true,
      class_medio_ambiente: {
        CodTemp: this.temporada,
        CodPlanta: this.frigorifico,
        CodExportadora: localStorage.getItem('exportadora'),
        CodUsuario: this.usuario
      },
      class_paginacion: { CantiReg: 50, Pagina: this.pagina }
    };
    this.wsCommon.listarGuiaRecepcion(data_).subscribe(result => {
      console.log('RESPONSE GUIA RECEPCION', result);
      if (result.lista.length > 0) {
        if (result.lista.length < 50) {
          this.sinDatos = true;
        } else {
          this.sinDatos = false;
        }
        this.dataSource2 = new MatTableDataSource(result.lista);
        this.cargando = false;
      } else {
        this.sinDatos = true;
        this.snackBar.open(' ✖  No existen mas datos para visualizar', null, { duration: 800 })
      }
    }, error => {
      this.sinDatos = true;
      this.snackBar.open(' ✖  No existen mas datos para visualizar', null, { duration: 800 });
      this.cargando = false;
    });
  }

  agregar() {
    this.router.navigate(['/recfrutagranel']);
    // this.router.navigate(['/recepcionfrutagranel']);
  }

  agregarVariedad() {
    this.router.navigate(['/recfrutagranelvariedad']);
  }

  editarRecepcion(data: any) {
    console.log('EDITAR RECEPCION', data);
    // if (data.EsPorEspecie) {
    //   this.router.navigate(['recfrutagranel'], { queryParams: { id: data.num_guia_recepcion, frigorifico: this.frigorifico } });
    // } else {
    //   this.router.navigate(['recfrutagranelvariedad'], { queryParams: { id: data.num_guia_recepcion, frigorifico: this.frigorifico } });
    // }
    this.router.navigate(['recfrutagranel'], { queryParams: { id: data.num_guia_recepcion, frigorifico: this.frigorifico } });
    // this.router.navigate(['recepcionfrutagranel'], { queryParams: { id: data.num_guia_recepcion, frigorifico: this.frigorifico, zona: this.zona } });
  }

  envSalida() {
    this.router.navigate(['/env_salida']);
  }

  destare(item: any) {
    let extras: NavigationExtras = {
      queryParams: {
        temporada: localStorage.getItem('temporada'),
        planta: '  22',
        zona: this.zona,
        exportadora: localStorage.getItem('NidExportadora'),
        usuario: localStorage.getItem('User'),
        num_recepcion: item['num_guia_recepcion']
      }
    };
    this.router.navigate(['/destare'], extras);
  }

  trazabilidad() {
    this.router.navigate(['/rec_trazabilidad']);
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  siguiente() {
    if (this.sinDatos) {
      // console.log('SIN DATOS');
      return;
    }
    // console.log('ESTA ACTIVO', this.isActive);
    if (this.isActive) {
      this.mostrarTabla = false;
      this.pagina++;
      this.listarCerradas();
    } else {
      this.pagina++;
      this.mostrarTabla = true;
      this.listarPendientes();
    }
  }

  anterior() {
    this.pagina--;
    if (this.pagina == 0) {
      this.pagina = 1;
      return;
    }
    if (this.isActive) {
      this.mostrarTabla = false;
      this.listarCerradas();
    } else {
      this.mostrarTabla = true;
      this.listarPendientes();
    }
  }
}