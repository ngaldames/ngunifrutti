import { IngresoPesoBrutoComponent } from './../../../../../shared/dialogs/ingreso-peso-bruto/ingreso-peso-bruto.component';
import { ConfirmacionComponent } from './../../../../../shared/dialogs/confirmacion/confirmacion.component';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { ApilogisticaService } from '../../../../../services/apilogistica.service';
import { ApiCommonService } from '../../../../../services/apicommonservice.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import chileanRut from 'chilean-rut';

@Component({
  selector: 'app-recepcion-fruta-granel',
  templateUrl: './recepcion-fruta-granel.component.html',
  styleUrls: ['./recepcion-fruta-granel.component.css']
})

export class RecepcionFrutaGranelComponent implements OnInit {
  //REFERENCIAS
  @ViewChild('ingresopeso') refIngresoPeso: ElementRef;
  @ViewChild('rutchofer') refRutChofer: ElementRef;
  @ViewChild('pesoEnvaseEntrada') refPesoEnt: ElementRef;
  @ViewChild('pesoEnvaseSalida') refPesoSal: ElementRef;

  //DATA SOURCES
  columnasVisiblesDetalle: string[] = ['especie', 'variedad', 'sdp', 'pesobruto', 'cant_env', 'env', 'neto', 'promedio', 'linea', 'icons'];
  columnasVisiblesEntrada: string[] = ['1', '2', '3', '4', '5', '6', 'icons'];
  columnasVisiblesSalida: string[] = ['1', '2', '3', '4', '5', '6', '7', 'icons'];
  columnasVisiblesDestare: string[] = ['1', '2', '3', '4'];
  dataSourceDetalle = new MatTableDataSource([]);
  dataSourceEnvasesEntrada = new MatTableDataSource([]);
  dataSourceEnvasesSalida = new MatTableDataSource([]);
  dataSourceDestare = new MatTableDataSource([]);

  // VARIABLES CABECERA
  editaCabecera: boolean = false;
  choferEncontrado: boolean = false;
  checkCamionCarro: boolean = true;
  mostrarPeso: boolean = false;
  muestraTabs: boolean = false;
  validaIngresaLinea: boolean = false;

  //VARIABLES DETALLE
  cargaDetalle: boolean = false;

  ingresoPeso: number = null;
  balanza: any;
  permiteGuardar: boolean = false;

  //ARREGLOS GENERALES
  huertos: any = [];
  frigorificos: any = [];
  intencionesProductor: any = [];
  secciones: any = [];
  balanzas: any = [];
  envases: any = [];

  /////////////////////////////////////////////////////////
  temporada: string = localStorage.getItem('temporada');
  usuario = localStorage.getItem('User');
  exportadora: string = localStorage.getItem('exportadora');
  planta: string = localStorage.getItem('planta');
  nombrePlanta: string = localStorage.getItem('Nplanta');
  zona: number = 0;
  //////////////////////////////////////////////////////////
  nombreChofer: string = null;
  cabecera: any = {
    id: null,
    idGuiaProductor: null,
    fechaCosecha: null,
    fechaRecepcion: null,
    fechaDestare: null,
    patenteCamion: '',
    patenteCarro: '',
    chofer: { id: null },
    idPesa: null,
    zona: { id: null },
    temporada: { id: this.temporada },
    empresa: { id: null, nombre: null },
    huerto: { id: null, codCSG: null },
    frigorifico: { id: null },
    pesoBrutoCamion: 0,
    pesoBrutoCarro: 0,
    pesoBrutoTotal: 0,
    pesoEnvasesIn: 0,
    pesoEnvasesOut: 0,
    pesoTaraCamion: 0,
    pesoTaraCarro: 0,
    pesoTaraTotal: 0,
    pesoNeto: 0,
    glosa: '',
    codSeccion: null,
    terminada: false,
    usuario: { id: this.usuario },
    exportadora: { id: this.exportadora },
    codTransaccionProceso: 'A',
    zonaProductor: { id: null, codProductor: null }
  };

  //CALCULOS GENERALES
  calculosGenerales: any = [{
    PesoBrutoEntrada: 0,
    PesoBrutoSalida: 0,
    PesoEnvasesEntrada: 0,
    PesoEnvasesSalida: 0,
    PesoNetoEntrada: 0,
    PesoNetoSalida: 0,
    PesoPromedio: 0
  }];

  /////////////////////////
  detalleRepecion: any = [];

  ///////////////////
  //ENVASE ENTRADA //
  envasesEntrada: any = [];
  ingresoEnvaseEntrada: boolean = false;
  cargaEnvaseEntrada: boolean = false;
  envaseEntradaAsignar: any = {
    envase: {
      id: null,
      nombre: null,
      peso: 0,
      pesoMinimo: 0,
      pesoMaximo: 0,
      precioEmpresa: 0,
      precioOtros: 0,
      totalKilos: 0,
      cantidadEmpresa: 0,
      cantidadOtro: 0,
      totalPeso: 0
    }
  };
  envaseSeleccionEntrada: any = [];
  pesoEnvaseDetEntrada: any;
  cantUnidetEnvaseEntrada: any;
  cantOtroDetEnvaseEntrada: any;
  totalKilosEnvasesDetEntrada: any;

  ///////////////////
  // ENVASE SALIDA //
  glosas: any = [];
  encabezadoGuardadoEnvaseSalida: boolean = false;
  cargaEnvaseSalida: boolean = false;
  ingresoEnvaseSalida: boolean = false;
  habilitaInformeSII: boolean = false;
  envaseSalidaCabecera: any = {
    id: null,
    huerto: { id: null },
    glosa: { id: null }
  };
  envaseSalidaAsignar: any = {
    envase: {
      id: null,
      nombre: null,
      peso: 0,
      pesoMinimo: 0,
      pesoMaximo: 0,
      precioEmpresa: 0,
      precioOtros: 0,
      totalKilos: 0,
      cantidadEmpresa: 0,
      cantidadOtro: 0,
      totalPeso: 0
    }
  };
  /////////////
  // DESTARE //
  cargaDestare: boolean = true;

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router,
    private params: ActivatedRoute,
    private wsLogistica: ApilogisticaService,
    private wsCommon: ApiCommonService
  ) { }

  ngOnInit() {
    this.obtenerZona();
    this.leerParametrosCabecera();
  }

  tabs(indice: any) {
    if (indice === 0) {
      console.log('TAB DETALLE RECEPCION');
      this.cargarDetallesRecepcion(this.cabecera.id);
    }
    if (indice === 1) {
      console.log('TAB ENVASE ENTRADA');
      this.cargarListaEnvases();
      this.cargarEnvasesEntrada(this.cabecera.id);
    }
    if (indice === 2) {
      console.log('TAB ENVASE SALIDA');
      this.cargarListaEnvases();
      this.cargarGlosas();
      this.obtenerEncabezadoEnvaseSalida();
      this.cargarDetalleEnvaseSalida(this.cabecera.id);
    }
    if (indice === 3) {
      console.log('TAB DESTARE');
      this.cargarDestare(this.cabecera.id);
    }
  }

  leerParametrosCabecera() {
    this.params.queryParams
      .subscribe((data) => {
        console.log('REQUEST ', data);
        if (!data.id && !data.frigorifico && !data.zona) {
          this.editaCabecera = false;
          this.cabecera.fechaCosecha = new Date().toISOString();
          this.cabecera.fechaRecepcion = new Date().toISOString();
          this.cabecera.fechaDespacho = new Date().toISOString();
          this.cabecera.fechaDestare = new Date().toISOString();
          // this.cabecera.zona.id = this.zona;
        } else {
          this.editaCabecera = true;
          this.cabecera.id = data.id;
          this.cabecera.frigorifico.id = data.frigorifico;
          this.zona = data.zona;
          this.obtenerEncabezado();
        }
      });
  }

  cargarCalculosGenerales() {
    this.calculosGenerales = [{
      PesoBrutoEntrada: 0, PesoBrutoSalida: 0, PesoEnvasesEntrada: 0, PesoEnvasesSalida: 0,
      PesoNetoEntrada: 0, PesoNetoSalida: 0, PesoPromedio: 0
    }];
    if (this.cabecera.id) {
      let req = {
        CodExportadora: this.exportadora,
        CodPlanta: this.cabecera.frigorifico.id,
        CodTemp: this.temporada,
        CodZona: this.zona,
        NumRecep: this.cabecera.id
      };
      this.wsLogistica.obtenerCalculosGenerales(req)
        .subscribe((data) => {
          if (data.valido) {
            this.calculosGenerales = data.enc;
          }
          console.log('RESPONSE CALCULOS GENERALES', data);
        }, (error) => {
          this.calculosGenerales = [{
            PesoBrutoEntrada: 0, PesoBrutoSalida: 0, PesoEnvasesEntrada: 0, PesoEnvasesSalida: 0,
            PesoNetoEntrada: 0, PesoNetoSalida: 0, PesoPromedio: 0
          }];
          console.error('ERROR OBTENER CALCULOS GENERALES', error);
        });
    }
  }

  especieVariedadOpcion(idPesa: any) {
    if (idPesa) {
      this.cabecera.idPesa = idPesa.toString();
      if (this.cabecera.idPesa === 1) {
        this.checkCamionCarro = true;
      } else if (this.cabecera.idPesa === 2) {
        this.checkCamionCarro = false;
      }
    } else {
      this.cabecera.idPesa = null;
    }
  }

  //////////////
  // CABECERA //
  //////////////
  obtenerEncabezado() {
    if (this.cabecera.frigorifico.id && this.cabecera.id && this.zona) {
      this.wsLogistica.obtenerEncabezado(this.exportadora, this.cabecera.frigorifico.id, this.temporada, this.zona, this.cabecera.id)
        .subscribe((data) => {
          console.log('CABECERA RESPONSE', data);
          if (data.valido) {
            this.cabecera = data.enc;
            this.cabecera.idPesa = this.cabecera.idPesa.toString();
            this.nombreChofer = data.enc.chofer.nombre;
            this.cabecera.zonaProductor.id = this.cabecera.zonaProductor.id.trim() + '-' + this.cabecera.huerto.id.trim();
            this.obtenerIntencionesProductor(this.cabecera.empresa.id);
            this.seleccionaIntencion(this.cabecera.zonaProductor.id.trim() + '-' + this.cabecera.huerto.id.trim());
            this.cargarSecciones(this.cabecera.frigorifico.id);
            this.cargarBalanzas(this.cabecera.codSeccion);
            this.cargarDetallesRecepcion(this.cabecera.id);
            this.cargarEnvasesEntrada(this.cabecera.id);
            this.cargarEnvasesSalida(this.cabecera.id);
            this.cargarDestare(this.cabecera.id);
            if (data.enc.pesoTaraTotal > 0) {
              this.validaIngresaLinea = true;
            } else {
              this.validaIngresaLinea = false;
            }
            this.cargarCalculosGenerales();
            this.muestraTabs = true;
          } else {
            this.muestraTabs = false;
            this.editaCabecera = false;
            this.snackBar.open('No se pudo obtener cabecera', null, { duration: 3000 });
          }
        }, (error) => {
          this.muestraTabs = false;
          this.editaCabecera = false;
          console.log('ERROR OBTENER CABECERA', error);
        });
    }
  }

  guardarCabeceraRecepcion() {
    let rutChofer = null;
    let idPesa = null;
    let carga = this.snackBar.open('Procesando...', null, { duration: 0 });
    if (this.cabecera.chofer.id || this.cabecera.idPesa) {
      rutChofer = chileanRut.unformat(this.cabecera.chofer.id);
      idPesa = parseInt(this.cabecera.idPesa, 10);
      this.cabecera.chofer.id = rutChofer;
      this.cabecera.idPesa = idPesa;
    }
    if (!this.editaCabecera) {
      this.wsLogistica.guardarEncabezado(this.cabecera)
        .subscribe(async (response) => {
          console.log('RESPONSE CREA ENCABEZADO', await response);
          if (response.valido) {
            carga.dismiss();
            this.cabecera.id = response.guiaRespuesta;
            this.editaCabecera = true;
            this.muestraTabs = true;
            this.obtenerEncabezado();
            this.snackBar.open(' ✔  Cabecera creada correctamente', null, { duration: 2000 });
          } else {
            carga.dismiss();
            this.snackBar.open('Error al crear la cabecera', null, { duration: 4000 });
          }
        }, (error) => {
          console.log('ERROR AL CREAR CABECERA', error);
          carga.dismiss();
          this.muestraTabs = false;
          this.snackBar.open('Error al crear la cabecera', null, { duration: 4000 });
        });
    } else {
      console.log('EDITA CABECERA');
      this.wsLogistica.actualizarEncabezado(this.cabecera)
        .subscribe((response) => {
          console.log('RESPONSE EDITAR CABECERA', response);
          carga.dismiss();
          this.cabecera.id = response.guiaRespuesta;
          this.editaCabecera = true;
          this.obtenerEncabezado();
          this.snackBar.open(' ✔  Cabecera creada correctamente', null, { duration: 2000 });
        }, (error) => {
          console.log('ERROR AL EDITAR CABECERA', error);
          this.muestraTabs = false;
          this.snackBar.open('Error al editar la cabecera', null, { duration: 4000 });
        });
    }
  }

  obtenerProductor(rut: string) {
    this.wsCommon.cargarEmpresa(rut)
      .subscribe((data) => {
        console.log('PRODUCTOR', data);
        this.cabecera.empresa.nombre = data.nombre;
        this.obtenerIntencionesProductor(data.id);
      }, (error) => {
        console.error('ERROR PRODUCTOR', error);
      });
  }
  //INTENCIONES PRODUCTOR
  obtenerIntencionesProductor(rut: string) {
    if (rut) {
      this.wsLogistica.cargarZonaProductor(rut, this.temporada)
        .subscribe((data) => {
          console.log('OBTENER INTENCIONES PRODUCTOR', data)
          this.intencionesProductor = data.items;
        }, (error) => {
          console.error('ERROR CARGAR ZONA PRODUCTOR', error);
        });
    }
  }

  seleccionaIntencion(codigo: any) {
    console.log('SELECCION INTENCION', codigo);
    if (codigo) {
      let cod = codigo.split('-');
      let temporada = localStorage.getItem('temporada');
      let zona = cod[0];
      let empresa = cod[1];
      if (empresa && zona) {
        this.cabecera.zonaProductor.id = zona;
        this.cargarHuertos(empresa, temporada, zona);
      }
    }
  }
  // HUERTO
  cargarHuertos(empresa: any, temporada: any, zona: any) {
    this.huertos = [];
    if (empresa && temporada && zona) {
      this.wsCommon.cargarHuertos(empresa, temporada, zona)
        .subscribe((result) => {
          console.log('RESPONSE HUERTOS', result);
          this.huertos = result.items;
        }, (error) => {
          console.error('ERROR CARGANDO HUERTOS', error);
        });
    }
  }

  seleccionHuerto(huerto: any) {
    this.cabecera.huerto.id = huerto.id;
    this.cabecera.huerto.codCSG = huerto.codCSG;
  }
  // FRIGORIFICO
  obtenerFrigorifico() {
    this.frigorificos = [];
    if (this.exportadora && this.zona) {
      this.wsCommon.cargarFrigorifico(this.exportadora, this.zona)
        .subscribe((result) => {
          this.frigorificos = result.items;
        }, (error) => {
          console.error('ERROR AL RESCATAR FRIGORIFICO', error);
        });
    }
  }

  seleccionFrigorifico(frigo: any) {
    console.log('FRIGORIFICO SELECCION', frigo);
    if (frigo) {
      this.cargarSecciones(frigo);
    }
  }

  // ZONA
  obtenerZona() {
    this.zona = null;
    if (this.temporada, this.planta) {
      this.wsCommon.cargarZona(this.temporada, this.planta)
        .subscribe((data) => {
          console.log('ZONA', data);
          this.zona = data.items[0].id;
          this.cabecera.zona.id = data.items[0].id;
          this.obtenerFrigorifico();
        }, (error) => {
          console.error('ERROR AL RESCATAR ZONA', error);
        });
    }
  }
  // CHOFER
  validaRutEntrada(rut: any) {
    if (rut.length === 0 || rut === undefined) {
      this.choferEncontrado = false;
      this.cabecera.chofer.id = null;
      this.nombreChofer = null;
    }
  }

  obtenerChofer() {
    if (this.cabecera.chofer.id) {
      let dv = null;
      if (this.cabecera.chofer.id.length === 9) {
        dv = this.cabecera.chofer.id.substring(8);
        this.cabecera.chofer.id = chileanRut.format(this.cabecera.chofer.id, dv);
        this.validarRutChofer(this.cabecera.chofer.id);
      } else if (this.cabecera.chofer.id.length === 8) {
        dv = this.cabecera.chofer.id.substring(7);
        this.cabecera.chofer.id = chileanRut.format(this.cabecera.chofer.id, dv);
        this.validarRutChofer(this.cabecera.chofer.id);
      } else {
        this.snackBar.open('El largo del rut es inválido', null, { duration: 1000 });
        this.refRutChofer.nativeElement.focus();
        this.choferEncontrado = false;
        this.cabecera.chofer.id = null;
        this.nombreChofer = null;
        return;
      }
    }
    if (this.cabecera.chofer.id) {
      this.wsCommon.cargarChofer(chileanRut.unformat(this.cabecera.chofer.id))
        .subscribe((data) => {
          console.log('RESPONSE CARGA CHOFER', data);
          if (data.id && data.nombre) {
            this.choferEncontrado = false;
            let rut = data.id.trim();
            let dv = null;
            if (rut.length === 9) {
              dv = rut.substring(8);
            } else if (rut.length === 8) {
              dv = rut.substring(7);
            }
            this.cabecera.chofer.id = chileanRut.format(rut, dv);
            this.nombreChofer = data.nombre.trim();
          }
        }, (error) => {
          console.error('ERROR AL OBTENER CHOFER', error);
          this.nombreChofer = null;
          this.choferEncontrado = true;
        });
    }
  }

  agregarChofer() {
    let req = { id: chileanRut.unformat(this.cabecera.chofer.id), nombre: this.nombreChofer };
    this.wsCommon.agregarChofer(req)
      .subscribe((data) => {
        console.log('RESPONSE AGREGAR CHOFER', data);
        this.choferEncontrado = false;
      }, (err) => {
        console.error('ERROR AGREGAR CHOFER', err);
        this.choferEncontrado = true;
      });
  }

  validarRutChofer(rut: string) {
    if (!chileanRut.validate(rut)) {
      this.snackBar.open('Rut inválido', null, { duration: 1000 });
      this.refRutChofer.nativeElement.focus();
      this.cabecera.chofer.id = null;
      this.nombreChofer = null;
    }
  }

  patenteMayusculas(patente: any, tipo: number) {
    if (tipo === 1) {
      this.cabecera.patenteCamion = patente.toUpperCase()
    } else if (tipo === 2) {
      this.cabecera.patenteCarro = patente.toUpperCase()
    }
  }
  // BALANZAS
  cargarBalanzas(sec: string) {
    this.balanzas = [];
    if (sec) {
      this.wsCommon.cargarBalanzas(sec, this.zona, this.cabecera.frigorifico.id)
        .subscribe((respose) => {
          console.log('RESPONSE BALANZA', respose);
          this.balanzas = respose.items;
        }, (error) => {
          console.error('ERROR AL OBTENER BALANZAS', error);
        });
    }
  }

  seleccionBalanza(balanza: any) {
    console.log('BALANZA SELECCION', balanza);
  }
  // SECCION
  cargarSecciones(frigorifico: string) {
    this.secciones = [];
    console.log('METODO SECCION', this.temporada, this.zona, frigorifico);
    if (this.temporada && this.zona && frigorifico)
      this.wsCommon.cargarSeccion(this.temporada, this.zona, frigorifico)
        .subscribe((result) => {
          console.log(result);
          this.secciones = result.items;
        }, (error) => {
          console.log('ERROR AL CARGAR LAS SECCIONES', error);
        });
  }

  seleccionSeccion(seccion: any) {
    console.log('SECCION', seccion);
    if (seccion.value) {
      this.cargarBalanzas(seccion.value);
    }
  }

  validaPatente(patente: string) {
    console.log('VALIDA PATENTE', patente);
  }

  cambioModoPesaje(event: any) {
    this.ingresoPeso = null;
    setTimeout(() => {
      this.refIngresoPeso.nativeElement.focus();
    });
    if (event == 1) {
      this.checkCamionCarro = true;
    } else if (event == 2) {
      this.checkCamionCarro = false;
    }
  }

  cambioPeso(peso: any) {
    if (this.checkCamionCarro) {
      this.cabecera.pesoBrutoCamion = parseInt(peso.target.value, 10);
    } else {
      this.cabecera.pesoBrutoCarro = parseInt(peso.target.value, 10);
    }
    this.cabecera.pesoBrutoTotal = this.cabecera.pesoBrutoCamion + this.cabecera.pesoBrutoCarro;
    console.log('SUMA', this.cabecera.pesoBrutoTotal);
    if (this.cabecera.pesoBrutoTotal) {
      if (this.cabecera.pesoBrutoTotal > 0) {

      } else {

      }
    }
  }

  validadorCampos() {
    if (this.cabecera.frigorifico.id && this.cabecera.fechaDespacho && this.cabecera.fechaDespacho && this.cabecera.chofer.id
      && this.nombreChofer && this.cabecera.patenteCamion && this.cabecera.idPesa == 2 && this.balanza && this.cabecera.idGuiaProductor) {
      this.permiteGuardar = true;
      return true;
    }
    if (this.cabecera.frigorifico.id && this.cabecera.fechaDespacho && this.cabecera.fechaDespacho && this.cabecera.chofer.id
      && this.nombreChofer && this.cabecera.patenteCamion && this.cabecera.idPesa == 1 && this.balanza && this.cabecera.pesoBrutoTotal > 0 && this.cabecera.idGuiaProductor) {
      this.permiteGuardar = true;
      return true;
    }
  }

  /////////////
  // DETALLE //
  /////////////
  cargarDetallesRecepcion(idRecepcion: string) {
    this.detalleRepecion = [];
    this.dataSourceDetalle = new MatTableDataSource(this.detalleRepecion);
    if (idRecepcion) {
      this.cargaDetalle = true;
      this.wsLogistica.obtenerListaRecepcionlinea(this.exportadora, this.cabecera.frigorifico.id, this.temporada, this.zona, idRecepcion)
        .subscribe((data) => {
          console.log('REPSONSE DETALLES', data);
          if (data.valido) {
            this.detalleRepecion = data.lista.items;
            this.dataSourceDetalle = new MatTableDataSource(this.detalleRepecion);
          }
          this.cargaDetalle = false;
        }, (error) => {
          if (error.error.mensaje) {
            // this.snackBar.open(error.error.mensaje, null, { duration: 3000 });
          }
          this.cargaDetalle = false;
          console.error('ERROR AL OBTENER DETALLE', error);
        });
    }
  }

  crearReporte() {
    this.reporte();
  }

  reporte() {
    let totalUni = 0;
    let totalOtro = 0;
    let pesoEnvasesReporte = 0;
    let nombreEnvaseLinea = '';
    let nombreEnvaseSalida = '';
    this.dataSourceEnvasesEntrada.data.forEach(ln => {
      nombreEnvaseLinea = ln.envaseCosecha.nombre;
      totalUni += ln.cantidadEmpresa;
      totalOtro += ln.cantidadOtros;
      pesoEnvasesReporte += ln.pesoEnvase;
    });
    // Envases de salida
    let totalUniSalida = 0;
    let totalOtroSalida = 0;
    let pesoEnvasesReporteSalida = 0;
    this.dataSourceEnvasesSalida.data.forEach(ln => {
      nombreEnvaseSalida = ln.envaseCosecha.nombre;
      totalUniSalida += ln.cantidadEmpresa;
      totalOtroSalida += ln.cantidadOtros;
      pesoEnvasesReporteSalida += (ln.cantidadEmpresa + ln.cantidadOtros) * parseFloat(ln.pesoTara);
    });
    return {
      totalUniReporte: totalUni, totalOtrosReporte: totalOtro, pesoEnvasesReporte: pesoEnvasesReporte,
      totalUniSalida: totalUniSalida, totalOtroSalida: totalOtroSalida, pesoEnvasesReporteSalida: pesoEnvasesReporteSalida,
      nombreEnvaseLinea: nombreEnvaseLinea, nombreEnvaseSalida: nombreEnvaseSalida
    }
  }

  agregarDetalle() {
    console.log('VALIDA INGRESA LINEA', this.validaIngresaLinea);
    let dialogo = this.dialog.open(IngresoPesoBrutoComponent, {
      disableClose: true, width: '800px',
      data: { cabecera: this.cabecera, detalles: this.dataSourceDetalle.data, envasesSalida: this.dataSourceEnvasesSalida.data }
    });
    dialogo.afterClosed().subscribe(resultado => {
      this.cargarDetallesRecepcion(this.cabecera.id);
      console.log('RESPONSE', resultado);
    });
  }

  editarVariedadPesa(detalle: any) {

  }

  modificarPesoBruto(detalle: any) {

  }

  eliminarLineaDetalle(detalle: any) {
    console.log('ELIMINAR DETALLE', detalle);
    let request = {
      NumRecep: this.cabecera.id,
      CodZona: this.zona,
      CodFrigorifico: this.cabecera.frigorifico.id,
      NumLinea: detalle.numLinea,
      CodExportadora: this.exportadora,
      CodTemp: this.temporada
    };
    console.log('REQUEST ELIMINA', request);
    if (detalle.numLinea && this.cabecera.id) {
      this.wsLogistica.eliminarLinea(request)
        .subscribe((data) => {
          console.log('RESPONSE ELIMINA LINEA DETALLE', data);
          if (data.valido) {
            this.snackBar.open('Línea (' + detalle.numLinea + ') eliminada correctamente', null, { duration: 3000 });
          } else {
            this.snackBar.open('No se pudo eliminar la línea', null, { duration: 3000 });
          }
          this.cargarCalculosGenerales();
          this.cargarDetallesRecepcion(this.cabecera.id);
        }, (error) => {
          this.snackBar.open('No se pudo eliminar', null, { duration: 3000 });
          this.cargarCalculosGenerales();
          this.cargarDetallesRecepcion(this.cabecera.id);
          console.error('ERROR ELIMINA LINEA DETALLE', error);
        });
    }
  }

  totalEnvasesDetalle() {
    let suma = 0;
    this.detalleRepecion.forEach(ln => {
      suma += ln.numEnvasesCont;
    })
    return suma;
  }

  ////////////////////
  // ENVASE ENTRADA //
  ////////////////////
  cargarEnvasesEntrada(idRecepcion: string) {
    this.envasesEntrada = [];
    this.dataSourceEnvasesEntrada = new MatTableDataSource([]);
    if (idRecepcion && this.zona && this.temporada && this.cabecera.frigorifico.id) {
      this.cargaEnvaseEntrada = true;
      this.wsLogistica.obtenerListaEnvaseEntrada(this.exportadora, this.cabecera.frigorifico.id, this.temporada, this.zona, idRecepcion)
        .subscribe((data) => {
          console.log('RESPONSE ENVASES ENTRADA', data);
          if (data.valido) {
            this.envasesEntrada = data.lista.items;
            this.dataSourceEnvasesEntrada = new MatTableDataSource(data.lista.items);
          }
          this.cargaEnvaseEntrada = false;
        }, (error) => {
          this.cargaEnvaseEntrada = false;
          console.log('ERROR CARGANDO ENVASES ENTRADA', error);
        });
    }
  }

  cargarListaEnvases() { //CARGA EL LISTADO DE LOS ENVASES PARA AGREGAR ENTRADA/SALIDA
    this.envases = [];
    this.wsCommon.cargarEnvasesLista()
      .subscribe((data) => {
        console.log('RESPONSE ENVASES', data);
        this.envases = data.items;
      }, (error) => {
        console.log('ERROR CARGANDO ENVASES', error);
      });
  }

  seleccionEnvaseEntrada(envase: any) {
    if (envase) {
      this.wsCommon.cargarPesoZona(this.cabecera.zona.id, envase)
        .subscribe((response) => {
          try {
            this.envaseEntradaAsignar = {
              envase: {
                id: response.items[0]['envaseCosecha']['id'],
                peso: response.items[0]['pesoEstado'],
                pesoMinimo: response.items[0]['pesoMinimo'],
                pesoMaximo: response.items[0]['pesoMaximo'],
                precioEmpresa: 0,
                precioOtros: 0,
                totalKilos: 0,
                cantidadEmpresa: 0,
                cantidadOtro: 0,
                totalPeso: 0
              }
            };
          } catch (error) {
            this.envaseEntradaAsignar = { envase: { id: null, peso: 0, pesoMinimo: 0, pesoMaximo: 0, precioEmpresa: 0, precioOtros: 0, totalKilos: 0, cantidadEmpresa: 0, cantidadOtro: 0, totalPeso: 0 } };
            this.snackBar.open('Envase sin peso', null, { duration: 1000 });
          }
        }, (error) => {
          console.error('ERROR PESO ENVASES', error);
          this.envaseEntradaAsignar = { envase: { id: null, peso: 0, pesoMinimo: 0, pesoMaximo: 0, precioEmpresa: 0, precioOtros: 0, totalKilos: 0, cantidadEmpresa: 0, cantidadOtro: 0, totalPeso: 0 } };
          this.snackBar.open('No se pudieron obtener los pesos del envase', null, { duration: 1000 });
        });
    }
  }

  recalcularDetEnvEnt() {
    this.envaseEntradaAsignar.envase.totalPeso = (this.envaseEntradaAsignar.envase.cantidadEmpresa + this.envaseEntradaAsignar.envase.cantidadOtro) * this.envaseEntradaAsignar.envase.peso;
    if (this.envaseEntradaAsignar.envase.totalPeso > 0 && (this.envaseEntradaAsignar.envase.cantidadEmpresa + this.envaseEntradaAsignar.envase.cantidadOtro) > 0) {
      this.ingresoEnvaseEntrada = true;
    } else {
      this.ingresoEnvaseEntrada = false;
    }
  }

  agregarDetalleEnvaseEntrada() {
    console.log('ENVASE A ASIGNAR', this.envaseEntradaAsignar);
    if (this.cabecera.id && this.envaseEntradaAsignar.envase.id) {
      let request = {
        guia: {
          id: this.cabecera.id,
          zona: { id: this.cabecera.zona.id },
          temporada: { id: this.cabecera.temporada.id },
          frigorifico: { id: this.cabecera.frigorifico.id },
          exportadora: { id: this.cabecera.exportadora.id }
        },
        envaseCosecha: { id: this.envaseEntradaAsignar.envase.id },
        cantidad: parseFloat(this.envaseEntradaAsignar.envase.cantidadEmpresa + this.envaseEntradaAsignar.envase.cantidadOtro),
        cantidadEmpresa: parseFloat(this.envaseEntradaAsignar.envase.cantidadEmpresa.toString()),
        cantidadOtros: parseFloat(this.envaseEntradaAsignar.envase.cantidadOtro.toString()),
        esContenedor: false
      };
      this.wsLogistica.agregarEnvasesEntradaIndividual(request)
        .subscribe((response) => {
          console.log('RESPONSE AGREGAR ENVASE ENTRADA', response);
          if (response.valido) {
            this.snackBar.open('Envase ingresado correctamente', null, { duration: 2000 });
            this.cargarEnvasesEntrada(this.cabecera.id);
            this.cargarCalculosGenerales();
          }
        }, (error) => {
          console.error('ERROR AL AGREGAR ENVASE', error);
          this.snackBar.open('Envase ingresado correctamente', null, { duration: 2000 });
        });
    }
  }

  eliminarEnvasesEntrada(envase: any) {
    console.log('ENVASE ELIMINA', envase);
    const confirmacion = this.dialog.open(ConfirmacionComponent, {
      width: '550px',
      data: {
        titulo: 'Eliminar envase de entrada',
        subtitulo: '¿Está seguro que desea eliminar ' + envase.envaseCosecha.nombre + '?'
      }
    });
    confirmacion.afterClosed()
      .subscribe((data) => {
        if (data.confirmacion) {
          if (envase.id && envase.envaseCosecha.id) {
            this.wsLogistica.eliminarEnvaseEntrada(envase.id, envase.envaseCosecha.id, envase.exportadora.id, envase.zona.id, envase.temporada.id)
              .subscribe((response) => {
                console.log('RESPONSE ELIMINA ENVASE ENTRADA', response);
                if (response.valido) {
                  this.cargarEnvasesEntrada(this.cabecera.id);
                  this.cargarCalculosGenerales();
                }
              }, (error) => {
                this.snackBar.open('El registro no fue eliminado', null, { duration: 2000 });
                console.error('ERROR AL ELIMINAR ENVASE ENTRADA', error);
              });
          }
        }
      });
  }

  validarTaraEntrada(peso: any) {
    if (peso) {
      if (parseFloat(peso) <= this.envaseEntradaAsignar.envase.pesoMaximo && parseFloat(peso) >= this.envaseEntradaAsignar.envase.pesoMinimo) {
        this.envaseEntradaAsignar.envase.peso = peso;
        this.recalcularDetEnvEnt();
      } else {
        this.envaseEntradaAsignar.envase.peso = null;
        this.refPesoEnt.nativeElement.focus();
        this.recalcularDetEnvEnt();
        this.snackBar.open('El peso ingresado no está en el rango', null, { duration: 1000 });
      }
    }
  }

  ///////////////////
  // ENVASE SALIDA //
  ///////////////////
  cargarEnvasesSalida(idRecepcion: string) {

  }

  // CABECERA ENVASES SALIDA
  obtenerEncabezadoEnvaseSalida() {
    if (this.cabecera.id) {
      let request = {
        exportadora: this.cabecera.exportadora.id,
        planta: this.cabecera.frigorifico.id,
        temporada: this.cabecera.temporada.id,
        zona: this.cabecera.zona.id,
        num_recep: parseInt(this.cabecera.id)
      };
      console.log('REQUEST CABECERA ENVASES SALIDA', JSON.stringify(request));
      this.wsLogistica.obtenerCabeceraEnvaseSalida(request)
        .subscribe((response) => {
          console.log('RESPONSE CABECERA ENVASE SALIDA', response);
          if (response.items[0].huerto.id) {
            if (response.items[0].huerto.id) {
              if (response.items[0].glosa) {
                this.encabezadoGuardadoEnvaseSalida = true;
                this.envaseSalidaCabecera.huerto.id = response.items[0].huerto.id;
                this.envaseSalidaCabecera.id = response.items[0].id;
                this.envaseSalidaCabecera.glosa.id = response.items[0].glosa;
                // console.log('GLOSA CARGADA', this.envaseSalidaCabecera.glosa.id);
              } else {
                this.encabezadoGuardadoEnvaseSalida = false;
              }
            } else {
              this.encabezadoGuardadoEnvaseSalida = false;
            }
          } else {
            this.encabezadoGuardadoEnvaseSalida = false;
          }
        }, (error) => {
          this.encabezadoGuardadoEnvaseSalida = false;
          console.error('ERROR AL OBTENER CABECERA ENVASE SALIDA', error);
        });
    }
  }

  cargarDetalleEnvaseSalida(idRecepcion: string) {
    this.dataSourceEnvasesSalida = new MatTableDataSource([]);
    if (idRecepcion) {
      this.cargaEnvaseSalida = true;
      this.wsLogistica.obtenerListaEnvaseSalida(this.cabecera.exportadora.id, this.cabecera.frigorifico.id, this.cabecera.temporada.id, this.cabecera.zona.id, idRecepcion)
        .subscribe((response) => {
          console.log('RESPONSE ENVASE SALIDA', response);
          if (response.valido) {
            this.dataSourceEnvasesSalida = new MatTableDataSource(response.lista.items);
          }
          this.cargaEnvaseSalida = false;
        }, (error) => {
          this.cargaEnvaseSalida = false;
          console.log('ERROR AL CARGAR ENVASES SALIDA', error);
        });
    }
  }

  guardarEncabezadoEnvSalida() {
    if (this.envaseSalidaCabecera.id) {
      if (this.envaseSalidaCabecera.huerto.id) {
        let request = {
          id: this.envaseSalidaCabecera.id,
          guiaRecepcion: {
            id: this.cabecera.id,
            zona: { id: this.cabecera.zona.id },
            temporada: { id: this.cabecera.temporada.id },
            frigorifico: { id: this.cabecera.frigorifico.id },
            exportadora: { id: this.cabecera.exportadora.id }
          },
          huerto: { id: this.envaseSalidaCabecera.huerto.id, empresa: { id: this.cabecera.empresa.id } },
          fecha: this.cabecera.fechaCosecha,
          glosa: this.envaseSalidaCabecera.glosa.id,
          usuario: { id: this.cabecera.usuario.id },
          esNula: false
        };
        let cargando = this.snackBar.open('Procesando...', null, { duration: 0 });
        this.wsLogistica.agregarEnvaseSalida(request)
          .subscribe((response) => {
            console.log('RESPONSE GURDA ENVASE SALIDA', response);
            if (response.valido) {
              cargando.dismiss();
              this.snackBar.open(' ✔ Envase agregado correctamente', null, { duration: 2000 });
              this.obtenerEncabezadoEnvaseSalida();
            } else {
              cargando.dismiss();
              this.snackBar.open('Error al ingresar envase', null, { duration: 2000 });
            }
          }, (error) => {
            cargando.dismiss();
            this.snackBar.open('Error al ingresar envase', null, { duration: 2000 });
            console.error('ERROR GUARDA ENVASE', error);
          });
      } else {
        this.snackBar.open('Debe seleccionar huerto', null, { duration: 2000 });
      }
    } else {
      this.snackBar.open('Debe ingresar Nº guía', null, { duration: 2000 });
    }
  }

  selecionGlosa(id: any) {
    // console.log('GLOSA SELECCIONADA', id);
    if (id) {
      this.envaseSalidaCabecera.glosa.id = id;
    }
  }

  selecionEnvaseSalida(envase: any) {
    if (envase) {
      this.wsCommon.cargarPesoZona(this.cabecera.zona.id, envase)
        .subscribe((response) => {
          try {
            this.envaseSalidaAsignar = {
              envase: {
                id: response.items[0]['envaseCosecha']['id'],
                peso: response.items[0]['pesoEstado'],
                pesoMinimo: response.items[0]['pesoMinimo'],
                pesoMaximo: response.items[0]['pesoMaximo'],
                precioEmpresa: 0,
                precioOtros: 0,
                totalKilos: 0,
                cantidadEmpresa: 0,
                cantidadOtro: 0,
                totalPeso: 0
              }
            };
          } catch (error) {
            this.envaseSalidaAsignar = { envase: { id: null, peso: 0, pesoMinimo: 0, pesoMaximo: 0, precioEmpresa: 0, precioOtros: 0, totalKilos: 0, cantidadEmpresa: 0, cantidadOtro: 0, totalPeso: 0 } };
            this.snackBar.open('Envase sin peso', null, { duration: 1000 });
          }
        }, (error) => {
          console.error('ERROR PESO ENVASES', error);
          this.envaseSalidaAsignar = { envase: { id: null, peso: 0, pesoMinimo: 0, pesoMaximo: 0, precioEmpresa: 0, precioOtros: 0, totalKilos: 0, cantidadEmpresa: 0, cantidadOtro: 0, totalPeso: 0 } };
          this.snackBar.open('No se pudieron obtener los pesos del envase', null, { duration: 1000 });
        });
    }
  }

  validarTaraEnvaseSalida(peso: any) {
    if (peso) {
      if (parseFloat(peso) <= this.envaseSalidaAsignar.envase.pesoMaximo && parseFloat(peso) >= this.envaseSalidaAsignar.envase.pesoMinimo) {
        this.envaseSalidaAsignar.envase.peso = peso;
        this.recalcularDetEnvSal();
      } else {
        this.envaseSalidaAsignar.envase.peso = null;
        this.recalcularDetEnvSal();
        this.refPesoSal.nativeElement.focus();
        this.snackBar.open('El peso ingresado no está en el rango', null, { duration: 1000 });
      }
    }
  }

  agregarDetalleEnvaseSalida() {
    console.log('ENVASE SALIDA ASIGNAR', this.envaseSalidaAsignar);
    if (this.envaseSalidaAsignar.envase.totalPeso > 0) {
      let request = {
        envasesINOUT: { id: this.envaseSalidaCabecera.id },
        envaseCosecha: { id: this.envaseSalidaAsignar.envase.id },
        temporada: { id: this.cabecera.temporada.id },
        exportadora: { id: this.cabecera.exportadora.id },
        zona: { id: this.cabecera.zona.id },
        cantidadEmpresa: this.envaseSalidaAsignar.envase.cantidadEmpresa,
        cantidadOtros: this.envaseSalidaAsignar.envase.cantidadOtro,
        pesoTara: parseFloat(this.envaseSalidaAsignar.envase.peso.toString()),
        cantidadTotal: parseFloat(this.envaseSalidaAsignar.envase.totalPeso.toString())
      };
      this.wsLogistica.agregarEnvaseSalidaDetalle(request)
        .subscribe((response) => {
          if (response.valido) {
            this.snackBar.open('Envase agregado correctamente', null, { duration: 1000 });
            this.ingresoEnvaseSalida = false;
            this.envaseSalidaAsignar = {
              envase: {
                id: null, nombre: null, peso: 0, pesoMinimo: 0, pesoMaximo: 0,
                precioEmpresa: 0, precioOtros: 0, totalKilos: 0, cantidadEmpresa: 0, cantidadOtro: 0, totalPeso: 0
              }
            };
            console.log('RESPONSE AGREGAR DETALLE ENVASE SALIDA', response);
          }
          this.cargarDetalleEnvaseSalida(this.cabecera.id);
        }, (error) => {
          this.cargarDetalleEnvaseSalida(this.cabecera.id);
          this.snackBar.open('Error al ingresar envase de salida', null, { duration: 3000 });
          console.error('ERROR AL INGRESAR DETALLE ENV SALIDA', error);
        });
    } else {
      this.snackBar.open('Debe ingresar una cantidad de envases', null, { duration: 3000 });
      return;
    }
  }

  eliminarEnvasesSalida(envase: any) {
    console.log('ELIMINAR ENVASE SALIDA', envase);
    const confirmacion = this.dialog.open(ConfirmacionComponent, {
      width: '550px',
      data: {
        titulo: 'Eliminar envase de salida',
        subtitulo: '¿Está seguro que desea eliminar ' + envase.envaseCosecha.nombre + '?'
      }
    });
    confirmacion.afterClosed()
      .subscribe((data) => {
        if (data.confirmacion) {
          this.wsLogistica.eliminarEnvaseEntrada(this.envaseSalidaCabecera.id, envase.envaseCosecha.id, envase.exportadora.id, envase.zona.id, envase.temporada.id)
            .subscribe((response) => {
              console.log('RESPONSE AL ELIMINAR ENVASE SALIDA', response);
              if (response.valido) {
                this.snackBar.open('Envase eliminado correctamente', null, { duration: 1000 });
                this.cargarDetalleEnvaseSalida(this.cabecera.id);
              }
            }, (error) => {
              console.error('ERROR AL ELIMINAR ENVASE', error);
            });
        }
      });
  }

  recalcularDetEnvSal() {
    this.envaseSalidaAsignar.envase.totalPeso = (this.envaseSalidaAsignar.envase.cantidadEmpresa + this.envaseSalidaAsignar.envase.cantidadOtro) * this.envaseSalidaAsignar.envase.peso;
    if (this.envaseSalidaAsignar.envase.totalPeso > 0) {
      this.ingresoEnvaseSalida = true;
    } else {
      this.ingresoEnvaseSalida = false;
    }
  }

  cargarGlosas() {
    this.glosas = [];
    this.wsCommon.cargarGlosas('28')
      .subscribe((data) => {
        console.log('RESPONSE GLOSAS', data);
        this.glosas = data;
      }, (error) => {
        console.error('ERROR GLOSAS', error);
        this.glosas = [];
      });
  }

  seleccionaGlosa(glosa: any) {
    console.log('GLOSA SELECCION', glosa);
    if (glosa) {
      this.envaseSalidaCabecera.glosa.id = glosa;
    }
  }

  guiaDespacho(tipo: number) {

  }

  anularGuia() {
    let data = {
      id: this.envaseSalidaCabecera.id,
      guiaRecepcion: {
        id: this.cabecera.id,
        zona: { id: this.cabecera.zona.id },
        temporada: { id: this.cabecera.temporada.id },
        frigorifico: { id: this.cabecera.frigorifico.id },
        exportadora: { id: this.cabecera.exportadora.id }
      },
      huerto: { id: this.cabecera.huerto.id, empresa: { id: this.cabecera.empresa.id } },
      fecha: this.cabecera.fechaCosecha,
      glosa: this.glosas.glosa,
      usuario: { id: this.cabecera.usuario.id },
      esNula: 1
    };
    this.wsLogistica.anularGuia(data)
      .subscribe((result) => {
        console.log('RESPONSE ANULAR GUIA', result);
        if (result.valido) {
          this.dataSourceEnvasesSalida.data.forEach(element => {
            this.eliminarEnvasesSalida(element);
          });
        }
        this.encabezadoGuardadoEnvaseSalida = false;
      }, (error) => {
        this.snackBar.open('Error al anular guía', null, { duration: 2000 });
        console.error('ERROR ANULAR GUIA', error);
      });
  }

  /////////////
  // DESTARE //
  /////////////
  cargarDestare(idRecepcion: string) {
    this.dataSourceDestare = new MatTableDataSource([]);
    if (idRecepcion) {
      this.cargaDestare = true;
      this.wsLogistica.obtenerListaRecepcionlinea(this.exportadora, this.cabecera.frigorifico.id, this.temporada, this.zona, idRecepcion)
        .subscribe((data) => {
          console.log('LISTADO DESTARE', data);
          if (data.valido) {
            this.dataSourceDestare = new MatTableDataSource(data.lista.items);
          } else {
            this.dataSourceDestare = new MatTableDataSource([]);
          }
          this.cargaDestare = false;
        }, (error) => {
          this.cargaDestare = false;
          console.error('ERROR AL CARGAR LISTADO DESTARE', error);
          this.dataSourceDestare = new MatTableDataSource([]);
        });
    }
  }
}