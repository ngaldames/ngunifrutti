import { ApiCommonService } from './../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../services/apilogistica.service';
import { Subscription } from 'rxjs';
import { Abrir_lotesComponent } from './../../../../../shared/dialogs/abrir_lotes/abrir_lotes.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Imp_folio_binsComponent } from '../dialogs/imp_folio_bins/imp_folio_bins.component';

@Component({
  selector: 'app-cku-con-tabla',
  templateUrl: './cku-con-tabla.component.html',
  styleUrls: ['./cku-con-tabla.component.css']
})
export class CkuConTablaComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['especie', 'variedad', 'env_cosecha', 'bultos', 'peso_neto', 'promedio', 'folio_cku', 'tipo_ducha', 'tipo_frio',
    'cond_cku', 'pre_frio', 'icons'];
  dataSource = new MatTableDataSource([]);
  especie: string;
  variedad: string;
  env_cosecha: string;
  bultos: number;
  peso_neto: number;
  promedio: number;
  folio_cku: number;
  tipo_ducha: string;
  tipo_frio: string;
  cond_cku: string;
  pre_frio: string;
  icons: any;
  sub: Subscription;
  cku: any = [];
  asignarValida: boolean = false;
  ckuAsignar: any = {
    id: null,
    envaseCosecha: { id: null, nombre: null },
    bultos: null,
    peso_neto: null,
    promedio: null,
    zona: { id: null },
    temporada: { id: null },
    frigorifico: { id: null },
    nroLote: null,
    codCuartel: null,
    huerto: { id: null },
    empresa: { id: null },
    variedad: { idsnap: null, id: null, nombre: null },
    especie: { idsnap: null, id: null, nombre: null },
    condicion: { id: null, nombre: null },
    fechaCosecha: null,
    pesoBruto: null,
    pesoTara: null,
    pesoNeto: null,
    pesoEnvasesIn: null,
    pesoEnvasesOut: null,
    codEnvCosContenedor: null,
    numEnvasesCont: null,
    numLinea: null,
    exportadora: { id: null, nombre: null },
    cuentaCorriente: null,
    folioCKU: null,
    tipoDucha: { id: null },
    tipoFrio: { id: null },
    contenedor: { id: null },
    snapAptitud: null,
    codCondicion: null,
    preFrio: null
  };
  //Areglos generales
  especies: any = [];
  variedades: any = [];
  envasesCosecha: any = [];
  condsCKU: any = [];
  tiposFrios: any = [];
  aptitudes: any = [];
  tipoDuchas: any = [];
  cabecera: any = [];

  constructor(
    private logisticaService: ApilogisticaService,
    private apiCommonService: ApiCommonService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private route: ActivatedRoute
  ) {
    this.leerParametro();
    this.cargarDatos();
    this.cargarLineasCku();
    this.cargarCabecera();
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  leerParametro() {
    this.route.queryParams
      .subscribe(params => {
        this.cku = params;
        console.log('LLEGA', params);
      });
  }

  cargarCabecera() {
    let req = {
      cod_temp: this.cku.temporada,
      cod_planta: this.cku.planta,
      cod_exportadora: this.cku.exportadora,
      cod_zona: this.cku.zona,
      num_recepcion: this.cku.num_recepcion
    };
    this.logisticaService.listadoEncabezadoCKU(req)
      .subscribe((data) => {
        let DatosEnc = data.DatosEnc;
        let valido = data.valido;
        let mensaje = data.mensaje;
        console.log('CARGA CABECERA',data);
        if (valido) {
          this.cabecera = DatosEnc;
        } else {
          this.cabecera = [];
          this.snackBar.open('Sin cabecera asociada', null, { duration: 3000 });
        }
        console.log('ENCABEZADO CKU RESPONSE ', data);
      }, (error) => {
        this.snackBar.open('Error al cargar cabecera', null, { duration: 3000 });
      });
  }


  cargarDatos() {
    // Deberian ser de la API
    this.apiCommonService.cargarEspecies()
      .subscribe((data) => {
        let lista = data.items;
        this.especies = lista;
        // console.log('ESPECIES', data);
      }, (error) => {
        console.error('ERROR ESPECIES', error);
      });
    this.apiCommonService.cargarDuchas()
      .subscribe((data) => {
        this.tipoDuchas = data;
        // console.log('DUCHAS', data);
      }, (error) => {
        console.error('ERROR DUCHAS', error);
      });
  }

  obtenerCondicion(item: any) {
    // console.log('ITEM condicion', item);
    this.ckuAsignar.condicion.nombre = item.nombre;
  }

  cargarLineasCku() {
    const req = {
      cod_Temp: this.cku.temporada,
      cod_Planta: this.cku.planta,
      cod_Exportadora: this.cku.exportadora,
      cod_Zona: this.cku.zona,
      num_Recepcion: this.cku.num_recepcion
    };
    this.sub = this.logisticaService.listadoDetalleCku(req)
      .subscribe((data) => {
        console.log('DETALLE CKU', data);
        if (data.valido) {
          this.dataSource = data.lista.items;
        } else {
          this.snackBar.open('No hay detalles asociados', null, { duration: 3000 });
        }
      }, (error) => {
        console.error('ERROR DETALLE CKU', error);
        this.snackBar.open('Error al cargar detalle CKU', null, { duration: 3000 });
      });
  }

  asignarCKU(item: any) {
    this.ckuAsignar = [];
    // console.log('ITEM LINEA CKU', item);
    if (item) {
      this.asignarValida = true;
      item.pesoNeto = item.pesoBruto - item.pesoTara;
      console.log('PESO NETO ',item.pesoNeto);
      this.ckuAsignar = item;
      if (this.ckuAsignar.especie.id != undefined && this.ckuAsignar.exportadora.id != undefined) {
        this.apiCommonService.cargarTipoFrio(this.ckuAsignar.especie.id, this.ckuAsignar.exportadora.id)
          .subscribe((data) => {
            this.tiposFrios = data;
            // console.log('TIPOS DE FRIO', data);
          }, (error) => {
            console.error('ERROR TIPO FRIO', error);
            this.snackBar.open('Error al cargar tipos de frio', null, { duration: 3000 });
          });
        this.apiCommonService.cargarCondicionesCKU(this.ckuAsignar.especie.id, this.ckuAsignar.exportadora.id)
          .subscribe((data) => {
            // console.log('CONDICIONES', data)
            this.condsCKU = data;
          }, (error) => {
            console.error('ERROR CONDICIONES', error);
            this.snackBar.open('Error al cargar condiciones', null, { duration: 3000 });
          });
        this.apiCommonService.cargarTipoFrio(this.ckuAsignar.especie.id, this.ckuAsignar.exportadora.id)
          .subscribe((data) => {
            this.aptitudes = data;
          }, (error) => {
            console.error('ERROR TIPO FRIO', error);
            this.snackBar.open('Error al cargar aptitudes', null, { duration: 3000 });
          });
      } else {
        this.snackBar.open('Se requiere la especie y exportadora', null, { duration: 3000 });
        this.asignarValida = false;
      }
    } else {
      this.asignarValida = false;
      this.snackBar.open('No posee CKU', null, { duration: 2000 });
    }
  }

  actualizarLinea() {
    console.log('ACTUALIZA CKU ', this.ckuAsignar);
    let req = {
      id: this.ckuAsignar.id, // GUIA RECEPCION
      zona: { id: this.ckuAsignar.zona.id },
      temporada: { id: this.ckuAsignar.temporada.id },
      frigorifico: { id: this.ckuAsignar.frigorifico.id },
      // nroLote: this.ckuAsignar.nroLote,
      codCuartel: this.ckuAsignar.codCuartel,
      huerto: { id: this.ckuAsignar.huerto.id },
      empresa: { id: this.ckuAsignar.empresa.id },
      envaseCosecha: { id: this.ckuAsignar.envaseCosecha.id },
      // variedad: { idsnap: this.ckuAsignar.variedad.idsnap, id: this.ckuAsignar.variedad.idsnap },
      // especie: { idsnap: this.ckuAsignar.especie.idsnap, id: this.ckuAsignar.especie.idsnap },
      variedad: { idsnap: this.ckuAsignar.variedad.idsnap, nombre: this.ckuAsignar.variedad.nombre },
      especie: { idsnap: this.ckuAsignar.especie.idsnap, nombre: this.ckuAsignar.especie.nombre },
      fechaCosecha: this.ckuAsignar.fechaCosecha,
      condicion: { id: this.ckuAsignar.condicion.id, nombre: this.ckuAsignar.condicion.nombre },
      pesoBruto: this.ckuAsignar.pesoBruto,
      pesoTara: this.ckuAsignar.pesoTara,
      pesoEnvasesIn: this.ckuAsignar.pesoEnvasesIn,
      pesoEnvasesOut: this.ckuAsignar.pesoEnvasesOut,
      // codEnvCosContenedor: this.ckuAsignar.codEnvCosContenedor,
      numEnvasesCont: this.ckuAsignar.numEnvasesCont,
      numLinea: this.ckuAsignar.numLinea,
      exportadora: { id: this.ckuAsignar.exportadora.id },
      cuentaCorriente: this.ckuAsignar.cuentaCorriente,
      folioCKU: this.ckuAsignar.folioCKU,
      tipoDucha: { id: this.ckuAsignar.tipoDucha.id },
      tipoFrio: { id: this.ckuAsignar.tipoFrio.id },
      kilosNetosIn: this.ckuAsignar.kilosNetosIn,
      KilosNetosOut: this.ckuAsignar.KilosNetosOut,
      contenedor: { id: this.ckuAsignar.contenedor.id },
      snapAptitud: this.ckuAsignar.tipoFrio.id,
      // codCondicion: this.ckuAsignar.codCondicion,
      preFrio: this.ckuAsignar.preFrio,
    };
    this.logisticaService.guardarDestalleLineaCKU(req)
      .subscribe((data) => {
        console.log('RESPUESTA ACTUALIZA', data);
        let valido = data.valido;
        if (valido) {
          this.leerParametro();
          this.snackBar.open('Asignado correctamente', null, { duration: 2000 });
          this.cancelarLinea();
        } else {
          this.snackBar.open('No se pudo actualizar la línea', null, { duration: 2000 });
        }
      }, (error) => {
        console.error('ERROR GUARDA', error);
        this.snackBar.open('Error al actualizar', null, { duration: 2000 });
      });
  }

  cancelarLinea() {
    this.ckuAsignar = {
      id: null,
      envaseCosecha: { id: null, nombre: null },
      bultos: null,
      peso_neto: null,
      promedio: null,
      zona: { id: null },
      temporada: { id: null },
      frigorifico: { id: null },
      nroLote: null,
      codCuartel: null,
      huerto: { id: null },
      empresa: { id: null },
      variedad: { idsnap: null, id: null, nombre: null },
      especie: { idsnap: null, id: null, nombre: null },
      condicion: { id: null, nombre: null },
      fechaCosecha: null,
      pesoBruto: null,
      pesoTara: null,
      pesoEnvasesIn: null,
      pesoEnvasesOut: null,
      codEnvCosContenedor: null,
      numEnvasesCont: null,
      numLinea: null,
      exportadora: { id: null, nombre: null },
      cuentaCorriente: null,
      folioCKU: null,
      tipoDucha: { id: null },
      tipoFrio: { id: null },
      contenedor: { id: null },
      snapAptitud: null,
      codCondicion: null,
      preFrio: null
    };
    this.asignarValida = false;
    this.cargarLineasCku();
  }

  imprimirFolio(item: any) {
    console.log('ITEM IMPRESION', item);
    // console.log('ITEM IMPRESION NUMERO RECEPCION',  this.cku.num_recepcion.num_Guia_Recepcion);
    if(item.folioCKU > 0){
      const dialogRef = this.dialog.open(Imp_folio_binsComponent, {
        width: '500px',
        disableClose: true,
        data: { num_recepcion: this.cku.num_recepcion, num_guia_recepcion: this.cabecera.num_Guia_Recepcion , item }
      });
      dialogRef.afterClosed()
        .subscribe((data) => {
        }, (error) => {
          console.error('ERROR CERRANDO FOLIO', error);
        });
    }else{
      this.snackBar.open('Primero debe asignar folio CKU', null, { duration: 2000 });
    }
  }

  guardar() {
    this.snackBar.open(' ✔ \xa0\xa0 Datos guardados correctamente', null, { duration: 2000 });
    this.router.navigate(['/rec_cku']);
  }

  abrirLote(item: any) {
    const dialogRef = this.dialog.open(Abrir_lotesComponent, {
      width: '1100px',
      disableClose: true,
      data: item
    });
    dialogRef.afterClosed()
      .subscribe(data => {
        // console.log('RETORNO ', data);
      });
  }
}