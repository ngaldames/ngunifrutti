import { Subscription } from 'rxjs';
import { ApilogisticaService } from './../../../../../services/apilogistica.service';
import { ApiCommonService } from './../../../../../services/apicommonservice.service';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import chileanRut from 'chilean-rut';

@Component({
  selector: 'app-des-envvacios',
  templateUrl: './des-envvacios.component.html',
  styleUrls: ['./des-envvacios.component.css']
})

export class DesEnvvaciosComponent implements OnInit {
  displayedColumns2: string[] = ['1', '2', '3', '4', '5', '6'];
  @ViewChild('rutInput') rutReference: ElementRef;
  datasource = new MatTableDataSource([]);
  sub: Subscription;
  groupDefault = 0;
  radioGroup = [{ id: 1, nombre: 'Recepción', flag: true }, { id: 0, nombre: 'Despacho', flag: false }];
  mostrarFecha: boolean = true;
  mostrarGuardar: boolean = false;
  rut: string;
  productor: any = [];
  huertos: any = [];
  frigorificos: any = [];
  envases: any = [];
  cabeceraAsignar: any = {
    rut: null, tipo: 0, fecha: null, nro_guia: 0, observacion: null, codHuerto: null
  };
  huertoSeleccion: any = [];
  frigoSeleccion: any = [];
  envaseSeleccion: any = [];
  envaseAsignar: any = {
    envaseCosecha: { id: null }
  };
  asignarValida: boolean = false;

  constructor(
    private snackBar: MatSnackBar,
    private apiCommonService: ApiCommonService,
    private apiLogisticaService: ApilogisticaService
  ) { }

  ngOnInit() {
    this.cargarEnvases();
  }

  cargarEnvases() {
    this.apiCommonService.cargarEnvasesCosecha()
      .subscribe((data) => {
        // console.log('Listado envases', data);
        this.envases = data.items;
      }, (error) => {
        this.snackBar.open('Error al listar envases', null, { duration: 800 })
      });
  }

  guardarCabecera() {
    if (this.cabeceraAsignar.rut && this.cabeceraAsignar.fecha && this.cabeceraAsignar.codHuerto && this.cabeceraAsignar.nro_guia && this.cabeceraAsignar.observacion) {
      if (this.cabeceraAsignar.rut.length > 0) {
        if (this.cabeceraAsignar.codHuerto.length > 0) {
          if (this.cabeceraAsignar.nro_guia.length > 0) {
            if (this.cabeceraAsignar.observacion.length > 0) {
              let fecha = new Date(this.cabeceraAsignar.fecha).toISOString();
              let req = {
                id: Number(this.cabeceraAsignar.nro_guia),
                // id: 0,
                guiaRecepcion: {
                  // id: Number(this.cabeceraAsignar.nro_guia),
                  zona: { id: this.huertoSeleccion.zona.id },
                  temporada: { id: localStorage.getItem('temporada') },
                  // frigorifico: { id: null },
                  frigorifico: { id: this.frigoSeleccion.id },
                  exportadora: { id: localStorage.getItem('exportadora') }
                },
                huerto: {
                  id: this.huertoSeleccion.id,
                  empresa: { id: this.cabeceraAsignar.rut }
                },
                fecha: fecha,
                glosa: this.cabeceraAsignar.observacion,
                usuario: {
                  id: localStorage.getItem('User')
                },
                esNula: false,
                esInput: Boolean(this.cabeceraAsignar.tipo)
              };
              this.apiLogisticaService.guardarCabeceraEnvases(req)
                .subscribe((data) => {
                  let valido = data.valido;
                  if (valido) {
                    this.snackBar.open('Cabecera guardada correctamente', null, { duration: 2000 })
                    this.cargaEnvasesVacios();
                  } else {
                    this.snackBar.open('No se guardó cabecera', null, { duration: 2000 })
                  }
                  console.log('RESPONSE CABECERA', data);
                }, (error) => {
                  console.log('ERROR CABECERA', error);
                  this.snackBar.open('Error al guardar cabecera', null, { duration: 2000 })
                });
              console.log('Guardar cabecera', req);

            } else {
              this.snackBar.open('Ingrese observación', null, { duration: 2000 })
            }
          } else {
            this.snackBar.open('Ingrese un numero de guía', null, { duration: 2000 })
          }
        } else {
          this.snackBar.open('Seleccione un huerto', null, { duration: 2000 })
        }
      } else {
        this.snackBar.open('Ingrese un rut', null, { duration: 2000 })
      }
    } else {
      this.snackBar.open('Ingrese campos requeridos', null, { duration: 2000 })
    }
  }

  cargaEnvasesVacios() {
    this.datasource = new MatTableDataSource([]);
    if (this.frigoSeleccion.id && this.huertoSeleccion.zona.id && this.frigoSeleccion.id) {
      if (this.frigoSeleccion.id.length > 0 && this.huertoSeleccion.zona.id.length > 0 && this.frigoSeleccion.id.length > 0) {
        let req = {
          frigorifico: this.frigoSeleccion.id,
          zona: this.huertoSeleccion.zona.id,
          // num_recep: this.cabeceraAsignar.nro_guia,
          // num_recep: 0,
          num_cabecera: this.cabeceraAsignar.nro_guia,
          class_medio_ambiente: {
            cod_Temp: localStorage.getItem('temporada'),
            cod_Planta: this.frigoSeleccion.id,
            cod_Exportadora: localStorage.getItem('exportadora')
          }
        };
        this.sub = this.apiLogisticaService.listadoEnvasesVacios(req)
          .subscribe((data) => {
            console.log('LISTADO ENVASES', data);
            this.datasource = data.items;
            console.log('DATA SOURCE', this.datasource);
          }, (error) => {
            this.snackBar.open('Sin envases disponibles', null, { duration: 800 })
          });
      }
    }
  }

  editarEnvase(linea: any) {
    console.log('EDITAR ', linea);
    if (linea.envaseCosecha.id) {
      this.asignarValida = true;
      this.envaseAsignar = linea;
    } else {
      this.envaseAsignar = [];
      this.asignarValida = false;
    }
  }

  eliminarEnvase(linea: any) {

  }

  sumaCantidadesEnvases() {
    this.envaseAsignar.cantidadTotal = parseInt(this.envaseAsignar.cantidadOtros, 10) + parseInt(this.envaseAsignar.cantidadEmpresa, 10);
  }

  actualizarLinea() {
    console.log('Actualiza linea ', this.envaseAsignar);
    let req = {
      envasesINOUT: { id: Number(this.cabeceraAsignar.nro_guia) },
      envaseCosecha: { id: this.envaseSeleccion.id },
      temporada: { id: localStorage.getItem('temporada') },
      exportadora: { id: localStorage.getItem('exportadora') },
      zona: { id: this.huertoSeleccion.zona.id },
      cantidadEmpresa: this.envaseAsignar.cantidadEmpresa,
      cantidadOtros: this.envaseAsignar.cantidadOtros,
      cantidadTotal: this.envaseAsignar.cantidadTotal
    };

    if (this.asignarValida) {
      this.apiLogisticaService.editaLineaEnvase(req)
        .subscribe((data) => {
          console.log('RESPONSE EDITA ENVASES', data);
          let valido = data.valido;
          if (valido) {
            this.snackBar.open('Editado correctamente', null, { duration: 800 });
            this.cargaEnvasesVacios();
            this.cancelarLinea();
          } else {
            this.snackBar.open('Error al guardar envase', null, { duration: 800 });
          }
        }, (error) => {
          console.log('ERROR GUARDAR ENVASES', error);
        });
    } else {
      this.apiLogisticaService.guardarLineaEnvase(req)
        .subscribe((data) => {
          console.log('RESPONSE GUARDA ENVASES', data);
          let valido = data.valido;
          if (valido) {
            this.snackBar.open('Ingresado correctamente', null, { duration: 800 });
            this.cargaEnvasesVacios();
            this.cancelarLinea();
          } else {
            this.snackBar.open('Error al guardar envase', null, { duration: 800 });
          }
        }, (error) => {
          console.log('ERROR GUARDAR ENVASES', error);
        });
      // this.snackBar.open('Agrega envase', null, { duration: 800 });
    }
  }

  cancelarLinea() {
    this.envaseAsignar = {
      envaseCosecha: { id: '' }
    };
    this.asignarValida = false;
  }

  obtenerEnvase(envase: any) {
    console.log('SELECCION ENVASE', envase);
    if (envase.id) {
      this.envaseSeleccion = envase;
    } else {
      this.envaseSeleccion = [];
    }
  }

  obtenerHuerto(huerto: any) {
    // console.log('HUERTO ITEM ', huerto);
    if (huerto.id.length > 0) {
      this.huertoSeleccion = huerto;
      this.cabeceraAsignar.codHuerto = huerto.id;
      // this.cargaEnvasesVacios();
    } else {
      this.huertoSeleccion = [];
      this.cabeceraAsignar.codHuerto = '';
    }
    this.apiCommonService.cargarFrigorifico(localStorage.getItem('exportadora'), this.huertoSeleccion.zona.id)
      .subscribe((data) => {
        console.log('OBTEBER FRIGORIFICO', data);
        this.frigorificos = data.items;
      }, (error) => {
        console.log('Error al cargar frigorificos');
      });
  }

  obtenerFrigorifico(frig: any) {
    // console.log('FRIGORIFICO ITEM', frig);
    if (frig.id.length > 0) {
      this.frigoSeleccion = frig;
      this.cargaEnvasesVacios();
    } else {
      this.frigoSeleccion = [];
    }
  }

  formatoRut() {
    if (this.rut) {
      if (this.rut.length === 9) {
        var dv = this.rut.substring(8);
        this.rut = chileanRut.format(this.rut, dv);
        this.validarRut(this.rut);
      } else if (this.rut.length === 8) {
        var dv = this.rut.substring(7);
        this.rut = chileanRut.format(this.rut, dv);
        this.validarRut(this.rut);
      } else {
        this.snackBar.open('El largo del rut no corresponde', null, { duration: 800 })
        this.rut = "";
        this.rutReference.nativeElement.focus();
      }
    }
  }

  validarRut(rut: string) {
    // if (!chileanRut.validate(rut)) {
    //   this.snackBar.open('Rut inválido', null, { duration: 800 })
    //   this.rut = "";
    //   this.rutReference.nativeElement.focus();
    // }else{
    //   this.buscaProductor(rut);
    // }
    this.buscaProductor(rut);
  }

  buscaProductor(rut: string) {
    let rutSinFormato = chileanRut.unformat(rut);
    this.apiCommonService.obtenerProductor(rutSinFormato)
      .subscribe((data) => {
        console.log('PRODUCTOR ', data);
        if (data.id.length > 0) {
          this.productor = data;
          this.cabeceraAsignar.rut = rutSinFormato;
          this.buscarHuertos(rutSinFormato);
        } else {
          this.productor = [];
          this.snackBar.open('Productor no encontrado', null, { duration: 2000 })
        }
      }, (error) => {
        this.snackBar.open('Error al cargar productor', null, { duration: 2000 })
      });
  }

  buscarHuertos(rut_empresa: string) {
    this.apiCommonService.listadoHuerto(rut_empresa)
      .subscribe((data) => {
        console.log('LISTA HUERTO ', data);
        if (data.items.length > 0) {
          this.huertos = data.items;
        } else {
          this.snackBar.open('Productor sin huertos asociados', null, { duration: 2000 })
        }
      }, (error) => {
        this.snackBar.open('Error al cargar huertos', null, { duration: 2000 })
      });
  }

  recepcionDespachoOpciones(event: any) {
    this.cabeceraAsignar.tipo = event.value;
    if (event.value == 0) {
      this.mostrarFecha = true;
    } else if (event.value == 1) {
      this.mostrarFecha = false;
    }
  }
}