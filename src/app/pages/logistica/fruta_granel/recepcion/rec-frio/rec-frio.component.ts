import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';

const ELEMENT_DATA = [
  {
    planta: 'Romeral', lote: 'H423', fecha: '11/01/2019', nombreproductor: 'Juan Perez', cod_productor: '34242342',
    variedad: 'bins', bandejas: '32', totalkilos: '432', hrllegada: '13:25', icons: ''
  },
  {
    planta: 'Romeral', lote: 'H423', fecha: '11/01/2019', nombreproductor: 'Andres Vargas', cod_productor: '34242342',
    variedad: 'bins', bandejas: '32', totalkilos: '432', hrllegada: '13:25', icons: ''
  },
  {
    planta: 'Romeral', lote: 'H423', fecha: '11/01/2019', nombreproductor: 'Fernando P.', cod_productor: '34242342',
    variedad: 'bins', bandejas: '32', totalkilos: '432', hrllegada: '13:25', icons: ''
  }
];

const ELEMENT_DATA2 = [
  {
    planta: 'Romeral', lote: 'H423', fecha: '11/01/2019', nombreproductor: 'Oscar Ramírez', cod_productor: '34242342',
    variedad: 'bins', bandejas: '32', totalkilos: '152', hrllegada: '13:25', icons: ''
  }
];

@Component({
  selector: 'app-rec-frio',
  templateUrl: './rec-frio.component.html',
  styleUrls: ['./rec-frio.component.css']
})
export class RecFrioComponent implements OnInit {
  planta: string;
  lote: string;
  fecha: any;
  nombreproductor: string;
  cod_productor: any;
  variedad: string;
  bandejas: any;
  totalkilos: any;
  hrllegada: any;
  icons: any;
  isActive: boolean;
  esFavorito: boolean;
  displayedColumns: string[] = ['planta', 'lote', 'fecha', 'nombreproductor', 'cod_productor', 'variedad',
    'bandejas', 'totalkilos', 'hrllegada', 'icons'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource2 = new MatTableDataSource(ELEMENT_DATA2);
  mostrarTabla: boolean = true;

  constructor(private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  applyFilter2(filterValue: string) {
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  onToggleChange(event) {
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.mostrarTabla = false;
    }
    else {
      this.mostrarTabla = true;
    }
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }
}
