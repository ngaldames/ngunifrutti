import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-env_salida',
  templateUrl: './env_salida.component.html',
  styleUrls: ['./env_salida.component.css']
})
export class Env_salidaComponent implements OnInit {
  displayedColumns2: string[] = ['1', '2', '3', '4', '5', '6', '7'];
  dataSource: any = [];
  constructor(public snackBar: MatSnackBar, private router: Router) { }

  ngOnInit() {
  }

  guardar() {
    this.snackBar.open(' ✔ \xa0\xa0 Datos guardados correctamente', null, { duration: 2000 });
    this.router.navigate(['/pantallacku'])
  }


}
