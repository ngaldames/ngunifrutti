import { Despacho_trazabilidadComponent } from './../../../../../shared/dialogs/despacho_trazabilidad/despacho_trazabilidad.component';
import { Ot_relacionada_trazabilidadComponent } from './../../../../../shared/dialogs/ot_relacionada_trazabilidad/ot_relacionada_trazabilidad.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { DefFolioComponent } from '../../../../../shared/dialogs/def-folio/def-folio.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

const ELEMENT_DATA = [
  {
    foliobins: '4234234', kilos: '545', estado: 'Procesado', icons: ''
  },
  {
    foliobins: '5676887', kilos: '352', estado: 'Procesado', icons: ''
  }
];

const ELEMENT_DATA2 = [
  {
    temporada: '2019-2020', guia: '54345', lote: 'H423', fecha: '11/01/2019', cod_productor: '54gfg', huerto: '4334',
    guia_productor: '5435', cod_especie: '32', especie: 'bins', patente: 'VF-78-39'
  }
];

const ELEMENT_DATA3 = [
  {
    id: '1', fecha: '11/01/2019', cod: '1', tipo_proceso:'normal', nro: '2', tipo_frio:'01', variedad: '45'
  },
  {
    id: '2', fecha: '11/01/2019', cod: '1', tipo_proceso: 'normal', nro: '1', tipo_frio: '01', variedad: 'hw'
  },
  {
    id: '2', fecha: '11/01/2019', cod: '1', tipo_proceso: 'normal', nro: '1', tipo_frio: '01', variedad: 'hw'
  }
];

const ELEMENT_DATA4 = [
  {
    nro_despacho: '145', fecha: '11/01/2019', tipo_venta: 'Nacional', mercado: 'interno', etiqueta: 'Unifrutti', caja: '10', kg: '44'
  },
  {
    nro_despacho: '65', fecha: '11/01/2019', tipo_venta: 'Nacional', mercado: 'interno', etiqueta: 'Unifrutti', caja: '11', kg: '48'
  },
  {
    nro_despacho: '112', fecha: '11/01/2019', tipo_venta: 'Nacional', mercado: 'interno', etiqueta: 'Unifrutti', caja: '12', kg: '120'
  }
];

@Component({
  selector: 'app-rec-trazabilidad',
  templateUrl: './rec-trazabilidad.component.html',
  styleUrls: ['./rec-trazabilidad.component.css']
})

export class RecTrazabilidadComponent implements OnInit {

  temporada: string;
  guia: string;
  lote: string;
  fecha: any;
  zona: string;
  cod_productor: any;
  huerto: string;
  guia_productor: any;
  cod_especie: any;
  especie: any;
  patente: any;

  id: any;
  cod: any;
  tipo: string;
  nro: number;
  tipo_frio: any;
  tipo_proceso: any;
  variedad: any;

  foliobins: any;
  kilos: any;
  estado: any;
  icons: any;

  nro_despacho: any;
  tipo_venta: string;
  mercado: string;
  etiqueta: any;
  caja: any;
  kg: any;

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  displayedColumns: string[] = ['foliobins', 'kilos', 'estado', 'icons'];
  displayedColumns2: string[] = ['temporada', 'guia', 'lote', 'fecha', 'zona', 'cod_productor', 'huerto',
    'guia_productor', 'cod_especie', 'especie', 'patente'];
  displayedColumns3: string[] = ['id', 'fecha', 'cod', 'tipo_proceso', 'nro', 'tipo_frio', 'variedad'];
  displayedColumns4: string[] = ['nro_despacho', 'fecha', 'tipo_venta', 'mercado', 'etiqueta', 'caja', 'kg'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource2 = new MatTableDataSource(ELEMENT_DATA2);
  dataSource3 = new MatTableDataSource(ELEMENT_DATA3);
  dataSource4 = new MatTableDataSource(ELEMENT_DATA4);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Items por página';
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  applyFilter2(filterValue: string) {
    this.dataSource3.filter = filterValue.trim().toLowerCase();
  }

  applyFilter3(filterValue: string) {
    this.dataSource4.filter = filterValue.trim().toLowerCase();
  }

  definicion(): void {
    const dialogRef = this.dialog.open(DefFolioComponent, {
      width: '450px',
    });
  }

  ot_relacionada(): void {
    const dialogRef = this.dialog.open(Ot_relacionada_trazabilidadComponent, {
      width: '600px',
    });
  }

  despacho(): void {
    const dialogRef = this.dialog.open(Despacho_trazabilidadComponent, {
      width: '600px',
    });
  }
}
