import { ApilogisticaService } from './../../../../../services/apilogistica.service';
import { ApiCommonService } from './../../../../../services/apicommonservice.service';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Pipe({ name: 'miles' })
// export class MyPipe implements PipeTransform {
//   transform(val: any) {
//     return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
//   }
// }
@Component({
  selector: 'app-destare',
  templateUrl: './destare.component.html',
  styleUrls: ['./destare.component.css']
})
export class DestareComponent implements OnInit {
  displayedColumns3: string[] = ['1', '2', '3', '4', 'icons'];
  dataSource: any = [];
  cku: any = [];
  especies: any = [];
  variedad: any = [];
  variedades: any = [];
  lineaAsignar: any = [];
  especieSeleccion: string;
  variedadSeleccion: string;
  pesoBrutoIngresado: any;
  bit_cabecera_guarda2: boolean;
  cabecera: any = {
    id: null,
    fechaCosecha: null,
    empresa: { id: null, nombre: null },
    huerto: { id: null, nombre: null },
    pesoBrutoCamion: null,
    pesoBrutoTotal: null,
    chofer: { id: null, nombre: null },
    frigorifico: { id: null, nombre: null },
    glosa: null,
    pesoEnvasesIn: null,
    pesoEnvasesOut: null,
  };

  detalle: any = [{}];
  tarja: any = [];
  especieInforme: string;
  lineaDestare: any = {
    especie: { id: null },
    variedad: { id: null },
  };

  arrEnvasesEntrada: any = [];
  arrEnvasesSalida = [];
  //REPORTE
  totalUniReporte = 0
  totalOtrosReporte = 0
  pesoEnvasesReporte = 0
  guiaProductor = '';
  //DESTARE
  pesoDestarePorEspecie = 0;
  totalEnvases: any;
  totalPesoEnvase: any;
  bit_modo_pesaje: boolean;
  observaciones = '';
  especie: string;
  nombrePlanta: string = localStorage.getItem('Nplanta');
  public now: Date = new Date();
  horaActual = this.now = new Date();

  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private apiCommonService: ApiCommonService,
    private apiLogisticaService: ApilogisticaService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.leerParametro();
    this.cargarEspecies();
    this.obtenerCabecera();
    this.cargarListadoDetalle();
    this.obtenerEnvases();
  }

  leerParametro() {
    this.route.queryParams.subscribe(params => { this.cku = params; });
  }

  obtenerEnvases() {
    this.arrEnvasesEntrada = [];
    this.apiLogisticaService.obtenerListaEnvaseEntrada(this.cku.exportadora, this.cku.planta, this.cku.temporada, this.cku.zona, this.cku.num_recepcion)
      .subscribe(data => {
        // console.log('ENVASES ENTRADA', data)
        this.arrEnvasesEntrada = data.lista.items;
      });
    this.arrEnvasesSalida = []
    this.apiLogisticaService.obtenerListaEnvaseSalida(this.cku.exportadora, this.cku.planta, this.cku.temporada, this.cku.zona, this.cku.num_recepcion)
      .subscribe(data => {
        // console.log('ENVASES SALIDA', data);
        this.arrEnvasesSalida = data.lista.items;
      }, (error) => {
        // this.snackBar.open(error.error.mensaje, null, { duration: 3000 });
        console.error('ERROR CARGANDO ENVASES SALIDA', error);
      });
    this.reporte();
  }

  obtenerCabecera() {
    this.apiLogisticaService.obtenerEncabezado(this.cku.exportadora, this.cku.planta, this.cku.temporada, this.cku.zona, this.cku.num_recepcion)
      .subscribe((data) => {
        this.cabecera = data.enc;
        if (data.enc.idPesa == 2) {
          this.bit_modo_pesaje = true;
        } else {
          this.bit_modo_pesaje = false;
        }
        console.log('CEBECERA EN DESTARE', this.cabecera);
      }, (error) => {
        console.error('ERROR CARGANDO CABECERA', error);
      });
  }

  cargarEspecies() {
    this.apiCommonService.cargarEspecies()
      .subscribe((data) => {
        this.especies = data.items;
      }, (error) => {
        console.error('ERROR CARGANDO ESPECIES', error);
      });
  }    
  
  
  agregarLineas() {
    let promesas = new Promise((resolve, reject)=>{
      this.destarePorEspecie();
      if (this.bit_cabecera_guarda2 === true) {
        resolve();
      }
    });

    promesas.then(
      () => console.log('termino')
    ).catch( error => console.log('Error en la promesa', error));

  }
  agregarLinea() {
    if (this.bit_modo_pesaje) {
      if (this.especieSeleccion != undefined) {
        if (this.variedadSeleccion != undefined) {
          this.destarePorVariedad();
        } else {
          this.snackBar.open('Debe seleccionar variedad', null, { duration: 3000 });
        }
      } else {
        this.snackBar.open('Debe seleccionar especie', null, { duration: 3000 });
      }
    } else {
      this.destarePorEspecie();
    }
  }

  destarePorVariedad() {
    if (this.pesoBrutoIngresado > 0 && this.lineaDestare.variedad.id && this.lineaDestare.especie.id) {
      let resto = 0;
      let total_tara_peso = 0;
      let total_pesos_bruto = 0;
      //OBTENER PESOS TOTALES DE LAS LINEAS
      this.detalle.forEach(element => {
        total_tara_peso += element.pesoTara;
        total_pesos_bruto += element.pesoBruto;
      });
      // OBTIENE TOTALES PARA ACTUALIZAR LA CABECERA
      this.cabecera.pesoNeto = total_pesos_bruto - total_tara_peso - (this.cabecera.pesoEnvasesIn - this.cabecera.pesoEnvasesOut);
      resto = this.cabecera.pesoNeto / this.totalEnvases;
      this.cabecera.pesoTaraTotal = this.pesoBrutoIngresado;
      this.cabecera.pesoTaraCamion = this.pesoBrutoIngresado;
      // ACTUALIZA LA CABECERA CON LOS PESOS
      this.apiLogisticaService.actualizarEncabezado(this.cabecera)
        .subscribe(async (dataEnc) => {
          console.log('RESPONSE GUARDA CABECERA', dataEnc);
          if (dataEnc.valido) {
            console.log('CABECERA ACTUALIZADA CORRECTAMENTE');
            let taraCalculo = 0;
            // taraCalculo = this.lineaDestare.pesoTara; // AQUI EL CALCULO
            taraCalculo = this.pesoBrutoIngresado * this.lineaDestare.numEnvasesCont / this.totalEnvases; // AQUI EL CALCULO
            let dataDetalle = {
              id: this.lineaDestare.id,
              zona: { id: this.lineaDestare.zona.id },
              temporada: { id: this.lineaDestare.temporada.id },
              frigorifico: { id: this.lineaDestare.frigorifico.id },
              codCuartel: this.lineaDestare.codCuartel,
              huerto: { id: this.lineaDestare.huerto.id },
              empresa: { id: this.lineaDestare.empresa.id },
              variedad: { idsnap: this.lineaDestare.variedad.idsnap },
              especie: { idsnap: this.lineaDestare.especie.idsnap },
              fechaCosecha: this.lineaDestare.fechaCosecha,
              pesoBruto: this.lineaDestare.pesoBruto,
              // pesoTara: parseFloat(this.lineaDestare.pesoTara.toFixed(2)), //ASI ESTABA
              pesoTara: Math.round(taraCalculo), //NUEVO
              pesoEnvasesIn: this.lineaDestare.pesoEnvasesIn,
              pesoEnvasesOut: this.lineaDestare.pesoEnvasesOut,
              envaseCosecha: { id: this.lineaDestare.envaseCosecha.id },
              numEnvasesCont: this.lineaDestare.numEnvasesCont,
              numLinea: this.lineaDestare.numLinea,
              exportadora: { id: this.lineaDestare.exportadora.id },
              cuentaCorriente: false,
              folioCKU: this.lineaDestare.folioCKU,
              tipoDucha: { id: this.lineaDestare.tipoDucha.id },
              tipoFrio: { id: this.lineaDestare.tipoFrio.id },
              preFrio: false,
              kilosNetosOut: this.lineaDestare.pesoBruto - this.pesoBrutoIngresado - (this.lineaDestare.pesoEnvasesIn + this.lineaDestare.pesoEnvasesOut),
              // KilosNetosIn: resto * this.lineaDestare.numEnvasesCont,
              kilosNetosIn: this.lineaDestare.KilosNetosIn,
              snapAptitud: this.lineaDestare.snapAptitud,
              contenedor: { id: this.lineaDestare.contenedor.id },
              condicion: { id: this.lineaDestare.condicion.id },
              aptitudNombre: this.lineaDestare.aptitudNombre,
              SPD: ''
            };
            console.log('LINEA DETALLE INGRESO', dataDetalle);
            this.apiLogisticaService.actualizarDetalleEncabezado(dataDetalle)
              .subscribe((data) => {
                console.log('RESPONSE ACTUALIZA DETALLE', data);
                if (data.valido) {
                  let num_linea = this.lineaDestare.numLinea;
                  let reqTarja = {
                    num_recep: this.cabecera.id,
                    linea: num_linea,
                    zona: this.cabecera.zona.id,
                    frigorifico: this.cabecera.frigorifico.id,
                    exportadora: this.cabecera.exportadora.id,
                    temporada: this.cabecera.temporada.id,
                    peso: resto
                  };
                  this.apiLogisticaService.actualizarTarjas(reqTarja)
                    .subscribe((data) => {
                      console.log('RESPONSE ACTUALIZA TARJA', data);
                      if (data.valido) {
                        this.snackBar.open('Linea tarja actualizada correctamente', null, { duration: 2000 });
                        this.cancelarLinea();
                        this.cargarListadoDetalle();
                      } else {
                        this.snackBar.open('Error al actualizar linea tarja', null, { duration: 2000 });
                        this.cancelarLinea();
                        this.cargarListadoDetalle();
                      }
                    }, (error) => {
                      console.error('ERROR ACTUALIZANDO TARJA', error);
                      this.snackBar.open('Error al actualizar linea', null, { duration: 2000 });
                      this.cancelarLinea();
                      this.cargarListadoDetalle();
                    });
                  this.snackBar.open('Linea actualizada correctamente', null, { duration: 2000 });
                  this.cancelarLinea();
                  this.cargarListadoDetalle();
                } else {
                  this.snackBar.open('Error al actualizar linea', null, { duration: 2000 });
                  this.cancelarLinea();
                  this.cargarListadoDetalle();
                }
              }, (error) => {
                this.snackBar.open('Error al actualizar linea', null, { duration: 2000 });
                console.error('ERROR ACTUALIZA', error);
              });
          } else {
            this.snackBar.open('Error al actualizar cabecera', null, { duration: 2000 });
            console.log('ERROR ACTUALIZA CABECERA');
          }
        }, (error) => {
          this.snackBar.open('Error al actualizar cabecera', null, { duration: 2000 });
          console.error('ERROR A GUARDAR CABECERA', error);
        });

      this.cargarListadoDetalle();
    } else {
      this.snackBar.open('Faltan parámetros', null, { duration: 3000 });
    }
  }

  async destarePorEspecie() {
    let bit_cabecera_guarda: boolean;
    if (this.pesoBrutoIngresado > 0) {
      let resto = 0;
      this.cabecera.pesoNeto = this.cabecera.pesoBrutoTotal - this.pesoBrutoIngresado - (this.cabecera.pesoEnvasesIn - this.cabecera.pesoEnvasesOut);
      resto = this.cabecera.pesoNeto / this.totalEnvases;
      this.cabecera.pesoTaraTotal = this.pesoBrutoIngresado;
      this.cabecera.pesoTaraCamion = this.pesoBrutoIngresado;
      // ACTUALIZA LA CABECERA CON LOS PESOS
      this.apiLogisticaService.actualizarEncabezado(this.cabecera)
        .subscribe(async (data) => {
          // console.log('RESPONSE GUARDA CABECERA', data);
          if (data.valido) {
            let arr = await this.cargarListadoDetalle();
            bit_cabecera_guarda = true;
            this.bit_cabecera_guarda2 = true;
            //ITERAR LOS DETALLES Y GUARDAR
            //SI GUARDA LA CABECERA SIN ERRORES, COMIENZA A GUARDAR DETALLES
            let proTara = this.pesoBrutoIngresado / this.totalEnvases;
            this.cargarListaDetalles();
          } else {
            bit_cabecera_guarda = false;
          }
        }, (error) => {
          console.error('ERROR GUARDAR CABECERA', error);
          bit_cabecera_guarda = false;
        });
    } else {
      this.snackBar.open('Debe ingresar peso bruto', null, { duration: 3000 });
    }
    this.cargarListadoDetalle();
  }
  cargarListaDetalles(){
    let resto = 0;
    resto = this.cabecera.pesoNeto / this.totalEnvases;
    let proTara = this.pesoBrutoIngresado / this.totalEnvases;
    this.detalle.forEach(element => {
      // console.log('ELEMENT DETALLE', element);
      // element.pesoTara = this.pesoBrutoIngresado * element.numEnvasesCont / this.totalEnvases;
      // console.log('PESO TARA', element.pesoTara);
      // console.log('PESO INGRESADO', this.pesoBrutoIngresado);
      // console.log('CANTIDAD ENVASES', element.numEnvasesCont);
      // console.log('TOTAL ENVASES', this.totalEnvases);
      let data = {
        id: element.id,
        zona: { id: element.zona.id },
        temporada: { id: element.temporada.id },
        frigorifico: { id: element.frigorifico.id },
        codCuartel: element.codCuartel,
        huerto: { id: element.huerto.id },
        empresa: { id: element.empresa.id },
        variedad: { idsnap: element.variedad.idsnap },
        especie: { idsnap: element.especie.idsnap },
        fechaDestare: '',
        fechaCosecha: element.fechaCosecha,
        pesoBruto: element.pesoBruto, // ASI ESTABA
        pesoTara: Math.round(element.numEnvasesCont * proTara),//ASI ESTABA
        pesoEnvasesIn: element.pesoEnvasesIn,
        pesoEnvasesOut: element.pesoEnvasesOut,
        envaseCosecha: { id: element.envaseCosecha.id },
        numEnvasesCont: element.numEnvasesCont,
        numLinea: element.numLinea,
        exportadora: { id: element.exportadora.id },
        cuentaCorriente: false,
        folioCKU: element.folioCKU,
        tipoDucha: { id: element.tipoDucha.id },
        tipoFrio: { id: element.tipoFrio.id },
        preFrio: false,
        kilosNetosIn: element.kilosNetosIn,
        kilosNetosOut: element.pesoBruto - Math.round(element.numEnvasesCont * proTara) - (element.pesoEnvasesIn - element.pesoEnvasesOut), // NUM_PESOENVOUT_RECGRANEL
        snapAptitud: element.snapAptitud,
        contenedor: { id: element.contenedor.id },
        condicion: { id: element.condicion.id },
        aptitudNombre: element.aptitudNombre,
        SPD: ''
      };
      console.log('LINEA INGRESO', data);
      let num_linea = element.numLinea;
      this.apiLogisticaService.actualizarDetalleEncabezado(data) // ACTUALIZA CADA DETALLE
        .subscribe(async (data) => {
          console.log('RESPONSE ACTUALIZA DETALLE', data);
          if (await data.valido) {
            // ACTUALIZAR LAS TARJAS
            let reqTarjas = {
              num_recep: this.cabecera.id,
              linea: num_linea,
              zona: this.cabecera.zona.id,
              frigorifico: this.cabecera.frigorifico.id,
              exportadora: this.cabecera.exportadora.id,
              temporada: this.cabecera.temporada.id,
              peso: resto
            };
            // this.cargarListadoDetalle();
            this.apiLogisticaService.actualizarTarjas(reqTarjas)
              .subscribe(async (data) => {
                console.log('RESPONSE GUARDA TARJA', await data);
                this.cargarListadoDetalle();
              }, (error) => {
                console.error('ERROR GUARDA TARJA', error);
              });
          } else {
            console.log('NO GUARDO LAS TARJAS');
          }
        }, (error) => {
          console.error('ERROR ACTUALIZA', error);
        });
    });
  }
  destarePorVariedadBKP() {
    if (this.pesoBrutoIngresado > 0 && this.lineaDestare.variedad.id && this.lineaDestare.especie.id) {
      let resto = 0;
      let total_tara_peso = 0;
      let total_pesos_bruto = 0;
      //OBTENER PESOS TOTALES DE LAS LINEAS
      this.detalle.forEach(element => {
        total_tara_peso += element.pesoTara;
        total_pesos_bruto += element.pesoBruto;
      });
      this.cabecera.pesoNeto = total_pesos_bruto - total_tara_peso - (this.cabecera.pesoEnvasesIn - this.cabecera.pesoEnvasesOut);
      resto = this.cabecera.pesoNeto / this.totalEnvases;
      this.cabecera.pesoTaraTotal = this.pesoBrutoIngresado;
      this.cabecera.pesoTaraCamion = this.pesoBrutoIngresado;
      // ACTUALIZA LA CABECERA CON LOS PESOS
      this.apiLogisticaService.actualizarEncabezado(this.cabecera)
        .subscribe(async (data) => {
          console.log('RESPONSE GUARDA CABECERA', data);
        }, (error) => {
          console.error('ERROR A GUARDAR CABECERA', error);
        });
      let data = {
        id: this.lineaDestare.id,
        zona: { id: this.lineaDestare.zona.id },
        temporada: { id: this.lineaDestare.temporada.id },
        frigorifico: { id: this.lineaDestare.frigorifico.id },
        codCuartel: this.lineaDestare.codCuartel,
        huerto: { id: this.lineaDestare.huerto.id },
        empresa: { id: this.lineaDestare.empresa.id },
        variedad: { idsnap: this.lineaDestare.variedad.idsnap },
        especie: { idsnap: this.lineaDestare.especie.idsnap },
        fechaCosecha: this.lineaDestare.fechaCosecha,
        pesoBruto: this.lineaDestare.pesoBruto,
        // pesoTara: parseFloat(this.lineaDestare.pesoTara.toFixed(2)), //ASI ESTABA
        pesoTara: Math.round(this.lineaDestare.pesoTara), //ASI ESTABA
        pesoEnvasesIn: this.lineaDestare.pesoEnvasesIn,
        pesoEnvasesOut: this.lineaDestare.pesoEnvasesOut,
        envaseCosecha: { id: this.lineaDestare.envaseCosecha.id },
        numEnvasesCont: this.lineaDestare.numEnvasesCont,
        numLinea: this.lineaDestare.numLinea,
        exportadora: { id: this.lineaDestare.exportadora.id },
        cuentaCorriente: false,
        folioCKU: this.lineaDestare.folioCKU,
        tipoDucha: { id: this.lineaDestare.tipoDucha.id },
        tipoFrio: { id: this.lineaDestare.tipoFrio.id },
        preFrio: false,
        kilosNetosOut: this.lineaDestare.pesoBruto - this.pesoBrutoIngresado - (this.lineaDestare.pesoEnvasesIn - this.lineaDestare.pesoEnvasesOut),
        // KilosNetosIn: resto * this.lineaDestare.numEnvasesCont,
        kilosNetosIn: this.lineaDestare.KilosNetosIn,
        snapAptitud: this.lineaDestare.snapAptitud,
        contenedor: { id: this.lineaDestare.contenedor.id },
        condicion: { id: this.lineaDestare.condicion.id },
        aptitudNombre: this.lineaDestare.aptitudNombre,
        SPD: ''
      };
      console.log('LINEA INGRESO', data);
      this.apiLogisticaService.actualizarDetalleEncabezado(data)
        .subscribe((data) => {
          console.log('RESPONSE ACTUALIZA DETALLE', data);
          if (data.valido) {
            this.snackBar.open('Linea actualizada correctamente', null, { duration: 2000 });
            this.cancelarLinea();
            this.cargarListadoDetalle();
          } else {
            this.snackBar.open('Error al actualizar linea', null, { duration: 2000 });
            this.cancelarLinea();
            this.cargarListadoDetalle();
          }
        }, (error) => {
          console.error('ERROR ACTUALIZA', error);
        });

      let num_linea = this.lineaDestare.numLinea;
      let obtenerTarja = {
        num_recep: this.cabecera.id,
        zona: this.cabecera.zona.id,
        frigorifico: this.cabecera.frigorifico.id,
        exportadora: this.cabecera.exportadora.id,
        temporada: this.cabecera.temporada.id,
        linea: num_linea
      };
      this.apiLogisticaService.obtenerTarjas(obtenerTarja)
        .subscribe((data) => {
          data.items.forEach(element => {
            let reqTarja = {
              numRecepcion: this.cabecera.id,
              zona: { id: this.cabecera.zona.id },
              temporada: { id: this.cabecera.temporada.id },
              frigorifico: { id: this.cabecera.frigorifico.id },
              linea: num_linea,
              folio: element.folio,
              kilosNetos: (this.lineaDestare.pesoBruto - this.pesoBrutoIngresado - (this.lineaDestare.pesoEnvasesIn - this.lineaDestare.pesoEnvasesOut)) / this.lineaDestare.numEnvasesCont,
              cantidad: element.cantidad,
              vaciado: false,
              exportadora: { id: this.cabecera.exportadora.id },
              Camara: { id: element.camara.id },
              seccionPlanta: { id: element.seccionPlanta.id },
              fecha: element.fecha
            };
            //ACTUALIZA LA TARJA
            this.apiLogisticaService.actualizaLineaGuiaRecepcion(reqTarja)
              .subscribe((data) => {
                console.log('RESPONSE ACTUALIZA TARJA', data);
                if (data.valido) {
                } else {
                }
              }, (error) => {
                console.error('ERROR ACTUALIZA TARJA', error);
              });
          });
        }, (error) => {
          console.error('ERROR OBTENER TARJA', error);
        });
      this.cargarListadoDetalle();
    } else {
      this.snackBar.open('Faltan parámetros', null, { duration: 3000 });
    }
  }

  async destarePorEspecieBKP() {
    let bit_cabecera_guarda: boolean;
    if (this.pesoBrutoIngresado > 0) {
      let resto = 0;
      this.cabecera.pesoNeto = this.cabecera.pesoBrutoTotal - this.pesoBrutoIngresado - (this.cabecera.pesoEnvasesIn - this.cabecera.pesoEnvasesOut);
      console.log(this.cabecera.pesoBrutoTotal)
      console.log(this.cabecera.pesoTaraTotal)
      console.log(this.cabecera.pesoEnvasesIn)
      console.log(this.cabecera.pesoEnvasesOut)
      resto = this.cabecera.pesoNeto / this.totalEnvases;
      this.cabecera.pesoTaraTotal = this.pesoBrutoIngresado;
      this.cabecera.pesoTaraCamion = this.pesoBrutoIngresado;
      // ACTUALIZA LA CABECERA CON LOS PESOS
      this.apiLogisticaService.actualizarEncabezado(this.cabecera)
        .subscribe(async (data) => {
          console.log('RESPONSE GUARDA CABECERA', data);
          if (data.valido) {
            let arr = await this.cargarListadoDetalle();
            bit_cabecera_guarda = true;
            console.log(arr)
            console.log("await")
            console.log(this.detalle)
            //ITERAR LOS DETALLES Y GUARDAR
            //SI GUARDA LA CABECERA SIN ERRORES, COMIENZA A GUARDAR DETALLES
            let total_detalles: number = 0;
            let correctos_detalle: number = 0;
            let errores_detalle: number = 0;
            let correctos_tarja: number = 0;
            let errores_tarja: number = 0;

            this.detalle.forEach(element => {
              total_detalles++;
              element.pesoTara = this.pesoBrutoIngresado * element.numEnvasesCont / this.totalEnvases;
              console.log('PESO TARA', element.pesoTara);
              console.log('PESO INGRESADO', this.pesoBrutoIngresado);
              console.log('CANTIDAD ENVASES', element.numEnvasesCont);
              console.log('TOTAL ENVASES', this.totalEnvases)
              let data = {
                id: element.id,
                zona: { id: element.zona.id },
                temporada: { id: element.temporada.id },
                frigorifico: { id: element.frigorifico.id },
                codCuartel: element.codCuartel,// SDP
                huerto: { id: element.huerto.id },
                empresa: { id: element.empresa.id },
                variedad: { idsnap: element.variedad.idsnap },
                especie: { idsnap: element.especie.idsnap },
                fechaCosecha: element.fechaCosecha,
                pesoBruto: element.pesoBruto,
                pesoTara: parseFloat(element.pesoTara.toFixed(2)),///
                pesoEnvasesIn: element.pesoEnvasesIn,/////
                pesoEnvasesOut: element.pesoEnvasesOut,/////
                envaseCosecha: { id: element.envaseCosecha.id },
                numEnvasesCont: element.numEnvasesCont,
                numLinea: element.numLinea,
                exportadora: { id: element.exportadora.id },
                cuentaCorriente: false,
                folioCKU: element.folioCKU,
                tipoDucha: { id: element.tipoDucha.id },
                tipoFrio: { id: element.tipoFrio.id },
                preFrio: false,
                kilosNetosIn: element.KilosNetosIn, ///// ???????
                kilosNetosOut: element.pesoBruto - element.pesoTara - (element.pesoEnvasesIn - element.pesoEnvasesOut), ///// ???????
                // KilosNetosIn: resto * element.numEnvasesCont, ///// ?????
                snapAptitud: element.snapAptitud, //// ?????
                contenedor: { id: element.contenedor.id },
                condicion: { id: element.condicion.id },
                aptitudNombre: element.aptitudNombre,
                SPD: ''
              };
              console.log('LINEA INGRESO', data);
              this.apiLogisticaService.actualizarDetalleEncabezado(data)
                .subscribe((data) => {
                  console.log('RESPONSE ACTUALIZA DETALLE', data);
                  if (data.valido) {
                    correctos_detalle++;
                  } else {
                    errores_detalle++;
                  }
                }, (error) => {
                  errores_detalle++;
                  console.error('ERROR ACTUALIZA', error);
                });

              let num_linea = element.numLinea;
              let obtenerTarja = {
                num_recep: this.cabecera.id,
                zona: this.cabecera.zona.id,
                frigorifico: this.cabecera.frigorifico.id,
                exportadora: this.cabecera.exportadora.id,
                temporada: this.cabecera.temporada.id,
                linea: num_linea
              };
              this.apiLogisticaService.obtenerTarjas(obtenerTarja)
                .subscribe((data) => {
                  data.items.forEach(element => {
                    let reqTarja = {
                      numRecepcion: this.cabecera.id,
                      zona: { id: this.cabecera.zona.id },
                      temporada: { id: this.cabecera.temporada.id },
                      frigorifico: { id: this.cabecera.frigorifico.id },
                      linea: num_linea,
                      folio: element.folio,
                      kilosNetos: resto,
                      cantidad: element.cantidad,
                      vaciado: false,
                      exportadora: { id: this.cabecera.exportadora.id },
                      Camara: { id: element.camara.id },
                      seccionPlanta: { id: element.seccionPlanta.id },
                      fecha: element.fecha
                    };
                    //ACTUALIZA LA TARJA
                    this.apiLogisticaService.actualizaLineaGuiaRecepcion(reqTarja)
                      .subscribe((data) => {
                        console.log('RESPONSE ACTUALIZA TARJA', data);
                        if (data.valido) {
                          correctos_tarja++;
                        } else {
                          errores_tarja++;
                        }
                      }, (error) => {
                        errores_tarja++;
                        console.error('ERROR ACTUALIZA TARJA', error);
                      });
                  });
                }, (error) => {
                  errores_tarja++;
                  console.error('ERROR OBTENER TARJA', error);
                });
            });
            //VERIFICA SI GRABA
            console.log('VALIDA', correctos_detalle === total_detalles);
            if (correctos_detalle === total_detalles) {
              this.pesoBrutoIngresado = '';
              this.snackBar.open('Destare realizado correctamente', null, { duration: 2000 });
            } else if (errores_tarja > 0) {
              this.snackBar.open('Error al guardar tarja', null, { duration: 2000 });
            }
          } else {
            bit_cabecera_guarda = false;
          }
        }, (error) => {
          console.error('ERROR GUARDAR CABECERA', error);
          bit_cabecera_guarda = false;
        });
    } else {
      this.snackBar.open('Debe ingresar peso bruto', null, { duration: 3000 });
    }
    this.cargarListadoDetalle();
  }

  seleccionEspecie(especie: any) {
    this.variedades = [];
    this.especieSeleccion = especie.id;
    if (this.bit_modo_pesaje) {
      this.apiCommonService.cargarVarieades(especie.id)
        .subscribe((data) => {
          this.variedades = data.items;
        }, (error) => {
          console.error('ERROR CARGANDO VARIEDADES', error);
        });
    }
  }

  obtenerVariedad(variedad: any) {
    this.variedadSeleccion = variedad.id;
  }

  async cargarListadoDetalle() {
    // this.pesoBrutoIngresado = '';
    this.detalle = [];
    try {
      const data = await this.apiLogisticaService.obtenerListaRecepcionlinea(this.cku.exportadora, this.cku.planta, this.cku.temporada, this.cku.zona, this.cku.num_recepcion).toPromise();
      console.log('LISTADO DETALLE', data);
      this.dataSource = data.lista.items;
      let total_bines = 0;
      let total_peso = 0;
      data.lista.items.forEach(element => {
        // console.log('ELEMENT', element);
        total_bines += element.numEnvasesCont;
        total_peso += element.pesoEnvasesIn;
      });
      this.totalEnvases = total_bines;
      this.totalPesoEnvase = total_peso;
      // let peso_tara;
      this.detalle = data.lista.items;
      this.especieInforme = data.lista.items[0].especie.nombre;
    }
    catch (error) {
      console.error('ERROR LISTADO DETALLE', error);
    }
  }

  calcularDestare() {
    if (!this.bit_modo_pesaje) {
      this.agregarDestareEspecie();
    } else {
      this.agregarDestareVariedad();
    }
  }

  agregarDestareEspecie() {
    let sumaTaraActual = 0;
    let destare = parseFloat(this.pesoDestarePorEspecie.toString());
    let totalEnvases = 0;
    // ENVASES TOTAL
    if (this.dataSource.length > 0) {
      this.dataSource.forEach(element => {
        totalEnvases += parseFloat(element.numEnvasesCont);
      });
      this.dataSource.forEach(element => {
        let calculo = (destare / totalEnvases) * element.numEnvasesCont;
      });
    }
  }

  recepcionFinal() {
    let reqUpdate = {
      id: this.cabecera.id,
      idGuiaProductor: this.cabecera.idGuiaProductor,
      fechaCosecha: this.cabecera.fechaCosecha,
      fechaRecepcion: this.cabecera.fechaRecepcion,
      fechaDespacho: this.cabecera.fechaDespacho,
      patenteCamion: this.cabecera.patenteCamion,
      patenteCarro: this.cabecera.patenteCarro,
      chofer: { id: this.cabecera.chofer.id, nombre: this.cabecera.chofer.nombre },
      idPesa: this.cabecera.idPesa,
      zona: { id: this.cabecera.zona.id },
      temporada: { id: this.cabecera.temporada.id },
      empresa: { id: this.cabecera.empresa.id, nombre: this.cabecera.empresa.nombre },
      huerto: { id: this.cabecera.huerto.id, nombre: this.cabecera.huerto.nombre },
      frigorifico: { id: this.cabecera.frigorifico.id, nombre: this.cabecera.frigorifico.nombre },
      pesoBrutoCamion: this.cabecera.pesoBrutoCamion,
      pesoBrutoCarro: this.cabecera.pesoBrutoCarro,
      pesoBrutoTotal: this.cabecera.pesoBrutoTotal,
      pesoEnvasesIn: this.cabecera.pesoEnvasesIn,
      pesoEnvasesOut: this.cabecera.pesoEnvasesOut,
      pesoTaraCamion: this.cabecera.pesoTaraCamion,
      pesoTaraCarro: this.cabecera.pesoTaraCarro,
      pesoTaraTotal: this.cabecera.pesoTaraTotal,
      pesoNeto: this.cabecera.pesoNeto,
      fechaDestare: this.cabecera.fechaDestare,
      glosa: this.cabecera.glosa,
      codSeccion: this.cabecera.codSeccion,
      terminada: true,
      usuario: { id: this.cabecera.usuario.id, nombre: this.cabecera.usuario.nombre },
      exportadora: { id: this.cabecera.exportadora.id, nombre: this.cabecera.exportadora.nombre },
      codTransaccionProceso: this.cabecera.codTransaccionProceso,
      zonaProductor: { id: this.cabecera.zonaProductor.id }
    };
    this.apiLogisticaService.actualizarEncabezado(reqUpdate)
      .subscribe((data) => {
        console.log('RESPONSE FINALIZA CABECERA', data);
        this.snackBar.open(' ✔ \xa0\xa0 Cabecera actualizada correctamente', null, { duration: 2000 });
      }, (error) => {
        console.error('ERROR CABECERA', error);
      });

    let data = {
      codTemporada: this.cabecera.temporada.id,
      codZona: this.cabecera.zona.id,
      numRecepcion: this.cabecera.id,
      codExportadora: this.cabecera.exportadora.id,
      codFrigorifico: this.cabecera.frigorifico.id
    };
    this.apiLogisticaService.recepcionFinal(data)
      .subscribe((result) => {
        console.log('PROCESO FINAL', result);
        this.snackBar.open(' ✔ \xa0\xa0 Proceso finalizado correctamente', null, { duration: 2000 });
      }, (error) => {
        console.log('ERROR EN PROCESO FINAL', error);
        console.log('ERROR', error.error);
        this.snackBar.open(error.error, null, { duration: 10000 });
      });
  }

  reporte() {
    let totalEmpresa = 0;
    let totalOtro = 0;
    let pesoEnvasesReporte = 0;
    let nombreEnvaseLinea = '';
    let nombreEnvaseSalida = '';
    if (this.arrEnvasesEntrada) {
      this.arrEnvasesEntrada.forEach(ln => {
        nombreEnvaseLinea = ln.envaseCosecha.nombre;
        totalEmpresa += ln.cantidadEmpresa;
        totalOtro += ln.cantidadOtros;
        pesoEnvasesReporte += ln.pesoEnvases;
      });
    } else {
      pesoEnvasesReporte = 0;
    }
    this.totalUniReporte = totalEmpresa;
    this.totalOtrosReporte = totalOtro;
    this.pesoEnvasesReporte = pesoEnvasesReporte;
    ///// Envases desalida
    let totalUniSalida = 0;
    let totalOtroSalida = 0;
    let pesoEnvasesReporteSalida = 0;
    if (this.arrEnvasesSalida.length) {
      this.arrEnvasesSalida.forEach(ln => {
        nombreEnvaseSalida = ln.nombre;
        totalUniSalida += ln.cantEnvasesUni;
        totalOtroSalida += ln.cantEnvasesOtros;
        pesoEnvasesReporteSalida += (ln.cantEnvasesUni + ln.cantEnvasesOtros) * parseFloat(ln.tara);
      });
    } else {
      pesoEnvasesReporteSalida = 0;
    }
    return {
      totalUniReporte: totalEmpresa,
      totalOtrosReporte: totalOtro,
      pesoEnvasesReporte: pesoEnvasesReporte,
      totalUniSalida: totalUniSalida,
      totalOtroSalida: totalOtroSalida,
      pesoEnvasesReporteSalida: pesoEnvasesReporteSalida,
      nombreEnvaseLinea: nombreEnvaseLinea,
      nombreEnvaseSalida: nombreEnvaseSalida
    }
  }

  destararLinea(linea: any) {
    console.log('Linea DESTARE', linea);
    // console.log('Linea DESTARE', JSON.stringify(linea));
    this.lineaDestare = linea;
  }

  cancelarLinea() {
    this.lineaDestare = {
      especie: { id: null },
      variedad: { id: null },
    };
    this.pesoBrutoIngresado = '';
  }

  sumaBultos() {
    let suma = 0;
    this.detalle.forEach(ln => {
      suma += ln.numEnvasesCont;
    });
    return suma;
  }

  sumaPesoNeto() {
    let suma = 0;
    this.detalle.forEach(ln => {
      suma += ln.KilosNetosOut;
    });
    return suma;
  }

  agregarDestareVariedad() { }

  guardar() {
    this.snackBar.open(' ✔ \xa0\xa0 Datos guardados correctamente', null, { duration: 2000 });
    this.router.navigate(['/pantallacku'])
  }
}