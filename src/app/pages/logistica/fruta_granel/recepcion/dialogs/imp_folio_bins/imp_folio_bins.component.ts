import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-imp_folio_bins',
  templateUrl: './imp_folio_bins.component.html',
  styleUrls: ['./imp_folio_bins.component.css']
})
export class Imp_folio_binsComponent implements OnInit {
  listaBins: any = [];
  lineaCKU: any = [];
  seccionPlanta: any = [];
  num_recepcion: any = [];
  zpl_cargado: any = [];
  impresoras: any = [];
  // csg_codigo: any = [];
  impresoraSeleccion: any = [];
  num_guia_recepcion: any = [];
  cargando: boolean = false;
  seccionSel: any = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<Imp_folio_binsComponent>,
    private serviceApiLogistica: ApilogisticaService,
    private serviceApiCommon: ApiCommonService,
    private snackBar: MatSnackBar
  ) {
    if (this.data) {
      this.lineaCKU = this.data.item;
      this.num_recepcion = this.data.num_recepcion;
      this.num_guia_recepcion = this.data.num_guia_recepcion;
    }
    console.log('LLEGA A MODAL IMPRESION', this.lineaCKU);
    this.cargarSeccionesPlanta();
  }

  ngOnInit() { }

  cargarSeccionesPlanta() {
    this.serviceApiCommon.cargarSeccionPlanta(this.lineaCKU.zona.id, this.lineaCKU.frigorifico.id)
      .subscribe((data) => {
        // console.log('RESPONSE SECCION PLANTA', this.seccionPlanta);
        this.seccionPlanta = data.items;
        if (this.seccionPlanta) {
          if (this.seccionPlanta.length === 1) {
            this.seccionSel = this.seccionPlanta[0].id;
            this.obtenerZpl(this.seccionSel);
            this.rescatarFolios(this.seccionSel);
          }
        }
      }, (error) => {
        console.error('ERROR SECCION', error);
        this.snackBar.open('Error al cargar seccion planta', null, { duration: 2000 })
      });
  }

  obtenerZpl(seccion: any) {
    if (seccion) {
      let req = {
        // id: this.num_recepcion,
        id: 1,
        seccion: seccion,
        zona: this.lineaCKU.zona.id,
        frigorifico: this.lineaCKU.frigorifico.id
      };
      this.serviceApiCommon.cargarEtiquetasZpl(req)
        .subscribe((data) => {
          // console.log('RESPONSE ETIQUETA ZPL', data);
          this.zpl_cargado = data;
        }, (error) => {
          this.zpl_cargado = [];
          console.error('ERROR OBTENER ZPL', error);
          this.snackBar.open('Error al obtener zpl', null, { duration: 2000 })
        });
    }
  }

  rescatarFolios(seccion: any) {
    this.cargando = true;
    this.listaBins = [];
    if (seccion) {
      this.obtenerZpl(seccion);
      this.cargarImpresoras(seccion, this.lineaCKU.zona.id, this.lineaCKU.frigorifico.id);
      let reqBins = {
        exportadora: this.lineaCKU.exportadora.id,
        temporada: this.lineaCKU.temporada.id,
        zona: this.lineaCKU.zona.id,
        num_recep: this.num_recepcion,
        frigorifico: this.lineaCKU.frigorifico.id,
        cod_linea: this.lineaCKU.numLinea
      };
      this.serviceApiLogistica.obtenerListadoBines(reqBins)
        .subscribe((data) => {
          let valido = data['valido'];
          if (valido) {
            this.listaBins = data['lista']['items'];
            this.listaBins.forEach(element => {
              element.check = false;
            });
            this.cargando = false;
          } else {
            this.crearFolios(seccion);
          }
          console.log('RESPONSE FOLIOS CARGADOS', data);
        }, (error) => {
          console.error('ERROR LISTADO BINES', error);
        });
    } else {
      this.cargando = false;
    }
  }

  cargarImpresoras(seccion: string, zona: string, frigorifico: string) {
    this.serviceApiCommon.cargarListadoImpresoras(seccion, zona, frigorifico)
      .subscribe((data) => {
        this.impresoras = data.items;
        console.log('IMPRESORAS RESPONSE', data);
      }, (error) => {
        console.error('ERROR CARGA IMPRESORAS', error);
      });
  }

  crearFolios(seccion: any) {
    this.listaBins = [];
    // let kilosNeto = parseInt(this.lineaCKU.kilosNetosIn) - parseInt(this.lineaCKU.pesoTara);
    if (seccion) {
      let kilosNeto = parseInt(this.lineaCKU.kilosNetosIn);
      let reqImpFolio = {
        numRecepcion: this.num_guia_recepcion,
        zona: { id: this.lineaCKU.zona.id },
        temporada: { id: this.lineaCKU.temporada.id },
        frigorifico: { id: this.lineaCKU.frigorifico.id },
        linea: this.lineaCKU.numLinea,
        folio: this.lineaCKU.folioCKU,
        kilosNetos: kilosNeto,
        cantidad: this.lineaCKU.numEnvasesCont,
        vaciado: false,
        exportadora: { id: this.lineaCKU.exportadora.id },
        seccionPlanta: { id: seccion },
        fecha: this.lineaCKU.fechaCosecha
      };
      this.serviceApiLogistica.imprimirFolio(reqImpFolio)
        .subscribe((data) => {
          let valido = data['valido'];
          if (valido) {
            this.listaBins = data['lista'];
            this.listaBins.forEach(element => {
              element.check = false;
            });
          } else {
            this.snackBar.open('Sin bins', null, { duration: 2000 })
          }
          this.cargando = false;
          console.log('RESPONSE IMPRIME FOLIOS', data);
        }, (error) => {
          this.cargando = false;
          console.error('ERROR IMPRIMIR FOLIO', error);
          this.snackBar.open('Error al cargar listado', null, { duration: 2000 })
        });
    } else {
      this.cargando = false;
    }
  }

  seleccionImpresora(impresora: any) {
    this.impresoraSeleccion = impresora;
    console.log('Impresora Seleccion', this.impresoraSeleccion);
  }


  seleccionSecccion(event: any) {
    // console.log('SELECCION ', event.value);
    if (event.value) {
      this.seccionSel = event.value;
      this.rescatarFolios(event.value);
    }
  }

  imprimirBines() {
    if (this.zpl_cargado.detalle) {
      let cantidad = 0;
      this.listaBins.forEach((item: any, index: number) => {
        if (item.check) {
          cantidad++;
          this.imprimir(item.folio);
        }
      });
      if (cantidad === 0) {
        this.snackBar.open('Debe seleccionar al menos un bin para imprimir', null, { duration: 5000 })
      }
    } else {
      this.snackBar.open('Sin código zpl para imprimir', null, { duration: 2000 })
    }
  }

  imprimir(nro_bin: any) {
    // CONSUMIER APIS PARA LLENAR LAS VARIABLES
    let csg = '';
    if (this.lineaCKU.huerto.codCSG || this.lineaCKU.huerto.codCSG != null) {
      csg = this.lineaCKU.huerto.codCSG;
    } else {
      csg = 'No tiene';
    }
    let fechaFormateada = new Date(this.lineaCKU.fechaCosecha).toLocaleDateString();
    var rem_cod_prod = this.zpl_cargado.detalle.replace(/#cod_prod#/gi, this.lineaCKU.huerto.id);
    var rem_des_prod = rem_cod_prod.replace(/#des_prod#/gi, this.lineaCKU.huerto.nombre);
    var rem_csg = rem_des_prod.replace(/#csg#/gi, csg);
    var rem_huerto = rem_csg.replace(/#huerto#/gi, this.lineaCKU.huerto.nombre);
    var rem_cod_var = rem_huerto.replace(/#cod_var#/gi, this.lineaCKU.variedad.id);
    var rem_variedad = rem_cod_var.replace(/#des_var#/gi, this.lineaCKU.variedad.nombre);
    var rem_aptitud = rem_variedad.replace(/#aptitud#/gi, this.lineaCKU.aptitudNombre);
    var rem_fecha = rem_aptitud.replace(/#fecha#/gi, fechaFormateada);
    var rem_id_bins = rem_fecha.replace(/#id_bins#/gi, nro_bin);
    var rem_nro_bins = rem_id_bins.replace(/#nro_bins#/gi, nro_bin);
    // console.log('ZPL:', rem_nro_bins);
    console.log('IP', this.impresoraSeleccion.IP);
    if (this.impresoraSeleccion.IP) {
      let req = {
        zpl: rem_nro_bins,
        ip: this.impresoraSeleccion.IP.trim(),
        puerto: this.impresoraSeleccion.PORT.trim()
      };
      this.serviceApiLogistica.imprimirZpl(req)
        .subscribe((data) => {
          let valido = data.realizado;
          if (valido) {
            this.snackBar.open(data.mensaje, null, { duration: 3000 });
          } else {
            this.snackBar.open(data.mensaje, null, { duration: 3000 });
          }
          console.log('RESPONSE IMPRIMIR ZPL', data);
        }, (error) => {
          console.error('ERROR IMPRIMIR ZPL', error);
        });
    } else {
      this.snackBar.open('Debe seleccionar una impresora', null, { duration: 2000 })
    }
  }

  cerrar() {
    this.dialogRef.close();
  }

  checkTodo(event: any) {
    if (event.checked) {
      this.listaBins.forEach(element => {
        element.check = true;
      });
    } else {
      this.listaBins.forEach(element => {
        element.check = false;
      });
    }
    console.log(this.listaBins);
  }
}