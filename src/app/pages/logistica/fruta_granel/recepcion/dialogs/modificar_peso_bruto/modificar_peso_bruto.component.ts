import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modificar_peso_bruto',
  templateUrl: './modificar_peso_bruto.component.html',
  styleUrls: ['./modificar_peso_bruto.component.css']
})
export class Modificar_peso_brutoComponent implements OnInit {
  mostrarPeso: number;
  elementoPesado: number;
  linea : any  =[];
  pesoBruto = 0;
  pesoBrutoLinea = 0;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<Modificar_peso_brutoComponent>,
  ) {
    console.log( data );
    this.mostrarPeso = parseInt(data['mostrarPeso']);
    this.elementoPesado = parseInt(data['elementoPesado']);
    this.pesoBruto = parseFloat(data['pesoBruto']);
    this.linea = data.linea
    this.pesoBrutoLinea = this.linea.pesoBruto;
  }

  ngOnInit() { }
  
  close(): void {
    this.dialogRef.close();
  }

  cambiarPeso(){
    console.log(this.pesoBrutoLinea);
    this.dialogRef.close({nuevoPesoBruto:this.pesoBrutoLinea})
  }
}