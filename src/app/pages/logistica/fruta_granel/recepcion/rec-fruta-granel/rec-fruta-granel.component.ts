import { Modificar_peso_brutoComponent } from './../dialogs/modificar_peso_bruto/modificar_peso_bruto.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { PesoBrutoComponent } from '../../../../../shared/dialogs/peso-bruto/peso-bruto.component';
import { Router, ActivatedRoute } from '@angular/router';
import { ApilogisticaService } from '../../../../../services/apilogistica.service';
import { ApiCommonService } from '../../../../../services/apicommonservice.service';
import { FormControl } from '@angular/forms';
import chileanRut from 'chilean-rut';
import * as moment from 'moment';

let ELEMENT_DATA = [];
let ELEMENT_DATA_AUX = [];
let ELEMENT_DATA_DESTARA = [];
let ELEMENT_DATA_ENVASES_ENTRADA = [];
let ELEMENT_DATA_ENVASES_SALIDA = [];
@Component({
  selector: 'app-rec-fruta-granel',
  templateUrl: './rec-fruta-granel.component.html',
  styleUrls: ['./rec-fruta-granel.component.css']
})
export class RecFrutaGranelComponent implements OnInit {
  @ViewChild('rutInput') rutReference: ElementRef;
  @ViewChild('pesobruto') refPesoBruto: ElementRef;
  @ViewChild('cambioPeso') refCambioPeso: any;
  @ViewChild('ingresopeso') refIngresoPeso: ElementRef;
  @ViewChild('inputHuerto') refInputHuerto: any;
  @ViewChild('patente') refPatente: any;
  displayedColumns: string[] = ['especie', 'variedad', 'sdp', 'pesobruto', 'cant_env', 'env', 'neto', 'promedio', 'linea', 'icons'];
  displayedColumns2: string[] = ['1', '2', '3', '4', '5', '6', 'icons'];
  displayedColumns3: string[] = ['1', '2', '3', '4'];
  displayedColumns4: string[] = ['1', '2', '3', '4', '5', '6', '7', 'icons'];
  arr_envases = [{ nombre: null, id: null, pesoEnvase: 0.0 }];
  envase = '';
  pesoEnvaseDetEntrada = 0;
  cantUnidetEnvaseEntrada = 0;
  cantOtroDetEnvaseEntrada = 0;
  totalEnvasesDetEntrada = 0;
  totalKilosEnvasesDetEntrada = 0;
  numArrDetEnvEnt = 0;

  envaseSalida = '';
  envaseEntrada = '';
  pesoEnvaseDetSalida = 0.0;
  cantUnidetEnvaseSalida = 0;
  cantOtroDetEnvaseSalida = 0;
  totalEnvasesDetSalida = 0;
  totalKilosEnvasesDetSalida = 0;
  numArrDetEnvSal = 0;

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
  dataSourceEnvasesEntrada = new MatTableDataSource(ELEMENT_DATA_ENVASES_ENTRADA);
  dataSourceEnvasesSalida = new MatTableDataSource(ELEMENT_DATA_ENVASES_SALIDA);

  especie: string;
  variedad: string;
  sdp: any;
  pesobruto: number = 0;
  env: any;
  neto: any;
  promedio: any = 0;
  linea: number;
  rut: string = null;
  isLinear: boolean;
  mostrarPeso = '1';
  icons: any;
  patenteValida: boolean = true;
  totalValida: boolean = true;
  pesoBruto: number = 0;
  pesoBrutoTotal: number = 0;
  pesoEnvase: number = 0;
  pesoPorEspecie: number = 0;
  checkCamion: boolean = false;
  pesoCamion: number = 0;
  pesoCarro: number = 0;
  ingresoPeso: string = '';
  elementoPesado = 0;
  totalSuma: number = 0;
  especies: any = [];
  pesoEnvaseSalida = 0;
  variedades = [];
  pesoDestareVariedad = 0;
  pesoDestarePorEspecie = 0;
  pesoBrutoSalida = 0;
  sumaTotalEnvases = 0;
  arrEnvasesEntrada = [];
  arrEnvasesSalida = [];
  pesoVariedadRomanaCamionCarro = 0;
  productor = '';
  huerto = '';
  nombreHuerto = '';
  huertos = [];
  zona = 0;
  frigorifico = 0;
  frigorificos = [];
  fechaCosecha = new Date();
  fechaDespacho = new Date();
  fechaRecepcion = new Date();
  hora = new Date().getHours() + ':' + new Date().getMinutes();
  chofer = '';
  nombreChofer = null;
  patenteCamion = '';
  patenteCarro = '';
  modoPesaje: string = '1';
  seccion = '0';
  secciones = [];
  balanza = '';
  balanzas = [];
  tipoPesaje = 1;
  productorIntencion: any = 0;
  productorIntenciones = [];
  csg: string = '';
  lineasIngresadas = []; // Guarda todas las lineas para luego tener info para descontar en envases de entrada y salidad
  numeroLineasOcupadas = []; // Arreglo utilizado para que al momento de eliminar una linea no recalculo todas las lineas 
  /// Ya que las lineas ya estaran ingresadas en la base de datos 
  numeroLineasOcupadasPeso = [];
  guiaProductor = '';
  idGuia = 0;
  encabezadoGuardado = false;
  choferNoEncontrado = true;
  nGuiaUnifrutti = 0;
  idRetorno: number;
  temporada = localStorage.getItem('temporada');
  usuario = localStorage.getItem('User');
  exportadora = localStorage.getItem('exportadora');
  planta = localStorage.getItem('planta');
  nombrePlanta = localStorage.getItem('Nplanta');
  empresaSeleccionada = '';
  zonaSeleccionada = '';
  permiteGuardar = false;
  selected = new FormControl(0);
  dataLinea = {};
  actualizarEncabezado = false;
  listaEnvases = [];
  dataVariedadPesa = [];// Arreglo para actualizar variedad pesa 
  observaciones = '';
  encabezadoGuardadoEnvaseSalida = false;
  ELEMENT_DATA_: any = [];
  destare_aux = [];
  linea_aux = [];
  deshabilitarBtnPesoEnt = false;
  ///////////////////////
  totalUniReporte = 0;
  totalOtrosReporte = 0;
  pesoEnvasesReporte = 0;
  ///////////////////////
  taraCargada = false;
  editandoRecepcion = false;
  idRecepcion: string = null;
  idContenedor = 0;
  f = 0;

  detalleUpdate: any = [];
  glosas: any = [];
  glosaGlob: any;
  glosaNombreGlob: any;

  frigorificoInforme: string;
  comunaInforme: string;
  telefonoInforme: string;
  huertoDireccion: string;
  telefonoEmpresa: string;
  informeSII: any = {};
  habilitaInformeSII: boolean = false;

  numeroRecepcion: any = null;
  envaseSalidaMinimo: number = 0;
  envaseSalidaMaximo: number = 0;
  muestraTabs: boolean = false;

  calculosGenerales: any = [{
    PesoBrutoEntrada: 0,
    PesoBrutoSalida: 0,
    PesoEnvasesEntrada: 0,
    PesoEnvasesSalida: 0,
    PesoNetoEntrada: 0,
    PesoNetoSalida: 0,
    PesoPromedio: 0,
    CantTotalEnvasesEntrada: 0
  }];

  validaIngresaLinea: boolean = false;

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router,
    private params: ActivatedRoute,
    private wsLogistica: ApilogisticaService,
    private wsCommon: ApiCommonService
  ) {
    ELEMENT_DATA = [];
    ELEMENT_DATA_AUX = [];
    ELEMENT_DATA_DESTARA = [];
    ELEMENT_DATA_ENVASES_ENTRADA = [];
    ELEMENT_DATA_ENVASES_SALIDA = [];
    this.ELEMENT_DATA_ = ELEMENT_DATA;
    this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
    this.dataSourceEnvasesSalida = new MatTableDataSource(this.arrEnvasesSalida);
    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
  }

  ngOnInit() {
    this.rescatarZona();
    this.cargarGlosas();
    this.checkCamion = true;
    this.lineasIngresadas = [];
    this.dataSource = new MatTableDataSource([]);
    this.dataSourceEnvasesEntrada = new MatTableDataSource([]);
    this.dataSourceEnvasesSalida = new MatTableDataSource([]);
    this.dataSourceDestare = new MatTableDataSource([]);
  }

  tabs(data: any) {
    this.selected.setValue(data);
    if (data === 1 && !this.taraCargada) {
      // console.log('TAB ENVASE ENTRADA');
      this.cargarListaEnvases();
    }
    if (data === 2 && !this.taraCargada) {
      // console.log('TAB ENVASE SALIDA');
      this.cargarListaEnvases();
    }

    if (data === 0) {
      // console.log('TAB RECEPCION');
      this.cargarLineasRecepcion();
    }
    if (data === 1) {
      // console.log('TAB ENVASE ENTRADA');
    }
    if (data === 2) {
      // console.log('TAB ENVASE SALIDA');
    }
    if (data === 3) {
      // console.log('TAB DESTARE');
      if (!this.verificarEnvasesSalExisten()) {
        let msg = 'No ha agregado envases de salida, ¿Desea agregarlos?';
        let action = 'Agregar';
        let snack = this.snackBar.open(msg, action, { duration: 5000 });
        snack.afterDismissed().toPromise().then(ele => {
          if (ele.dismissedByAction) {
            this.selected.setValue(2);
          }
        });
      }
    }
  }

  ////////////////////////
  // CABECERA RECEPCION //
  ////////////////////////
  obtenerParametrosEncabezado() { //OBTIENE LOS PARAMETROS DE LA PANTALLA ANTERIOR
    this.params.queryParams.subscribe(result => {
      // console.log('OBTIENE DATOS DE LA PANTALLA ANTERIOR',result);
      if (!result.id && !result.frigorifico) {
        this.editandoRecepcion = false;
        return;
      }
      this.idRecepcion = result.id;
      // console.log('NUMERO RECEPCION', this.idRecepcion);
      this.numeroRecepcion = result.id;
      // this.obtenerEncabezadoEnvaseSalida();
      this.idGuia = result.id;
      this.frigorifico = result.frigorifico;
      this.f = result.frigorifico;
      this.editandoRecepcion = true;
      this.cargarLineasRecepcion();
      this.cargarEnvasesEntrada();
      this.wsLogistica.obtenerListaEnvaseSalida(this.exportadora, result.frigorifico, this.temporada, this.zona, result.id)
        .subscribe((result) => {
          // console.log('RESPONSE ENVASES SALIDA', result);
          this.arrEnvasesSalida = [];
          this.dataSourceEnvasesSalida = new MatTableDataSource(this.arrEnvasesSalida);
          result.lista.items.forEach(element => {
            let data = element;
            data['envaseCosecha'] = element.envaseCosecha,
              data['nombre'] = element.envaseCosecha.nombre,
              data['tara'] = element.pesoTara,
              data['cantEnvasesUni'] = element.cantidadEmpresa,
              data['cantEnvasesOtros'] = element.cantidadOtros,
              data['cantEnvases'] = element.cantidadTotal,
              data['num_guia'] = element.envasesINOUT.id,
              data['id'] = element.envaseCosecha.id
            this.guiaProductor = element.envasesINOUT.id
            this.arrEnvasesSalida.push(data);
          });
          // console.log('ARREGLO ENVASES VACIOS', this.arrEnvasesSalida);
          this.dataSourceEnvasesSalida = new MatTableDataSource(this.arrEnvasesSalida);
          // this.pesoPromedio();
        });
      this.obtenerEncabezado();
    });
  }

  obtenerEncabezado() { //OBTIENE EL ENCABEZADO DESDE LA API
    this.huertos = [];
    this.secciones = [];
    this.balanzas = [];
    this.wsLogistica.obtenerEncabezado(this.exportadora, this.frigorifico, this.temporada, this.zona, this.idRecepcion)
      .subscribe((result) => {
        // console.log('RESPONSE ENCABEZADO API', result);
        this.rut = result.enc.empresa.id;
        this.idGuia = result.enc.id;
        this.idRecepcion = result.enc.id;
        this.productor = result.enc.empresa.nombre;
        this.rescatarProductorIntencion(this.rut);
        this.productorIntencion = result.enc.zonaProductor.id.trim() + '-' + result.enc.huerto.id.trim();
        this.huerto = result.enc.huerto.id;
        this.huertoDireccion = result.enc.huerto.direccion;
        this.telefonoEmpresa = result.enc.empresa.telefono;
        this.comunaInforme = result.enc.huerto.comuna.nombre;
        this.telefonoInforme = result.enc.empresa.telefono;
        this.nombreHuerto = result.enc.huerto.nombre;
        this.temporada = result.enc.temporada.id;
        this.zona = result.enc.zonaProductor.id;
        this.csg = result.enc.huerto.codCSG;
        this.modoPesaje = result.enc.idPesa.toString();
        this.mostrarPeso = this.modoPesaje.toString();
        this.pesoCamion = result.enc.pesoBrutoCamion;
        this.pesoCarro = result.enc.pesoBrutoCarro;
        this.seccion = result.enc.codSeccion;
        this.pesoBruto = result.enc.pesoBrutoTotal;
        this.pesoBrutoTotal = result.enc.pesoBrutoTotal;
        this.wsCommon.cargarHuertos(result.enc.huerto.id.trim(), this.temporada, this.zona)
          .subscribe((result) => {
            // console.log('RESPONSE HUERTOS CABECERA', result);
            this.huertos = result.items;
          }, (error) => {
            // console.error('ERROR CARGANDO HUERTOS', error);
          });
        this.encabezadoGuardado = true;
        this.actualizarEncabezado = true;
        this.guiaProductor = result.enc.idGuiaProductor;
        this.frigorifico = result.enc.frigorifico.id;
        this.empresaSeleccionada = result.enc.huerto.id.trim();
        this.zonaSeleccionada = result.enc.zonaProductor.id.trim();
        this.wsCommon.cargarSeccion(this.temporada, this.zona, this.frigorifico)
          .subscribe((result) => {
            // console.log('RESPONSE SECCION', result);
            this.secciones = result.items;
          });
          // this.temporada,
        this.wsCommon.cargarBalanzas(this.seccion,  this.zona, this.frigorifico)
          .subscribe((result) => {
            // console.log('RESPONSE BALANZA', result);
            this.balanzas = result.items;
          });
        // this.fechaCosecha = result.enc.fechaCosecha
        // this.fechaDespacho = result.enc.fechaDespacho
        // this.fechaRecepcion = result.enc.fechaRecepcion,
        this.chofer = result.enc.chofer.id;
        this.nombreChofer = result.enc.chofer.nombre;
        this.patenteCamion = result.enc.patenteCamion;
        this.patenteCarro = result.enc.patenteCarro;
        this.tipoPesaje = result.enc.idPesa;
        this.pesoEnvase = result.enc.pesoEnvasesIn;
        if (result.enc.pesoTaraTotal > 0) {
          this.validaIngresaLinea = true;
        } else {
          this.validaIngresaLinea = false;
        }
        this.muestraTabs = true;
        this.cargarCalculosGenerales();
      }, (error) => {
        console.error('ERROR AL OBTENER CABECERA', error);
        this.snackBar.open('Error al obtener cabecera de recepción', null, { duration: 4000 });
        this.muestraTabs = false;
      });
  }

  guardarCabeceraRecepcion() { //METODO GUARDA ENCABEZADO RECEPCION
    let pesoEnvaseSalidaEnvio = 0;
    let now = new Date();
    let f_recepcion = new Date(this.fechaRecepcion).toISOString();
    let f_cosecha = new Date(this.fechaCosecha).toISOString();
    let f_despacho = new Date(this.fechaDespacho).toISOString();
    let f_destare = new Date(now.toString()).toISOString();
    if (parseInt(this.modoPesaje.toString(), 10) == 2) {
      ELEMENT_DATA.forEach(element => {
        this.pesoBrutoTotal = 0;
        this.pesoBrutoTotal += element.pesoBruto;
      });
    }
    pesoEnvaseSalidaEnvio = 0;
    this.arrEnvasesSalida.forEach(element => {
      pesoEnvaseSalidaEnvio += element.tara * element.cantEnvases;
    });
    let data = {
      id: this.idRecepcion, // NUM_RECEPCION_RECEPCION
      idGuiaProductor: this.guiaProductor, // NUM_GUIAPROD
      fechaCosecha: f_cosecha,
      fechaRecepcion: f_recepcion,
      fechaDespacho: f_despacho,
      fechaDestare: f_destare,
      patenteCamion: this.patenteCamion,
      patenteCarro: this.patenteCarro,
      chofer: { id: chileanRut.unformat(this.chofer) },
      idPesa: parseInt(this.modoPesaje.toString(), 10), // modoPesaje
      zona: { id: this.zona },
      temporada: { id: localStorage.getItem('temporada') },
      empresa: { id: this.rutReference.nativeElement.value },
      huerto: { id: this.huerto },
      frigorifico: { id: this.frigorifico },
      pesoBrutoCamion: this.pesoCamion,
      pesoBrutoCarro: this.pesoCarro,
      pesoBrutoTotal: this.pesoBrutoTotal,
      pesoEnvasesIn: this.pesoEnvase,
      pesoEnvasesOut: pesoEnvaseSalidaEnvio,
      pesoTaraCamion: 0,
      pesoTaraCarro: 0,
      pesoTaraTotal: this.totalTara(),
      pesoNeto: this.totalPesoNeto(),
      // fechaDestare: f_recepcion,
      glosa: '',
      codSeccion: this.seccion,
      terminada: false,
      usuario: { id: localStorage.getItem('User') },
      exportadora: { id: localStorage.getItem('exportadora') },
      codTransaccionProceso: 'A',
      zonaProductor: { id: this.zona }
    };
    let carga = this.snackBar.open('Procesando...', null, { duration: 0 });
    if (this.actualizarEncabezado) {
      this.wsLogistica.actualizarEncabezado(data) //ACTUALIZA ENCABEZADO
        .subscribe((result) => {
          // console.log('RESPONSE ACTUALIZA ENCABEZADO', result);
          carga.dismiss();
          this.actualizarEncabezado = true;
          this.snackBar.open(' ✔  Encabezado actualizado correctamente', null, { duration: 3000 });
          this.obtenerEncabezado();
          this.muestraTabs = true;
        }, (error) => {
          console.error('ERROR AL ACTUALIZAR ENCABEZADO', error);
          carga.dismiss();
          this.muestraTabs = false;
          this.snackBar.open('Error al actualizar encabezado ', null, { duration: 4000 });
        });
    } else {
      this.wsLogistica.guardarEncabezado(data) //CREA NUEVO ENCABEZADO
        .subscribe((result) => {
          // console.log('RESPONSE CREA ENCABEZADO', result);
          carga.dismiss();
          this.idGuia = result.guiaRespuesta;
          this.idRecepcion = result.guiaRespuesta;
          this.actualizarEncabezado = true;
          this.encabezadoGuardado = true;
          this.deshabilitarBtnPesoEnt = true;
          this.muestraTabs = true;
          this.snackBar.open(' ✔  Encabezado creado correctamente', null, { duration: 2000 });
        }, (error) => {
          console.error('ERROR AL CREAR CABECERA', error);
          carga.dismiss();
          this.muestraTabs = false;
          this.snackBar.open('Error al crear cebecera', null, { duration: 4000 });
        });
    }
    this.cargarCalculosGenerales();
  }

  rescatarZona() {
    const temporada = localStorage.getItem('temporada');
    const planta = localStorage.getItem('planta');
    this.wsCommon.cargarZona(temporada, planta)
      .subscribe((result) => {
        // console.log('RESPONSE ZONA', result);
        this.zona = result.items[0].id;
        this.obtenerParametrosEncabezado();
        this.rescatarFrigorifico();
        this.obtenerInformeSII();
        this.obtenerEncabezadoEnvaseSalida();
        this.cargarCalculosGenerales();
      }, (error) => {
        console.error('ERROR AL RESCATAR', error);
      });
  }

  rescatarFrigorifico() {
    const empresa = localStorage.getItem('exportadora');
    this.wsCommon.cargarFrigorifico(empresa, this.zona)
      .subscribe((result) => {
        this.frigorificos = result.items;
      }, (error) => {
        console.error('ERROR AL RESCATAR FRIGORIFICO', error);
      });
  }

  cargarCalculosGenerales() {
    this.calculosGenerales = [{
      PesoBrutoEntrada: 0, PesoBrutoSalida: 0, PesoEnvasesEntrada: 0, PesoEnvasesSalida: 0,
      PesoNetoEntrada: 0, PesoNetoSalida: 0, PesoPromedio: 0, CantTotalEnvasesEntrada: 0,
    }];
    if (this.idRecepcion) {
      let req = {
        CodExportadora: localStorage.getItem('exportadora'),
        CodPlanta: this.frigorifico,
        CodTemp: localStorage.getItem('temporada'),
        CodZona: this.zona,
        NumRecep: this.idRecepcion
      };
      // console.log(req)
      this.wsLogistica.obtenerCalculosGenerales(req)
        .subscribe((data) => {
          // console.log('CALCULOS GENERALES', data);
          // console.log(data);
          if (data.valido) {
            this.calculosGenerales = data.enc;
            if (this.calculosGenerales.PesoEnvasesSalida > 0) {
              let req = {
                num_recep: this.idRecepcion,
                zona: this.zona,
                frigorifico: this.frigorifico,
                temporada: this.temporada,
                TotalPesoEnvSalida: this.calculosGenerales.PesoEnvasesSalida,
                TotalEnvasesEntrada: this.calculosGenerales.CantTotalEnvasesEntrada,
                exportadora: this.exportadora
              };
              this.wsLogistica.detalleGuiaRecepcionEnvaseSalida(req)
                .subscribe((response) => {
                  // console.log('RESPONSE PATCH ENVASE SALIDA', response);
                }, (error) => {
                  console.error('ERROR PATCH ENVASE SALIDA', error);
                });
            }
          }
          // console.log('RESPONSE CALCULOS GENERALES', data);
        }, (error) => {
          this.calculosGenerales = [{
            PesoBrutoEntrada: 0, PesoBrutoSalida: 0, PesoEnvasesEntrada: 0, PesoEnvasesSalida: 0,
            PesoNetoEntrada: 0, PesoNetoSalida: 0, PesoPromedio: 0
          }];
          console.error('ERROR OBTENER CALCULOS GENERALES', error);
        });
    }
  }

  cambioModoPesaje(event: any) {
    this.ingresoPeso = '';
    setTimeout(() => {
      this.refIngresoPeso.nativeElement.focus();
    });
    if (event == 1) {
      this.checkCamion = true;
    } else if (event == 2) {
      this.checkCamion = false;
    }
  }

  cambioPeso(peso: any) {
    if (this.checkCamion) {
      this.pesoCamion = parseInt(peso.target.value, 10);
    } else {
      this.pesoCarro = parseInt(peso.target.value, 10);
    }
    this.pesoBrutoTotal = this.pesoCamion + this.pesoCarro;
    if (this.pesoBrutoTotal) {
      if (this.pesoBrutoTotal > 0) {
        this.totalValida = false;
      } else {
        this.totalValida = true;
      }
    } else {
      this.totalValida = true;
    }
    this.validadorCampos();
  }

  formatoRut(rut: string) {
    this.wsCommon.cargarEmpresa(rut)
      .subscribe((result) => {
        this.productor = result.nombre;
        this.rescatarProductorIntencion(result.id);
      }, (error) => {
        console.error('ERROR AL CARGAR EMPRESA', error);
      });
  }

  rescatarProductorIntencion(rut: string) {
    const temporada = localStorage.getItem('temporada');
    this.wsLogistica.cargarZonaProductor(rut, temporada)
      .subscribe((result) => {
        this.productorIntenciones = result.items;
      }, (error) => {
        console.error('ERROR CARGAR ZONA PRODUCTOR', error);
      });
  }

  cargarHuertos(empresa: any, temporada: any, zona: any) {
    this.huertos = [];
    this.wsCommon.cargarHuertos(empresa, temporada, zona)
      .subscribe((result) => {
        // console.log('RESPONSE HUERTOS', result);
        this.huertos = result.items;
        this.rescatarZona();
      }, (error) => {
        console.error('ERROR CARGANDO HUERTOS', error);
      });
  }

  selectProductoresInt(data: any) {
    // console.log(data);
    const info = data.value.split('-');
    this.empresaSeleccionada = info[1];
    const temporada = localStorage.getItem('temporada');
    this.zonaSeleccionada = info[0];
    this.cargarHuertos(this.empresaSeleccionada, temporada, this.zonaSeleccionada);
  }

  obtenerFrigo(frigorifico: string) {
    this.frigorificoInforme = frigorifico;
  }

  selFrigorifico(data: any) {
    this.cargarSeccion();
  }

  cargarSeccion() {
    let temporada = localStorage.getItem('temporada');
    this.wsCommon.cargarSeccion(temporada, this.zona, this.frigorifico)
      .subscribe((result) => {
        // console.log(result);
        this.secciones = result.items;
      });
  }

  selSeccion(data: any) {
    this.cargarBalanzas();
  }

  cargarBalanzas() {
    let temporada = localStorage.getItem('temporada');
    this.wsCommon.cargarBalanzas(this.seccion, this.zona, this.frigorifico)
      .subscribe((result) => {
        this.balanzas = result.items;
      });
  }

  selBalanza(data: any) {
    // console.log(this.balanza);
    this.validadorCampos();
  }

  selectHuerto(data: any) {
    const h = this.huertos.find(element => {
      return element.id === data;
    });
    this.csg = h.codCSG;
  }

  verificarChofer() {
    if (this.chofer) {
      if (this.chofer.length === 9) {
        var dv = this.chofer.substring(8);
        this.chofer = chileanRut.format(this.chofer, dv);
        this.validarRutChofer(this.chofer);
      } else if (this.chofer.length === 8) {
        var dv = this.chofer.substring(7);
        this.chofer = chileanRut.format(this.chofer, dv);
        this.validarRutChofer(this.chofer);
      } else {
        this.snackBar.open('El largo del rut no corresponde', null, { duration: 800 })
        this.chofer = '';
        this.nombreChofer = '';
        return;
      }
    }

    if (this.chofer) {
      this.wsCommon.validadorRut(chileanRut.unformat(this.chofer))
        .subscribe((result) => {
          if (result.valido) {
            this.choferNoEncontrado = true;
            this.wsCommon.cargarChofer(chileanRut.unformat(this.chofer)).subscribe(result => {
              this.nombreChofer = result.nombre;
              this.choferNoEncontrado = false;
            }, err => {
              this.choferNoEncontrado = true;
              this.nombreChofer = '';
            });
          } else {
            this.chofer = '';
            this.nombreChofer = '';
            this.snackBar.open(result.mensaje, null, { duration: 2000 });
          }
        });
    }
  }

  validarRutChofer(rut: string) {
    if (!chileanRut.validate(rut)) {
      this.snackBar.open('Rut inválido', null, { duration: 1000 });
      this.chofer = '';
      this.nombreChofer = '';
    }
  }

  agregarChofer() {
    const data = { id: chileanRut.unformat(this.chofer), nombre: this.nombreChofer };
    this.wsCommon.agregarChofer(data)
      .subscribe((result) => {
        // console.log('RESPONSE AGREGAR CHOFER', result);
        this.choferNoEncontrado = false;
      }, (err) => {
        console.error('ERROR AGREGAR CHOFER', err);
        this.choferNoEncontrado = true;
      });
  }

  validaPatente(patente: string) {
    if (patente.length > 0) {
      this.patenteValida = false;
    } else {
      this.patenteValida = true;
    }
  }

  patenteUpper() {
    this.patenteCamion = this.patenteCamion.toUpperCase();
    if (this.patenteCamion && this.temporada && this.frigorifico && this.exportadora && this.zona) {
      let req = {
        CodTemp: this.temporada,
        CodPlanta: this.frigorifico,
        CodExportadora: this.exportadora,
        CantRegistro: 50,
        Pagin: 1,
        CodZona: this.zona,
        Patente: this.patenteCamion
      };
      this.wsLogistica.validaCamionEnPlanta(req)
        .subscribe((response) => {
          // console.log('RESPONSE VALIDA', response);
          if (!response.valido) {
            // this.patenteCamion = null;
            this.refPatente.nativeElement.focus();
            this.snackBar.open(response.mensaje, null, { duration: 2000 });
          }
        }, (error) => {
          console.error('ERROR AL OBTENER VALIDACION CAMION', error);
          if (!error.error.valido) {
            // this.patenteCamion = null;
            this.refPatente.nativeElement.focus();
            this.snackBar.open(error.error.mensaje, null, { duration: 4000 });
          }
        });
    } else {
      this.patenteCamion = null;
      this.snackBar.open('Faltan parámetros para validar', null, { duration: 2000 });
    }
    this.patenteCarro = this.patenteCarro.toUpperCase();
  }

  /////////////////////////////
  // TAB 0 DETALLE RECEPCION //
  /////////////////////////////
  cargarLineasRecepcion() {
    ELEMENT_DATA = [];
    ELEMENT_DATA_DESTARA = [];
    this.wsLogistica.obtenerListaRecepcionlinea(this.exportadora, this.frigorifico, this.temporada, this.zona, this.idRecepcion) // VARIABLE F SE SETEA EN 0 se cambio
      .subscribe((resultLineas) => {
        // console.log('RESPONSE LINEA', resultLineas);
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        // console.log('DATA SOURCE', this.dataSource);
        this.pesoEnvase = 0;
        this.pesoBrutoSalida = 0;
        resultLineas.lista.items.forEach(element => {
          let data = element;
          data['especieNombre'] = element.especie.nombre,
            data['variedadNombre'] = element.variedad.nombre,
            data['sdp'] = element.SDP,
            data['pesoBruto'] = element.pesoBruto,
            data['cantEnvases'] = element.numEnvasesCont,
            data['envaseCosecha'] = element.envaseCosecha,
            data['calculoNeto'] = element.KilosNetosOut,
            data['promedio'] = element.KilosNetosOut / element.numEnvasesCont,
            data['linea'] = element.numLinea
          data['destare'] = element['destare'] ? element['destare'] : 0;
          data['pesoEnvaseSalida'] = 0;
          data['tara'] = 0
          data['pesoTara'] = data['pesoTara']
          data['pesoEnvases'] = element.pesoEnvasesIn;
          data['folioCKU'] = element.folioCKU
          data['tipoDucha'] = element.tipoDucha.nombre
          data['tipoFrio'] = element.tipoFrio.nombre
          data['condicion'] = element.condicion.nombre
          data['preFrio'] = element.preFrio
          data['condicionId'] = element.condicion.id
          data['tipoDuchaId'] = element.tipoDucha.id
          data['tipoFrioId'] = element.tipoFrio.id
          data['snapAptitud'] = element.snapAptitud
          ELEMENT_DATA.push(data);
          this.lineasIngresadas.push(data);
          const data_ = {
            especie: element.especie.nombre,
            variedad: element.variedad.nombre,
            pesoDestare: element.pesoTara,
            pesoTara: element.pesoTara,
            cantidadEnvases: element.cantEnvases,
            linea: element.linea
          };
          if (element.pesoTara > 0) {
            ELEMENT_DATA_DESTARA.push(data_);
          }
        });
        // FIN FOREACH
        // console.log('ELEMENT DATA', ELEMENT_DATA);
        this.listaDestare();
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        // console.log('DATA SOURCE', this.dataSource);

        this.ELEMENT_DATA_ = ELEMENT_DATA;
        this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
        // console.log(this.tipoPesaje.toString());
        ELEMENT_DATA.forEach(element => {
          this.pesoEnvase = this.pesoEnvase + element.pesoEnvasesIn;
          this.pesoBrutoSalida = this.pesoBrutoSalida + element.pesoTara;
          if (parseInt(this.tipoPesaje.toString()) === 2) {
            this.obtenerEncabezado();
          }
        });
      }, (error) => {
        console.error('ERROR AL OBTENER LINEA', error);
        this.dataSource = new MatTableDataSource([]);
      });
    this.cargarCalculosGenerales();
  }

  agregarNuevaLinea() {
    if (!this.validaIngresaLinea) {
      let ultimaLinea = 1;
      if (ELEMENT_DATA.length > 0) {
        ultimaLinea = ELEMENT_DATA[ELEMENT_DATA.length - 1].linea + 1;
      }
      const dialogRef = this.dialog.open(PesoBrutoComponent, {
        disableClose: true,
        width: '600px',
        data: {
          mostrarPeso: this.mostrarPeso,
          elementoPesado: this.elementoPesado,
          pesoVariedadRomanaCamionCarro: this.pesoBruto,
          huerto: this.huerto,
          editando: false,
          ambiente: {
            zonaSeleccionada: this.zonaSeleccionada,
            productor: this.empresaSeleccionada,
            zona: localStorage.getItem('planta'),
            temporada: localStorage.getItem('temporada'),
            frigorifico: this.frigorifico,
            rutEmpresa: this.rutReference.nativeElement.value,
            idGuia: this.idGuia,
            guiaProductor: this.guiaProductor,
            productorNombre: this.productor,
            linea: ultimaLinea,
            fechaCosecha: this.fechaCosecha,
            controldepeso: true,
            isActive: false,
            seccion: this.seccion,
            csg: this.csg,
            nombreHuerto: this.refInputHuerto.triggerValue,
            patenteCamion: this.patenteCamion,
            patenteCarro: this.patenteCarro,
            chofer: chileanRut.unformat(this.chofer),
            fechaRecepcion: this.fechaRecepcion,
            fechaDespacho: this.fechaDespacho
          }
        }
      });
      dialogRef.afterClosed()
        .subscribe(result => {
          // console.log('RESULTADO AGREGAR LINEA', result);
          if (this.mostrarPeso === '2' && result.tipoPeso) { /// por variedad - romana
            // console.log('RESPUESTA AGREGA LINEA ROMANA', result);
            this.lineasIngresadas.push(result);
            this.tipoPesaje = 2;
            this.pesoVariedadRomanaCamionCarro += parseInt(result.otros.pesoVariedadRomanaCamionCarro, 10);
            this.pesoBruto = result.otros.pesoVariedadRomanaCamionCarro;
            this.pesoBrutoTotal = result.otros.pesoVariedadRomanaCamionCarro;
            this.elementoPesado = result.otros.elementoPesado;
            this.calculoVariedadRomana(result);
          }
          if (this.mostrarPeso === '2' && !result.tipoPeso) { /// por variedad - pesa
            // console.log('RESPUESTA AGREGA LINEA PESA', result);
            let ind_editar = this.dataVariedadPesa.findIndex(element => {
              return element.linea == result.dataVariedadPesa.id;
            });
            // console.log('INDICE EDITAR', ind_editar);
            if (ind_editar >= 0) {
              this.dataVariedadPesa[ind_editar] = result.dataVariedadPesa;
            } else {
              this.dataVariedadPesa.push(result.dataVariedadPesa);
            }
            result.resultadoVariedadPesa.forEach(element => {
              let arr_nuevo = element.data;
              this.lineasIngresadas.push(element);
              const ind = arr_nuevo.findIndex(elementVP => {
                return elementVP.esContenedor === true;
              });
              if (this.lineasIngresadas.length === 1) {
                // this.lineasIngresadas[ 0 ].data[ind]['linea'] = 1
              } else { }
              // this.calcularLineasVariedadPesa(  arr_nuevo[ind] )
            });
            this.calcularLineasVariedadPesa(result.variedadPesa);
            this.listarEnvasesEntradaVariedadPesa(result.variedadPesa.envases);
            this.pesoBrutoTotal = 0;
            this.pesoEnvase = 0;
            this.pesoEnvaseSalida = 0;
            ELEMENT_DATA.forEach(element => {
              this.pesoBrutoTotal += element.pesoBruto;
            });
            this.arrEnvasesEntrada.forEach(element => {
              this.pesoEnvase += (element.cantEnvases * element.tara);
            });
            this.arrEnvasesSalida.forEach(element => {
              this.pesoEnvase += (element.cantEnvases * element.tara);
            });
            this.actualizarEncabezado = true;
            this.modoPesaje = '3';
            this.guardarCabeceraRecepcion();
            this.tipoPesaje = 2;
            this.cargarCalculosGenerales();
          }
          if (this.mostrarPeso === '1') { /// por especie
            console.log('LINEA POR ESPECIE', result);
            this.lineasIngresadas.push(result);
            this.tipoPesaje = 1;
            this.calculoEspecie(result);
          }
        });
    } else {
      this.snackBar.open('La recepción ya tiene destare', null, { duration: 3000 });
    }
  }

  async eliminarLineaDetalle(linea: any) {
    // console.log('LINEA A ELIMINAR', linea);

    if (this.modoPesaje.toString() === '1') {
      const index = ELEMENT_DATA.findIndex(res => {
        return res['linea'] === linea.linea;
      });
      let data = {
        NumRecep: this.idGuia,
        CodZona: this.zona,
        CodFrigorifico: this.frigorifico,
        NumLinea: linea.linea,
        CodExportadora: localStorage.getItem('exportadora'),
        CodTemp: localStorage.getItem('temporada'),
        PesoEnvaseEntrada: linea.pesoEnvasesIn
      }
      this.eliminarDestareEspecie(linea);
      this.wsLogistica.eliminarLinea(data)
        .subscribe((result) => {
          // console.log('RESPONSE ELIMINAR LINEA', result);
          this.snackBar.open(' ✔  Línea eliminada correctamente', null, { duration: 2000 });
          let ln = this.arrEnvasesEntrada.filter(element => {
            return element.linea == linea.linea;
          });
          let restaPesoEnvases = 0;
          ln.forEach(async element => {
            restaPesoEnvases += element['tara'] * element['cantEnvases'];
          });
          this.pesoEnvase -= restaPesoEnvases;
          this.cargarEnvasesEntrada();
          //////////////////////////////////// RECALCULAR //////////////////////////////////77
          ELEMENT_DATA.splice(index, 1);
          this.sumaTotalEnvases = 0;
          ELEMENT_DATA.forEach((element, index) => {
            let totalEnvases = element.cantEnvases;
            this.sumaTotalEnvases += totalEnvases;
          });
          ELEMENT_DATA.forEach((element, index) => {
            element['pesoBruto'] = this.pesoBrutoTotal / this.sumaTotalEnvases * element['cantEnvases']
            element['destare'] = element['destare'] ? element['destare'] : 0;
            element['calculoNeto'] = element['pesoBruto'] - element['destare'] -
              (element['pesoEnvases'] - element['pesoEnvaseSalida']);
            let promedio = element['calculoNeto'] / element['cantEnvases'];
            element['promedio'] = promedio;
          });
          ////////////////////////////////////////////////////////////////
          this.actualizarLineasEspecieEditar();
        }, (error) => {
          console.error('ERROR A GUARDAR LINEA', error);
          this.snackBar.open(' Error Al Eliminar', null, { duration: 2000 });
        });
    }

    if (this.modoPesaje.toString() === '2') {
      let index = ELEMENT_DATA.findIndex(res => {
        return res['linea'] === linea.linea;
      })
      index = ELEMENT_DATA.findIndex(res => {
        return res['linea'] === linea.linea;
      });
      let data = {
        NumRecep: this.idGuia,
        CodZona: this.zona,
        CodFrigorifico: this.frigorifico,
        NumLinea: linea.linea,
        CodExportadora: localStorage.getItem('exportadora'),
        CodTemp: localStorage.getItem('temporada')
      };
      this.wsLogistica.eliminarLinea(data)
        .subscribe((result) => {
          // console.log('RESPONSE ELIMINAR LINEA', result);
          this.snackBar.open(' ✔  Eliminado Correctamente', null, { duration: 2000 });
          this.recalcularEliminarLineaVariedadRomana(index)
          this.guardarCabeceraRecepcion();
        }, (error) => {
          console.error('ERROR AL ELIMINAR LINEA', error);
          this.snackBar.open(' ✔  Error Al Eliminar', null, { duration: 2000 });
        });
    }
    if (this.modoPesaje.toString() === null) {
      const index = this.lineasIngresadas.findIndex(res => {
        return res.data[0]['linea'] === linea.linea;
      });
      let data = {
        NumRecep: this.idGuia,
        CodZona: this.zona,
        CodFrigorifico: this.frigorifico,
        NumLinea: linea.linea,
        CodExportadora: localStorage.getItem('exportadora'),
        CodTemp: localStorage.getItem('temporada')
      };
      this.wsLogistica.eliminarLinea(data)
        .subscribe((result) => {
          // console.log('RESPONSE ELIMINAR LINEA', result);
          this.recalcularEliminarLineaVariedadPesa(index);
          this.snackBar.open(' ✔  Eliminado Correctamente', null, { duration: 2000 });
        }, (error) => {
          console.error('ERROR ELIMINAR LINEA', error)
          this.snackBar.open(' ✔  Error Al Eliminar', null, { duration: 2000 });
        });
    }
  }

  guardarDetalle(dataDetalle: any, idContendor: any) {
    const f_cosecha = new Date(this.fechaCosecha).toISOString();
    console.log('DATA DETALLE', dataDetalle);
    let folioCKU = 0;
    let tipoDuchaID = '00';
    let tipoFrioID = '00';
    let preFrio = false;
    let snapAptitud = '00';
    let condicionID = '00';
    let codCuartel = '2';
    console.log('DETALLE RECATADO', dataDetalle);
    if (this.lineasIngresadas['folioCKU'] === undefined) {
      this.wsLogistica.obtenerListaRecepcionlinea(this.exportadora, this.frigorifico, this.temporada, this.zona, this.idRecepcion)
        .subscribe((resultLineas) => {
          // console.log('RESPONSE METODO TRUCHO', this.detalleUpdate);
          this.detalleUpdate = resultLineas.lista.items;
          folioCKU = this.detalleUpdate[0]['folioCKU'];
          tipoDuchaID = this.detalleUpdate[0]['tipoDucha']['id'];
          tipoFrioID = this.detalleUpdate[0]['tipoFrio']['id'];
          preFrio = this.detalleUpdate[0]['preFrio'];
          snapAptitud = this.detalleUpdate[0]['snapAptitud'];
          condicionID = this.detalleUpdate[0]['condicion']['id'];
          codCuartel = this.detalleUpdate[0]['codCuartel'];
        }, (error) => {
          console.error('NO FUNCIONO METODO TRUCHO', error);
        });
    }
    let pesoTara = 0;
    if (dataDetalle.Pesotara === undefined) {
      pesoTara = 0;
    } else {
      pesoTara = dataDetalle.Pesotara;
    }

    this.dataLinea = {
      id: this.idGuia,
      zona: { id: this.zona },
      temporada: { id: localStorage.getItem('temporada') },
      frigorifico: { id: this.frigorifico },
      codCuartel: codCuartel,
      huerto: { id: this.huerto },
      empresa: { id: this.rutReference.nativeElement.value },
      variedad: { idsnap: dataDetalle.variedad },
      especie: { idsnap: dataDetalle.especie },
      fechaCosecha: f_cosecha,
      pesoBruto: dataDetalle.pesoBruto,
      pesoTara: dataDetalle.Pesotara,
      pesoEnvasesIn: dataDetalle.pesoEnvasesIn,
      pesoEnvasesOut: dataDetalle.pesoEnvasesOut,
      envaseCosecha: { id: dataDetalle.codigoEnvase },
      numEnvasesCont: dataDetalle.cantidadEnvases,
      numLinea: dataDetalle.linea,
      exportadora: { id: localStorage.getItem('exportadora') },
      cuentaCorriente: false,
      folioCKU: folioCKU,
      tipoDucha: { id: tipoDuchaID },
      tipoFrio: { id: tipoFrioID },
      preFrio: preFrio,
      kilosNetosIn: dataDetalle.pesoNeto,
      KilosNetosOut: dataDetalle.pesoNeto,
      snapAptitud: snapAptitud,
      contenedor: { id: idContendor },
      condicion: { id: condicionID }
    };
  }

  actualizarLineasEspecie(dataDetalle: any) { //ESTE DEJA LA CAGA
    // console.log('LO QUE LLEGA EN LOS ENVASES', dataDetalle);
    let f_cosecha = new Date(this.fechaCosecha).toISOString();
    let folioCKU = 0;
    let tipoDuchaID = '00';
    let tipoFrioID = '00';
    let preFrio = false;
    let snapAptitud = '00';
    let condicionID = '00';
    let codCuartel = '2';
    this.wsLogistica.obtenerListaRecepcionlinea(this.exportadora, this.f, this.temporada, this.zona, this.idRecepcion)
      .subscribe((resultLineas) => {
        // console.log('RESPONSE LISTA RECPEPCION LINEA', this.detalleUpdate);
        this.detalleUpdate = resultLineas.lista.items;
        if (dataDetalle.folioCKU === 0) { // SI EL FOLIO VIENE 0 LO LLENA CON EL METODO
          folioCKU = this.detalleUpdate[0]['folioCKU'];
        } else {
          folioCKU = dataDetalle.folioCKU;
        }
        tipoDuchaID = this.detalleUpdate[0]['tipoDucha']['id'];
        tipoFrioID = this.detalleUpdate[0]['tipoFrio']['id'];
        preFrio = this.detalleUpdate[0]['preFrio'];
        snapAptitud = this.detalleUpdate[0]['snapAptitud'];
        condicionID = this.detalleUpdate[0]['condicion']['id'];
        codCuartel = this.detalleUpdate[0]['codCuartel'];
      }, (error) => {
        console.error('ERROR LISTA RECEPCION', error);
      });
    let reqDetalle = {
      id: this.idGuia,
      zona: { id: this.zona },
      temporada: { id: localStorage.getItem('temporada') },
      frigorifico: { id: this.frigorifico },
      codCuartel: codCuartel,
      huerto: { id: this.huerto },
      empresa: { id: this.rutReference.nativeElement.value },
      variedad: { idsnap: dataDetalle.variedad.id || dataDetalle.variedad },
      especie: { idsnap: dataDetalle.especie.id || dataDetalle.especie },
      fechaCosecha: f_cosecha,
      pesoBruto: dataDetalle.pesoBruto,
      pesoTara: dataDetalle.pesoTara,
      pesoEnvasesIn: dataDetalle.pesoEnvases,
      pesoEnvasesOut: dataDetalle.pesoEnvSalida,
      envaseCosecha: { id: dataDetalle.envaseCosecha.id },
      numEnvasesCont: dataDetalle.cantEnvases,
      numLinea: dataDetalle.linea,
      exportadora: { id: localStorage.getItem('exportadora') },
      cuentaCorriente: false,
      folioCKU: folioCKU,
      tipoDucha: { id: tipoDuchaID },
      tipoFrio: { id: tipoFrioID },
      preFrio: preFrio,
      kilosNetosIn: dataDetalle.calculoNeto,
      KilosNetosOut: dataDetalle.calculoNeto,
      snapAptitud: snapAptitud,
      contenedor: { id: dataDetalle['contenedor']['id'] },
      condicion: { id: condicionID }
    };
    this.wsLogistica.actualizaDetallePatch(reqDetalle) /// ESTE DEJABA EL PESO EN CERO, YA SE SOLUCIONO
      .subscribe((result) => {
        // console.log('RETORNA PATCH', result);
        this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
        // console.log(parseInt(this.tipoPesaje.toString()));
        if (parseInt(this.tipoPesaje.toString()) === 2) {
          this.pesoBrutoTotal = 0;
          ELEMENT_DATA.forEach(element => {
            this.pesoBrutoTotal += element.pesoBruto
          });
        }
        this.guardarCabeceraRecepcion();
        this.cargarLineasRecepcion();
        this.pesoBrutoSalida = 0;
        ELEMENT_DATA.forEach(element => {
          this.pesoBrutoTotal += element.pesoBruto
        });
      }, (error) => {
        console.error('ERROR AL ACTUALIZAR DETALLE', error);
        this.pesoBrutoSalida = 0;
        ELEMENT_DATA_DESTARA = JSON.parse(JSON.stringify(this.destare_aux));
        this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
        ELEMENT_DATA = JSON.parse(JSON.stringify(this.linea_aux))
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        ELEMENT_DATA_DESTARA.forEach(element => {
          this.pesoBrutoSalida += parseFloat(element.pesoDestare);
        });
      });
  }

  ///////////////////////////
  // TAB 1 ENVASES ENTRADA //
  ///////////////////////////
  cargarListaEnvases() { //CARGA EL LISTADO DE LOS ENVASES PARA AGREGAR ENTRADA/SALIDA
    this.wsCommon.cargarEnvasesLista()
      .subscribe((result) => {
        this.arr_envases = result.items;
        // console.log('RESPONSE CARGA ENVASES', result);
        this.arr_envases.forEach(async (element) => {
          let tara = await this.wsCommon.cargarPesoZona(this.zona, element.id).toPromise();
          if (tara.items.length > 0) {
            element['pesoEnvase'] = tara.items[0].pesoEstado;
            element['taraMaximo'] = tara.items[0].pesoMaximo;
            element['taraMinimo'] = tara.items[0].pesoMinimo;
          }
        });
        this.taraCargada = true;
      }, (error) => {
        console.error('ERROR AL CARGAR LISTA ENVASES', error);
        this.snackBar.open('Error al argar lista envases', null, { duration: 2000 });
      });
  }

  listarEnvasesEntrada(listaEnvases: any, detalle: any) {
    let aux_envaseEntrada = JSON.parse(JSON.stringify(this.arrEnvasesEntrada));
    this.guardarDetalle(detalle, listaEnvases.idContendor);
    let envSal = JSON.parse(JSON.stringify(listaEnvases.resultado));
    envSal.forEach(element => {
      const index = this.verificar_listarEnvasesEntrada(element.envaseCosecha.id);
      if (index >= 0) {
        const nuevaSuma = this.agrupar_listaEnvaseEntrada(index, element);
        this.arrEnvasesEntrada[index].cantEnvasesUni = nuevaSuma.uni;
        this.arrEnvasesEntrada[index].cantEnvasesOtros = nuevaSuma.otro;
        this.arrEnvasesEntrada[index].cantEnvases = nuevaSuma.uni + nuevaSuma.otro;
      } else {
        if (element.check) {
          this.arrEnvasesEntrada.push(element);
        }
      }
    });
    let arr_env_entrada = [];
    envSal.forEach(envEntrada => {
      let data = {
        id: this.idGuia,
        zona: { id: this.zona },
        temporada: { id: localStorage.getItem('temporada') },
        frigorifico: { id: this.frigorifico },
        linea: parseInt(detalle.linea),
        envaseCosecha: { id: envEntrada.envaseCosecha.id },
        cantidadEmpresa: parseFloat(envEntrada.cantEnvasesUni),
        cantidadOtros: parseFloat(envEntrada.cantEnvasesOtros),
        esContenedor: envEntrada.esContenedor,
        contenedor: { id: listaEnvases.idContendor },
        exportadora: { id: localStorage.getItem('exportadora') }
      };
      arr_env_entrada.push(data);
    });
    let carga = this.snackBar.open('Procesando...', null, { duration: 0 });
    this.wsLogistica.agregarEnvasesEntrada(this.dataLinea, arr_env_entrada)
      .subscribe((result) => {
        console.log('AGREGAR LINEA RESPONSE', result);
        carga.dismiss();
        this.snackBar.open(' ✔  Recepcion creada', null, { duration: 2000 });
        this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.arrEnvasesEntrada = aux_envaseEntrada;
        if (parseInt(this.modoPesaje.toString(), 10) == 2) {
          ELEMENT_DATA.forEach(element => {
            this.pesoBrutoTotal += element.pesoBruto
          });
        }
        this.guardarCabeceraRecepcion();
      }, (error) => {
        console.error('ERROR AL CREAR DETALLE RECEPCION ENVASES ENTRADA', error);
        carga.dismiss();
        this.snackBar.open(' Error al crear recepci´pn', null, { duration: 2000 });
      });
  }

  cargarEnvasesEntrada() {
    this.arrEnvasesEntrada = [];
    this.wsLogistica.obtenerListaEnvaseEntrada(this.exportadora, this.frigorifico, this.temporada, this.zona, this.idRecepcion)
      .subscribe((result) => {
        result.lista.items.forEach(element => {
          let data = element;
          data['envaseCosecha'] = element.envaseCosecha,
            data['tara'] = element.pesoTara,
            data['cantEnvasesUni'] = element.cantidadEmpresa,
            data['cantEnvasesOtros'] = element.cantidadOtros
          data['nombre'] = element.contenedor.nombre
          data['cantEnvases'] = element.cantidadEmpresa + element.cantidadOtros
          this.arrEnvasesEntrada.push(data);
        });
        this.arrEnvasesEntrada.forEach(async element => {
          let ps = await this.wsCommon.cargarPesoZona(this.zona, element.envaseCosecha.id).toPromise()
          if (ps.items.length >= 0) {
            element['tara'] = ps.items[0].pesoEstado
          }
        });
        this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
      }, (error) => {
        console.error('ERROR AL OBTENER ENVASES ENTRADA', error);
        this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
      });
  }

  agregarDetalleEnvaseEntrada() {
    let index = this.arr_envases.findIndex(element => {
      return element.id == this.numArrDetEnvEnt.toString()
    });
    let existe = this.arrEnvasesEntrada.findIndex(el => {
      return el.envaseCosecha.nombre == this.arr_envases[index].nombre
    });
    if (existe >= 0) {
      this.snackBar.open('El envase ya existe', null, { duration: 2000 });
      // console.log('ya existe envase')
      return;
    }
    let total = this.cantUnidetEnvaseEntrada + this.cantOtroDetEnvaseEntrada;
    let data = {
      envaseCosecha: { nombre: this.arr_envases[index].nombre, id: this.arr_envases[index].id },
      tara: this.pesoEnvaseDetEntrada,
      cantEnvasesUni: this.cantUnidetEnvaseEntrada,
      cantEnvasesOtros: this.cantOtroDetEnvaseEntrada,
      cantEnvases: total,
      idEnvase: this.arr_envases[index].id,
      num_guia: this.nGuiaUnifrutti,
      delete: true
    };
    let request = {
      guia: {
        id: this.idGuia,
        zona: { id: this.zona },
        temporada: { id: localStorage.getItem('temporada') },
        frigorifico: { id: this.frigorifico },
        exportadora: { id: localStorage.getItem('exportadora') }
      },
      envaseCosecha: { id: data.idEnvase },
      cantidad: parseFloat(this.cantUnidetEnvaseEntrada.toString()) + parseFloat(this.cantOtroDetEnvaseEntrada.toString()),
      cantidadEmpresa: parseFloat(this.cantUnidetEnvaseEntrada.toString()),
      cantidadOtros: parseFloat(this.cantOtroDetEnvaseEntrada.toString()),
      esContenedor: false
    };
    this.wsLogistica.agregarEnvasesEntradaIndividual(request)
      .subscribe((result) => {
        this.snackBar.open(' ✔  Envase Ingresado', null, { duration: 2000 });
        this.arrEnvasesEntrada.push(data);
        this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
        this.pesoEnvase = this.pesoEnvase + (total * this.pesoEnvaseDetEntrada);
      }, (error) => {
        console.error('ERROR AGREGAR ENTRADA INDIVIDUAL', error);
        this.snackBar.open(' Error al Ingresar', null, { duration: 2000 });
      });
    this.cargarCalculosGenerales();
  }

  eliminarEnvasesEntrada(data: any) {
    // console.log('ELIMINAR ENVASE ENTRADA', data);
    let index = this.arrEnvasesEntrada.findIndex(element => {
      return element.envaseCosecha.nombre.trim() == data.envaseCosecha.nombre.trim();
    });
    let id = this.arrEnvasesEntrada[index].num_guia;
    let env_cosecha = data.idEnvase;
    let exportadora = localStorage.getItem('exportadora');
    let zona = this.zona;
    let temporada = localStorage.getItem('temporada');
    this.wsLogistica.eliminarEnvaseEntrada(id, env_cosecha, exportadora, zona, temporada)
      .subscribe(result => {
        if (result.valido) {
          this.arrEnvasesEntrada.splice(index, 1);
          let PES = 0;
          this.arrEnvasesEntrada.forEach(element => {
            PES += (element.tara * element.cantEnvases);
          });
          let ultimo = ELEMENT_DATA.length - 1;
          ELEMENT_DATA[ultimo]['pesoEnvaseSalida'] -= (data.cantEnvases * data.tara)
          ELEMENT_DATA[ultimo]['destare'] = ELEMENT_DATA[ultimo]['destare'] + ELEMENT_DATA[ultimo]['pesoEnvaseSalida']
          ELEMENT_DATA[ultimo]['calculoNeto'] = ELEMENT_DATA[ultimo]['pesoBruto'] - ELEMENT_DATA[ultimo]['destare'] -
            (ELEMENT_DATA[ultimo]['pesoEnvases'] - ELEMENT_DATA[ultimo]['pesoEnvaseSalida']);
          ELEMENT_DATA[ultimo]['promedio'] = ELEMENT_DATA[ultimo]['calculoNeto'] / ELEMENT_DATA[ultimo]['total']
          this.pesoEnvase = PES;
          this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
          this.guardarCabeceraRecepcion();
        }
      }, error => {
        this.snackBar.open('Error al eliminar envase entrada', null, { duration: 2000 });
        console.error('ERROR ELIMINAR ENVASE ENTRADA', error)
      });
    this.cargarCalculosGenerales();
  }

  selecionEnvase(ind: any) {
    this.numArrDetEnvEnt = ind;
    this.pesoEnvaseDetEntrada = this.arr_envases[ind].pesoEnvase;
  }

  sumarPesoEnvasesEntrada(data: any) {
    data.forEach(element => {
      if (element.check) {
        this.pesoEnvase = this.pesoEnvase + (element.cantEnvases * element.tara);
      }
    });
    // console.log('PESO ENVASE', this.pesoEnvase);
  }

  selecionEnvaseEntrada(ind: any) {
    let index = this.arr_envases.findIndex(element => {
      return element.id == ind;
    })
    this.numArrDetEnvEnt = ind;
    this.pesoEnvaseDetEntrada = this.arr_envases[index].pesoEnvase;
  }

  validarTaraEntrada(valor: any) {
    let index = this.arr_envases.findIndex(element => {
      return element.id == this.envaseEntrada;
    });
    if (parseFloat(valor) > this.arr_envases[index]['taraMaximo'] || parseFloat(valor) < this.arr_envases[index]['taraMinimo']) {
      this.pesoEnvaseDetEntrada = this.arr_envases[index]['pesoEnvase'];
    }
  }

  recalcularDetEnvEnt(event: any) {
    this.totalEnvasesDetEntrada =
      parseInt(this.cantUnidetEnvaseEntrada.toString(), 10) +
      parseInt(this.cantOtroDetEnvaseEntrada.toString(), 10)
    this.totalKilosEnvasesDetEntrada = this.totalEnvasesDetEntrada * this.pesoEnvaseDetEntrada
  }

  //////////////////////////
  // TAB 2 ENVASES SALIDA //
  //////////////////////////
  agregarDetalleEnvaseSalida() {
    if (this.cantUnidetEnvaseSalida > 0 || this.cantOtroDetEnvaseSalida > 0) {
      // this.snackBar.open('SON MAYORES A 0', null, { duration: 3000 });
    } else {
      this.snackBar.open('Debe ingresar una cantidad de envases', null, { duration: 3000 });
      return;
    }
    let index = this.arr_envases.findIndex(element => {
      return element.id == this.numArrDetEnvSal.toString();
    });
    let existe = this.arrEnvasesSalida.findIndex(el => {
      return el.nombre == this.arr_envases[index].nombre
    });
    if (existe >= 0) {
      this.snackBar.open('El envase ya fue ingresado', null, { duration: 2000 });
      this.limpiarLineaEnvaseSalida();
      return;
    }
    let total = this.cantUnidetEnvaseSalida + this.cantOtroDetEnvaseSalida;
    let data_ = {
      nombre: this.arr_envases[index].nombre,
      tara: this.pesoEnvaseDetSalida,
      cantEnvasesUni: this.cantUnidetEnvaseSalida,
      cantEnvasesOtros: this.cantOtroDetEnvaseSalida,
      cantEnvases: total,
      id: this.envaseSalida,
      num_guia: this.nGuiaUnifrutti
    };
    // console.log('DATA ENVIO AL ARREGLO', data_);
    // console.log('ELEMENT DATA AL INSERTAR', ELEMENT_DATA);
    // console.log('TIPO PESAJE AL INSERTAR', this.tipoPesaje);
    // let aux_envaseSalida = JSON.parse(JSON.stringify(this.arrEnvasesSalida))
    if (parseInt(this.tipoPesaje.toString()) === 2) {
      // this.guardarEnvaseSalidaVariedadRomana(total, this.pesoEnvaseDetSalida)
      let data = {
        envasesINOUT: { id: this.nGuiaUnifrutti },
        envaseCosecha: { id: this.envaseSalida },
        temporada: { id: localStorage.getItem('temporada') },
        exportadora: { id: localStorage.getItem('exportadora') },
        zona: { id: this.zona },
        cantidadEmpresa: this.cantUnidetEnvaseSalida,
        cantidadOtros: this.cantOtroDetEnvaseSalida,
        cantidadTotal: parseFloat(this.cantUnidetEnvaseSalida.toString()) + parseFloat(this.cantOtroDetEnvaseSalida.toString())
      };
      this.arrEnvasesSalida.push(data_);
      // console.log('ARREGLO ENVASES SALIDA', this.arrEnvasesSalida);
      // console.log('REQUEST AGREGA ENVASE', JSON.stringify(data));
      // return;
      this.wsLogistica.agregarEnvaseSalidaDetalle(data)
        .subscribe((result) => {
          // console.log('RESPONSE AL INGRESAR ENVASE SALIDA', result);
          this.dataSourceEnvasesSalida = new MatTableDataSource(this.arrEnvasesSalida);
          this.pesoEnvaseSalida = this.pesoEnvaseSalida + (total * this.pesoEnvaseDetSalida);
          // console.log(this.pesoEnvaseSalida);
          this.recalcularLineasRecVarRomana(total, this.pesoEnvaseDetSalida);
          this.recalcularLineasEnvaseSalida();
          this.snackBar.open(' ✔  Envase Ingresado', null, { duration: 2000 });
          this.limpiarLineaEnvaseSalida();
          this.guardarCabeceraRecepcion();
        }, (error) => {
          console.log('ERROR AL INGRESAR ENVASE SALIDA', error);
          // this.arrEnvasesSalida = aux_envaseSalida;
          // this.arrEnvasesSalida = [];
          this.snackBar.open('Error al ingresar envase', null, { duration: 2000 });
        })
    }
    if (parseInt(this.tipoPesaje.toString()) === 1) { // ESPECIE
      // this.guardarEnvaseSalida(total);
      let data = {
        envasesINOUT: { id: this.idRetorno },
        envaseCosecha: { id: this.envaseSalida },
        temporada: { id: localStorage.getItem('temporada') },
        exportadora: { id: localStorage.getItem('exportadora') },
        zona: { id: this.zona },
        cantidadEmpresa: this.cantUnidetEnvaseSalida,
        cantidadOtros: this.cantOtroDetEnvaseSalida,
        pesoTara: parseFloat(this.pesoEnvaseDetSalida.toString()),
        cantidadTotal: parseFloat(this.cantUnidetEnvaseSalida.toString()) + parseFloat(this.cantOtroDetEnvaseSalida.toString())
      };
      // console.log('ESTO ES LO QUE SE ENVIA', data);
      // return;
      this.wsLogistica.agregarEnvaseSalidaDetalle(data)
        .subscribe((result) => {
          this.recalcularLineasRecEspecie(total, this.pesoEnvaseDetSalida);
          this.arrEnvasesSalida.push(data_);
          this.recalcularLineasEnvaseSalida();
          this.dataSourceEnvasesSalida = new MatTableDataSource(this.arrEnvasesSalida)
          this.pesoEnvaseSalida = this.pesoEnvaseSalida + (total * this.pesoEnvaseDetSalida)
          this.snackBar.open(' ✔  Envase Ingresado', null, { duration: 2000 });
          this.limpiarLineaEnvaseSalida();
          this.guardarCabeceraRecepcion();
        }, (error) => {
          console.log('ERROR AL INGRESAR ENVASE SALIDA ESPECIE', error);
          this.snackBar.open('Error al ingresar envase', null, { duration: 2000 });
        });
    }
    this.cargarCalculosGenerales();
    this.obtenerInformeSII();
  }

  eliminarEnvasesSalida(data: any) {
    let ultimo = ELEMENT_DATA.length - 1;
    // ELEMENT_DATA[ultimo]['pesoEnvaseSalida'] -= (data.cantEnvases * data.tara)
    // ELEMENT_DATA[ultimo]['pesoTara'] = ELEMENT_DATA[ultimo]['pesoTara']
    // ELEMENT_DATA[ultimo]['calculoNeto'] = ELEMENT_DATA[ultimo]['pesoBruto'] - ELEMENT_DATA[ultimo]['pesoTara'] -
    //   (ELEMENT_DATA[ultimo]['pesoEnvases'] - ELEMENT_DATA[ultimo]['pesoEnvaseSalida']);
    // ELEMENT_DATA[ultimo]['promedio'] = ELEMENT_DATA[ultimo]['calculoNeto'] / ELEMENT_DATA[ultimo]['cantEnvases']
    let index = this.arrEnvasesSalida.findIndex(element => {
      return element.nombre.trim() == data.nombre.trim()
    });
    // console.log(this.arrEnvasesSalida[index])
    // let id = this.arrEnvasesSalida[index].num_guia
    let id = this.idRetorno.toString();
    let env_cosecha = this.arrEnvasesSalida[index].id
    let exportadora = localStorage.getItem('exportadora')
    let zona = this.zona
    let temporada = localStorage.getItem('temporada')
    console.log('id',id,'env_cos', env_cosecha,'expor', exportadora,'zona', zona,'temp', temporada);
    // return;
    this.wsLogistica.eliminarEnvaseEntrada(id, env_cosecha, exportadora, zona, temporada)
      .subscribe((result) => {
        console.log('RESPUESTA ELIMINAR LINEA',result);
        // if (result.valido) {
        if (result === null) {
          this.arrEnvasesSalida.splice(index, 1);
          let PES = 0;
          this.arrEnvasesSalida.forEach(element => {
            PES += (element.tara * element.cantEnvases)
          });
          let ultimo = ELEMENT_DATA.length - 1;
          ELEMENT_DATA[ultimo]['pesoEnvaseSalida'] = PES
          ELEMENT_DATA[ultimo]['pesoTara'] = ELEMENT_DATA[ultimo]['pesoTara']
          ELEMENT_DATA[ultimo]['calculoNeto'] = ELEMENT_DATA[ultimo]['pesoBruto'] - ELEMENT_DATA[ultimo]['pesoTara'] -
            (ELEMENT_DATA[ultimo]['pesoEnvases'] - ELEMENT_DATA[ultimo]['pesoEnvaseSalida']);
          ELEMENT_DATA[ultimo]['promedio'] = ELEMENT_DATA[ultimo]['calculoNeto'] / ELEMENT_DATA[ultimo]['cantEnvases']
          // console.log(ELEMENT_DATA[ultimo]);
          this.pesoEnvaseSalida = PES;
          this.recalcularLineasEnvaseSalida();
          this.dataSourceEnvasesSalida = new MatTableDataSource(this.arrEnvasesSalida)
          this.actualizarLineasEspecie(ELEMENT_DATA[ultimo]);
          // this.recalcularLineasRecEspecie(ELEMENT_DATA[ultimo],ELEMENT_DATA[ultimo].tara)
          this.guardarCabeceraRecepcion();
        }
      }, (error) => {
        console.error('ERROR ELIMINAR ENVASE ENTRADA', error);
      });
    this.cargarCalculosGenerales();
  }

  listarEnvasesSalida(listaEnvases: any) {
    listaEnvases.resultado.forEach(element => {
      const index = this.verificar_listarEnvasesSalida(element.id);
      if (index >= 0) {
        const nuevaSuma = this.agrupar_listaEnvaseSalida(index, element);
        this.arrEnvasesSalida[index].cantEnvasesUni = nuevaSuma.uni;
        this.arrEnvasesSalida[index].cantEnvasesOtros = nuevaSuma.otro;
      } else {
        this.arrEnvasesSalida.push(element);
      }
    });
    this.dataSourceEnvasesSalida = new MatTableDataSource(this.arrEnvasesSalida);
  }

  obtenerInformeSII() {
    this.informeSII = {};
    if (this.idRecepcion) {
      let req = {
        CodExportadora: localStorage.getItem('exportadora'),
        CodPlanta: this.frigorifico,
        CodTemp: localStorage.getItem('temporada'),
        CodZona: this.zona,
        NumRecep: this.idRecepcion
      };
      this.wsLogistica.obtenerInformeSII(req)
        .subscribe((data) => {
          // console.log('RESPONSE INFORME SII', data);
          if (data.valido) {
            this.habilitaInformeSII = true;
            this.informeSII = data.enc;
          } else {
            this.habilitaInformeSII = false;
            console.error('ERROR AL OBTENER INFORME SII', data);
          }
        }, (error) => {
          this.habilitaInformeSII = false;
          console.error('ERROR AL OBTENER INFORME SII', error);
        });
    }
  }

  guiaDespacho(flag: number) {
    if (flag === 1) {
      this.snackBar.open('Guía generada correctamente', null, { duration: 2000 });
    }
    this.obtenerInformeSII();
  }

  anularGuia() {
    let data = {
      id: this.idRetorno,
      guiaRecepcion: {
        id: this.idGuia,
        zona: { id: this.zona },
        temporada: { id: this.temporada },
        frigorifico: { id: this.frigorifico },
        exportadora: { id: this.exportadora }
      },
      huerto: { id: this.huerto, empresa: { id: this.rut } },
      fecha: this.fechaCosecha,
      glosa: this.glosaGlob,
      usuario: { id: this.usuario },
      esNula: 1
    };
    console.log(data);
    // return;
    this.wsLogistica.anularGuia(data)
      .subscribe((result) => {
        console.log('RESPONSE ANULAR GUIA', result);
        if (result === null) {

          this.arrEnvasesSalida.forEach(element => {
            this.eliminarEnvasesSalida(element);
          });

          this.obtenerEncabezadoEnvaseSalida();
          this.nGuiaUnifrutti = null;
          this.huerto = null;
          this.glosaGlob = null;
        }
        this.encabezadoGuardadoEnvaseSalida = false;
      }, (error) => {
        this.snackBar.open('Error al anular guía', null, { duration: 2000 });
        console.error('ERROR ANULAR GUIA', error);
      });
    this.cargarCalculosGenerales();
  }

  obtenerGlosa(glosa: any) {
    if (glosa.id) {
      this.glosaGlob = glosa.id;
      this.glosaNombreGlob = glosa.nombre;
    }
  }

  cargarGlosas() {
    this.wsCommon.cargarGlosas('28')
      .subscribe((data) => {
        // console.log('RESPONSE GLOSAS', data);
        this.glosas = data;
      }, (error) => {
        console.error('ERROR GLOSAS', error);
        this.glosas = [];
      });
  }

  selecionGlosa(item: any) {
    // console.log('ITEM GLOSA', item);
  }

  selecionEnvaseSalida(ind: any) {
    this.envaseSalidaMinimo = 0;
    this.envaseSalidaMaximo = 0;
    let index = this.arr_envases.findIndex(element => {
      return element.id == ind;
    });
    this.numArrDetEnvSal = ind;
    if (this.arr_envases[index].pesoEnvase) {
      this.pesoEnvaseDetSalida = this.arr_envases[index].pesoEnvase;
      this.envaseSalidaMinimo = this.arr_envases[index]['taraMinimo'];
      this.envaseSalidaMaximo = this.arr_envases[index]['taraMaximo'];
    } else {
      this.snackBar.open('El envase no tiene peso', null, { duration: 3000 })
      this.pesoEnvaseDetSalida = null;
      this.envaseSalidaMinimo = 0;
      this.envaseSalidaMaximo = 0;
    }
  }

  async actualizarPesoBruto() {
    this.sumaTotalEnvases = 0;
    ELEMENT_DATA.forEach((element, index) => {
      let totalEnvases = element.cantEnvases;
      this.sumaTotalEnvases += totalEnvases;
    });
    ELEMENT_DATA.forEach((element, index) => {
      element['pesoBruto'] = this.pesoBrutoTotal / this.sumaTotalEnvases * element['cantEnvases'];
      element['destare'] = element['destare'] ? element['destare'] : 0;
      element['calculoNeto'] = element['pesoBruto'] - element['destare'] - (element['pesoEnvases'] - element['pesoEnvaseSalida']);
      let promedio = element['calculoNeto'] / element['cantEnvases'];
      element['promedio'] = promedio;
    });
    this.actualizarLineasEspecieEditar();
  }

  totalTara() {
    let pesotara = 0;
    ELEMENT_DATA.forEach(element => {
      pesotara += element.pesoTara;
    });
    return pesotara;
  }

  totalPesoNeto() {
    let totalneto = 0;
    ELEMENT_DATA.forEach(element => {
      totalneto += element.calculoNeto;
    });
    let totalEnvEnt = 0;
    this.arrEnvasesEntrada.forEach(element => {
      totalEnvEnt += element.cantEnvases * element.tara;
    });
    return totalneto;
  }

  validarTaraEnvaseSalida(valor: any) {
    let index = this.arr_envases.findIndex(element => {
      return element.id == this.envaseSalida;
    });
    if (parseFloat(valor) > this.arr_envases[index]['taraMaximo'] && parseFloat(valor) < this.arr_envases[index]['taraMinimo']) {
      this.pesoEnvaseDetSalida = this.arr_envases[index]['pesoEnvase'];
    } else {
      this.snackBar.open('El rango de peso envase no corresponde', null, { duration: 3000 });
      this.pesoEnvaseDetSalida = null;
    }
    // console.log('PESO ENVASE SALIDA EN VALIDA TARA', this.pesoEnvaseDetSalida);
  }

  recalcularLineasEnvaseSalida() {
    let pesoTotalEnvases = 0;
    let cantEnvasesEntrada = 0;
    this.arrEnvasesSalida.forEach(element => {
      pesoTotalEnvases += element.tara * element.cantEnvases;
    });
    this.arrEnvasesEntrada.forEach(element => {
      cantEnvasesEntrada += element.cantEnvases;
    });
    let req = {
      num_recep: this.idRecepcion,
      zona: this.zona,
      frigorifico: this.frigorifico,
      temporada: this.temporada,
      TotalPesoEnvSalida: pesoTotalEnvases,
      TotalEnvasesEntrada: cantEnvasesEntrada,
      // pesoBruto: sumaEnvases,
      // pesoEnvasesIn: cantEnvases,
      exportadora: this.exportadora,
    };
    this.wsLogistica.detalleGuiaRecepcionEnvaseSalida(req)
      .subscribe((data) => {
        // console.log('PATCH RECALCULA R', data);
        if (data.num_recep > 0) {
          this.snackBar.open('Recalculado correctamente', null, { duration: 2000 });
        }
      }, (error) => {
        console.error('PATCH RECALCULA E', error);
      });
  }

  limpiarLineaEnvaseSalida() {
    this.envaseSalida = null;
    this.cantUnidetEnvaseSalida = 0;
    this.pesoEnvaseDetSalida = 0;
    this.cantOtroDetEnvaseSalida = 0;
    this.totalKilosEnvasesDetSalida = 0;
    this.envaseSalidaMaximo = 0;
    this.envaseSalidaMinimo = 0;
  }

  guardarEnvaseSalida(total: any) {
    let data = {
      envasesINOUT: { id: this.nGuiaUnifrutti },
      envaseCosecha: { id: this.envaseSalida },
      temporada: { id: localStorage.getItem('temporada') },
      exportadora: { id: localStorage.getItem('exportadora') },
      zona: { id: this.zona },
      cantidadEmpresa: this.cantUnidetEnvaseSalida,
      cantidadOtros: this.cantOtroDetEnvaseSalida,
      cantidadTotal: parseFloat(this.cantUnidetEnvaseSalida.toString()) + parseFloat(this.cantOtroDetEnvaseSalida.toString())
    }
    this.wsLogistica.agregarEnvaseSalidaDetalle(data)
      .subscribe((result) => {
        this.recalcularLineasRecEspecie(total, this.pesoEnvaseDetSalida);
        this.snackBar.open(' ✔  Envase ingresado', null, { duration: 2000 });
        this.guardarCabeceraRecepcion();
      }, (error) => {
        this.snackBar.open('Error al ingresar envase', null, { duration: 2000 });
      });
  }

  guardarEnvaseSalidaVariedadRomana(total: any, tara: any) {
    let data = {
      envasesINOUT: { id: this.nGuiaUnifrutti },
      envaseCosecha: { id: this.envaseSalida },
      temporada: { id: localStorage.getItem('temporada') },
      exportadora: { id: localStorage.getItem('exportadora') },
      zona: { id: this.zona },
      cantidadEmpresa: this.cantUnidetEnvaseSalida,
      cantidadOtros: this.cantOtroDetEnvaseSalida,
      cantidadTotal: parseFloat(this.cantUnidetEnvaseSalida.toString()) + parseFloat(this.cantOtroDetEnvaseSalida.toString())
    };
    this.recalcularLineasRecVarRomana(total, tara);
    this.wsLogistica.agregarEnvaseSalidaDetalle(data)
      .subscribe((result) => {
        this.snackBar.open(' ✔  Envase Ingresado', null, { duration: 2000 });
        this.guardarCabeceraRecepcion();
      }, (error) => {
        this.snackBar.open('Error al ingresar envase', null, { duration: 2000 });
      });
  }

  recalcularDetEnvSal(event: any) {
    this.totalEnvasesDetSalida = parseInt(this.cantUnidetEnvaseSalida.toString(), 10) + parseInt(this.cantOtroDetEnvaseSalida.toString(), 10);
    this.totalKilosEnvasesDetSalida = this.totalEnvasesDetSalida * this.pesoEnvaseDetSalida;
  }

  ///////////////////
  // TAB 3 DESTARE //
  ///////////////////
  recalcularDestareVariedadRomana() {
    ELEMENT_DATA.forEach((data, index) => {
      ELEMENT_DATA_DESTARA.forEach(destare => {
        if (data.variedadNombre === destare.variedad && data.especieNombre === destare.especie) {
          let tara = parseFloat(destare.pesoTara);
          this.agregarDestareVariedadRomana(tara, index);
        }
      });
    });
  }

  listaDestare() {
    let data: any = [];
    const especiesRepetidad = [];
    ELEMENT_DATA.forEach(result => {
      const index = this.posicionEspecieDestare(result.especieNombre);
      if (index >= 0) {
        data = { variedadNombre: result.variedadNombre, totalBultos: result.total };
        const indexVariedad = this.posicionVariedadDestare(index, result.variedadNombre);
        if (indexVariedad < 0) {
          this.especies[index].variedades.push(data);
        }
      } else {
        especiesRepetidad.push(result.especieNombre);
        const dataVariedad = {
          variedadNombre: result.variedadNombre,
          totalBultos: result.total
        };
        data = {
          especie: result.especieNombre,
          variedades: [dataVariedad]
        };
        this.especies.push(data);
      }
    });
  }

  agregarDestareVariedadRomana(tara: any, index: any) {
    ELEMENT_DATA[index]['destare'] = tara + ELEMENT_DATA[index]['pesoEnvaseSalida']
    ELEMENT_DATA[index]['pesoTara'] = tara + ELEMENT_DATA[index]['pesoEnvaseSalida']
    ELEMENT_DATA[index]['calculoNeto'] = ELEMENT_DATA[index]['pesoBruto'] - ELEMENT_DATA[index]['pesoTara'] - (ELEMENT_DATA[index]['pesoEnvasesIn'] - ELEMENT_DATA[index]['pesoEnvaseSalida']);
    ELEMENT_DATA[index]['promedio'] = ELEMENT_DATA[index]['calculoNeto'] / ELEMENT_DATA[index]['cantEnvases']
    this.actualizarLineasEspecie(ELEMENT_DATA[index]);
  }

  agregarDestareVariedad() {
    if (this.pesoDestareVariedad === 0 || !this.pesoDestareVariedad) {
      this.snackBar.open('Ingrese datos', null, { duration: 800 });
      return;
    }
    let destarar = false;
    ELEMENT_DATA.forEach(element => {
      if (element.folioCKU <= 0) {
        destarar = true;
      }
    });
    if (destarar) {
      this.snackBar.open('No existe CKU para esta recepción', null, { duration: 2000 });
      // console.log('No se puede destarar');
      // return;
    }
    this.linea_aux = JSON.parse(JSON.stringify(ELEMENT_DATA));
    this.destare_aux = JSON.parse(JSON.stringify(ELEMENT_DATA_DESTARA));
    const filtroLista: any = this.especies[this.especie].variedades.filter(result => {
      return result.variedadNombre === this.variedad;
    });
    const data = {
      especie: this.especies[this.especie].especie,
      variedad: filtroLista[0].variedadNombre,
      pesoDestare: this.pesoDestareVariedad,
      pesoTara: this.pesoDestareVariedad,
      cantidadEnvases: filtroLista[0].totalBultos,
    };
    ELEMENT_DATA_DESTARA.push(data);
    // this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
    this.pesoBrutoSalida = 0;
    ELEMENT_DATA_DESTARA.forEach(element => {
      this.pesoBrutoSalida += parseFloat(element.pesoDestare);
    });
    this.recalcularDestareVariedadRomana();
    if (this.tipoPesaje === 3) {
    }
  }

  agregarDestareEspecie() {
    let destarar = false;
    ELEMENT_DATA.forEach(element => {
      if (element.folioCKU <= 0) {
        destarar = true;
      }
    });
    if (destarar) {
      this.snackBar.open('No existe CKU para esta recepción', null, { duration: 2000 });
      // console.log('No se puede destarar');
      return;
    }
    let sumaTaraActual = 0;
    let destare = parseFloat(this.pesoDestarePorEspecie.toString());
    this.destare_aux = JSON.parse(JSON.stringify(ELEMENT_DATA_DESTARA));
    this.linea_aux = JSON.parse(JSON.stringify(ELEMENT_DATA));
    if (ELEMENT_DATA_DESTARA.length > 0) {
      ELEMENT_DATA_DESTARA.forEach(element => {
        sumaTaraActual += parseFloat(element.pesoDestare);
      });
    }
    if (sumaTaraActual > 0) {
      destare = sumaTaraActual;
    }
    let totalEnvases = this.totalEnvases(ELEMENT_DATA);
    ELEMENT_DATA.forEach(element => {
      let calculo = (destare / totalEnvases) * element.cantEnvases;
      const data = {
        especie: element.especieNombre,
        variedad: element.variedadNombre,
        pesoDestare: calculo,
        pesoTara: calculo,
        cantidadEnvases: element.cantEnvases,
        linea: element.linea
      };
      ELEMENT_DATA_DESTARA.push(data);
    });
    this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
    this.pesoBrutoSalida = parseInt(this.pesoDestarePorEspecie.toString(), 10);
    this.recalcularDestareEspecie();
    this.pesoDestarePorEspecie = 0;
  }

  eliminarDestareEspecie(linea: any) {
    let index = ELEMENT_DATA_DESTARA.findIndex(element => {
      return element.linea == linea.linea;
    });
    if (index < 0) {
      return;
    }
    ELEMENT_DATA_DESTARA.splice(index, 1);
    this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
    this.pesoBrutoSalida = 0;
    ELEMENT_DATA_DESTARA.forEach(element => {
      this.pesoBrutoSalida += parseFloat(element.pesoDestare);
    });
  }

  async recalcularDestareEspecie() {
    // console.log('RECALCULAR DESTARE ESPECIE', ELEMENT_DATA_DESTARA);
    ELEMENT_DATA_DESTARA.forEach(element => {
      let index = ELEMENT_DATA.findIndex(element_data => {
        return element_data.linea == element.linea;
      });
      ELEMENT_DATA[index]['destare'] = ELEMENT_DATA[index]['destare'] + element.pesoDestare
      ELEMENT_DATA[index]['pesoTara'] = ELEMENT_DATA[index]['destare'] + element.pesoDestare
      ELEMENT_DATA[index]['calculoNeto'] = ELEMENT_DATA[index]['pesoBruto'] - ELEMENT_DATA[index]['destare'] - (ELEMENT_DATA[index]['pesoEnvases'] - ELEMENT_DATA[index]['pesoEnvaseSalida']);
      ELEMENT_DATA[index]['promedio'] = ELEMENT_DATA[index]['calculoNeto'] / ELEMENT_DATA[index]['cantEnvases']
    });
    for (let index = 0; index < ELEMENT_DATA.length; index++) {
      const element = ELEMENT_DATA[index];
      let r = await this.actualizarLineasEspecie(element);
    }
    this.calculosGenerales();
  }

  obtenerEncabezadoEnvaseSalida() {
    if (this.idRecepcion) {
      let req = {
        exportadora: this.exportadora,
        planta: this.frigorifico,
        temporada: this.temporada,
        zona: this.zona,
        num_recep: parseInt(this.idRecepcion)
      };
      // console.log('CABECERA ENVASES SALIDA', JSON.stringify(req));
      this.wsLogistica.obtenerCabeceraEnvaseSalida(req)
        .subscribe((data) => {
          console.log('CABECERA ENVASES SALIDA', data);
          if (data.items[0].huerto.id) {
            if (data.items[0].huerto.id) {
              if (data.items[0].glosa) {
                this.encabezadoGuardadoEnvaseSalida = true;
                this.huerto = data.items[0].huerto.id;
                this.nGuiaUnifrutti = data.items[0].numDTE;
                this.glosaGlob = data.items[0].glosa;
                this.idRetorno = data.items[0].id;
              } else {
                this.encabezadoGuardadoEnvaseSalida = false;
              }
            } else {
              this.encabezadoGuardadoEnvaseSalida = false;
            }
          } else {
            this.encabezadoGuardadoEnvaseSalida = false;
          }
        }, (error) => {
          this.encabezadoGuardadoEnvaseSalida = false;
          console.error('ERROR AL OBTENER CABECERA ENVASE SALIDA', error);
        });
    }
  }

  guardarEncabezadoEnvSalida() {
    if (this.nGuiaUnifrutti > 0) {
      if (this.huerto) {
        const f_cosecha = new Date(this.fechaCosecha).toISOString();
        let data = {
          id: 0,
          guiaRecepcion: {
            id: this.idGuia,
            zona: { id: this.zona },
            temporada: { id: localStorage.getItem('temporada') },
            frigorifico: { id: this.frigorifico },
            exportadora: { id: localStorage.getItem('exportadora') }
          },
          huerto: { id: this.huerto, empresa: { id: this.rutReference.nativeElement.value } },
          fecha: f_cosecha,
          glosa: this.glosaGlob,
          usuario: { id: localStorage.getItem('User') },
          esNula: false,
          numDTE: this.nGuiaUnifrutti
        };
        let snack = this.snackBar.open('Procesando...', null, { duration: 0 });
        // console.log(data);
        // return;
        this.wsLogistica.agregarEnvaseSalida(data)
          .subscribe((result) => {
            console.log('respuesta datos',result);
            this.idRetorno = result.id;
            console.log(this.idRetorno);
            
            // ACA ESTOY

            snack.dismiss();
            this.snackBar.open(' ✔ Envase agregado correctamente', null, { duration: 2000 });
            // console.log('RESPONSE GURDA ENVASE SALIDA', result);
            this.encabezadoGuardadoEnvaseSalida = true;
          }, (error) => {
            snack.dismiss();
            this.snackBar.open('Error al ingresar envase', null, { duration: 2000 });
            console.error('ERROR GUARDA ENVASE', error);
          });
        this.obtenerEncabezadoEnvaseSalida();
      } else {
        this.snackBar.open('Debe ingresar huerto', null, { duration: 2000 });
      }
    } else {
      this.snackBar.open('Debe ingresar Nº guía', null, { duration: 2000 });
    }
    this.cargarCalculosGenerales();
  }

  selEspecieDestare(especie: any) {
    this.variedades = this.especies[especie].variedades;
  }

  selVariedadDestare(variedad: any) {
  }

  pesoBrutoVariedadPesa() {
    let sumaPesoBruto = 0;
    ELEMENT_DATA.forEach(element => {
      sumaPesoBruto += element.pesoBruto;
    });
    this.pesoBrutoTotal = sumaPesoBruto;
  }

  editarVariedadPesa(data: any) {
    let dataEditar = this.dataVariedadPesa.findIndex(element => {
      return element.linea == data.linea;
    });
    const dialogRef = this.dialog.open(PesoBrutoComponent, {
      disableClose: true,
      width: '600px',
      data: {
        mostrarPeso: this.mostrarPeso,
        elementoPesado: this.elementoPesado,
        pesoVariedadRomanaCamionCarro: this.pesoBruto,
        huerto: this.huerto,
        editando: true,
        ambiente: {
          zonaSeleccionada: this.zonaSeleccionada,
          productor: this.empresaSeleccionada,
          zona: localStorage.getItem('planta'),
          temporada: localStorage.getItem('temporada'),
          frigorifico: this.frigorifico,
          rutEmpresa: this.rutReference.nativeElement.value,
          idGuia: this.idGuia,
          productorNombre: this.productor,
          linea: data.linea,
          fechaCosecha: this.fechaCosecha,
          controldepeso: false,
          isActive: true
        },
        dataVariedaPesa: this.dataVariedadPesa[dataEditar]
      }
    });
    dialogRef.afterClosed()
      .subscribe(result => {
        if (this.mostrarPeso === '2' && !result.tipoPeso) { /// por variedad - pesa
          this.dataVariedadPesa[dataEditar].restoPesajeBins = result.dataVariedadPesa.restoPesajeBins;
          this.calcularLineasVariedadPesa(result.variedadPesa);
          this.listarEnvasesEntradaVariedadPesa(result.variedadPesa.envases);
          this.tipoPesaje = 2;
        }
      });
  }

  recepcionFinal() {
    let data = {
      codTemporada: localStorage.getItem('temporada'),
      codZona: this.zona,
      numRecepcion: this.idGuia,
      codExportadora: localStorage.getItem('exportadora'),
      codFrigorifico: this.frigorifico
    };
    this.wsLogistica.recepcionFinal(data)
      .subscribe((data) => {
        // console.log('RECEPCION FINAL RESPONSE', data);
        this.snackBar.open('Proceso finalizado correctamente', null, { duration: 2000 });
      }, (error) => {
        console.error('ERROR AL FINALIZAR RECEPCION FINAL', error)
        this.snackBar.open('Error al finalizar proceso', null, { duration: 2000 });
      });
    this.cargarCalculosGenerales();
  }

  ValidarSinLineas() {
    if (ELEMENT_DATA.length == 0) { return true; }
    return false;
  }

  /////////////////////////////////
  // GENERALES (NO SE QUE HACEN) //
  /////////////////////////////////
  recalcularEliminarLineaVariedadPesa(index: any) {
    this.lineasIngresadas.splice(index, 1);
    ELEMENT_DATA = [];
    ELEMENT_DATA_DESTARA = [];
    ELEMENT_DATA_AUX = [];
    this.arrEnvasesEntrada = [];
    this.pesoEnvase = 0;
    if (this.lineasIngresadas.length === 0) {
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
      this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
    } else {
      this.lineasIngresadas.forEach(element => {
        this.calcularLineasVariedadPesa(element.data[0]);
      });
    }
    this.calculosGenerales();
  }

  recalcularEliminarLineaVariedadRomana(index: any) {
    let linea = ELEMENT_DATA[index].linea;
    this.lineasIngresadas.splice(index, 1);
    ELEMENT_DATA.splice(index, 1);
    // ELEMENT_DATA = []
    this.pesoEnvase = 0;
    if (ELEMENT_DATA.length === 0) {
      ELEMENT_DATA_AUX = [];
      this.arrEnvasesEntrada = [];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
      this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
    } else {
      ELEMENT_DATA.forEach(element => {
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        let ind = ELEMENT_DATA_DESTARA.findIndex(el => {
          return el.linea == linea;
        });
        if (ind >= 0) {
          ELEMENT_DATA_DESTARA.splice(ind, 1);
        }
      });
    }
    this.pesoBrutoTotal = 0;
    ELEMENT_DATA.forEach(element => {
      this.pesoBrutoTotal += element.pesoBruto;
    });
    this.calculosGenerales();
  }

  recalcularEliminarLineaEspecie(index: any) {
    // console.log(this.lineasIngresadas);
    this.lineasIngresadas.splice(index, 1);
    ELEMENT_DATA = [];
    ELEMENT_DATA_DESTARA = [];
    ELEMENT_DATA_AUX = [];
    this.arrEnvasesEntrada = [];
    this.pesoEnvase = 0;
    if (this.lineasIngresadas.length === 0) {
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.ELEMENT_DATA_ = ELEMENT_DATA
      this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
      this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
    }
    this.lineasIngresadas.forEach(element => {
      this.calculoEspecie(element);
    });
    this.agregarDestareEspecie();
    this.calculosGenerales();
  }

  recalcularEliminarEnvSalida() {
    this.arrEnvasesEntrada.forEach(element => { });
  }

  calcularLineasVariedadPesa(datavariedadPesa: any) {
    let ind = ELEMENT_DATA.findIndex(element => {
      return element.variedad == datavariedadPesa.detalle[0].variedad;
    });
    if (ind >= 0) {
      ELEMENT_DATA[ind]['calculoNeto'] = datavariedadPesa.detalle[0].pesoNeto
      ELEMENT_DATA[ind]['promedio'] = datavariedadPesa.detalle[0].promedio;
      ELEMENT_DATA[ind]['cantEnvases'] = datavariedadPesa.detalle[0].cantidadEnvases;
      ELEMENT_DATA[ind]['envaseCosecha']['nombre'] = datavariedadPesa.detalle[0].envaseNombre
      ELEMENT_DATA[ind]['pesoBruto'] = datavariedadPesa.detalle[0].pesoBruto
    } else {
      datavariedadPesa.detalle[0]['calculoNeto'] = datavariedadPesa.detalle[0].pesoNeto;
      datavariedadPesa.detalle[0]['promedio'] = datavariedadPesa.detalle[0].promedio;
      datavariedadPesa.detalle[0]['cantEnvases'] = datavariedadPesa.detalle[0].cantidadEnvases;
      datavariedadPesa.detalle[0]['envaseCosecha'] = []
      datavariedadPesa.detalle[0]['envaseCosecha']['nombre'] = datavariedadPesa.detalle[0].envaseNombre
      ELEMENT_DATA.push(datavariedadPesa.detalle[0]);
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    }
    this.pesoBrutoVariedadPesa();
    this.calculosGenerales();
  }

  modificarPesoBruto(data: any) {
    let index = ELEMENT_DATA.findIndex(element => {
      return data.linea == element.linea;
    });
    const dialogRef = this.dialog.open(Modificar_peso_brutoComponent, {
      width: '400px',
      data: {
        mostrarPeso: this.mostrarPeso,
        elementoPesado: this.elementoPesado,
        linea: data,
        pesoBruto: this.pesoBruto
      }
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        const element = ELEMENT_DATA[index];
        element['pesoBruto'] = result.nuevoPesoBruto
        const calculoNeto = element['pesoBruto'] - (element['pesoEnvasesIn']);
        element['promedio'] = calculoNeto / element['cantEnvases'];
        element['calculoNeto'] = calculoNeto;
        this.actualizarLineasEspecie(ELEMENT_DATA[index]);
      });
  }

  agregarEnvasesDeSalida() {
    const envasesSalida = 55;
    const index = ELEMENT_DATA.length - 1;
    ELEMENT_DATA[index]['pesoEnvSalida'] = ELEMENT_DATA[index]['tara'] * envasesSalida;
    ELEMENT_DATA[index]['pesoEnvase'] = ELEMENT_DATA[index]['tara'] * ELEMENT_DATA[index]['cantEnvases'];
    ELEMENT_DATA[index]['calculoNeto'] = ELEMENT_DATA[index]['tara'] * ELEMENT_DATA[index]['cantEnvases'];
    ELEMENT_DATA[index]['pesoTara'] = 11800 + ELEMENT_DATA[index]['pesoEnvSalida'];
    ELEMENT_DATA[index]['calculoNeto'] = parseFloat(ELEMENT_DATA[index]['pesoBruto']) - parseFloat(ELEMENT_DATA[index]['pesoTara']) - (parseFloat(ELEMENT_DATA[index]['pesoEnvase']) - parseFloat(ELEMENT_DATA[index]['pesoEnvSalida']));
    ELEMENT_DATA[index]['promedio'] = ELEMENT_DATA[index]['calculoNeto'] / ELEMENT_DATA[index]['cantEnvases'];
  }

  calculoVariedadRomana(result: any) {
    // console.log('CALCULO ROMANA', result);
    const ind = result.resultado.findIndex(element => {
      return element.esContenedor === true;
    });
    let linea = 1;
    if (ELEMENT_DATA.length > 0) {
      linea = parseInt(ELEMENT_DATA[ELEMENT_DATA.length - 1].linea) + 1;
    } else {
      linea = 1;
    }
    result.resultado[ind]['linea'] = linea;
    ELEMENT_DATA.push(result.resultado[ind]);
    ELEMENT_DATA_AUX.push(result.resultado[ind]);
    this.linea_aux = JSON.parse(JSON.stringify(ELEMENT_DATA));
    this.sumaTotalEnvases = 0;
    this.calculoVariedadRomanaLineas(result, ind, linea);
    this.promedio = this.pesoBrutoTotal / ELEMENT_DATA.length;
    this.listaDestare();
    this.sumarPesoEnvasesEntrada(result.resultado);
    this.recalcularDestareVariedadRomana();
  }

  calculoVariedadRomanaLineas(result: any, ind: any, linea: any) {
    let pesoTotalSalida = 0;
    // this.arrEnvasesSalida.forEach(element => { });
    ELEMENT_DATA.forEach((element, index) => {
      let cantEnvases = 0;
      let tara = 0;
      if (this.arrEnvasesSalida.length > 0) {
        let arrSalida = this.arrEnvasesSalida[this.arrEnvasesSalida.length - 1];
        cantEnvases = arrSalida.cantEnvases;
        tara = arrSalida.tara;
      }
      let pesoEnv = 0;
      if (element['tara'] != 0) {
        pesoEnv = element['tara'] * element['cantEnvases'];
      } else {
        pesoEnv = element['pesoEnvases'];
      }
      // console.log(pesoEnv);
      if (index === 0) {
        element['pesoBruto'] = this.pesoBruto;
      } else {
        element['pesoBruto'] = (ELEMENT_DATA[index - 1].pesoTara);
      }
      element['linea'] = element.linea;
      element['cantEnvases'] = parseFloat(element.cantEnvases);
      this.sumaTotalEnvases += element['cantEnvases'];
      element['pesoEnvases'] = pesoEnv;
      element['pesoEnvasesIn'] = pesoEnv;
      element['pesoEnvaseSalida'] = 0;
      /// calculo neto
      element['destare'] = element['destare'] ? element['destare'] : 0;
      element['pesoTara'] = element['pesoTara'] ? element['pesoTara'] : 0;
      // element['pesoTara'] += element['pesoTara'] + cantEnvases * tara
      element['calculoNeto'] = element['pesoBruto'] - element['pesoTara'] - (element['pesoEnvases'] - element['pesoEnvaseSalida']);
      // promedio
      let promedio = element['calculoNeto'] / element['cantEnvases'];
      element['promedio'] = promedio;
      element['contenedor'] = []
      element['contenedor']['id'] = result.idContendor
    });
    let pb = ELEMENT_DATA[ELEMENT_DATA.length - 1]['pesoBruto']
    let data = {
      linea: linea,
      especie: result.resultado[ind].especie,
      variedad: result.resultado[ind].variedad,
      pesoBruto: pb,
      tara: result.resultado[ind]['destare'] ? result.resultado[ind]['destare'] : 0,
      pesoEnvasesIn: result.resultado[ind]['tara'] * result.resultado[ind]['cantEnvases'],
      pesoEnvasesOut: 0,
      codigoEnvase: result.resultado[ind].envaseCosecha.id,
      cantidadEnvases: result.resultado[ind].cantEnvases,
      pesoNeto: result.resultado[ind].calculoNeto
    }
    this.listarEnvasesEntrada(result, data);
  }

  obtenerPesoDestare(especie: any, variedad: any) {
    const valor_destare = ELEMENT_DATA_DESTARA.find(destare => {
      return destare.especie === especie && destare.variedad === variedad;
    });
    return valor_destare;
  }

  calculoPesoVariedadRomana(index: any) {
    const totalBins = this.totalEnvases(ELEMENT_DATA);
    const element = ELEMENT_DATA[index];
    this.pesoEnvase = this.pesoEnvase + (element['cantEnvases'] * element.tara);
    element['pesoBruto'] = ((this.pesoBruto / totalBins) * element['cantEnvases']).toFixed(2);
    const calculoNeto = element['pesoBruto'] - (element['cantEnvases'] * element.tara);
    element['promedio'] = calculoNeto / element['cantEnvases'];
    element['calculoNeto'] = calculoNeto;
  }

  calculoEspecie(result: any) {
    this.idContenedor = result.idContendor;
    this.elementoPesado = result.otros.elementoPesado;
    const ind = result.resultado.findIndex(element => { //AQUI YA ESTA LA TARA
      return element.esContenedor === true;
    });
    let linea = 1;
    if (ELEMENT_DATA.length > 0) {
      linea = parseInt(ELEMENT_DATA[ELEMENT_DATA.length - 1].linea) + 1;
    } else {
      linea = 1;
    }
    result.resultado[ind].linea = linea;
    ELEMENT_DATA.push(result.resultado[ind]);
    this.sumaTotalEnvases = 0;
    const totalBins = this.totalEnvases(ELEMENT_DATA);
    ELEMENT_DATA.forEach((element, index) => {
      let totalEnvases = element.cantEnvases;
      element['cantEnvases'] = parseFloat(totalEnvases);
      element['pesoBruto'] = Math.round((this.pesoBrutoTotal / totalBins) * totalEnvases); //PESO BRUTO
      let calculoNeto = parseFloat(element['pesoBruto']) - (totalEnvases * element.tara);
      element['promedio'] = calculoNeto / totalEnvases;
      element['destare'] = element['destare'] ? element['destare'] : 0;
      element['pesoTara'] = element['destare'] ? element['destare'] : 0;
      element['pesoEnvaseSalida'] = 0;
      element['pesoEnvases'] = element['tara'] * totalEnvases;
      element['pesoEnvasesIn'] = element['tara'] * totalEnvases;
      element['calculoNeto'] = parseFloat(element['pesoBruto']) - parseFloat(element['pesoTara']) -
        (parseFloat(element['pesoEnvases']) - parseFloat(element['pesoEnvaseSalida']));
      element['lineaIngresada'] = this.lineasIngresadas.length;
      element['contenedor'] = [];
      element['contenedor']['id'] = this.idContenedor;
      this.sumaTotalEnvases += totalEnvases;
      // console.log(element);
    });
    // console.log('CALCULO ESPECIE LINEA', linea);
    let data = {
      linea: linea,
      especie: result.resultado[ind].especie,
      variedad: result.resultado[ind].variedad,
      PesoTara: result.resultado[ind]['pesoTara'] ? result.resultado[ind]['pesoTara'] : 0,
      pesoEnvasesIn: result.resultado[ind]['tara'] * result.resultado[ind]['cantEnvases'],
      pesoEnvasesOut: 0,
      codigoEnvase: result.resultado[ind].envaseCosecha.id,
      cantidadEnvases: result.resultado[ind].cantEnvasesUni + result.resultado[ind].cantEnvasesOtros,
      pesoBruto: this.pesoBrutoTotal * (result.resultado[ind].cantEnvasesUni + result.resultado[ind].cantEnvasesOtros) / this.sumaTotalEnvases,
      pesoNeto: result.resultado[ind].calculoNeto,
      totalPesoTaraIN: result.totalPesoTaraIN,
    };
    // console.log('CALCULO RESULTADO ESPECIE', data);
    this.guardarDetalle(data, result.idContendor);
    let envSal = JSON.parse(JSON.stringify(result.resultado));
    envSal.forEach(element => {
      const index = this.verificar_listarEnvasesEntrada(element.envaseCosecha.id);
      if (index >= 0) {
        const nuevaSuma = this.agrupar_listaEnvaseEntrada(index, element);
        this.arrEnvasesEntrada[index].cantEnvasesUni = nuevaSuma.uni;
        this.arrEnvasesEntrada[index].cantEnvasesOtros = nuevaSuma.otro;
        this.arrEnvasesEntrada[index].cantEnvases = nuevaSuma.uni + nuevaSuma.otro;
      } else {
        if (element.check) {
          this.arrEnvasesEntrada.push(element);
        }
      }
    });
    let arr_env_entrada = [];
    envSal.forEach(envEntrada => {
      let data_ = {
        id: this.idGuia,
        zona: { id: this.zona },
        temporada: { id: localStorage.getItem('temporada') },
        frigorifico: { id: this.frigorifico },
        linea: linea,
        envaseCosecha: { id: envEntrada.envaseCosecha.id },
        cantidadEmpresa: parseFloat(envEntrada.cantEnvasesUni),
        cantidadOtros: parseFloat(envEntrada.cantEnvasesOtros),
        esContenedor: envEntrada.esContenedor,
        contenedor: { id: result.idContendor },
        exportadora: { id: localStorage.getItem('exportadora') },
        pesoEnvase: envEntrada.tara
      };
      arr_env_entrada.push(data_);
    });
    if (ELEMENT_DATA_DESTARA.length > 0) {
      ELEMENT_DATA_DESTARA = [];
    }
    let carga = this.snackBar.open('Procesando...', null, { duration: 0 });
    //AQUI GUARDA LA LINEA
    this.wsLogistica.agregarEnvasesEntrada(this.dataLinea, arr_env_entrada) // ESTE AGREGA LA LINEA / NO ENVASE ENTRADA
      .subscribe((data) => {
        // console.log('RESPONSE AGREGAR LINEA', data);
        carga.dismiss();
        this.snackBar.open(' ✔  Recepcion creada', null, { duration: 2000 });
        this.promedio = this.pesoBrutoTotal / ELEMENT_DATA.length;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada)
        if (ELEMENT_DATA_DESTARA.length > 0) {
          ELEMENT_DATA_DESTARA = [];
        }
        this.sumarPesoEnvasesEntrada(result.resultado); // suma envases de entrada
        this.guardarCabeceraRecepcion();
        this.actualizarLineasEspecieEditar();
        this.ELEMENT_DATA_ = ELEMENT_DATA;
      }, (error) => {
        console.error('ERROR AL INGRESAR ENVASES ENTRADA', error);
        carga.dismiss();
        this.snackBar.open('Error al ingresar detalle', null, { duration: 2000 });
      })
    // console.log('ELEMENT DATA SALIDA', ELEMENT_DATA);
    this.ELEMENT_DATA_ = ELEMENT_DATA;
  }

  async actualizarLineasEspecieEditar() {
    for (let index = 0; index <= ELEMENT_DATA.length - 1; index++) {
      let element = ELEMENT_DATA[index];
      let dataDetalle = element;
      const f_cosecha = new Date(this.fechaCosecha).toISOString();
      let data = {
        id: this.idGuia,
        zona: { id: this.zona },
        temporada: { id: localStorage.getItem('temporada') },
        frigorifico: { id: this.frigorifico },
        codCuartel: '2',// SDP
        huerto: { id: this.huerto },
        empresa: { id: this.rutReference.nativeElement.value },
        variedad: { idsnap: dataDetalle.variedad.id || dataDetalle.variedad },
        especie: { idsnap: dataDetalle.especie.id || dataDetalle.especie },
        fechaCosecha: f_cosecha,
        pesoBruto: dataDetalle.pesoBruto,
        pesoTara: dataDetalle.pesoTara,
        pesoEnvasesIn: dataDetalle.pesoEnvasesIn,
        pesoEnvasesOut: dataDetalle.pesoEnvSalida,
        envaseCosecha: { id: dataDetalle.envaseCosecha.id },
        numEnvasesCont: dataDetalle.cantEnvases,
        numLinea: dataDetalle.linea,
        exportadora: { id: localStorage.getItem('exportadora') },
        cuentaCorriente: false,
        folioCKU: 0,
        tipoDucha: { id: '00' },
        tipoFrio: { id: '00' },
        preFrio: false,
        kilosNetosIn: dataDetalle.calculoNeto,
        KilosNetosOut: dataDetalle.calculoNeto,
        snapAptitud: '00',
        contenedor: { "id": dataDetalle['contenedor']['id'] },
        condicion: { id: '00' },
      };
      try {
        let result = await this.wsLogistica.actualizarDetalleEncabezado(data).toPromise();
        this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
        this.pesoBrutoSalida = 0;
        ELEMENT_DATA_DESTARA.forEach(element => {
          this.pesoBrutoSalida += parseFloat(element.pesoDestare);
        });
        // this.guardarCabeceraRecepcion();
      } catch (error) {
        this.pesoBrutoSalida = 0;
        ELEMENT_DATA_DESTARA = JSON.parse(JSON.stringify(this.destare_aux))
        this.dataSourceDestare = new MatTableDataSource(ELEMENT_DATA_DESTARA);
        ELEMENT_DATA = JSON.parse(JSON.stringify(this.linea_aux))
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        console.log('', error);
        ELEMENT_DATA_DESTARA.forEach(element => {
          this.pesoBrutoSalida += parseFloat(element.pesoDestare);
        });
      }
    }
    if (ELEMENT_DATA.length == 0) {
      this.guardarCabeceraRecepcion();
    }
    this.cargarLineasRecepcion();
  }

  recalcularLineasRecVarPesa(total: any, tara: any) {
    let ultimo = ELEMENT_DATA.length - 1;
    ELEMENT_DATA[ultimo]['destare'] += (total * tara)
    ELEMENT_DATA[ultimo]['pesoEnvaseSalida'] += (total * tara)
    ELEMENT_DATA[ultimo]['calculoNeto'] = ELEMENT_DATA[ultimo]['pesoBruto'] - ELEMENT_DATA[ultimo]['destare'] - (ELEMENT_DATA[ultimo]['pesoEnvases'] - ELEMENT_DATA[ultimo]['pesoEnvaseSalida']);
    ELEMENT_DATA[ultimo]['promedio'] = ELEMENT_DATA[ultimo]['total'] * ELEMENT_DATA[ultimo]['calculoNeto']
    this.actualizarLineasEspecie(ELEMENT_DATA[ultimo]);
  }

  recalcularLineasRecVarRomana(total: any, tara: any) {
    let ultimo = ELEMENT_DATA.length - 1;
    ELEMENT_DATA[ultimo]['pesoEnvaseSalida'] = this.pesoEnvaseSalida
    ELEMENT_DATA[ultimo]['pesoTara'] += (total * tara)
    ELEMENT_DATA[ultimo]['destare'] = ELEMENT_DATA[ultimo]['destare'] + ELEMENT_DATA[ultimo]['pesoEnvaseSalida']
    ELEMENT_DATA[ultimo]['calculoNeto'] = ELEMENT_DATA[ultimo]['pesoBruto'] - ELEMENT_DATA[ultimo]['pesoTara'] - (ELEMENT_DATA[ultimo]['pesoEnvasesIn'] - ELEMENT_DATA[ultimo]['pesoEnvaseSalida']);
    ELEMENT_DATA[ultimo]['promedio'] = ELEMENT_DATA[ultimo]['calculoNeto'] / ELEMENT_DATA[ultimo]['cantEnvases']
    ELEMENT_DATA[ultimo]['calculoNeto'] / ELEMENT_DATA[ultimo]['total']
    console.log(ELEMENT_DATA[ultimo]);
    this.actualizarLineasEspecie(ELEMENT_DATA[ultimo]);
  }

  recalcularLineasRecEspecie(total: any, tara: any) {
    let ultimo = ELEMENT_DATA.length - 1;
    ELEMENT_DATA[ultimo]['pesoEnvaseSalida'] += (total * tara)
    ELEMENT_DATA[ultimo]['pesoEnvSalida'] = ELEMENT_DATA[ultimo]['pesoEnvaseSalida']
    // console.log(ELEMENT_DATA[ultimo]);
    // console.log(total, tara);
    ELEMENT_DATA[ultimo]['calculoNeto'] = ELEMENT_DATA[ultimo]['pesoBruto'] - ELEMENT_DATA[ultimo]['pesoTara'] - (ELEMENT_DATA[ultimo]['pesoEnvasesIn'] - ELEMENT_DATA[ultimo]['pesoEnvaseSalida']);
    ELEMENT_DATA[ultimo]['promedio'] = ELEMENT_DATA[ultimo]['calculoNeto'] / ELEMENT_DATA[ultimo]['cantEnvases']
    // console.log(ELEMENT_DATA);
    this.actualizarLineasEspecie(ELEMENT_DATA[ultimo]);
  }

  listarEnvasesEntradaVariedadPesa(listaEnvases: any) {
    let envasesEntrada = [];
    listaEnvases.forEach(element => {
      let index = this.arrEnvasesEntrada.findIndex(elementEnv => {
        return elementEnv.envaseCosecha.id == element.envaseId;
      });
      if (index >= 0) {
        this.arrEnvasesEntrada[index].cantEnvasesUni = this.arrEnvasesEntrada[index].cantEnvasesUni + element.cantidadEmpresa
        this.arrEnvasesEntrada[index].cantEnvasesOtros = this.arrEnvasesEntrada[index].cantEnvasesOtros + element.cantidadOtros
        this.arrEnvasesEntrada[index].cantEnvases = this.arrEnvasesEntrada[index].cantEnvases + (element.cantidadEmpresa + element.cantidadOtros)
      } else {
        let data = {
          envaseCosecha: { nombre: element.envaseNombre, id: element.envaseId },
          tara: element.tara,
          cantEnvasesUni: element.cantidadEmpresa,
          cantEnvasesOtros: element.cantidadOtros,
          cantEnvases: element.cantidadEmpresa + element.cantidadOtros,
        };
        envasesEntrada.push(data);
        this.arrEnvasesEntrada = envasesEntrada;
      }
    });
    this.dataSourceEnvasesEntrada = new MatTableDataSource(this.arrEnvasesEntrada);
  }

  verificar_listarEnvasesEntrada(id: any) {
    return this.arrEnvasesEntrada.findIndex(result => result.envaseCosecha.id === id);
  }

  agrupar_listaEnvaseEntradaVariedadPesa(index: number, element: any) {
    const sumaUniffutti = this.arrEnvasesEntrada[index].cantEnvases + element.cantEnvases;
    const sumaOtro = this.arrEnvasesEntrada[index].cantEnvasesOtros + element.cantEnvasesOtros;
    return { uni: sumaUniffutti, otro: sumaOtro };
  }

  agrupar_listaEnvaseEntrada(index: number, element: any) {
    const sumaUniffutti = this.arrEnvasesEntrada[index].cantEnvasesUni + element.cantEnvasesUni;
    const sumaOtro = this.arrEnvasesEntrada[index].cantEnvasesOtros + element.cantEnvasesOtros;
    return { uni: sumaUniffutti, otro: sumaOtro };
  }

  verificar_listarEnvasesSalida(id: any) {
    return this.arrEnvasesSalida.findIndex(result => result.id === id);
  }

  agrupar_listaEnvaseSalida(index: number, element: any) {
    const sumaUniffutti = this.arrEnvasesSalida[index].cantEnvasesUni + element.cantEnvasesUni;
    const sumaOtro = this.arrEnvasesSalida[index].cantEnvasesOtros + element.cantEnvasesOtros;
    return { uni: sumaUniffutti, otro: sumaOtro };
  }

  posicionEspecieDestare(nombre: any) {
    return this.especies.findIndex(elemento => elemento.especie === nombre);
  }

  posicionVariedadDestare(index: number, nombre: string) {
    return this.especies[index].variedades.findIndex(elemento => elemento.variedadNombre === nombre);
  }

  totalEnvases(data: any) {
    let total = 0;
    data.forEach(element => {
      total += parseFloat(element.cantEnvases);
    });
    return total;
  }

  totalEnvasesEspecie(data: any) {
    let total = 0;
    data.forEach(element => {
      total += parseFloat(element.cantEnvases);
    });
    return total;
  }

  volverCku() {
    this.router.navigate(['/pantallacku'], { queryParams: { mn: '3.1.1.1.' } });
  }

  especievariedadOption(event: any) {
    if (event.value == '1') {
      this.totalValida = true;
    } else if (event.value == '2') {
      this.totalValida = false;
    }
    this.mostrarPeso = event.value;
  }

  validadorCampos() {
    if (this.frigorifico && this.fechaDespacho && this.fechaDespacho && this.chofer
      && this.nombreChofer && this.patenteCamion && this.balanza && this.pesoBrutoTotal > 0 && this.guiaProductor) {
      this.permiteGuardar = true;
      return true;
    }
  }

  verificarEnvasesSalExisten() {
    if (this.arrEnvasesSalida.length > 0) {
      return true;
    }
    return false;
  }

  sumaBultos() {
    let suma = 0;
    ELEMENT_DATA.forEach(ln => {
      suma += ln.cantEnvases;
    })
    return suma;
  }

  sumaPesoNeto() {
    let suma = 0;
    ELEMENT_DATA.forEach(ln => {
      suma += ln.calculoNeto;
    })
    return suma;
  }

  crearReporte() {
    this.reporte();
  }

  reporte() {
    let totalUni = 0;
    let totalOtro = 0;
    let pesoEnvasesReporte = 0;
    let nombreEnvaseLinea = '';
    let nombreEnvaseSalida = '';
    this.arrEnvasesEntrada.forEach(ln => {
      nombreEnvaseLinea = ln.envaseCosecha.nombre;
      totalUni += ln.cantEnvasesUni;
      totalOtro += ln.cantEnvasesOtros;
      pesoEnvasesReporte += ln.pesoEnvases;
    });
    this.totalUniReporte = totalUni;
    this.totalOtrosReporte = totalOtro;
    this.pesoEnvasesReporte = pesoEnvasesReporte;
    ///// Envases desalida
    let totalUniSalida = 0;
    let totalOtroSalida = 0;
    let pesoEnvasesReporteSalida = 0;
    this.arrEnvasesSalida.forEach(ln => {
      nombreEnvaseSalida = ln.nombre
      totalUniSalida += ln.cantEnvasesUni
      totalOtroSalida += ln.cantEnvasesOtros
      pesoEnvasesReporteSalida += (ln.cantEnvasesUni + ln.cantEnvasesOtros) * parseFloat(ln.tara)
    });
    return {
      totalUniReporte: totalUni, totalOtrosReporte: totalOtro, pesoEnvasesReporte: pesoEnvasesReporte,
      totalUniSalida: totalUniSalida, totalOtroSalida: totalOtroSalida, pesoEnvasesReporteSalida: pesoEnvasesReporteSalida,
      nombreEnvaseLinea: nombreEnvaseLinea, nombreEnvaseSalida: nombreEnvaseSalida
    }
  }
}