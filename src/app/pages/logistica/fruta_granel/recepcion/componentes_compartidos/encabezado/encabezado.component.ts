import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';
import { Component, OnInit, ViewChild, ElementRef, Input, OnChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import chileanRut from 'chilean-rut';

@Component({
  selector: 'app-encabezado',
  templateUrl: './encabezado.component.html',
  styleUrls: ['./encabezado.component.css']
})
export class EncabezadoComponent implements OnInit, OnChanges {
  @ViewChild('rutInput') rutReference: ElementRef;
  @Input() cku: string;
  rut: string = null;
  ckuEnt: any;
  cabecera: any = [];
  pesajeGlob: boolean;
  radioOpciones: any = [];

  constructor(
    private snackBar: MatSnackBar,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService
  ) { }

  ngOnInit() { }

  ngOnChanges() {
    console.log('CABECERA: ', this.cku);
    this.ckuEnt = this.cku;
    this.cargarCabecera();
  }

  cargarCabecera() {
    let req = {
      cod_temp: this.ckuEnt.temporada,
      cod_planta: this.ckuEnt.planta,
      cod_exportadora: this.ckuEnt.exportadora,
      cod_zona: this.ckuEnt.zona,
      num_recepcion: this.ckuEnt.num_recepcion
    };
    this.logisticaService.listadoEncabezadoCKU(req)
      .subscribe((data) => {
        console.log('CABECERA LISTA', data);
        let DatosEnc = data.DatosEnc;
        let valido = data.valido;
        let mensaje = data.mensaje;
        if (valido) {
          this.cabecera = DatosEnc;
        } else {
          this.cabecera = [];
          this.snackBar.open('Sin cabecera asociada', null, { duration: 3000 });
        }
        console.log('ENCABEZADO CKU RESPONSE ', data);
      }, (error) => {
        this.snackBar.open('Error al cargar cabecera', null, { duration: 3000 });
      });
  }

  formatoRut() {
    if (this.rut) {
      if (this.rut.length === 9) {
        var dv = this.rut.substring(8);
        this.rut = chileanRut.format(this.rut, dv);
        this.validarRut(this.rut);
      } else if (this.rut.length === 8) {
        var dv = this.rut.substring(7);
        this.rut = chileanRut.format(this.rut, dv);
        this.validarRut(this.rut);
      } else {
        this.snackBar.open('El largo del rut no corresponde', null, { duration: 800 })
        this.rut = '';
        this.rutReference.nativeElement.focus();
      }
    }
  }

  validarRut(rut: string) {
    if (!chileanRut.validate(rut)) {
      this.snackBar.open('Rut inválido', null, { duration: 800 })
      this.rut = '';
      this.rutReference.nativeElement.focus();
    }
  }
}