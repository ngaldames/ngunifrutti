import { Nuevo_cambio_condicionComponent } from './../../../../../shared/dialogs/nuevo_cambio_condicion/nuevo_cambio_condicion.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Ver_cambio_condicionComponent } from './../../../../../shared/dialogs/ver_cambio_condicion/ver_cambio_condicion.component';
import { Ot_asociadas_condicionComponent } from './../../../../../shared/dialogs/ot_asociadas_condicion/ot_asociadas_condicion.component';

const ELEMENT_DATA = [
  {
    guia: '23123', linea: '1', productor: 'Oscar Vargas', origen: '3 MEDIANO PLAZ0', destino: '2 CORTO PLAZO'
  }
];

@Component({
  selector: 'app-consulta_cambio_condicion',
  templateUrl: './consulta_cambio_condicion.component.html',
  styleUrls: ['./consulta_cambio_condicion.component.css']
})
export class Consulta_cambio_condicionComponent implements OnInit {
  displayedColumns: string[] = ['guia', 'linea', 'productor', 'origen', 'destino', 'icons'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  constructor(private router: Router, public dialog: MatDialog) { }

  ngOnInit() {
  }
  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  nuevoCambio(){
    const dialogRef = this.dialog.open(Nuevo_cambio_condicionComponent, {
      width: '600px',
    });
  }
  ver() {
    const dialogRef = this.dialog.open(Ver_cambio_condicionComponent, {
      width: '700px',
    });
  }
  consultarOT() {
    const dialogRef = this.dialog.open(Ot_asociadas_condicionComponent, {
      width: '750px',
    });
  }
}
