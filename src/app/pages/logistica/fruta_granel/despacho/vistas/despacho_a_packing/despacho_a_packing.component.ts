import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSnackBar, MatDialog } from '@angular/material';
import { Router, NavigationExtras } from '@angular/router';
import { ApiCommonService } from '../../../../../../services/apicommonservice.service';
// import { ApilogisticaService } from 'src/app/services/apilogistica.service';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';
import { CerrarOTDespachoComponent } from '../../dialogs/cerrar-otdespacho/cerrar-otdespacho.component';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-despacho_a_packing',
  templateUrl: './despacho_a_packing.component.html',
  styleUrls: ['./despacho_a_packing.component.css']
})
export class Despacho_a_packingComponent implements OnInit {
  public cargando = true;
  tablaError: boolean = false;
  mostrarTabla: boolean = false;
  detalle: boolean = false;
  ot: number;
  fecha: any;
  productor: string;
  variedad: string;
  frio: string;
  linea: number;
  turno: string;
  icons: any;
  displayedColumns: string[] = ['ot', 'fecha_ot', 'productor', 'variedad', 'tipo_frio', 'Linea', 'turno', 'icons'];
  dataSource = new MatTableDataSource([]);
  esFavorito: boolean;
  mostrar: boolean = false;
  isActive: boolean;
  pendiente = true
  temporada: string = null;
  planta: string = null;
  exportadora: string = null;
  zona: string = null;
  usuario: string = null;
  frigorificos: any = [];
  frigoSel: any;
  mensaje :string = 'Cerrar ot';

  constructor(private router: Router, private snackBar: MatSnackBar, 
              public dialog: MatDialog,
              private logisticaService: ApilogisticaService,
              private commonService: ApiCommonService) {}

  ngOnInit() {
    this.obtieneZona();

    setTimeout(() => {
      this.obtenerListaPrincipalOt();  
    }, 1000);


    
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  despacho_packing_OT(item) {
    console.log(item);
    let extras: NavigationExtras = {
      queryParams:{
      'Linea':item.lineaMaquina,
      'asignacion':item.asignacion,
      'fecha_recepcion':item.fechaProceso,
      'ot':item.id,
      'productor':item.productor,
      'tipo_frio':item.tipoFrio,
      'turno':item.turno,
      'variedad':item.variedad,
      'planta':this.frigoSel,
      'zona':this.zona
      }
    }
    this.router.navigate(['/despacho_packing_por_ot'], extras);
  }
  despacho_packing_OTVerDetalle(item) {
    
    console.log(item);
    let extras: NavigationExtras = {
      queryParams:{
      'Linea':item.lineaMaquina,
      'asignacion':item.asignacion,
      'fecha_recepcion':item.fechaProceso,
      'ot':item.id,
      'productor':item.productor,
      'tipo_frio':item.tipoFrio,
      'turno':item.turno,
      'variedad':item.variedad,
      'planta':this.frigoSel,
      'zona':this.zona,
      'detalle':this.detalle
      }
    }
    this.router.navigate(['/despacho_packing_por_ot'], extras);
  }

  devolver_packing_OT() {
    this.router.navigate(['/devolver_packing_por_ot']);
  }
  anular_packing_OT() {
    this.router.navigate(['/anular_packing_por_ot']);
  }
  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  cambioEstado(event) {
    
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.mostrar = true;
      this.mensaje = 'Reasignar ot';
      this.obtenerListaPrincipalOt();
    }
    else {
      this.mostrar = false;
      this.mensaje = 'Cerrar ot';
      this.obtenerListaPrincipalOt();
    }
  }
  obtieneZona() {
    this.temporada = localStorage.getItem('temporada');
    this.planta = localStorage.getItem('planta');
    this.exportadora = localStorage.getItem('exportadora');
    this.usuario = localStorage.getItem('User');
    this.commonService.cargarZona(this.temporada, this.planta)
      .subscribe((result) => {
        // console.log('ZONAS', result);
        this.zona = result.items[0].id;
        console.log(this.zona);
        this.obtieneFrigorificos();
      }, (error) => {
        console.error('ERROR ZONAS', error);
        this.snackBar.open('Error al cargar zonas', null, { duration: 3000 });
      });
  }
  obtieneFrigorificos() {
    this.frigorificos = [];
    this.commonService.cargarFrigorifico(localStorage.getItem('exportadora'), localStorage.getItem('planta'))
      .subscribe((data) => {
        // console.log('FRIGORIFICOS', data);
        this.frigorificos = data.items;
        this.frigoSel = this.frigorificos[0].id;
        console.log('FRIGORIFICOS',this.frigorificos, this.frigoSel);
        // this.selFrigorifico(this.frigorificos[0].id);
        // this.cargandoFrigo = true;
      }, (error) => {
        console.error('ERROR FRIGORIFICOS', error);
        this.snackBar.open('Error al cargar frigorificos', null, { duration: 3000 });
      });
  }
  recargar(){
    this.obtieneFrigorificos();
    this.obtenerListaPrincipalOt();
  }

  obtenerListaPrincipalOt(){
    this.cargando = true;
    console.log(this.mostrar);
    // console.log(this.frigoSel, this.zona);
    let temporada = localStorage.getItem('temporada');
    // let planta = this.frigoSel;
    let exportadora = localStorage.getItem('exportadora');
    let usuario = localStorage.getItem('User');
    let zona = this.zona;
    
    let req = {
      temporada:temporada,
      zona_prod:zona,
      exportadora:exportadora,
      cerradas:this.mostrar,
      pagina:1,
      registros:20
    };
    this.logisticaService.obtenerListaPrincipalOt(req).subscribe(async result =>{
      console.log('OBTENER LISTA PRINCIPAL ',result);
      if ( await result.items.length >=1) {
          this.dataSource = new MatTableDataSource(result.items);
          this.cargando = false;
          this.tablaError = false;
          this.mostrarTabla = true;
        // setTimeout(() => {
        //   this.dataSource = new MatTableDataSource(result.items);
        //   this.cargando = false;
        //   this.tablaError = false;
        //   this.mostrarTabla = true;
        // }, 1000);

      }else {
        this.snackBar.open('Error al cargar lista principal', null, { duration: 3000 });
        this.tablaError = true;
        this.mostrarTabla = false;
      }
    },(error) =>{
      console.log(error);
      // this.cargando = false;
      // this.tablaError = true;
      // this.mostrarTabla = false;
    });
  }
  cerrar(item) {
    console.log(item);
    const dialogRef = this.dialog.open(CerrarOTDespachoComponent, {
      disableClose: true,
      width: '500px',
      height: 'auto',
      data:{'detalle':this.mostrar}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.cerrar) {
        const req = {
          temporada: { id: this.temporada},
          zona: { id: this.zona},
          id: item.id,
          cierre: 1
        };
        this.logisticaService.patchCerrar(req).subscribe((response) => {
          console.log(response);
          if (response.cierre === "1") {
            this.snackBar.open('Cerrado', null, { duration: 2000 });
            this.obtenerListaPrincipalOt();
          }
        }, (error) => {
          console.log(error);
        });
      }
      if (result.reasignar) {
        const req = {
          temporada: { id: this.temporada },
          zona: { id: this.zona },
          id: item.id,
          cierre: 0
        };
        this.logisticaService.patchCerrar(req).subscribe((response) => {
          console.log(response);
          if (response.cierre === "0") {
            this.snackBar.open('Reasignado', null, { duration: 2000 });
            this.obtenerListaPrincipalOt();
          }
        }, (error) => {
          console.log(error);
        });
      }
    });
  }
}
