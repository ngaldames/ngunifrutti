import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
const ELEMENT_DATA = [
  {
    guia: '22714', linea: '1', recepcion: '16/04/03', camara: '09 AC 09', envase: 'BINS', condicion: 'normal', desp: '0', kilos: '9.302', a_despachar:'21'
  }
];
@Component({
  selector: 'app-anular_despacho_packing',
  templateUrl: './anular_despacho_packing.component.html',
  styleUrls: ['./anular_despacho_packing.component.css']
})
export class Anular_despacho_packingComponent implements OnInit {
  guia: number;
  recepcion: any;
  camara: string;
  envase: string;
  condicion: string;
  linea: number;
  desp: number;
  kilos: any;
  a_despachar: number;
  displayedColumns: string[] = ['guia', 'linea', 'recepcion', 'camara', 'envase', 'condicion', 'desp', 'a_despachar', 'kilos'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private router: Router, public dialog: MatDialog, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Items por página';
  }
  volverDespachoPacking() {
    this.router.navigate(['/despacho_packing']);
  }
  anular() {
    this.snackBar.open('✔ Anulado', null, { duration: 800 });
    this.router.navigate(['/despacho_packing']);
  }
}
