import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
const ELEMENT_DATA = [
  {
    guia: '22714', linea: '1', recepcion: '16/04/03', camara: '09 AC 09', envase: 'BINS', condicion: 'normal', desp: '0', kilos: '9.302'
  }
];
@Component({
  selector: 'app-devolver_packing_por_ot',
  templateUrl: './devolver_packing_por_ot.component.html',
  styleUrls: ['./devolver_packing_por_ot.component.css']
})
export class Devolver_packing_por_otComponent implements OnInit {
  guia: number;
  recepcion: any;
  camara: string;
  envase: string;
  condicion: string;
  linea: number;
  desp: number;
  kilos: any;
  //icons: any;
  a_despachar: number;
  displayedColumns: string[] = ['guia', 'linea', 'recepcion', 'camara', 'envase', 'condicion', 'desp', 'a_despachar', 'kilos'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private router: Router, public dialog: MatDialog, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Items por página';
  }
  volverDespachoPacking() {
    this.router.navigate(['/despacho_packing']);
  }
  devolver() {
    this.snackBar.open(' ✔  Devuelto a packing', null, { duration: 800 });
    this.router.navigate(['/despacho_packing']);
  }
}
