import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router, NavigationExtras } from '@angular/router';
import { ApiCommonService } from '../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';
const ELEMENT_DATA = [
  {
    ot: '26616', fecha: '16/11/18', especie:'', origen: '60 TR PLTA UNIF', destino: 'ROMERAL', icons: ''
  }
];
@Component({
  selector: 'app-despacho_planta_terceros',
  templateUrl: './despacho_planta_terceros.component.html',
  styleUrls: ['./despacho_planta_terceros.component.css']
})
export class Despacho_planta_tercerosComponent implements OnInit {
  public cargando = true;
  ot: number;
  fecha: string;
  especie: string;
  origen: string;
  destino: string;
  icons: any;
  planta:any;
  temporada:any;
  zona_prod:any;
  exportadora:any;
  displayedColumns: string[] = ['id', 'fechaProceso','especie', 'origen', 'frigorifico', 'icons'];
  dataSource = new MatTableDataSource([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private router: Router, public dialog: MatDialog, 
              public snackBar: MatSnackBar,
              private logisticaService: ApilogisticaService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Items por página';
    this.planta = localStorage.getItem('Nplanta');
    this.temporada = localStorage.getItem('temporada');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona_prod = localStorage.getItem('planta');

    setTimeout(() => {
      this.listadoPrincipalDespachoATercero();
    }, 1000);

  }
  listadoPrincipalDespachoATercero(){
    const req = {
      temporada:this.temporada,
      zona_prod:this.zona_prod,
      exportadora:this.exportadora
    };
    this.logisticaService.listadoPrincipalDespachoATercero(req).subscribe((res) =>{
      console.log(res);
      this.dataSource = new MatTableDataSource(res.items);
      this.cargando = false;
    },(error) =>{
      console.log(error);  
    });
  }

  asignarBins(item){
    console.log(item);
    let extras: NavigationExtras = {
      queryParams:{
      'Linea':item.lineaMaquina,
      'asignacion':item.asignacion,
      'fechaProceso':item.fechaProceso,
      'ot':item.id,
      'productor':item.productor,
      'tipo_frio':item.tipoFrio,
      'turno':item.turno,
      'variedad':item.variedad,
      }
    }
    this.router.navigate(['/despacho_terceros_por_ot'], extras);
  }
}
