import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { Router, NavigationExtras } from '@angular/router';
import { ApiCommonService } from '../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';
import { bloomAdd } from '@angular/core/src/render3/di';

const ELEMENT_DATA = [
  {
    ot: '26616', fecha: '16/11/18', especie:'', origen: '60 TR PLTA UNIF', productor: '20 090 HURTADO', icons: ''
  }
];

@Component({
  selector: 'app-despacho_a_productores',
  templateUrl: './despacho_a_productores.component.html',
  styleUrls: ['./despacho_a_productores.component.css']
})
export class Despacho_a_productoresComponent implements OnInit {
  public cargando = true;
  isActive: boolean = false;
  concepto : string = '';
  ot: number;
  fecha: string;
  especie: string;
  origen: string;
  productor: string;
  icons: any;
  planta:any;
  temporada:any;
  zona_prod:any;
  exportadora:any;
  displayedColumns: string[] = ['id', 'fechaProceso','especie', 'origen', 'productor', 'icons'];
  dataSource = new MatTableDataSource([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private router: Router, public dialog: MatDialog, 
              public snackBar: MatSnackBar,
              private apicommon: ApiCommonService,
              private logisticaService: ApilogisticaService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.planta = localStorage.getItem('Nplanta');
    this.temporada = localStorage.getItem('temporada');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona_prod = localStorage.getItem('planta');

    setTimeout(() => {
      this.listadoPrincipalDevolverProductor();
    }, 1000);

  
  }
  listadoPrincipalDevolverProductor(){
    if (!this.isActive) {
      this.concepto = '  62'
    }else {
      this.concepto = '  64'
    }
    const req = {
      temporada:this.temporada,
      zona_prod:this.zona_prod,
      exportadora:this.exportadora,
      concepto:this.concepto
    };
    this.logisticaService.listadoPrincipalDevolucionProductor(req).subscribe(  (res)=>{
      console.log(res);
      this.dataSource = new MatTableDataSource(res.items);
      this.cargando = false;
    },(error)=>{
      console.log(error);
    });
  }

  asignarBins(item) {
    console.log(item);
    let extras: NavigationExtras = {
      queryParams:{
      'Linea':item.lineaMaquina,
      'asignacion':item.asignacion,
      'fechaProceso':item.fechaProceso,
      'ot':item.id,
      'productor':item.productor,
      'tipo_frio':item.tipoFrio,
      'turno':item.turno,
      'variedad':item.variedad,
      'concepto':this.concepto
      }
    }
    this.router.navigate(['/despacho_productores_por_ot'], extras);
  }
  cambioEstado(event) {
    
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.listadoPrincipalDevolverProductor();
    }
    else {
      this.listadoPrincipalDevolverProductor();
    }
  }
}
