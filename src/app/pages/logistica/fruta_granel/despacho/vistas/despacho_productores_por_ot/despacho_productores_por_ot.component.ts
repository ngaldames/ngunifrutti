import { Asignar_folios_productoresComponent } from './../../dialogs/asignar_folios_productores/asignar_folios_productores.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiCommonService } from '../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';

@Component({
  selector: 'app-despacho_productores_por_ot',
  templateUrl: './despacho_productores_por_ot.component.html',
  styleUrls: ['./despacho_productores_por_ot.component.css']
})
export class Despacho_productores_por_otComponent implements OnInit {
  folio: string;
  productor: string;
  especie: string;
  grupo: string;
  subgrupo: string;
  variedad: string;
  tipo_frio: string;
  condicion: string;
  envase: string;
  fecha_cosecha: string;
  planta: any;
  temporada: any;
  zona_prod: any;
  exportadora: any;
  ot: any;
  frigorifico: any;
  destino: any;
  fechaProceso: any;
  patente: string = '';
  observacion: string = '';
  concepto:string;
  guardarFolio: boolean = false;

  displayedColumns: string[] = ['folio', 'productor', 'especie', 'grupo', 'subgrupo', 'variedad', 'tipo_frio', 'condicion', 'envase', 'fecha_cosecha'];
  dataSource = new MatTableDataSource([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private apicommon: ApiCommonService,
    private logisticaService: ApilogisticaService,
    private activatedRouter: ActivatedRoute) {
    activatedRouter.queryParamMap.subscribe(params => {
      console.log(params);
      this.ot = params['params']['ot'];
      this.concepto = params['params']['concepto'];
    });
  }

  ngOnInit() {
    this.planta = localStorage.getItem('Nplanta');
    this.temporada = localStorage.getItem('temporada');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona_prod = localStorage.getItem('planta');

    this.encabezadoDespachoInterplanta();
    setTimeout(() => {
      this.obtenerDetalle();
    }, 500);
  }
  encabezadoDespachoInterplanta() {
    const req = {
      temporada: this.temporada,
      zona_prod: this.zona_prod,
      exportadora: this.exportadora,
      nro_ot: this.ot
    }

    this.logisticaService.getEncabezadodespachoInterplanta(req).subscribe((res) => {
      console.log('ENCABEZADO DESPACHO INTERPLANTA', res);

      this.frigorifico = res.frigorifico.nombre;
      this.destino = res.frigorificoDestino.nombre;
      this.fechaProceso = res.fechaProceso;
      this.envase = res.envaseCosecha.nombre;
      this.patente = res.patente;
      this.observacion = res.observaciones1.trim();

    }, (error) => {
      console.log(error);
    });

  }
  obtenerDetalle() {
    const req = {
      temporada: this.temporada,
      zona_prod: this.zona_prod,
      exportadora: this.exportadora,
      ot: this.ot
    }
    this.logisticaService.getDetalledespachoInterplanta(req).subscribe((res) => {
      console.log('DETALLE DESPACHO PRODUCTORES', res);

      this.dataSource = new MatTableDataSource(res.items);


    }
    ,(error)=>{console.log(error);
      if (!error.ok) {
        this.guardarFolio = true;
      }
      
    });
  }
  volverDespachoInterplanta() {
    this.router.navigate(['/despacho_productores']);
  }

  finalizar() {
    this.snackBar.open(' ✔  Despacho finalizado', null, { duration: 800 });
    this.router.navigate(['/despacho_productores'])
  }
  anadirFolios() {
    if (this.patente === '') {
      this.snackBar.open('Debe ingresar una patente', null, { duration: 3000 });
    }else{
    const dialogRef = this.dialog.open(Asignar_folios_productoresComponent, {
      width: '1200px',
      height: 'auto',
      data: { nro_ot: this.ot, patente: this.patente, observacion: this.observacion, concepto: this.concepto, guardarFolio: this.guardarFolio}
    });
    dialogRef.afterClosed()
      .subscribe((data) => {
        console.log(data);
        this.obtenerDetalle();
      }, (error) => {
        console.error('ERROR CERRANDO FOLIO', error);
      });
    }
  }
}
