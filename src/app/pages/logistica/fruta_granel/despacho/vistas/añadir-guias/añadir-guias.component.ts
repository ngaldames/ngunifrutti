import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar, MatTableDataSource, MatPaginator } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Asignar_folios_adespacharComponent } from '../../dialogs/asignar_folios_adespachar/asignar_folios_adespachar.component';
export interface datos {
  guia: number;
  recepcion: any;
  camara: string;
  envase: string;
  condicion: string;
  linea: number;
  desp: number;
  bultos: number;
  saldos: number;
  a_despachar: number;
  position: number;

}
const ELEMENT_DATA : datos[] = [
 /* {
    guia: 22714, linea: 1, recepcion: '16/04/03', camara: '09 AC 09', envase: 'BINS', condicion: 'normal', bultos: 32,
    saldos: 21, desp: 0, icons: ''
  }*/
];
@Component({
  selector: 'app-añadir-guias',
  templateUrl: './añadir-guias.component.html',
  styleUrls: ['./añadir-guias.component.css']
})
export class AñadirGuiasComponent implements OnInit {
  displayedColumns: string[] = ['select', 'guia', 'linea', 'recepcion', 'camara', 'envase', 'condicion', 'desp', 'a_despachar', 'bultos', 'saldos'];
  dataSource = new MatTableDataSource<datos>(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  selection = new SelectionModel<datos>(true, []);
  constructor(private router: Router, public dialog: MatDialog, public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: datos): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  volverDespachoPackingOT() {
    this.router.navigate(['/despacho_packing_por_ot']);
  }

  asignar_folio_despachar() {
    const dialogRef = this.dialog.open(Asignar_folios_adespacharComponent, {
      width: '400px',
    });
  }
  guardar() {
    this.snackBar.open(' ✔  Guías añadidas', null, { duration: 800 });
    this.router.navigate(['/despacho_packing_por_ot']);
  }
}
