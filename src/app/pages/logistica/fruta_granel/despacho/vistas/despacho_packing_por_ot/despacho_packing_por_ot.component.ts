import { Asignar_folios_adespacharComponent } from './../../dialogs/asignar_folios_adespachar/asignar_folios_adespachar.component';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Eliminar_folios_adespacharComponent } from '../../dialogs/eliminar_folios_adespachar/eliminar_folios_adespachar.component';
import { CerrarOTDespachoComponent } from '../../dialogs/cerrar-otdespacho/cerrar-otdespacho.component';

@Component({
  selector: 'app-despacho_packing_por_ot',
  templateUrl: './despacho_packing_por_ot.component.html',
  styleUrls: ['./despacho_packing_por_ot.component.css']
})
export class Despacho_packing_por_otComponent implements OnInit {
  packing: any;
  guia: number;
  recepcion: any;
  camara: string;
  envase: string;
  condicion: string;
  linea: number;
  desp: number;
  kilos: any;
  icons: any;
  ot: any;
  temporada: string = null;
  planta: string = null;
  exportadora: string = null;
  zona: string = null;
  usuario: string = null;
  productor: string = null;
  frigoOrigen: any = null;
  frigoDestino: string = null;
  a_despachar: number;
  datosEncabezado: any[] = [];
  asignacion: string = null;
  turno: string = null;
  tipo_frio: string = null;
  especie: any;
  variedad: any;
  detalle: any;
  displayedColumns: string[] = ['Guia', 'Linea', 'FechaRecepcion', 'Camara', 'Envase', 'Condicion', 'Desp', 'ADespachar', 'Kilos', 'icons'];
  dataSource = new MatTableDataSource([]);
  constructor(private router: Router, private activatedRouter: ActivatedRoute,
    public dialog: MatDialog, public snackBar: MatSnackBar,
    private logisticaService: ApilogisticaService) {
    activatedRouter.queryParamMap.subscribe(params => {
      console.log(params);
      this.planta = params['params']['planta'];
      this.zona = params['params']['zona'];
      this.ot = params['params']['ot'];
      this.linea = params['params']['Linea'];
      this.turno = params['params']['turno'];
      this.detalle = params['params']['detalle']
      if (this.detalle) {
        this.displayedColumns = ['Guia', 'Linea', 'FechaRecepcion', 'Camara', 'Envase', 'Condicion', 'Desp', 'ADespachar', 'Kilos',];
      }

      this.temporada = localStorage.getItem('temporada');
      this.exportadora = localStorage.getItem('exportadora');

    });
  }

  ngOnInit() {
    this.obtenerEncabezadoOt();
    this.obtenerDetelleOTOt();
  }

  volverDespachoPacking() {
    this.router.navigate(['/despacho_packing']);
  }
  // asignar_folio_despachar() {
  //   const dialogRef = this.dialog.open(Asignar_folios_adespacharComponent, {
  //     width: '400px',
  //   });
  // }
  despachar() {
    this.snackBar.open(' ✔  Despachado', null, { duration: 800 });
    this.router.navigate(['/despacho_packing']);
  }
  anadirGuia() {
    this.router.navigate(['/anadir_guias']);
  }
  obtenerEncabezadoOt() {
    let req = {
      temporada: this.temporada,
      zona_prod: this.zona,
      exportadora: this.exportadora,
      nro_ot: parseInt(this.ot) //'20055'//aca se cambia por this.ot
    }
    console.log(req);
    this.logisticaService.obtenerEncabezadoOt(req).subscribe(data => {
      console.log(data);
      if (data) {
        console.log('ENCABEZADO OT', data);
        this.asignacion = data.asignacion;
        this.packing = data.packing.nombre;
        this.productor = data.productor.nombre;
        this.tipo_frio = data.tipoFrio.nombre;
        this.especie = data.especie.nombre;
        this.variedad = data.variedad.nombre;
        this.frigoOrigen = data.frigorifico.nombre;

      }
      else {
        this.snackBar.open('Error al cargar el encabezado', null, { duration: 3000 });
      }
    }, (error) => {
      console.error('ERROR ZONAS', error);
      this.snackBar.open('Error al cargar zonas', null, { duration: 3000 });
    });
  }
  asignar_bins(item) {
    console.log(item);
    // if(item.folioCKU > 0){
    const dialogRef = this.dialog.open(Asignar_folios_adespacharComponent, {
      width: '500px',
      disableClose: true,
      data: {
        temporada: this.temporada, planta: this.planta, exportadora: this.exportadora, zona: this.zona, cod_ot: this.ot,
        especie: this.especie, variedad: this.variedad, item
      }
    });
    dialogRef.afterClosed()
      .subscribe((data) => {
        console.log(data);
        if (data.actualizar) {
          this.obtenerDetelleOTOt();
        }
      }, (error) => {
        console.error('ERROR CERRANDO FOLIO', error);
      });
    // }else{
    // this.snackBar.open('Primero debe asignar folio CKU', null, { duration: 2000 }); 
    // }
  }
  eliminar_bins(item) {
    console.log(item);
    const dialogRef = this.dialog.open(Eliminar_folios_adespacharComponent, {
      width: '500px',
      disableClose: true,
      data: { temporada: this.temporada, planta: this.planta, exportadora: this.exportadora, zona: this.zona, cod_ot: this.ot, item }
    });
    dialogRef.afterClosed()
      .subscribe((data) => {
        if (data.actualizar) {
          this.obtenerDetelleOTOt();
        }
      }, (error) => {
        console.error('ERROR CERRANDO FOLIO', error);
      });

  }
  obtenerDetelleOTOt() {
    let req = {
      CodTemp: this.temporada,
      CodZonaProd: this.zona,
      CodExportadora: this.exportadora,
      NumeroOt: this.ot//'65841'// aca se cambia por this.ot
    }
    this.logisticaService.obtenerDetelleOTOt(req).subscribe(data => {
      console.log(data);
      if (data.lista) {
        this.dataSource = new MatTableDataSource(data.lista);
      } else {
        this.snackBar.open('Error al cargar el detalle', null, { duration: 2000 });
      }
    }, (error) => {
      console.error('ERROR DETALLES', error);
    });
  }
  

}


