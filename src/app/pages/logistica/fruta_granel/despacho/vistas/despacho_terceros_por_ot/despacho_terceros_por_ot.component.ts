import { Asignar_folios_a_tercerosComponent } from './../../dialogs/asignar_folios_a_terceros/asignar_folios_a_terceros.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiCommonService } from '../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';

const ELEMENT_DATA = [
  {
    folio: '22714', productor: '1', especie: 'nnn', grupo: '8', subgrupo: '360', variedad: 'uniteno', tipo_frio: 'scarlet', condicion: 'F/C', envase: 'nnn', fecha_cosecha: '11'
  }
];

@Component({
  selector: 'app-despacho_terceros_por_ot',
  templateUrl: './despacho_terceros_por_ot.component.html',
  styleUrls: ['./despacho_terceros_por_ot.component.css']
})
export class Despacho_terceros_por_otComponent implements OnInit {
  folio: string;
  productor: string;
  especie: string;
  grupo: string;
  subgrupo: string;
  variedad: string;
  tipo_frio: string;
  condicion: string;
  envase: string;
  fecha_cosecha: string;
  planta: any;
  temporada: any;
  zona_prod: any;
  exportadora: any;
  ot: any;
  frigorifico: any;
  destino: any;
  fechaProceso: any;
  patente: string = '';
  observacion:string = '';
  guardarFolio: boolean = false;

  displayedColumns: string[] = ['folio', 'productor', 'especie', 'grupo', 'subgrupo', 'variedad', 'tipo_frio', 'condicion', 'envase', 'fecha_cosecha'];
  dataSource = new MatTableDataSource([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private apicommon: ApiCommonService,
    private logisticaService: ApilogisticaService,
    private activatedRouter: ActivatedRoute) {
    activatedRouter.queryParamMap.subscribe(params => {
      // console.log(params);
      this.ot = params['params']['ot'];
    });
  }

  ngOnInit() {
    this.planta = localStorage.getItem('Nplanta');
    this.temporada = localStorage.getItem('temporada');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona_prod = localStorage.getItem('planta');

    this.encabezadoDespachoInterplanta();
    setTimeout(() => {
      this.obtenerDetalle();
    }, 500);
  }

  encabezadoDespachoInterplanta() {
    const req = {
      temporada: this.temporada,
      zona_prod: this.zona_prod,
      exportadora: this.exportadora,
      nro_ot: this.ot
    }

    this.logisticaService.getEncabezadodespachoInterplanta(req).subscribe((res) => {
      console.log('ENCABEZADO DESPACHO A TERCEROS', res);

      this.frigorifico = res.frigorifico.nombre;
      this.destino = res.frigorificoDestino.nombre;
      this.fechaProceso = res.fechaProceso;
      this.envase = res.envaseCosecha.nombre;
      this.patente = res.patente;
      this.observacion = res.observaciones1.trim();

    }, (error) => {
      console.log(error);
    });

  }

  volverDespachoInterplanta() {
    this.router.navigate(['/despacho_planta_terceros']);
  }

  finalizar() {
    this.snackBar.open(' ✔  Despacho a terceros finalizado', null, { duration: 800 });
    this.router.navigate(['/despacho_planta_terceros'])
  }

  anadirFolios() {
    if (this.patente === '') {
      this.snackBar.open('Debe ingresar una patente', null, { duration: 3000 });
    } else {
      const dialogRef = this.dialog.open(Asignar_folios_a_tercerosComponent, {
        width: '1200px',
        height: 'auto',
        data: { nro_ot: this.ot, patente: this.patente, observacion: this.observacion, guardarFolio: this.guardarFolio }
      });
      dialogRef.afterClosed()
      .subscribe((data) => {
        console.log(data);
          this.obtenerDetalle();
      }, (error) => {
        console.error('ERROR CERRANDO FOLIO', error);
      });
    }
  }
  obtenerDetalle() {
    const req = {
      temporada: this.temporada,
      zona_prod: this.zona_prod,
      exportadora: this.exportadora,
      ot: this.ot
    }
    this.logisticaService.getDetalledespachoInterplanta(req).subscribe((res) => {
      console.log('DETALLE DESPACHO A TERCEROS', res);

      this.dataSource = new MatTableDataSource(res.items);

    }, (error) => { console.log(error);
      if (!error.ok) {
        this.guardarFolio = true;
      }
     });
  }
}
