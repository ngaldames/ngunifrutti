import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';
import { ActivatedRoute } from '@angular/router';
import { async, delay } from 'q';

 @Component({
  selector: 'app-asignar_folios_adespachar',
  templateUrl: './asignar_folios_adespachar.component.html',
  styleUrls: ['./asignar_folios_adespachar.component.css']
})

export class Asignar_folios_adespacharComponent implements OnInit {
  listaBins:any[] = [];

  asignarDialog = true;
  ot: any;
  temporada: string = null;
  planta: string = null;
  exportadora: string = null;
  zona: string = null;
  guia: number;
  recepcion: any;
  camara: string;
  envase: string;
  condicion: string;
  linea: number;
  desp: number;
  kilos: any;
  a_despachar: number;
  bins:any;
  seleccionadasTrue:any[]=[];
  seleccionadasFalse:any[]=[];
  claseFrigrifico:any=[];
  claseTemporada:any=[];
  claseZona:any=[];
  claseCamara:any=[];
  guia2:number;
  especie:any;
  variedad:any;
  huerto: any;

  usuarioCreacion:any;

  constructor(public dialogRef: MatDialogRef<Asignar_folios_adespacharComponent>,
    public snackBar: MatSnackBar,
    private logisticaService: ApilogisticaService,
    private params: ActivatedRoute, @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);

    // DATOS ENCABEZADOS
    this.temporada = data.temporada;
    this.planta = data.planta;
    this.exportadora = data.exportadora;
    this.zona = data.zona;
    this.ot = data.cod_ot;
    this.linea = data.item.Linea;
    this.guia2 = data.item.Guia;
    this.especie = data.item.Especie.nombre;
    this.variedad = data.item.Variedad.nombre;
    this.huerto = data.item.Huerto.nombre;
    this.camara = data.Camara;
    

    this.usuarioCreacion = localStorage.getItem('User');

  }

  ngOnInit() {
    setTimeout(() => {
      this.obtenerEncabezadoDespachoAPacking();
    }, 1000);
  }

  close(): void {
    this.dialogRef.close({actualizar:true});
  }

  checkTodo(event) {
    this.seleccionadasTrue = [];
    if (event.checked) {
      this.listaBins.forEach(element => {
        element.check = true;
        if (element.check) {
          const info = {
            folio: element.folio,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    } else {
      this.listaBins.forEach(element => {
        element.check = false;
        if (!element.check) {
          this.seleccionadasTrue= [];
        }
      });
    }
    console.log(this.listaBins);
  }
  checkSeleccionados(event){
    this.seleccionadasTrue = [];
    this.seleccionadasFalse = [];
    if (event.checked) {
      this.listaBins.forEach(element => {
        if (element.check) {
          const info = {
            folio: element.folio,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    }
    if (!event.checked) {
      console.log(this.listaBins);
      this.listaBins.forEach(element => {
        if (element.check) {
          const info = {
            folio: element.folio,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    }
  }
 
  obtenerEncabezadoDespachoAPacking() {
    const req = {
      CodTemp: this.temporada,
      CodZonaProd: this.zona,
      CodExportadora: this.exportadora,
      NumGuia:this.guia2,
      NumLinea:this.linea,
      NumeroOt: this.ot//Despues se cambia a this.ot
    };
    console.log(req);
    // return;
    this.logisticaService.obtenerEncabezadoDespachoAPacking(req).subscribe((response) => {
      console.log(response);
      if (response.valido) {
        this.listaBins = response.listaFolios;
        this.listaBins.forEach(element => {
          element.check = false;
        });
      }
      if (!response.valido) {
        this.listaBins = response.listaFolios;
        if(this.listaBins.length===0){
          this.seleccionadasTrue = [];
        }
      }

    }, (error) => {
      console.log(error);
    });
  }
  asignarFolios(){
    this.asignarDialog = false;
    let lista = [];
    let fecha = new Date();
    if (this.seleccionadasTrue.length === 0) {
      this.snackBar.open('Debe seleccionar folios', null, { duration: 3000 });
      return;
    }
    this.seleccionadasTrue.forEach(element =>{
      if(element.check){
        const req = {
          zona:{id:20},
          temporada:{id:'17/18'},
          nroOT:parseInt(this.ot),
          id:element.folio,
          status:'N',
          usuarioCreacion:'USR01'
        }
        lista.push(req);
        this.logisticaService.postAsignarBins(req).subscribe((response) =>{
          console.log(response);
          if (response) {
            this.obtenerEncabezadoDespachoAPacking();
          }
          else{
            this.snackBar.open('Error al asignar folios', null, { duration: 3000 });
          }
        },(error)=>{
          console.log(error);
        })
      }
    });

    setTimeout(() =>{
      console.log(this.seleccionadasTrue.length, this.listaBins.length);
      if (this.seleccionadasTrue.length === this.listaBins.length) {
        this.seleccionadasTrue = [];
        this.listaBins=[];
        this.obtenerEncabezadoDespachoAPacking();
      }else{
        this.obtenerEncabezadoDespachoAPacking();
      }
      this.snackBar.open('Folios Asignados', null, { duration: 3000 });
      this.asignarDialog = true;
    },3000);
    
    console.log(JSON.stringify(lista));
  }
}
