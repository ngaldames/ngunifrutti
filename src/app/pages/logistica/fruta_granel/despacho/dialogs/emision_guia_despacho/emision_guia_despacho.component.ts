import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-emision_guia_despacho',
  templateUrl: './emision_guia_despacho.component.html',
  styleUrls: ['./emision_guia_despacho.component.css']
})
export class Emision_guia_despachoComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<Emision_guia_despachoComponent>) { }

  ngOnInit() {
  }
  close(): void {
    this.dialogRef.close();
  }

  emitirGuia(){
    this.dialogRef.close();
  }
}
