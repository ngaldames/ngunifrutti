import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';
import { ActivatedRoute } from '@angular/router';
import { async } from 'q';

@Component({
  selector: 'app-eliminar_folios_adespachar',
  templateUrl: './eliminar_folios_adespachar.component.html',
  styleUrls: ['./eliminar_folios_adespachar.component.css']
})
export class Eliminar_folios_adespacharComponent implements OnInit {
  listaBins:any[] = [];

  color = 'primary';
  mode = 'determinate';
  value = 50;
  eliminarDialog = true;

  ot: any;
  temporada: string = null;
  planta: string = null;
  exportadora: string = null;
  zona: string = null;
  guia: any;
  recepcion: any;
  camara: string;
  envase: string;
  condicion: string;
  linea: any;
  desp: number;
  kilos: any;
  a_despachar: number;
  bins:any;
  seleccionadasTrue:any[]=[];
  seleccionadasFalse:any[]=[];
  claseFrigrifico:any=[];
  claseTemporada:any=[];
  claseZona:any=[];
  claseCamara:any=[];
  guia2:any;
  especie:any;
  variedad:any;
  huerto: any;

  usuarioCreacion:any;

  constructor(public dialogRef: MatDialogRef<Eliminar_folios_adespacharComponent>,
    public snackBar: MatSnackBar,
    private logisticaService: ApilogisticaService,
    private params: ActivatedRoute, @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);

    // DATOS ENCABEZADOS
    this.temporada = data.temporada;
    this.planta = data.planta;
    this.exportadora = data.exportadora;
    this.zona = data.zona;
    this.ot = data.cod_ot;
    this.guia2 = data.item.Guia;
    this.especie = data.item.Especie.nombre;
    this.variedad = data.item.Variedad.nombre;
    this.huerto = data.item.Huerto.nombre;
    this.camara = data.Camara;

    this.linea = data.item.Linea;
    

    this.usuarioCreacion = localStorage.getItem('User');

  }

  ngOnInit() {
    this.obtenerEncabezadoDespachoAPacking();
    console.log(this.linea);
  }

  close(): void {
    this.dialogRef.close({actualizar:true});
  }

  checkTodo(event) {
    this.seleccionadasTrue = [];
    if (event.checked) {
      this.listaBins.forEach(element => {
        element.check = true;
        if (element.check) {
          const info = {
            folio: element.folio,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    } else {
      this.listaBins.forEach(element => {
        element.check = false;
        if (!element.check) {
          this.seleccionadasTrue= [];
        }
      });
    }
    console.log(this.listaBins);
  }
  checkSeleccionados(event){
    this.seleccionadasTrue = [];
    this.seleccionadasFalse = [];
    if (event.checked) {
      this.listaBins.forEach(element => {
        if (element.check) {
          const info = {
            folio: element.folio,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    }
    if (!event.checked) {
      console.log(this.listaBins);
      this.listaBins.forEach(element => {
        if (element.check) {
          const info = {
            folio: element.folio,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    }
  }
 
  obtenerEncabezadoDespachoAPacking() {

    const req = {
      CodTemp: this.temporada,
      CodZonaProd: this.zona,
      CodExportadora: this.exportadora,
      NumGuia:this.guia2,
      NumLinea:this.linea,
      NumeroOt: this.ot//Despues se cambia a this.ot
    };
    console.log(req);
    // return;
    this.logisticaService.getAsignadosBins(req).subscribe((response) => {
      console.log(response);
      if (response.valido) {
        this.listaBins = response.listaFolios;
        this.listaBins.forEach(element => {
          element.check = false;
        });
      }

    }, (error) => {
      console.log(error);
    });
  }
  asignarFolios(){
    this.eliminarDialog = false;
    let lista = [];
    let fecha = new Date();
    if (this.seleccionadasTrue.length === 0) {
      this.snackBar.open('Debe seleccionar folios', null, { duration: 3000 });
      return;
    }
    this.seleccionadasTrue.forEach(element =>{
      if(element.check){
        const req = {
          temporada:this.temporada,
          zona_prod:this.zona,
          exportadora:this.exportadora,
          nro_ot:parseInt(this.ot),
          bins:element.folio
        }
        console.log(req);
        this.logisticaService.EliminarAsignadosBins(req).subscribe(( response) =>{
          console.log(response);
          if (response === null) {
            // this.obtenerEncabezadoDespachoAPacking();
            
          } else{
            this.snackBar.open('Error al eliminar los folios', null, { duration: 3000 });
          }
          
        },(error)=>{
          console.log(error);
        })
      }
    });
    setTimeout(() => {
      if (this.seleccionadasTrue.length === this.listaBins.length) {
        this.seleccionadasTrue = [];
        this.listaBins=[];
        this.obtenerEncabezadoDespachoAPacking();
      }else{
        this.obtenerEncabezadoDespachoAPacking();
      }
      this.snackBar.open('Folios eliminado', null, { duration: 3000 });
      this.eliminarDialog = true;
    }, 2000);
    console.log(JSON.stringify(lista));
  }
  // foliosOt() {
  //   const req = {
  //     temp: this.temporada,
  //     planta: this.planta,
  //     exportadora: this.exportadora,
  //     zona: this.zona,
  //     cod_ot: this.ot
  //   }
  //   this.logisticaService.foliosOt(req).subscribe((response) => {
  //     console.log(response);
  //     this.listaBins = response.lista;
  //     this.listaBins.forEach(element => {
  //       element.check = false;
  //     });
  //   }, (error) => {
  //     console.log(error);
  //   });
  // }
  
}
