import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-cerrar-otdespacho',
  templateUrl: './cerrar-otdespacho.component.html',
  styleUrls: ['./cerrar-otdespacho.component.css']
})
export class CerrarOTDespachoComponent implements OnInit {
  detalle: any;
  mensaje: string = '¿Seguro que desea cerrar la ot?';

  constructor(public dialogRef: MatDialogRef<CerrarOTDespachoComponent>,
    @Inject(MAT_DIALOG_DATA) public data :any) {
      console.log(data);
      
        this.detalle = data.detalle;
      
     }

  ngOnInit() {
    console.log(this.detalle);
    if (this.detalle) {
      this.mensaje = '¿Seguro que desea reasignar la ot?';
    }

  }
  Si(){
    if (this.detalle) {
      this.dialogRef.close({reasignar: true});
    }else {
      this.dialogRef.close({cerrar: true});
    }
  }
  No(){
    this.dialogRef.close();
  }
}
