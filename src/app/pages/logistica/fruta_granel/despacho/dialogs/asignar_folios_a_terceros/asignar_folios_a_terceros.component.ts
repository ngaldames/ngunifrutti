import { Component, OnInit, Inject } from '@angular/core';
import { MatTableDataSource, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';


@Component({
  selector: 'app-asignar_folios_a_terceros',
  templateUrl: './asignar_folios_a_terceros.component.html',
  styleUrls: ['./asignar_folios_a_terceros.component.css']
})
export class Asignar_folios_a_tercerosComponent implements OnInit {
  folio: string;
  productor: string;
  especie: string;
  grupo: string;
  subgrupo: string;
  variedad: string;
  tipo_frio: string;
  condicion: string;
  envase: string;
  fecha_cosecha: string;
  planta: any;
  temporada: any;
  zona_prod: any;
  exportadora: any;
  ot: any;
  patente: string;
  observacion: string;
  guardarFolio:boolean;

  listaBins: any[] = [];
  seleccionadasTrue: any[] = [];
  seleccionadasFalse: any[] = [];

  displayedColumns: string[] = ['select', 'id', 'productor.nombre', 'especie.nombre', 'grupo', 'subgrupo', 'variedad', 'tipoFrio', 'condicion', 'envaseCosecha', 'fechaCosecha'];
  dataSource = new MatTableDataSource([]);
  selection = new SelectionModel(true, []);
  constructor(public dialogRef: MatDialogRef<Asignar_folios_a_tercerosComponent>,
    public snackBar: MatSnackBar,
    private logisticaService: ApilogisticaService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.ot = data.nro_ot;
    this.patente = data.patente;
    this.observacion = data.observacion;
    this.guardarFolio = data.guardarFolio;
    console.log(data, this.ot);
    }

  ngOnInit() {
    this.planta = localStorage.getItem('Nplanta');
    this.temporada = localStorage.getItem('temporada');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona_prod = localStorage.getItem('planta');

    setTimeout(() => {
      this.listadoDisponiblesDespachar();
    }, 500);
  
  }

  close(): void {
    this.dialogRef.close();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.folio + 1}`;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  listadoDisponiblesDespachar() {
    const req = {
      temporada: this.temporada,
      zona_prod: this.zona_prod,
      exportadora: this.exportadora,
      nro_ot: this.ot,
      movimiento: '  58'
    }
    this.logisticaService.getDisponiblesADespacharOtros(req).subscribe((res) => {
      console.log(res);
      this.listaBins = res.items;
      this.dataSource = new MatTableDataSource(res.items);
      
      // z<xzx<zx<zx
    })
  }
  checkTodo(event) {
    this.seleccionadasTrue = [];
    if (event.checked) {
      this.listaBins.forEach(element => {
        element.check = true;
        if (element.check) {
          const info = {
            folio: element.folio,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    } else {
      this.listaBins.forEach(element => {
        element.check = false;
        if (!element.check) {
          this.seleccionadasTrue = [];
        }
      });
    }
  }
  checkSeleccionados(event) {
    this.seleccionadasTrue = [];
    this.seleccionadasFalse = [];
    if (event.check) {
      this.listaBins.forEach(element => {
        if (element.check) {
          const info = {
            folio: element.id,
            nroGuia: element.nroGuia,
            nroLinea: element.nroLinea,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    }
    if (!event.check) {
      this.listaBins.forEach(element => {
        if (element.check) {
          const info = {
            folio: element.id,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    }

  }
  asignarFolios() {
    console.log(this.seleccionadasTrue);

    if (this.seleccionadasTrue.length > 0) {

      this.seleccionadasTrue.forEach(element => {
      
        const req = {
          zona: { id: this.zona_prod },
          temporada: { id: this.temporada },
          nroOT: parseInt(this.ot),
          nroGuia: element.nroGuia,
          nroLinea: element.nroLinea,
          id: element.folio,
          patente: this.patente
        }
        this.logisticaService.postPopUpDisponiblesDespacharInterplanta(req).subscribe((res)=>{
          console.log(res);

          if (res === 'OK') {
            this.snackBar.open('Folios asignados', null, { duration: 3000 });
            setTimeout(() => {
              this.patchGuardarObservacion();
            }, 100);
            setTimeout(() => {
              if (this.guardarFolio) {
                this.postDisponiblesDespacharCopia();
              }
            }, 150);
              
          }
        },(error)=>{
          console.log(error);
        });
        console.log(JSON.stringify(req));
      });  

    }else{
      this.snackBar.open('Debe seleccionar uno o máS folios', null, { duration: 3000 });
    }
  }

  postDisponiblesDespacharCopia(){
    const req = {
      temporada: this.temporada,
      zona: this.zona_prod,
      exportadora: this.exportadora,
      nro_ot: parseInt(this.ot)
    }
    this.logisticaService.postDisponiblesDespacharCopia(req).subscribe((res)=>{

      console.log(res);

    },(error)=>{console.log(error);})
  }

  patchGuardarObservacion(){
    const req = {
      temporada: {id:this.temporada},
      id: parseInt(this.ot),
      zona: {id:this.zona_prod},
      observaciones1: this.observacion
    };
    this.logisticaService.patchGuardarObservacionDespacharInterplantaTerceros(req).subscribe((res)=>{
      console.log('RES PATCH DETALLE',res);
    }, (error) =>{
      console.log(error);
    });
  }
}
