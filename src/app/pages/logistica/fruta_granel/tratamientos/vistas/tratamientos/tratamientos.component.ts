import { Asignar_productoComponent } from './../../dialogs/asignar_producto/asignar_producto.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatTableDataSource, MatDialog, MatSort, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute, UrlHandlingStrategy } from '@angular/router';
import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { ApiseguridadService } from './../../../../../../services/apiseguridad.service';
import chileanRut from 'chilean-rut';
import { EliminarProductoComponent } from '../../dialogs/eliminar-producto/eliminar-producto.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

export interface Usuario {
  id: string;
  nombre: string;
}
@Component({
  selector: 'app-tratamientos',
  templateUrl: './tratamientos.component.html',
  styleUrls: ['./tratamientos.component.css']
})

export class TratamientosComponent implements OnInit {

  @ViewChild("focus") focus: ElementRef;

  myControl = new FormControl();
  filteredOptions: Observable<Usuario[]>;
  
  ////////////////////// DECLARACION DE VARIABLES //////////////////
  cod_operativo: any = null;
  ingrediente: string;
  dosis: number;
  unidad: string;
  icons: any;
  numeroSolicitud: number;
  listadoEspecies: any[] = [];
  listadoVariedades: any[] = [];
  especieSeleccion: any;
  variedadSeleccion: any;
  paisSeleccion: any;
  tipoFrutaSeleccion: any;
  camara: string;
  seccionInput: string;
  camaraseleccion:any;
  envaseTipoNumero:any;
  seccion: string;
  tipoTratamientoSelect: any;
  volumenCamara: any;
  rutProveedor: string = '';
  nombreRSocial: string = '';
  usuario: string = '';
  rutAplicador: string;
  nombrePersonaExterna: string;
  licenciaAplicador: string;
  temperatura: number;
  numeroSello: number;
  numFolio: number;
  nroFactura:number = null;
  fechaFactura:any;
  montoNetoFac: string = null;
  tipoCambio: string = null;

  ////////////////////// DECLARACION DE VARIABLES GLOBALES //////////////////
  temporada: string = null;
  exportadora: string = null;
  zona: string = null;
  frigorifico: string = null;

  /////////////////// LISTADOS /////////////////////
  listadoUsuario: any[] = [];
  listadoPaises: any[] = [];
  listadoTipotratamiento: any[] = [];
  listadoDetalleTipotratamiento: any[] = [];
  listadoCamaraSeccion: any[] = [];
  listadoCamara: any[] = [];
  listadoTipoFruta: any[] = [];
  listadoProveedor: any[] = [];
  listadoEnvaseCosecha: any[]=[];
  item: any[] = []; //ES LO QUE VIENE DE LA PANTALLA ANTERIOR
    //////////////////// BIT /////////////////////
  ocultaragregar: boolean;
  facturar: boolean;
  mostrarejecutar: boolean;
  editar: boolean;
  ejecutar: boolean = false;
  granel: boolean = true;
  ejecutarPost:boolean = false;
  facturada: boolean = false;

  ////////////// fechas ////////////

  fechaSolicitud:any;
  fechaEjecucion: any;
  fechaAplicacion = new Date();
  horaAplicacion: any;
  fechaEjecucionAplicacion = new Date();

  fechaCosechaAntigua: any;
  fechaCosechaReciente: any;
  fechaLlenadoCamraPrimero: any;
  fechaLlenadoCamaraUltimo: any;
  fechaInicioTratamiento = new Date();
  fechaTerminoTratamiento: any;
  fechasAnalisistratamiento: any[] = [];
  // fechaFactura = new Date();

  ///////////////////// PARA GUARDAR LAS FECHAS DONDE SE PIDAN///////////////
  //////////////// let f_cosecha = new Date(this.fechaCosecha).toISOString();/////////////
  ///////////////////// PARA GUARDAR LAS FECHAS DONDE SE PIDAN///////////////


  dataSource = new MatTableDataSource([]);
  dataSource2 = new MatTableDataSource([]);
  dataSource3 = new MatTableDataSource([]);
  dataSource4 = new MatTableDataSource([]);
  @ViewChild(MatSort) sort: MatSort;

  public displayedColumns: string[] = ['ingrediente', 'dosis', 'unidad'];
  public displayedColumns2: string[] = ['producto', 'ingrediente', 'dosis', 'unidad', 'icons'];
  public displayedColumns3: string[] = ['id','frigorifico', 'camara', 'm3', 'fechaEjecucion', 'especie', 'nro_bins'];
  public displayedColumns4: string[] = ['Tipo', 'Especie', 'Bultos', 'KG'];
  constructor(public dialog: MatDialog,
    private router: Router,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService,
    private seguridadService: ApiseguridadService,
    private activatedRouter: ActivatedRoute,
    private snackBar: MatSnackBar) {
    activatedRouter.queryParamMap.subscribe((params: any) => {
      // console.log(params);
      
      if (params.params.item) {
        this.numeroSolicitud = parseInt(params.params.item);  
      
      }else{
        this.numeroSolicitud = 0;
      } 
        // this.item = JSON.parse(params.params.item);
      // console.log(params.params.factura);
        
      if (params.params.factura === 'true') {
        this.facturar = true;
        this.ocultaragregar = true;
      } 
      
      if (params.params.editar === 'true') {
        this.editar = true;
      }

      // this.numeroSolicitud = this.item['id'];
      if (params.params.ejecutar === 'true') {
        this.ocultaragregar = true;
        this.ejecutar = true;
        // this.facturar = true;
      }
      if (params.params.ejecutar === 'false') {
        this.ocultaragregar = false;
        this.ejecutar = false;
      }
      
      this.temporada = params.params.temporada;
      this.zona = params.params.zona;
      this.frigorifico = params.params.frigorifico;
      this.exportadora = params.params.exportadora;
    });

    this.fechaAplicacion = new Date();
    this.horaAplicacion = this.fechaAplicacion.getHours() + ':' + this.fechaAplicacion.getMinutes();
    let hora = this.fechaAplicacion.getHours().toString().length === 1 ? '0' + '' + this.fechaAplicacion.getHours() : this.fechaAplicacion.getHours();
    let minutos = this.fechaAplicacion.getMinutes().toString().length === 1 ? '0' + '' + this.fechaAplicacion.getMinutes() : this.fechaAplicacion.getMinutes();
    this.horaAplicacion = hora + ':' + minutos;

    this.fechaTerminoTratamiento = new Date();
    this.fechaEjecucion = new Date();
    this.fechaSolicitud = new Date();

  }

  ngOnInit() {
    if (this.numeroSolicitud != 0) {
      this.getEncabezadoEditar();
      this.getCamaraSeccion();
      this.getTipoFruta();
      this.getEnvaseCosecha();
      this.getDetalleTratamientoEjecutar();
    }
    this.cargarEspecie();
    this.cargarPaises();
    this.cargarTipoTratamientos();
    // this.getCamara();

    this.usuario = localStorage.getItem('User');
    // this.getAnalisisTratamiento();
    // this.getTipoFruta();
  }
  displayFn(usuario?: Usuario): string | undefined {
    return usuario ? usuario.nombre : undefined;
  }
  private _filter(des: string): Usuario[] {
    const filterValue = des;
    return this.listadoUsuario.filter(option => option.nombre.toLowerCase().indexOf(filterValue) === 0);
  }
  /////////////////////////FUNCION EDITAR//////////////////////

  getEncabezadoEditar() {
    const req = {
      nrotratamiento: this.numeroSolicitud,
      temporada: this.temporada,
      exportadora: this.exportadora,
      zona_prod: this.zona,
      frigorifico: this.frigorifico
    };
    this.logisticaService.getTratamientoEditar(req).subscribe(async (res: any) => {
      // console.log(JSON.stringify(res));
      this.paisSeleccion = await res['pais']['id'];
      this.especieSeleccion = await res['especie']['idsnap'];
      this.getEspecieTratamiento(this.especieSeleccion);
      this.usuario = await res['usuario']['id'];
      this.rutProveedor = await res['proveedor']['rut'].trim();
      
      //////// EJECUTAR//////
      this.numFolio = res.numFolio;
      this.fechaAplicacion = res.fechaEjecucion;
      this.camaraseleccion = res.camara.id;
      this.seccionInput = res.seccion.id;
      this.volumenCamara = res.camara.metroscubico;
      this.temperatura = res.numTemp;
      this.tipoFrutaSeleccion = res.tipoFruta.id;
      this.envaseTipoNumero = res.envaseCosecha.id;
      // this.cod_operativo = res.operativo.id;
      this.rutAplicador = res.aplicadorTratamiento.rut;
      this.nombrePersonaExterna = res.aplicadorTratamiento.nombres;
      this.licenciaAplicador = res.aplicadorTratamiento.numLicencia;
      // console.log(res.numFolio);
      if (res.numFolio === '') {
        this.fechaCosechaAntigua = new Date();
        this.fechaCosechaReciente = new Date();
        this.fechaLlenadoCamraPrimero = new Date();
        this.fechaLlenadoCamaraUltimo = new Date();
      }else {
        this.fechaCosechaAntigua = res.fechaMinCosecha;
        this.fechaCosechaReciente = res.fechaMaxCosecha;
        this.fechaLlenadoCamraPrimero = res.fechaEntrada;
        this.fechaLlenadoCamaraUltimo = res.fechaUltimo;
      }


      this.numeroSello = res.numSello;
      const operativo = {
        id : res.operativo.id,
        nombre:res.operativo.nombre
      }
      //FACTURA
      this.nroFactura = res.numDTEFactura;
      this.fechaFactura = res.fechaDTE;
      this.montoNetoFac = res.montoDTE;
      this.tipoCambio = res.tipoCambioDTE;
      
      this.cod_operativo = operativo;
      this.getProveedorTratamiento();
      this.getFacturados();
      this.stockCamara();

      this.cargarProveedor(this.rutProveedor + '     ');
      this.tipoTratamientoSelect = await res['tipoTratamiento']['id'];
      this.cargarDetalleIngredientesActivos(this.tipoTratamientoSelect);
      if (res.fechaSolicitud) {
        this.fechaSolicitud = await res['fechaSolicitud'];
      }
      if (res.fechaEjecucion) {
        this.fechaEjecucion = await res['fechaEjecucion'];
      }
    })
  }

  /////////////////////////FUNCION EDITAR-ELIMINAR//////////////////////

  cambio(event) {
    // console.log(event);
    this.getEspecieTratamiento(event);
  }
  asignarProducto(element) {
    const dialogRef = this.dialog.open(Asignar_productoComponent, {
      disableClose: true,
      width: 'auto',
      data:{item:element, nrotratamiento: this.numeroSolicitud, frigorifico: this.frigorifico}
    })
    dialogRef.afterClosed().subscribe(result => this.getDetalleTratamientoEjecutar());
  }
  eliminarProducto(element){
    const req = {
      nrotratamiento: this.numeroSolicitud,
      temporada: this.temporada,
      exportadora: this.exportadora,
      zona_prod: this.zona,
      frigorifico: this.frigorifico,
      producto: element.producto.id
    }
    // console.log(element);
    // console.log(req);
    const dialogRef = this.dialog.open(EliminarProductoComponent, {
      disableClose: true,
      width: 'auto'
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result.eliminar) {
        this.logisticaService.deleteProductoDetalleTratamiento(req).subscribe((res)=>{
          // console.log(res);
          if (res === null) {
            this.snackBar.open('Producto eliminado', null, { duration: 3000 });
            this.getDetalleTratamientoEjecutar();
          }
        },(error)=>{
          console.log(error);
          this.snackBar.open('Error al eliminar el producto', null, { duration: 3000 });
        });
      }else{
        console.log('no hacer nada');
      }
    });
  }

  /////////////// --APIS COMMON-- -INICIO- -- /////////////
  cargarEspecie() {
    this.commonService.cargarEspecies().subscribe(async (res) => {
      // console.log('GET CARGAR ESPECIES', res);
      this.listadoEspecies = await res.items;

    }, (error) => {
      console.log('ERROR AL CARGAR ESPECIES', error);
    })
  }

  cargarPaises() {
    this.commonService.getPais().subscribe(async (res) => {
      // console.log('GET PAISES', res);
      this.listadoPaises = await res.items;
    }, (error) => {
      console.log('ERROR AL CARGAR LOS PAISES', error);
    });
  }
  buscarProveedor() {
    // console.log(this.rutProveedor);
    if (this.rutProveedor.length === 10) {
      this.cargarProveedor(this.rutProveedor + '     ');
    } else {
      this.nombreRSocial = '';
    }
  }
  cargarProveedor(rutProveedor: string) {
    this.commonService.getProveedor().subscribe(async (res) => {
      // console.log('GET PROVEEDOR',res);
      this.listadoProveedor = await res.items;
      this.listadoProveedor.forEach(element => {
        if (element.rut === rutProveedor) {
          this.nombreRSocial = element.nombre;
        }
      });
    }, (error) => {
      console.log('ERROR AL CARGAR PROVEEDOR', error);
    })
  }
  cargarTipoTratamientos() {
    this.commonService.getTipoTratamientoAll().subscribe(async (res) => {
      // console.log('GET TIPO TRATAMIENTO', res);
      
    }, (error) => {
      console.log('ERROR AL CARGAR TIPO-TRATAMIENTO', error);
    });
  }
  getEspecieTratamiento(codEspecie:string){
    this.commonService.getespecieTratamiento(codEspecie).subscribe(async (res)=>{
      // console.log(res);
      this.listadoTipotratamiento = await res.items;
    },(error)=>{
      console.log(error);
    });
  }
  cargarDetalleIngredientesActivos(codtratamientio: string) {
    this.commonService.getTipoTratamientoIngreActivo(codtratamientio).subscribe(async (res) => {
      this.listadoDetalleTipotratamiento = await res.items;
      this.dataSource = new MatTableDataSource(this.listadoDetalleTipotratamiento);
      this.dataSource.sort = this.sort;
      // console.log('GET DETALLE INGREDIENTES ACTIVOS', await res);

    }, (error) => {
      console.log('ERROR AL CARGAR DETALLE INGREDIENTE ACTIVO', error);
    });
  }
  tipoTraSelect(event) {
    // console.log(event);
    this.cargarDetalleIngredientesActivos(event);
  }

  getAnalisisTratamiento(camara: string) {
    const req = {
      temporada: this.temporada,
      camara: camara,
      zona: this.zona,
      frigorifico: this.frigorifico
    }
    // console.log(req);
    this.commonService.getAnalisisTratamiento(req).subscribe(async (res) => {
      // console.log('GET ANALISIS TRATAMIENTO', res);
      this.fechasAnalisistratamiento = await res.fechas;
      this.fechaCosechaAntigua = this.fechasAnalisistratamiento['FechaCosechaMinima'];
      this.fechaCosechaReciente = this.fechasAnalisistratamiento['FechaCosechaMaxima'];
      this.fechaLlenadoCamraPrimero = this.fechasAnalisistratamiento['FechaIngresoMinima'];
      this.fechaLlenadoCamaraUltimo = this.fechasAnalisistratamiento['FechaIngresoMaxima'];

    }, (error) => {
      console.log('ERROR AL CARGAR ANALISIS TRATAMIENTO', error);
    });
  }
  getCamaraSeccion() {
    const req = {
      seccion: 1,
      zona: this.zona,
      frigorifico: this.frigorifico
    }
    this.commonService.getCamaraSeccion(req).subscribe(async (res) => {
      this.listadoCamaraSeccion = await res.items;
      // console.log('GET CAMARA SECCION', this.listadoCamaraSeccion);
      this.camara = this.listadoCamaraSeccion[0]['id'];
      this.seccion = this.listadoCamaraSeccion[0]['seccionPlanta']['id'];
    }, (error) => {
      console.log('ERROR GET CAMARA SECCION', error);
    });
  }
  getCamara(event: string) {
    // console.log(event.replace("", ''));
    const req = {
      camara: this.camara,
      seccion: this.seccion,
      zona: this.zona,
      frigorifico: this.frigorifico
    };
    // console.log(req);
    this.commonService.getCamara(req).subscribe(async (res: any) => {
      // console.log('GET CAMARA', await res);
      this.listadoCamara = await res;
      this.seccionInput = await this.listadoCamara['seccionPlanta']['nombre'];
      this.volumenCamara = await this.listadoCamara['metroscubico'];
      this.getAnalisisTratamiento('  ' + event.trim());
      
    }, (error) => {
      console.log(error);
    });
  }
  getTipoFruta() {
    this.commonService.getTipoFruta(this.granel).subscribe(async (res) => {
      // console.log('GET TIPO FRUTA', await res);
      this.listadoTipoFruta = res.items;
    }, (error) => {
      console.log('ERROR TIPO FRUTA', error);
    });
  }
  getEnvaseCosecha() {
    this.commonService.cargarEnvasesCosecha().subscribe(async (res) => {
      // console.log('GET ENVASECOSECHA', res);
      this.listadoEnvaseCosecha = await res.items;
    }, (error) => {
      console.log('ERROR AL CARGAR ENVASE COSECHA', error);
    });
  }
  getEnvaseCosechaValor(event){
    // console.log(event);
  }

  /////////////// --APIS COMMON-- -TERMINO- -- /////////////

  /////////////// --APIS TRATAMIENTO-- -INICIO- -- /////////////

  guardarEncabezado() {
    // console.log(this.especieSeleccion, this.usuario, 
    //             this.rutProveedor, this.nombreRSocial, 
    //             this.paisSeleccion, this.tipoTratamientoSelect);
      // console.log(this.cod_operativo.id);
    if (this.especieSeleccion === undefined ) {
      this.snackBar.open('Debe seleccionar la especie', null, { duration: 3000 });
      return;
    }
    if (this.usuario === '') {
      this.snackBar.open('Debe ingresar un usuario', null, { duration: 3000 });
      return;
    }
    if (this.rutProveedor === '') {
      this.snackBar.open('Debe ingresar un rut', null, { duration: 3000 });
      return;
    }
    if (this.nombreRSocial=== '') {
      this.snackBar.open('Debe ingresar ua razón social', null, { duration: 3000 });
      return;
    }
    if (this.paisSeleccion === undefined) {
      this.snackBar.open('Debe seleccionar un país', null, { duration: 3000 });
      return;
    }
    if (this.tipoTratamientoSelect === undefined) {
      this.snackBar.open('Debe seleccionar un tipo tratamiento', null, { duration: 3000 });
      return;
    }
    let f_ejecucion = this.convertirFecha(this.fechaEjecucion) + 'T' + '00:00';
    let f_solicitud = this.convertirFecha(this.fechaSolicitud) + 'T' + '00:00';
    
    const req = {
      id: 0,
      exportadora: {id:this.exportadora},
      temporada: {id:this.temporada},
      zonaProductor: {id:this.zona},
      frigorifico: {id:this.frigorifico},
      fechaSolicitud: f_solicitud,
      fechaEjecucion: f_ejecucion,
      tipoTratamiento: {id:this.tipoTratamientoSelect.trim()},
      usuario: {id:this.usuario},
      especie: {idsnap:this.especieSeleccion},
      pais: {id:this.paisSeleccion.trim()},
      proveedor: {rut:this.rutProveedor}
      // numSello: this.numeroSello
    };
    // console.log(req);
    if (this.editar) {
      // console.log('SE EJECUTA EL PATCH EDITAR');
      this.logisticaService.patchTratamiento(req).subscribe(async (res) => {
        // console.log(res);
      });
    } else {
      console.log('SE EJECUTA EL POST');
      // return;
      this.logisticaService.postTratamiento(req).subscribe(
        (res) => {
          // console.log('RESPONSE POST TRATAMIENTO', res);
          if (res.id > 0) {
            this.snackBar.open('Registro guardado correctamente', null, { duration: 3000 });  
            this.router.navigate(['/listado_tratamientos']);
          }
        }, (error) => {
          console.log('ERROR AL REALIZAR EL POST', error);
          this.snackBar.open('Error al guardar los datos', null, { duration: 3000 });
        });
    }


  }
  /////////////// --APIS TRATAMIENTO-- -TERMINO- -- /////////////

  // STOCK CAMARA 

  stockCamara(){
    // console.log(this.tipoFrutaSeleccion);
    // console.log(this.camaraseleccion);
    const req = {
      temporada: this.temporada,
      camara: this.camaraseleccion,
      zona: this.zona,
      frigorifico: this.frigorifico
    }
    console.log(req);
    this.commonService.getAnalisisTratamientoGroup(req).subscribe((res:any)=>{
      // console.log(res);
      if (res.items.length > 0) {
        const info= {
          tipoFruta:this.tipoFrutaSeleccion
        }
        this.dataSource4 = new MatTableDataSource(res.items);
        
      }
    },(error)=>{
      console.log(error);
    })
  }

  // FIN STOCK CAMARA

  getUsuarioSearch(nombre: string) {
    if (nombre.length === 0) {
      this.listadoUsuario = [];
      return;
    }
    this.seguridadService.getUsuarioSearch(nombre).subscribe(async (res) => {
      // console.log('GET USUARIOS', res);
      this.listadoUsuario = await res.items;
    }, (error) => {
      console.log('ERROR AL CARGAR USUSARIOS', error);
    });
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith<string | any>(''),
        map(cod => typeof cod ? cod : cod.nombre),
        map(des => des ? this._filter(des) : this.listadoUsuario.slice())
      );

  }
  obtenerOperario(event) {
    
    console.log(event.option.value);
  }
  getAplicadorTratamiento(rut: string) {
    if (rut.length === 9) {
      var dv = rut.substring(8);
      this.rutAplicador = chileanRut.format(rut.trim(), dv);
      this.buscarAplicador(rut);
      this.ejecutarPost = true;
      
    }
    if (rut.length === 8) {
      var dv = rut.substring(7);
      this.rutAplicador = chileanRut.format(rut.trim(), dv);
      this.buscarAplicador(rut);
      this.ejecutarPost = false;
      
    }
    if (rut.length < 8) {
      this.ejecutarPost = false;
      this.licenciaAplicador = null;
      this.nombrePersonaExterna = null;
    }
  }
  buscarAplicador(rut:string){
    this.logisticaService.getAplicadortratamiento(rut).subscribe(async (res) => {
      // console.log(await res);
      if (res.rut.trim() === 9) {
        var dv = this.rutAplicador.substring(8);
        this.rutAplicador = chileanRut.format(res.rut.trim(), dv);
        this.ejecutarPost = false;
      }
      if (res.rut.trim() === 8) {
        var dv = this.rutAplicador.substring(8);
        this.rutAplicador = chileanRut.format(res.rut.trim(), dv);
        this.ejecutarPost = false;
      }
      this.nombrePersonaExterna = res.nombres;
      this.licenciaAplicador = res.numLicencia;
      this.ejecutarPost = true;
      
    }, (error) => {
      console.log(error)
      if (!error.ok) {
        console.log('ejecutar el post');
        this.ejecutarPost = false;
      };
    });
  }
  postAplicadortratamiento(){
    // console.log(this.rutAplicador, this.nombrePersonaExterna, this.licenciaAplicador);
    if (this.rutAplicador === '') {
      // console.log('FALTA AGREGAR EL RUT');
      return;
    }else 
    if (this.nombrePersonaExterna === null) {
      // console.log('FALTA AGREGAR EL NOMBRE null');
      return;
    }
    if (this.nombrePersonaExterna === ''){
      // console.log('FALTA AGREGAR EL NOMBRE vacio');
      return;
    }
    if (this.nombrePersonaExterna === undefined){
      // console.log('FALTA AGREGAR EL NOMBRE undefined');
      return;
    }
    if (this.licenciaAplicador === '') {
      // console.log('FALTA AGREGAR EL LICENCIA vacio');
      return;
    }
    if (this.licenciaAplicador === null) {
      // console.log('FALTA AGREGAR EL LICENCIA null ');
      return;
    }
    if (this.licenciaAplicador === undefined) {
      // console.log('FALTA AGREGAR EL LICENCIA undefined');
      return;
    }
    
    const req = {
      rut: chileanRut.unformat(this.rutAplicador),
      nombres: this.nombrePersonaExterna,
      numLicencia:this.licenciaAplicador
    };
    // console.log(req);
    this.logisticaService.postAplicadorTratamiento(req).subscribe(async(res)=>{
      // console.log('POST APLICADOR TRATAMIENTO',await res);
      this.buscarAplicador(chileanRut.unformat(this.rutAplicador));
    },(error)=>{
      console.log('ERROR AL EJECUTAR EL POST APLICADOR TRATAMIENTIO', error);
    });
  }
  getDetalleTratamientoEjecutar(){
    const req = {
      nrotratamiento: this.numeroSolicitud,
      temporada: this.temporada,
      exportadora: this.exportadora,
      zona_prod: this.zona,
      frigorifico: this.frigorifico
    };
    this.logisticaService.getDetalletratamientoEjecutar(req).subscribe(async(res)=>{
      // console.log(await JSON.stringify(res));
      await res.items.forEach(element => {
        if (element.cantidadReal > 0) {
          element.ingredienteActivo.dosis = element.cantidadReal;
        }
        this.dataSource2 = new MatTableDataSource(res.items);
      });

    },(error)=>{
      console.log(error);
    })
  }

  convertirFecha(fech: string) {
    var date = new Date(fech), mnth = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  patchGuardarDetalle(){
    let fechaTermino = this.convertirFecha(this.fechaTerminoTratamiento) + 'T' + '00:00';
    const req = {
      id: this.numeroSolicitud,
      exportadora: {id:this.exportadora},
      temporada: {id:this.temporada},
      zonaProductor: {id:this.zona},
      frigorifico: {id:this.frigorifico},
      fechaSolicitud: this.fechaSolicitud,
      fechaEjecucion: this.fechaEjecucion,
      fechaTermino: fechaTermino,
      tipoTratamiento: {id:this.tipoTratamientoSelect.trim()},
      usuario: {id:this.usuario},
      operativo: {id:this.cod_operativo.id},
      especie: {id:this.especieSeleccion},
      pais: {id:this.paisSeleccion.trim()},
      numFolio: 0,
      numTemp: this.temperatura,
      camara: {id:this.camaraseleccion},
      seccion: {id:this.seccion},
      proveedor: {rut:this.rutProveedor},
      tipoFruta: {id:this.tipoFrutaSeleccion},
      envaseCosecha: {id:this.envaseTipoNumero},
      embalaje: {id:null},
      aplicadorTratamiento: {rut: chileanRut.unformat(this.rutAplicador)},
      fechaMinCosecha: this.fechaCosechaAntigua,
      fechaMaxCosecha: this.fechaCosechaReciente,
      fechaEntrada: this.fechaLlenadoCamraPrimero,
      fechaUltimo: this.fechaLlenadoCamaraUltimo
      // numSello: this.numeroSello
    };
    // console.log(JSON.stringify(req));
    
    this.logisticaService.patchTratamiento(req).subscribe(async(res)=>{
      // console.log(await res);
      if (res === 'OK') {
        this.snackBar.open('Datos guardados correctamente', null, { duration: 3000 });
        const req = {
          id: this.numeroSolicitud,
          camara: {id : '  '+this.camaraseleccion.trim()},
          exportadora: {id:this.exportadora},
          temporada: {id:this.temporada},
          zonaProductor: {id:this.zona},
          frigorifico: {id:this.frigorifico}
        }
        // console.log(JSON.stringify(req));
        this.logisticaService.postBinsCopia(req).subscribe((res)=>{
          // console.log(res);
        },(error)=>{
          console.log(error);
        });

      }
      else{
        this.snackBar.open('Error al guardar los datos', null, { duration: 3000 });
      }
    },(error)=>{
      console.log(error);
    });
  }
  ///////////////// FACTURACION /////////////////

  getProveedorTratamiento(){
    const req = {
      temporada: this.temporada,
      zona: this.zona,
      exportadora: this.exportadora,
      rutProveedor: this.rutProveedor,
      condicion: 2,
    }
    this.logisticaService.getProveedorTratamiento(req).subscribe((res)=>{
      // console.log('GET PROVEEDOR TRATAMIENTO FACTURACION',res);
      
    },(error)=>{
      console.log(error);
    })
  }
  getFacturados(){
    const req = {
      nroFactura:this.nroFactura,
      temporada: this.temporada,
      zona_prod: this.zona,
      exportadora: this.exportadora,
      proveedor: this.rutProveedor,
      frigorifico: this.frigorifico,
      condicion: 1,
    }
    this.logisticaService.getFacturados(req).subscribe((res)=>{
      // console.log(res);
      this.dataSource3 = new MatTableDataSource(res.items);
    },(error)=>{
      console.log(error);
    })
  }
}