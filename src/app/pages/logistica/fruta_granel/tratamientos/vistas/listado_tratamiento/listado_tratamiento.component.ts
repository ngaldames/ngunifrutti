import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSlideToggleChange, MatDialog, MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';
import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';
import { Factura_asociadaComponent } from '../../dialogs/factura_asociada/factura_asociada.component';
const ELEMENT_DATA = [
  {
    solicitud: 'nn', pais: 'das', persona: 'aa', fecha_solicitud: '11', fecha_ejecucion: '11', rut: '11', razon_social: 'aa'
  }
];
@Component({
  selector: 'app-listado_tratamiento',
  templateUrl: './listado_tratamiento.component.html',
  styleUrls: ['./listado_tratamiento.component.css']
})
export class Listado_tratamientoComponent implements OnInit {
  solicitud: string;
  pais: string;
  persona: string;
  fecha_solicitud: string;
  fecha_ejecucion: string;
  rut: string;
  razon_social: string;
  icons: any;
  private sub = new Subject();
  displayedColumns: string[] = ['id', 'pais.nombre', 'usuario.id', 'fechaSolicitud', 'fechaEjecucion', 'proveedor.rut', 'proveedor.nombre', 'icons'];
  dataSource = new MatTableDataSource([]);
  esFavorito: boolean;
  isActive: boolean = false;
  estado: number = 0;
  temporada: string = null;
  planta: string = null;
  exportadora: string = null;
  zona: string = null;

  frigorificos: any = [];
  frigoSel: any;
  frigoSelecion: any;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    // OBTENER VARIABLES GLOBALES
    this.temporada = localStorage.getItem('temporada');
    this.planta = localStorage.getItem('planta');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona = localStorage.getItem('planta');

    this.obtieneFrigorificos();
    setTimeout(() => {
      this.getListadoPrincipalTratamiento();
    }, 1000);
  }
  obtieneFrigorificos() {
    this.frigorificos = [];
    this.commonService.cargarFrigorifico(localStorage.getItem('exportadora'), localStorage.getItem('planta'))
      .subscribe(async (res) => {
        this.frigorificos = await res.items;
        console.log('FRIGORIFICOS', res.items);
        this.frigoSelecion = this.frigorificos[0].id;
      }, (error) => {
        console.error('ERROR FRIGORIFICOS', error);
        this.snackBar.open('Error al cargar frigorificos', null, { duration: 3000 });
      });
  }
  cambioEstado(event) {
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.estado = 1;
      this.getListadoPrincipalTratamiento();
    }
    else {
      this.estado = 0;
      this.getListadoPrincipalTratamiento();
    }
  }

  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }
  btnEditar(item) {
    let ejecutar = false;
    let editar = true;
    let extras: NavigationExtras = {
      queryParams: {
        'ejecutar': ejecutar,
        'temporada': this.temporada,
        'exportadora': this.exportadora,
        'zona': this.zona,
        'frigorifico': this.frigoSelecion,
        'editar': editar,
        'item': item.id
      }
    };
    this.router.navigate(['/tratamientos'], extras);
  }
  btnEjecutar(item) {

    let ejecutar = true;
    console.log(item.id);
    let extras: NavigationExtras = {
      queryParams: {
        'ejecutar': ejecutar,
        'temporada': this.temporada,
        'exportadora': this.exportadora,
        'zona': this.zona,
        'frigorifico': this.frigoSelecion,
        'item': item.id
      }
    };
    this.router.navigate(['/tratamientos'], extras);
  }
  btnFactura(item){

    let factura = true;
    console.log(item.id);
    let extras: NavigationExtras = {
      queryParams: {
        'factura': factura,
        'temporada': this.temporada,
        'exportadora': this.exportadora,
        'zona': this.zona,
        'frigorifico': this.frigoSelecion,
        'item': item.id
      }
    };
    this.router.navigate(['/tratamientos'], extras);
  }
  agregar() {
    let ejecutar = false;
    let extras: NavigationExtras = {
      queryParams: {
        'ejecutar': ejecutar,
        'temporada': this.temporada,
        'exportadora': this.exportadora,
        'zona': this.zona,
        'frigorifico': this.frigoSelecion
      }
    };
    this.router.navigate(['/tratamientos'], extras);
  }
  facturar() {
    const dialogRef = this.dialog.open(Factura_asociadaComponent, {
      disableClose: true,
      width: '1300px',
      data:{frigorifico:this.frigoSelecion, temporada:this.temporada, exportadora:this.exportadora, zona:this.zona}
    })
    dialogRef.afterClosed().subscribe(result => this.getListadoPrincipalTratamiento());
  }
  anterior() { }

  siguiente() {
    console.log(this.frigoSelecion);
  }

  getListadoPrincipalTratamiento() {
    console.log(this.estado);
    const req = {
      temporada: this.temporada,
      zona_prod: this.zona,
      exportadora: this.exportadora,
      frigorifico: this.frigoSelecion,
      estado: this.estado,
      pagina: 1,
      filas: 50,
    };
    this.logisticaService.getListadoPrincipalTratamientos(req).subscribe(async (res) => {
      console.log(await res);

      this.dataSource = new MatTableDataSource(res.items);

    }, (error) => {
      console.log('ERROR LISTADO P TRATAMIENTO', error);
    });
  }
}

