import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-eliminar-producto',
  templateUrl: './eliminar-producto.component.html',
  styleUrls: ['./eliminar-producto.component.css']
})
export class EliminarProductoComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EliminarProductoComponent>) { }

  ngOnInit() {
  }
  Si(){
      this.dialogRef.close({eliminar: true});
  }
  No(){
    this.dialogRef.close({eliminar: false});
  }
}
