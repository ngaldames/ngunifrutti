import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';
import { ApiCommonService } from './../../../../../../services/apicommonservice.service';

@Component({
  selector: 'app-asignar_producto',
  templateUrl: './asignar_producto.component.html',
  styleUrls: ['./asignar_producto.component.css']
})
export class Asignar_productoComponent implements OnInit {

  /////////////////////// VARIABLES ////////////////////
  nrotratamiento: string = null;
  producto: string = null;
  idIngredienteActivo: number = null;
  nombreIngredienteActivo: string = null;
  unidad: string = null;
  idUnidad: number;
  productoDescripcion: string = null;
  dosis: number = null;
  idProducto: number = null;

  //////////////// LISTADOS ////////////////

  listadosProductos:any[]=[];

  //////////////// BIT ///////////////////
  put_post: boolean = false;

  ////////////////////// DECLARACION DE VARIABLES GLOBALES //////////////////
  temporada: string = null;
  exportadora: string = null;
  zona: string = null;
  frigorifico: string = null;
  planta: string = null;

  constructor(
    public dialogRef: MatDialogRef<Asignar_productoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService,
    private snackBar: MatSnackBar
    ) {
    console.log(data);
    this.nrotratamiento = data.nrotratamiento;//Nro tratamiento desde pagina anterior
    this.frigorifico = data.frigorifico;
    this.idIngredienteActivo = data.item['ingredienteActivo']['id'];
    this.nombreIngredienteActivo = data.item['ingredienteActivo']['nombre'];
    this.unidad = data.item['tipoUnidad']['nombre'];
    this.idUnidad = data.item['tipoUnidad']['id'];
    this.dosis = data.item['ingredienteActivo']['dosis'];
    if (data.item['producto']['id'] !== '') {
      console.log('SE EJECUTA EL PUT');
      this.idProducto = data.item['producto']['id'];
    }else{
      console.log(this.idProducto);
    }   

    ///// Definir si es POST O PUT /////  
    if (data.item['producto']['nombre'] !== '') {
      console.log('SE EJECUTA EL PUT');
    } 
    if (data.item['producto']['nombre'] === '') {
      console.log('SE EJECUTA EL POST');
    }
    // OBTENER VARIABLES GLOBALES
    this.temporada = localStorage.getItem('temporada');
    this.planta = localStorage.getItem('planta');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona = localStorage.getItem('planta');
  }

  ngOnInit() {
    this.getIngredienteActivoProducto();
  }
  close() {
    this.dialogRef.close();
  }
  getIngredienteActivoProducto(){
    const req = {
      codIngredienteActivo:this.idIngredienteActivo
    }
    this.commonService.getIngredienteActivoProducto(req).subscribe((res)=>{
      console.log(res);
      this.listadosProductos = res.items;
    },(error)=>{
      console.log(error);
    });
  }
  guardar(){
    if (this.data.item['producto']['nombre'] !== '') {
      console.log('SE EJECUTA EL PUT');
      this.putIngredienteActivo();
    } if (this.data.item['producto']['nombre'] === '') {
      console.log('SE EJECUTA EL POST');
      this.postIngredienteActivo();
    }

  }
  postIngredienteActivo(){
    const req = {
      tratamiento:{
          id: this.nrotratamiento,
          exportadora: {id:this.exportadora},
          temporada: {id:this.temporada},
          zonaProductor: {id:this.zona},
          frigorifico: {id:this.frigorifico}
      },
      producto:{id: this.idProducto},
      tipoUnidad:{id: this.idUnidad},
      cantidadReal: this.dosis
   }
   console.log(req);
   if (this.idProducto === null) {
    this.snackBar.open('Debe seleccionar un producto', null, { duration: 3000 });  
   }else{
     this.logisticaService.postDetalleTratamiento(req).subscribe((res)=>{
       if (res === 'OK') {
        this.snackBar.open('Producto agregado correctamente', null, { duration: 3000 });
       }
       console.log(res);
     },(error)=>{
      console.log(error);
      this.snackBar.open('Error al agregar el producto', null, { duration: 3000 });
     });
   }

  }
  putIngredienteActivo(){
    const req = {
      tratamiento:{
          id: this.nrotratamiento,
          exportadora: {id:this.exportadora},
          temporada: {id:this.temporada},
          zonaProductor: {id:this.zona},
          frigorifico: {id:this.frigorifico}
      },
      producto:{id: this.idProducto},
      tipoUnidad:{id: this.idUnidad},
      cantidadReal: this.dosis
   }
   console.log(JSON.stringify(req));
   if (this.idProducto === null) {
    this.snackBar.open('Debe seleccionar un producto', null, { duration: 3000 });  
   }else{
     this.logisticaService.putDetalleTratamiento(req).subscribe((res)=>{
       if (res === null) {
        this.snackBar.open('Producto actualizado correctamente', null, { duration: 3000 });
       }
       console.log(res);
     },(error)=>{
      console.log(error);
      this.snackBar.open('Error al actualizar el producto', null, { duration: 3000 });
     });
   }

  }
}
