import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatTableDataSource, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';

@Component({
  selector: 'app-factura_asociada',
  templateUrl: './factura_asociada.component.html',
  styleUrls: ['./factura_asociada.component.css']
})
export class Factura_asociadaComponent implements OnInit {
  nro: string;
  planta: string;
  camara: string;
  m3: string;
  fecha_aplicacion: string;
  especie: string;
  variedad: string;
  nro_bins: string;
  producto: string;
  rutProveedor: string = '';
  nombreRSocial: string = '';
  listadoProveedor: any[] = [];
  nroFactura:number = null;
  fechaFactura:any;
  montoNetoFac: string = null;
  tipoCambio: string = null;
  ////////////////////// DECLARACION DE VARIABLES GLOBALES //////////////////
  temporada: string = null;
  exportadora: string = null;
  zona: string = null;
  frigorifico: string = null;

  listaAFacturar: any[] = [];
  seleccionadasTrue: any[] = [];
  seleccionadasFalse: any[] = [];

  displayedColumns: string[] = ['select', 'id', 'frigorifico', 'camara', 'm3', 'fechaEjecucion', 'especie', 'nro_bins'];
  dataSource = new MatTableDataSource([]);
  selection = new SelectionModel(true, []);
  constructor(public dialogRef: MatDialogRef<Factura_asociadaComponent>,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);
    this.frigorifico = data.frigorifico;
    this.exportadora = data.exportadora;
    this.zona = data.zona;
    this.temporada = data.temporada;
  }

  ngOnInit() {

    this.fechaFactura = new Date();

  }
  close() {
    this.dialogRef.close();
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.folio + 1}`;
  }
  checkTodo(event) {
    this.seleccionadasTrue = [];
    if (event.checked) {
      this.listaAFacturar.forEach(element => {
        element.check = true;
        if (element.check) {
          const info = {
            id: element.id,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    } else {
      this.listaAFacturar.forEach(element => {
        element.check = false;
        if (!element.check) {
          this.seleccionadasTrue = [];
        }
      });
    }
    // console.log(this.listaBins);
  }
  checkSeleccionados(event) {
    // console.log(event);
    this.seleccionadasTrue = [];
    this.seleccionadasFalse = [];
    if (event.check) {
      this.listaAFacturar.forEach(element => {
        if (element.check) {
          const info = {
            id: element.id,
            // nroGuia: element.nroGuia,
            // nroLinea: element.nroLinea,
            check: true
          }
          this.seleccionadasTrue.push(info);
          // console.log(this.seleccionadasTrue);
        }
      });
    }
    if (!event.check) {
      this.listaAFacturar.forEach(element => {
        if (element.check) {
          const info = {
            folio: element.id,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    }

  }
  ///////////////// FACTURACION /////////////////

  getProveedorTratamiento() {
    const req = {
      temporada: this.temporada,
      zona: this.zona,
      exportadora: this.exportadora,
      rutProveedor: this.rutProveedor,
      condicion: 2,
    }
    this.logisticaService.getProveedorTratamiento(req).subscribe((res) => {
      console.log('GET PROVEEDOR TRATAMIENTO FACTURACION', res);
      this.listaAFacturar = res.items;
      this.dataSource = new MatTableDataSource(res.items);
    }, (error) => {
      console.log(error);
    })
  }
  facturar() {

  }
  buscarProveedor() {
    // console.log(this.rutProveedor);
    if (this.rutProveedor.length === 10) {
      this.cargarProveedor(this.rutProveedor + '     ');
    } else {
      this.nombreRSocial = '';
    }
  }
  cargarProveedor(rutProveedor: string) {
    this.commonService.getProveedor().subscribe(async (res) => {
      // console.log('GET PROVEEDOR',res);
      this.listadoProveedor = await res.items;
      this.listadoProveedor.forEach(element => {
        if (element.rut === rutProveedor) {
          this.nombreRSocial = element.nombre;
          this.getProveedorTratamiento();
        }
      });
    }, (error) => {
      console.log('ERROR AL CARGAR PROVEEDOR', error);
    })
  }
  convertirFecha(fech: string) {
    var date = new Date(fech), mnth = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }
  postFacturar() {
    console.log(this.rutProveedor);
    console.log(this.nroFactura);
    console.log(this.montoNetoFac);
    console.log(this.tipoCambio);
    console.log(this.nombreRSocial);
    if (this.rutProveedor === '') {
      this.snackBar.open('Debe ingresar un rut', null, { duration: 3000 });
      return;
    }
    if (this.nroFactura === null) {
      this.snackBar.open('Debe ingresar un número de factura', null, { duration: 3000 });
      return;
    }
    if (this.montoNetoFac === null) {
      this.snackBar.open('Debe ingresar un monto', null, { duration: 3000 });
      return;
    }
    if (this.tipoCambio === null) {
      this.snackBar.open('Debe ingresar un tipo de cambio', null, { duration: 3000 });
      return;
    }
    if (this.seleccionadasTrue.length <= 0) {
      this.snackBar.open('Debe seleccionar una factura', null, { duration: 3000 });
      return;
    }
    
    let fechaTermino = this.convertirFecha(this.fechaFactura) + 'T' + '00:00';
    
    if (this.seleccionadasTrue.length > 0) {

      this.seleccionadasTrue.forEach(element => {

        const req = {
          id: element.id,
          exportadora: { id: this.exportadora },
          temporada: { id: this.temporada },
          zonaProductor: { id: this.zona },
          frigorifico: { id: this.frigorifico },
          numDTEFactura: this.nroFactura,
          fechaDTE: fechaTermino,
          montoDTE: parseInt(this.montoNetoFac),
          tipoCambioDTE: parseInt(this.tipoCambio),
        }
        console.log(req);
        this.logisticaService.postTratamientoFactura(req).subscribe((res) => {
          console.log(res);
          if (res === 'OK') {
            this.snackBar.open('Facturada Correctamente', null, { duration: 3000 });
            this.getProveedorTratamiento();
          }
        })
      });
    }

  }

}
