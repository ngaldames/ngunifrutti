import { Nuevo_contenedorComponent } from './../../dialogs/nuevo_contenedor/nuevo_contenedor.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
const ELEMENT_DATA = [
  {
    tipo: 'Bins plástico con totem', especie: 'Cereza', variedad: 'Lapin', icons: ''
  },
  {
    tipo: 'Bins plástico', especie: 'Manzana', variedad: 'Fuji', icons: ''
  }
];
@Component({
  selector: 'app-lista_contenedores',
  templateUrl: './lista_contenedores.component.html',
  styleUrls: ['./lista_contenedores.component.css']
})
export class Lista_contenedoresComponent implements OnInit {
  tipo: string;
  especie: string;
  variedad: string;
  icons: any;
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatSort) sort: MatSort;

  public displayedColumns: string[] = ['tipo', 'especie', 'variedad', 'icons'];
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  agregarContenedor() {
    const dialogo = this.dialog.open(Nuevo_contenedorComponent, {
      disableClose: true,
      width: '800px',
    });
  }
}
