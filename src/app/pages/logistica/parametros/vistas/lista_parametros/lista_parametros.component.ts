import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista_parametros',
  templateUrl: './lista_parametros.component.html',
  styleUrls: ['./lista_parametros.component.css']
})
export class Lista_parametrosComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  parametros_contenedores(){
    this.router.navigate(['/parametros_contenedores']);
  }
}
