import { Carga_camionComponent } from './../../dialogs/carga_camion/carga_camion.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Asignar_folios_produccion_a_recibidorComponent } from '../../dialogs/asignar_folios_produccion_a_recibidor/asignar_folios_produccion_a_recibidor.component';
const ELEMENT_DATA = [
  {
    termografo: '1', tipo: 'nnnnn', serie: '11'
  }
];
 
@Component({
  selector: 'app-recepcion_camion',
  templateUrl: './recepcion_camion.component.html',
  styleUrls: ['./recepcion_camion.component.css']
})
export class Recepcion_camionComponent implements OnInit {
  displayedColumns: string[] = ['termografo', 'tipo', 'serie'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  esFavorito: boolean;
  constructor(private router: Router) { }

  ngOnInit() {
  }

 guardar(){
   this.router.navigate(['/listado_despacho_puerto_aeropuerto']);
 }
}