import { Component, OnInit } from '@angular/core';
import { Asignar_folios_produccion_a_recibidorComponent } from '../../dialogs/asignar_folios_produccion_a_recibidor/asignar_folios_produccion_a_recibidor.component';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';


const ELEMENT_DATA = [
  {
    folio: 'falta', especie: 'nnnnn', variedad: '11', origen: '11', rec_destino: ''
  }
];


@Component({
  selector: 'app-folios_productor_a_recibidor',
  templateUrl: './folios_productor_a_recibidor.component.html',
  styleUrls: ['./folios_productor_a_recibidor.component.css']
})
export class Folios_productor_a_recibidorComponent implements OnInit {
  displayedColumns: string[] = ['folio', 'especie', 'variedad', 'origen', 'rec_destino'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  esFavorito: boolean;
  constructor(private dialog: MatDialog, private router: Router) { }

  ngOnInit() {
  }

  agregarFolios() {
    const dialogRef = this.dialog.open(Asignar_folios_produccion_a_recibidorComponent, {
      width: '95vw',
      height: 'auto'
    }) 
  }

  guardar(){
    this.router.navigate(['/listado_principal_asignacion_prod']);
  }
}
