import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Carga_camionComponent } from '../../dialogs/carga_camion/carga_camion.component';
const ELEMENT_DATA = [
  {
    emp_transporte: 'falta', tipo_camion: 'nnnnn', peso_ingreso: '11', patente: '11', chofer: '', contenedor:'', tara:''
  }
];

@Component({
  selector: 'app-listado_despacho_puerto',
  templateUrl: './listado_despacho_puerto.component.html',
  styleUrls: ['./listado_despacho_puerto.component.css']
})
export class Listado_despacho_puertoComponent implements OnInit {
  displayedColumns: string[] = ['emp_transporte', 'tipo_camion', 'peso_ingreso', 'patente', 'chofer', 'contenedor', 'tara', 'icons'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  esFavorito: boolean;
  
  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  recepcion_camion(){
    this.router.navigate(['/recepcion_camion_despacho_puerto']);
  }


  editar() { }
  anterior() { }
  siguiente() { }

  carga_camion() {
    const dialogRef = this.dialog.open(Carga_camionComponent, {
      width: '90vw',
      height: 'auto'
    })
  }
}
