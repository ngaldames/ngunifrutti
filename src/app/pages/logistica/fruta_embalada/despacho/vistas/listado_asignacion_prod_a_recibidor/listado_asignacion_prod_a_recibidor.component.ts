import { Asignar_folios_produccion_a_recibidorComponent } from './../../dialogs/asignar_folios_produccion_a_recibidor/asignar_folios_produccion_a_recibidor.component';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

const ELEMENT_DATA = [
  {
    folio: 'falta', especie: 'nnnnn', variedad: '11', origen: '11', rec_destino:''
  }
];

@Component({
  selector: 'app-listado_asignacion_prod_a_recibidor',
  templateUrl: './listado_asignacion_prod_a_recibidor.component.html',
  styleUrls: ['./listado_asignacion_prod_a_recibidor.component.css']
})
export class Listado_asignacion_prod_a_recibidorComponent implements OnInit {
  private sub = new Subject();
  displayedColumns: string[] = ['folio', 'especie', 'variedad', 'origen', 'rec_destino'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  esFavorito: boolean;
  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  editar() { }
  anterior() { }
  siguiente() { }

  // asignar_folios_op() {
  //   this.router.navigate(['/nueva_asignacion_op']);
  // }
 anadirFolios(){
   this.router.navigate(['/folios_productor_a_recibidor']);
 }
  
}
