import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
const ELEMENT_DATA = [
  {
    fecha: 'falta', motivo: 'nnnnn', usuario: '11', observacion: '11'
  }
];

@Component({
  selector: 'app-listado_principal_folios_op',
  templateUrl: './listado_principal_folios_op.component.html',
  styleUrls: ['./listado_principal_folios_op.component.css']
})
export class Listado_principal_folios_opComponent implements OnInit {
  private sub = new Subject();
  displayedColumns: string[] = ['fecha', 'motivo', 'usuario', 'observacion'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  esFavorito: boolean;
  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  editar() { }
  anterior() { }
  siguiente() { }

  asignar_folios_op(){
    this.router.navigate(['/nueva_asignacion_op']);
  }
}
