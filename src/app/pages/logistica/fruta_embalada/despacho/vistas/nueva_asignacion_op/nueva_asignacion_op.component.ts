import { Agregar_folios_opComponent } from './../../dialogs/agregar_folios_op/agregar_folios_op.component';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
const ELEMENT_DATA = [
  {
    folio: '000', fecha: 'n00', especie: 'manzana', variedad: 'pink lady', categoria_real: '--', categoria_rotulada: '--', peso: '--', tipo_envase: '--', dimension: '--', etiqueta: '--', embalaje: '--', op_origen: '--', op_destino: '--'
  },
  {
    folio: '000', fecha: 'n00', especie: 'manzana', variedad: 'pink lady', categoria_real: '--', categoria_rotulada: '--', peso: '--', tipo_envase: '--', dimension: '--', etiqueta: '--', embalaje: '--', op_origen: '--', op_destino: '--'
  }
];
@Component({
  selector: 'app-nueva_asignacion_op',
  templateUrl: './nueva_asignacion_op.component.html',
  styleUrls: ['./nueva_asignacion_op.component.css']
})
export class Nueva_asignacion_opComponent implements OnInit {
  private sub = new Subject();
  displayedColumns: string[] = ['folio', 'fecha', 'especie', 'variedad', 'categoria_real', 'categoria_rotulada', 'peso', 'tipo_envase', 'dimension', 'etiqueta', 'embalaje', 'op_origen', 'op_destino'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  esFavorito: boolean;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  guardar(){
    this.router.navigate(['/listado_principal_folios_op']);
  }
  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  agregarFolios() {
    const dialogRef = this.dialog.open(Agregar_folios_opComponent, {
      width: '95vw',
      height: 'auto'
    })
  }

  editar() { }
  anterior() { }
  siguiente() { }

  asignar_folios_op() {
    this.router.navigate(['/nueva_asignacion_op']);
  }
}