import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource } from '@angular/material';
const ELEMENT_DATA = [
  {
    folio: '000', fecha: 'n00', especie: 'manzana', variedad: 'pink lady', categoria_real: '--', categoria_rotulada: '--', peso: '--', tipo_envase: '--', dimension: '--', etiqueta: '--', embalaje: '--', 
  },
  {
    folio: '000', fecha: 'n00', especie: 'manzana', variedad: 'pink lady', categoria_real: '--', categoria_rotulada: '--', peso: '--', tipo_envase: '--', dimension: '--', etiqueta: '--', embalaje: '--', 
  }
];

@Component({
  selector: 'app-carga_camion',
  templateUrl: './carga_camion.component.html',
  styleUrls: ['./carga_camion.component.css']
})
export class Carga_camionComponent implements OnInit {
  displayedColumns: string[] = ['folio', 'fecha', 'especie', 'variedad', 'categoria_real', 'categoria_rotulada', 'peso', 'tipo_envase', 'dimension', 'etiqueta', 'embalaje'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  constructor(public dialogRef: MatDialogRef<Carga_camionComponent>) { }

  ngOnInit() {
  }

  cerrar(){
    this.dialogRef.close();
  } 
  guardar(){
    this.dialogRef.close();
  }
}
