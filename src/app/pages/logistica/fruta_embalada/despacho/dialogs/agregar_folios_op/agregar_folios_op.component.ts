import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';


@Component({
  selector: 'app-agregar_folios_op',
  templateUrl: './agregar_folios_op.component.html',
  styleUrls: ['./agregar_folios_op.component.css']
})
export class Agregar_folios_opComponent implements OnInit {
 
  
  esFavorito: boolean;

  constructor(public dialogRef: MatDialogRef<Agregar_folios_opComponent>) { }

  ngOnInit() {
  }

  /** Whether the number of selected elements matches the total number of rows. */
 

  cerrar(){
    this.dialogRef.close();
  }

  anadirFolios(){
    this.dialogRef.close();
  }
  
}
