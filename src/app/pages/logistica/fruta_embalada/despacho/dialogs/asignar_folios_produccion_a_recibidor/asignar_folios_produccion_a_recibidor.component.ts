import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource } from '@angular/material';

const ELEMENT_DATA = [
  {
    folio: 'falta', especie: 'nnnnn', variedad: '11', origen: '11', rec_destino:''
  }
];

@Component({
  selector: 'app-asignar_folios_produccion_a_recibidor',
  templateUrl: './asignar_folios_produccion_a_recibidor.component.html',
  styleUrls: ['./asignar_folios_produccion_a_recibidor.component.css']
})
export class Asignar_folios_produccion_a_recibidorComponent implements OnInit {
  displayedColumns: string[] = ['folio', 'especie', 'variedad', 'origen', 'rec_destino'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  
  constructor(public dialogRef: MatDialogRef<Asignar_folios_produccion_a_recibidorComponent>,) { }

  ngOnInit() {
  }

  cerrar() {
    this.dialogRef.close();
  }
  anadirFolios() {
    this.dialogRef.close();
  }
}
