import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
export interface PeriodicElement {
  folio: string;
  fecha: string;
  especie: string;
  variedad: string;
  categoria_real: string;
  categoria_rotulada: string;
  peso: string;
  tipo_envase: string;
  dimension: string;
  etiqueta: string;
  embalaje: string;
  op_origen: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {
    folio: 'falta', fecha: 'nnnnn', especie: '11', variedad: '11', categoria_real: '', categoria_rotulada: '', peso: '', tipo_envase: '', dimension: '', etiqueta: '', embalaje: '', op_origen: ''
  },
  {
    folio: 'falta', fecha: 'nnnnn', especie: '11', variedad: '11', categoria_real: '', categoria_rotulada: '', peso: '', tipo_envase: '', dimension: '', etiqueta: '', embalaje: '', op_origen: ''
  },
  {
    folio: 'falta', fecha: 'nnnnn', especie: '11', variedad: '11', categoria_real: '', categoria_rotulada: '', peso: '', tipo_envase: '', dimension: '', etiqueta: '', embalaje: '', op_origen: ''
  }
];
@Component({
  selector: 'app-tabla_folios',
  templateUrl: './tabla_folios.component.html',
  styleUrls: ['./tabla_folios.component.css']
})
export class Tabla_foliosComponent implements OnInit {
  displayedColumns: string[] = ['select', 'folio', 'fecha', 'especie', 'variedad', 'categoria_real', 'categoria_rotulada', 'peso', 'tipo_envase', 'dimension', 'etiqueta', 'embalaje', 'op_origen'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  constructor() { }

  ngOnInit() {
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.folio + 1}`;
  }
}
