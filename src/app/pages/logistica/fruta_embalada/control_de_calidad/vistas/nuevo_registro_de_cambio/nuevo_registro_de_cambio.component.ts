import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';
import { Subject } from 'rxjs';
import { Asignar_folios_controldecalidadComponent } from '../../dialogs/asignar_folios_controldecalidad/asignar_folios_controldecalidad.component';
import { VerDetalleCondicionComponent } from '../../dialogs/ver-detalle-condicion/ver-detalle-condicion.component';


const ELEMENT_DATA = [
  {
    folio: '', guia: '', productor: '', especie: '', variedad: ''
  }
];
@Component({
  selector: 'app-nuevo_registro_de_cambio',
  templateUrl: './nuevo_registro_de_cambio.component.html',
  styleUrls: ['./nuevo_registro_de_cambio.component.css']
})
export class Nuevo_registro_de_cambioComponent implements OnInit {
  cargando: boolean;
  nro: number;
  cond_origen: string;
  cond_desitno: string;
  condicion: string;
  fecha: any;
  hora: string;
  usuario: string;
  icons: any;
  frigorifico: any;
  temporada: string;
  planta: any;
  zona_prod: any;
  exportadora: any;
  planta_zona: string;

  observacion: string = '';
  guardado: boolean = false;
  private sub = new Subject();
  displayedColumns: string[] = ['FolioPallet', 'NumGuia', 'Productor', 'Especie', 'Variedad', 'menu'];
  dataSource = new MatTableDataSource([]);
  esFavorito: boolean;
  condiciondestino: any[] = [];
  item: any[] = [];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService,
    private snackBar: MatSnackBar,
    private activatedRouter: ActivatedRoute
  ) {
    this.cargando = true;
    activatedRouter.queryParamMap.subscribe((params: any) => {
      this.planta = localStorage.getItem('Nplanta');
      this.temporada = localStorage.getItem('temporada');
      this.exportadora = localStorage.getItem('exportadora');
      this.zona_prod = localStorage.getItem('planta');
      this.frigorifico = params.params.frigorifico;
      console.log(params);
      if (params.params.nro) {
        this.nro = params.params.nro;
        this.guardado = true;
      }
      if (params.params.condicion) {
        this.condicion = params.params.condicion;
      }
      if (params.params.observacion) {
        this.observacion = params.params.observacion
      }
      if (params.params.fecha) {
        this.fecha = params.params.fecha;

        this.hora = this.fecha.substring(11, 16);
      }else{
        this.fecha = new Date();
        this.hora = this.fecha.getHours() + ':' + this.fecha.getMinutes();
        let hora = this.fecha.getHours().toString().length === 1 ? '0' + '' + this.fecha.getHours() : this.fecha.getHours();
        let minutos = this.fecha.getMinutes().toString().length === 1 ? '0' + '' + this.fecha.getMinutes() : this.fecha.getMinutes();
        this.hora = hora + ':' + minutos;
        console.log(this.fecha);
          
      }
    });
  }
  mostrarInformacion(item){
    console.log(item);
    const dialogRef = this.dialog.open(VerDetalleCondicionComponent, {
      // disableClose: true,
      width: '700px',
      data: { item, nro:this.nro, frigorifico:this.frigorifico, folioPallet:item.FolioPallet }
    })
    dialogRef.afterClosed().subscribe(() => { this.getDetalleCondicion(); }
    )

  }
  ngOnInit() {
    this.usuario = localStorage.getItem('User');
    this.getListadoCondicion();
    this.getDetalleCondicion();
  }
  getDetalleCondicion() {
    if (this.nro > 0) {
          const req = {
        CodTemp: this.temporada,
        CodZona: this.zona_prod,
        CodPlanta: this.frigorifico,
        CodExportadora: this.exportadora,
        NumeroCambio: this.nro
      }
      console.log(req);
      // return;
      this.commonService.getDetalleCambioCondicion(req).subscribe(async(res) => {
        console.log(res);
        if (res.lista.length > 0) {
          this.dataSource =  new MatTableDataSource(await res.lista);
          this.cargando = false;  
        }
        else{
          this.snackBar.open('Lista Vacia', null, { duration: 3000 });
        }
      }), (error) => {
        console.log(error);
      }
    }
  }

  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  asignarFolios() {
    if (this.condicion === undefined) {
      this.snackBar.open('debe seleccionar una condición', null, { duration: 800 })
      return;
    }
    const dialogRef = this.dialog.open(Asignar_folios_controldecalidadComponent, {
      disableClose: true,
      width: '700px',
      data: { frigorifico: this.frigorifico, condicion: this.condicion, nro: this.nro }
    })
    dialogRef.afterClosed().subscribe(() => { this.getDetalleCondicion(); }
    )

  }
  getListadoCondicion() {
    this.commonService.getListadoCondicion().subscribe((res) => {
      console.log(res);
      this.condiciondestino = res;
      this.cargando = false;
    });
  }
  convertirFecha(fech: string) {
    var date = new Date(fech), mnth = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }
  postGuardarEncabezado() {
    if (this.condicion === undefined) {
      this.snackBar.open('debe seleccionar una condición', null, { duration: 800 });
      return;
    }
    let fechaModificada = this.convertirFecha(this.fecha) + 'T' + this.hora;
    console.log(fechaModificada);
    const req = {
      id: 0,
      fecha: fechaModificada,
      condicion: { id: this.condicion },
      usuario: { id: this.usuario },
      zona: { id: this.zona_prod },
      zonaProductor: { id: this.zona_prod },
      temporada: { id: this.temporada },
      productor: { id: 0 },
      guiaProductor: 0,
      folioCKU: 0,
      observacion: this.observacion,
      frigorifico: { id: this.frigorifico },
      exportadora: { id: this.exportadora }
    }
    console.log(JSON.stringify(req));
    // return;
    this.commonService.postEncabezadoCondicion(req).subscribe((res) => {
      console.log(res);
      this.nro = res.id;
      this.guardado = true;
    })
  }

}
