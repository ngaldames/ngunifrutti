import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { MatTableDataSource, MatDialog, MatSnackBar, MatPaginator, MatSort } from '@angular/material';
import { Router, NavigationExtras } from '@angular/router';
import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';
@Component({
  selector: 'app-control_de_calidad',
  templateUrl: './control_de_calidad.component.html',
  styleUrls: ['./control_de_calidad.component.css']
})
export class Control_de_calidadComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cargando:boolean;
  nro: number;
  cond_origen: string;
  cond_desitno: string;
  fecha: string;
  hora: string;
  usuario: string;
  icons: any;
  frigorificos: any[] = [];
  frigoSelecion: any;
  contenidoString: string;
  pagina = 2;

  planta: any;
  temporada: any;
  zona_prod: any;
  exportadora: any;
  archivo: any;

  @ViewChild(MatSort) sort: MatSort;

  private sub = new Subject();
  displayedColumns: string[] = ['nro', 'cond_destino', 'usuario', 'fecha', 'hora', 'icons'];
  dataSource = new MatTableDataSource([]);
  esFavorito: boolean;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    // this.paginator._intl.itemsPerPageLabel = 'Items por página';
    this.dataSource.sort = this.sort;
    this.cargando = true;
    this.obtieneFrigorificos();

    this.planta = localStorage.getItem('Nplanta');
    this.temporada = localStorage.getItem('temporada');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona_prod = localStorage.getItem('planta');
  }
  
  obtenerTexto(texto) {
    console.log(texto);

  }
  obtieneFrigorificos() {
    this.frigorificos = [];
    this.commonService.cargarFrigorifico(localStorage.getItem('exportadora'), localStorage.getItem('planta'))
      .subscribe(async (res) => {
        this.frigorificos = await res.items;
        // console.log('FRIGORIFICOS', res.items);
        this.frigoSelecion = this.frigorificos[0].id;
        this.getListadoPrincipal();
      }, (error) => {
        // console.error('ERROR FRIGORIFICOS', error);
        this.snackBar.open('Error al cargar frigorificos', null, { duration: 3000 });
      });
  }
  filtrar(filterValue: string) {
    console.log(filterValue);
    if (filterValue.length > 0) {
      
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }
  nuevoRegistro() {
    let extras: NavigationExtras = {
      queryParams: {
        frigorifico: this.frigoSelecion
      }
    }
    this.router.navigate(['/nuevo_registro_cambio'], extras);
  }
  cambioFrigo(event){
    this.getListadoPrincipal();
  }
  getListadoPrincipal() {
    this.cargando = true;
    console.log(this.frigoSelecion);
    const req = {
      temporada: this.temporada,
      zona: this.zona_prod,
      exportadora: this.exportadora,
      frigorifico: this.frigoSelecion,
      pagina:this.pagina,
      filas: 50
    }
    console.log(req);
    this.commonService.getListadoPrincipalCambioCondicion(req).subscribe((res) => {
      console.log(res);
      if (res.items.length > 0) {
        this.dataSource = new MatTableDataSource(res.items.reverse());
        // this.dataSource.paginator = this.paginator;
        console.log(this.dataSource);
        // this.dataSource.sort = this.sort;
        this.cargando = false;
      }
      else {
        this.snackBar.open('No existen registros', null, { duration: 800 })
        this.cargando = false;
        this.dataSource = new MatTableDataSource([]);
      }
    }, (error) => {
      console.log(error);
    }
    );
  }

  editar(item) {
    let extras: NavigationExtras = {
      queryParams: {
        frigorifico: this.frigoSelecion,
        // item: JSON.stringify(item),
        nro: item.id,
        condicion:item.condicion.id,
        observacion:item.observacion,
        fecha:item.fecha
      }
    }
    this.router.navigate(['/nuevo_registro_cambio'], extras);
  }
  anterior() {
    this.pagina = this.pagina + 1;
    if (this.pagina >= 1) {
      this.getListadoPrincipal() ;
    }
   }

  siguiente() {
    this.pagina = this.pagina - 1;
    this.getListadoPrincipal();
   }
}



