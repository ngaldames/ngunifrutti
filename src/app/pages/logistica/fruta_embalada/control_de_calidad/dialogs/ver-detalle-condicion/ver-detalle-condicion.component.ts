import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { ApiCommonService } from '../../../../../../services/apicommonservice.service';

@Component({
  selector: 'app-ver-detalle-condicion',
  templateUrl: './ver-detalle-condicion.component.html',
  styleUrls: ['./ver-detalle-condicion.component.css']
})
export class VerDetalleCondicionComponent implements OnInit {

  frigorifico: any;
  temporada: string;
  planta: any;
  zona_prod: any;
  exportadora: any;
  planta_zona: string
  nro:number;
  folioPallet:string;
  displayedColumns: string[] = ['Productor', 'VariedadRotulada', 'Categoria', 'Calibre','Cajas','CondicionAnterior', 'CondicionActual'];
  dataSource = new MatTableDataSource([]);

  constructor(
    public dialogRef: MatDialogRef<VerDetalleCondicionComponent>,
    private commonService: ApiCommonService,
    @Inject(MAT_DIALOG_DATA) public data: any) {

      console.log(data)
      this.nro = data.nro;
      this.frigorifico = data.frigorifico;
      this.folioPallet = data.folioPallet;
      this.planta = localStorage.getItem('Nplanta');
      this.temporada = localStorage.getItem('temporada');
      this.exportadora = localStorage.getItem('exportadora');
      this.zona_prod = localStorage.getItem('planta');
      
      
     }

  ngOnInit() {
    this.getDetalleCondicion();
  }

  close(): void {
    this.dialogRef.close();
  }

  getDetalleCondicion() {
      const req = {
        CodTemp: this.temporada,
        CodZona: this.zona_prod,
        CodPlanta: this.frigorifico,
        CodExportadora: this.exportadora,
        folioPallet:this.folioPallet,
        NumeroCambio: this.nro
      }
      console.log(req);
      this.commonService.getDetallePorFolio(req).subscribe(async(res) => {
        console.log(res);

        var groupBy = function(xs, key) {
          return xs.reduce(function(rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };
        var caja = groupBy(res.lista, 'Productor');
        console.log(caja);

        this.dataSource =  new MatTableDataSource(await res.lista);
        // this.cargando = false;
      }), (error) => {
        console.log(error);
      }
    }
  

}
