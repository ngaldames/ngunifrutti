import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatTableDataSource, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ApilogisticaService } from '../../../../../../services/apilogistica.service';
import { ApiCommonService } from '../../../../../../services/apicommonservice.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
export interface Productor {
  nombre: string;
  id: number;
}

@Component({
  selector: 'app-asignar_folios_controldecalidad',
  templateUrl: './asignar_folios_controldecalidad.component.html',
  styleUrls: ['./asignar_folios_controldecalidad.component.css']
})
export class Asignar_folios_controldecalidadComponent implements OnInit {
  displayedColumns: string[] = ['select', 'id', 'condicion'];
  dataSource = new MatTableDataSource([]);
  selection = new SelectionModel(true, []);
  myControl = new FormControl();
  filterOption: Observable<Productor[]>;
  ListaEspecies: any[] = [];
  ListaVariedades: any[] = [];
  ListaZonas: any[] = [];
  ListaFiltro: any[] = [];
  ListaCantidad: any[] = [];
  ListaProductores: any[] = [];

  folio: any;
  guia: any;
  planilla: any;
  especie: any;
  variedad: any;
  zonaProductor: any;
  cantidadFolios: number;
  productor: any = null;
  frigorifio: any;
  seleccionadasTrue: any[] = [];
  condicion: any;
  nroCambioCondicion: any;

  muestra: boolean = false;

  planta: any;
  temporada: any;
  zona_prod: any;
  exportadora: any;

  // anfn funcion callback

  constructor(public dialogRef: MatDialogRef<Asignar_folios_controldecalidadComponent>,
    private logisticaService: ApilogisticaService,
    public snackBar: MatSnackBar,
    private commonService: ApiCommonService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);
    this.frigorifio = data.frigorifico;
    this.condicion = data.condicion
    this.nroCambioCondicion = data.nro
  }

  ngOnInit() {
    this.planta = localStorage.getItem('planta');
    this.temporada = localStorage.getItem('temporada');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona_prod = localStorage.getItem('planta');

    this.cargarespecies();
    this.cargarZonas();    
  }

  displayFn(productor?: Productor): string | undefined {
    // console.log('Display ', materia);
    return productor ? productor.nombre : undefined;
  }
  private _filter(des: string): Productor[] {
    const filterValue = des.toLowerCase();
    // console.log("EL DATO ES :", des);
    return this.ListaProductores.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }

  close(): void {
    this.dialogRef.close();
  }
  capturar(value) {
    console.log(value);
    this.cargarVarieades(value);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row): string {

    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.folio + 1}`;
  }

  applyFilterFolio(filterValue: string) {
    if (filterValue.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.folio = filterValue;
      this.conteo();
    }
    // console.log(filterValue)
  }
  applyFilterGuia(filterValue: string) {
    if (filterValue.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.guia = filterValue;
      this.conteo();
    }
    // console.log(filterValue)
  }
  applyFilterPlanilla(filterValue: string) {
    if (filterValue.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.planilla = filterValue;
    }
    // console.log(filterValue)
  }

  cargarespecies() {
    this.commonService.cargarEspecies().subscribe((res) => {
      // console.log(res);
      this.ListaEspecies = res.items;

    });
  }
  cargarVarieades(especie: string) {
    this.commonService.cargarVarieades(especie).subscribe((res) => {
      // console.log(res);
      this.ListaVariedades = res.items;
    })
  }
  cargarZonas() {
    this.commonService.cargarZonas().subscribe((res) => {
      // console.log(res);
      this.ListaZonas = res.items;
    })
  }
  checkTodo(event) {
    this.seleccionadasTrue = [];
    if (event.checked) {
      this.ListaFiltro.forEach(element => {
        element.check = true;
        if (element.check) {
          const info = {
            FolioPallet: element.FolioPallet,
            condicion: element.CondicionActual.nombre,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    } else {
      this.ListaFiltro.forEach(element => {
        element.check = false;
        if (!element.check) {
          this.seleccionadasTrue = [];
        }
      });
    }
    console.log(this.ListaFiltro);
  }
  checkSeleccionados(event) {
    // console.log(event);
    this.seleccionadasTrue = [];
    if (event.check) {
      this.ListaFiltro.forEach(element => {
        if (element.check) {
          const info = {
            FolioPallet: element.FolioPallet,
            condicion: element.CondicionActual.nombre,
            check: true
          }
          this.seleccionadasTrue.push(info);
          // console.log(this.seleccionadasTrue);
        }
      });
    }
    if (!event.check) {
      this.ListaFiltro.forEach(element => {
        if (element.check) {
          const info = {
            FolioPallet: element.FolioPallet,
            condicion: element.CondicionActual.nombre,
            check: true
          }
          this.seleccionadasTrue.push(info);
        }
      });
    }
    console.log(this.seleccionadasTrue);
  }
  conteo() {
    const req = {
      CodTemp: this.temporada,
      CodZona: this.zona_prod,
      CodExportadora: this.exportadora,
      CodPlanta: '  22',
      folioPallet: this.folio,
      guia: this.guia,
      planilla: this.planilla,
      CodEspecie: this.especie,
      CodVaridad: this.variedad,
      CodProductor: this.productor,
      CodZonaProductor: this.zonaProductor,
      CantRegistro: 50,
      Pagin: 1
    }
    this.commonService.conteo(req).subscribe((res) => {
      console.log('CONTEO', res);
      this.ListaCantidad = res.lista;
    }, (error) => {
      console.log(error);
    });
  }
  filtrar() {

    console.log(this.productor);
    // console.log(this.productor.id);
    if (this.folio === undefined) {
      this.folio = '';
    }
    if (this.guia === undefined) {
      this.guia = '';
    }
    if (this.planilla === undefined) {
      this.planilla = 0;
    }
    if (this.especie === undefined) {
      this.especie = '';
    }
    if (this.variedad === undefined) {
      this.variedad = '';
    }
    if (this.zonaProductor === undefined) {
      this.zonaProductor = '';
    }
    if (this.productor === null) {
      this.productor = '';
    }
    if (this.productor.id === undefined) {
      this.productor = '';
    }
    if (this.productor === '') {
      this.productor = '';
    }
    const req = {
      CodTemp: this.temporada,
      CodZona: this.zona_prod,
      CodExportadora: this.exportadora,
      CodPlanta: '  22',
      folioPallet: this.folio,
      guia: this.guia,
      planilla: this.planilla,
      CodEspecie: this.especie,
      CodVaridad: this.variedad,
      CodProductor: this.productor.id,
      CodZonaProductor: this.zonaProductor,
      CantRegistro: 50,
      Pagin: 1
    }
    console.log(JSON.stringify(req));
    // return;
    this.commonService.filtrar(req).subscribe((res) => {
      console.log(res);
      if (res.lista.length > 0) {
        this.muestra = true;
        this.conteo();
      }
      else {
        this.muestra = false;
      }
      this.ListaFiltro = res.lista;
      this.dataSource = new MatTableDataSource(this.ListaFiltro);

    }, (error) => {
      console.log(error);
      if (!error.ok) {
        this.muestra = false;
        this.ListaFiltro = [];
        this.dataSource = new MatTableDataSource([]);
        this.snackBar.open(error.error.mensaje, null, { duration: 3000 });
      }
    }
    );
  }
  getListadoProductor(zonaP) {
    const req = {
      exportadora: this.exportadora,
      zona: zonaP
    }
    this.commonService.getListadoProductores(req).subscribe((res) => {
      console.log('LISTADO PRODUCTORES', res);
      this.ListaProductores = res;

      this.filterOption = this.myControl.valueChanges
      .pipe(
        startWith<string | any>(''),
        map(cod => typeof cod === 'string' ? cod : cod.nombre),
        map(des => des ? this._filter(des) : this.ListaProductores.slice())
      );



    }, (error) => {
      console.log(error);
    }
    )
  }
  limpiar() {
    this.folio = undefined;
    this.guia = undefined;
    this.planilla = undefined;
    this.zonaProductor = undefined;
    this.especie = undefined;
    this.variedad = undefined;
    this.productor = '';
    this.muestra = false;
    this.dataSource = new MatTableDataSource([]);
    this.seleccionadasTrue = [];
  }
  obtenerCodigo(event){
    console.log(event);
    console.log(this.zonaProductor);
  }
  selectZonaProductor(event){
    console.log(event)
    this.getListadoProductor(event);
  }
  asignarFolios() {
    console.log(this.productor);
    if (this.planilla === undefined) {
      this.planilla = 0;
    }
    if (this.especie === undefined) {
      this.especie = '';
    }
    if (this.variedad === undefined) {
      this.variedad = '';
    }
    if (this.productor === '') {
      this.productor = 0;
    }

    console.log(this.seleccionadasTrue);
    if (this.seleccionadasTrue.length === 0) {
      this.snackBar.open('Debe seleccionar un folio o más', null, { duration: 3000 });
    }
    else {
      this.seleccionadasTrue.forEach(element => {
        const req = {
          temporada: this.temporada,
          zona: this.zona_prod,
          frigorifico: this.frigorifio,
          exportadora: this.exportadora,
          folio: element.FolioPallet,
          productor: this.productor.id,
          planilla: this.planilla,
          especie: this.especie,
          variedad: this.variedad,
          nuevaCondicion: this.condicion,
          codUsuario  : 'log'
        }
        console.log(JSON.stringify(req));
        // return;
        this.commonService.patchEncabezadoPalletFolio(req).subscribe((res) => {
          console.log('PATCH', res);
          if (res.cantDetalleCambiados > 0) {
            this.snackBar.open('Condicion cambiada con éxito', null, { duration: 3000 });
            this.filtrar();

            const request = {
              FolioPallet: element.FolioPallet,
              CodProductor: this.productor.id,
              NumCambio:this.nroCambioCondicion,
              CodTemporada: this.temporada,
              CodZona: this.zona_prod,
              planilla:this.planilla,
              guia:0,
              CodExportadora: this.exportadora,
              CodFrigorifico:this.frigorifio,
              nuevaCondicion: this.condicion,
              
            }
            console.log(JSON.stringify(request));
            // this.dialogRef.close({ api: request });
            
            this.commonService.postDetalleCambioCondicion(request).subscribe((res) => {
              console.log(res);
            },((error) => {
              this.snackBar.open(error.error.mensaje, null, { duration: 3000 });
            }
            ));

          }
          else {
            this.snackBar.open('Misma condición o error al cambiar condición', null, { duration: 3000 });
          }
        });
     
      });
    }
  }
  post(element){
   
  }

}
