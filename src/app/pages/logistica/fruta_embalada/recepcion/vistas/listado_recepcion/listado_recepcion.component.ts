import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router, NavigationExtras } from '@angular/router';
import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';


const ELEMENT_DATA = [
  {
    cod: 123, fecha: 'nnnnn', packing_satelite: 'nnnnn', frigorifico: '11', productor: '11', nro_guia: '11'
  }
];

@Component({
  selector: 'app-listado_recepcion',
  templateUrl: './listado_recepcion.component.html',
  styleUrls: ['./listado_recepcion.component.css']
})
export class Listado_recepcionComponent implements OnInit {
  cod: any;
  fecha: any;
  packing_satelite: any;
  frigorifico: string;
  productor: string;
  nro_guia: number;
  icons: any;
  frigorificos: any[] = [];
  frigoSelecion: any;

  private sub = new Subject();
  displayedColumns: string[] = ['cod', 'fecha', 'packing_satelite', 'frigorifico', 'productor', 'nro_guia', 'icons'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  esFavorito: boolean;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private logisticaService: ApilogisticaService,
    private commonService: ApiCommonService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.obtieneFrigorificos();
  }
  obtieneFrigorificos() {
    this.frigorificos = [];
    this.commonService.cargarFrigorifico(localStorage.getItem('exportadora'), localStorage.getItem('planta'))
      .subscribe(async (res) => {
        this.frigorificos = await res.items;
        // console.log('FRIGORIFICOS', res.items);
        this.frigoSelecion = this.frigorificos[0].id;
      }, (error) => {
        // console.error('ERROR FRIGORIFICOS', error);
        this.snackBar.open('Error al cargar frigorificos', null, { duration: 3000 });
      });
  }
  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }

  nuevaRecepcion() {
    let extras: NavigationExtras = {
      queryParams: {
        frigorifico: this.frigoSelecion
      }
    }
    this.router.navigate(['/nueva_recepcion_fruta_embalada'], extras);
  }

  editar() { }
  anterior() { }
  siguiente() { }
}

