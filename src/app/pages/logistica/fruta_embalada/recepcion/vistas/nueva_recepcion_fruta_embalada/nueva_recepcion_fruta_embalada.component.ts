import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiCommonService } from './../../../../../../services/apicommonservice.service';
import { ApilogisticaService } from './../../../../../../services/apilogistica.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { groupBy } from 'rxjs/operators';

const ELEMENT_DATA = [
  {
    cod: 123, fecha: 'nnnnn', packing_satelite: 'nnnnn', frigorifico: '11', productor: '11', nro_guia: '11'
  }
];

export interface FolioPalletI {
  folioPallet: string;
  cajas: string;
  // menu: any;
}
@Component({
  selector: 'app-nueva_recepcion_fruta_embalada',
  templateUrl: './nueva_recepcion_fruta_embalada.component.html',
  styleUrls: ['./nueva_recepcion_fruta_embalada.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class Nueva_recepcion_fruta_embaladaComponent implements OnInit {
  panelOpenState = false;
  public cargando: boolean;
  cod: any;
  fecha: any;
  hora: string;
  packing_satelite: any;
  frigorifico: string;
  productor: string;
  nro_guia: number;
  icons: any;
  frigorificos: any[] = [];
  frigoSelecion: any;
  lista:any[]=[];
  archivos:any;
  listaPallet:any[]=[];
  listadoFinal:any[]=[];
  listadoFinalFinal:any[]=[];
  huerto:any
  listaCajas:any[]=[];
  editandoRecepcion:any;
  productorIntencion:any;
  codProductor: number;
  observacion:string;
  productorIntenciones = [];
  rut: string = null;
  empresaSeleccionada = '';
  zonaSeleccionada = '';
  sicarga:string = 'INICIO';
  huertos = [];
  zona = 0;
  csg: string = '';
  temporada: string;
  planta: any;
  zona_prod: any;
  exportadora: any;

  private sub = new Subject();
  columnsToDisplay = ['FolioPallet', 'Cajas', 'Menu'];
  dataSourceTabla = new MatTableDataSource(null);
  displayedColumns: string[] = ['cod', 'fecha', 'packing_satelite', 'frigorifico', 'productor', 'nro_guia'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  expandedElement: FolioPalletI | null;
  esFavorito: boolean;

  constructor(
    private router: Router,
    private dialog: MatDialog,private commonService: ApiCommonService,
    private logisticaService: ApilogisticaService,
    private activatedRouter: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    activatedRouter.queryParamMap.subscribe((params: any) => {
      this.frigorifico = params.params.frigorifico;
      console.log(this.frigorifico);
    });
    this.planta = localStorage.getItem('Nplanta');
    this.temporada = localStorage.getItem('temporada');
    this.exportadora = localStorage.getItem('exportadora');
    this.zona_prod = localStorage.getItem('planta');

    this.fecha = new Date();
    this.hora = this.fecha.getHours() + ':' + this.fecha.getMinutes();
    let hora = this.fecha.getHours().toString().length === 1 ? '0' + '' + this.fecha.getHours() : this.fecha.getHours();
    let minutos = this.fecha.getMinutes().toString().length === 1 ? '0' + '' + this.fecha.getMinutes() : this.fecha.getMinutes();
    this.hora = hora + ':' + minutos;
   }

  ngOnInit() {

  }

  elegirArchivo(){
    this.listaCajas = [];
    this.listaPallet = [];
    var listaPallet:any[] = this.listaPallet;
    var listaCajas:any[] = this.listaCajas;
    var commonService = this.commonService;
    var sicargar = this.sicarga;
    var filass:any[]=[];
    console.log(sicargar);
    function leerArchivo(e) {

      var archivo = e.target.files[0];
      if (!archivo) {
        return;
      }
      sicargar = 'TERMINO';
      console.log(sicargar);
      var lector = new FileReader();
      lector.onload = function (e) {
        
      var contenido: any = e.target['result'];
      var inicioPallet = contenido.indexOf('<PALLET>');
      var terminoPallet = contenido.indexOf('</PALLET>');
      var inicioCaja = contenido.indexOf('<CAJA>');
      var terminoCaja = contenido.indexOf('</CAJA>');
      var indiceInicio = inicioCaja+8;
      var indicePallet = 97;
      var caja = contenido.substring(indiceInicio,terminoCaja-2)
      var pallet = contenido.substring(inicioPallet+9, terminoPallet-2);  
      // console.log(caja);
      var resto = caja;

      // console.log(resto);
      var linea = resto.split('\n');
      for(var line = 0; line < linea.length; line++){
        // console.log(linea[line]);

        const filas = {
          codTemporada: linea[line].substring(0,5),
          numCaja:linea[line].substring(6,15),
          folioAsignado:linea[line].substring(15,22),
          estadoPallet:linea[line].substring(22,23),
          codBasePallet:linea[line].substring(23,31),
          codPacking:linea[line].substring(31,35),
          numGuiaProductor:linea[line].substring(36,42),
          codZonaProductor:linea[line].substring(42,44),
          codProductor:linea[line].substring(44,47),
          codHuerto:linea[line].substring(47,50),
          fechaEmbalaje:linea[line].substring(51,61),
          fechaCosecha:linea[line].substring(61,71),
          codEspecie:linea[line].substring(71,72),
          codVariedadReal:linea[line].substring(75,78),
          codVariedadRotulada:linea[line].substring(79,82),
          codCategoriaReal:linea[line].substring(83,84),
          codCategoriaRotulada:linea[line].substring(87,88),
          codPeso:linea[line].substring(91,95),
          codTipoEnvase:linea[line].substring(96,98),
          codDimension:linea[line].substring(100,104),
          codEtiqueta:linea[line].substring(104,105),
          codEmbalaje:linea[line].substring(108,112),
          codMercado:linea[line].substring(112,115),
          codPais:linea[line].substring(116,120),
          codTipoFrio:linea[line].substring(120,124),
          codPreFrio:linea[line].substring(124,125),
          codCalibre:linea[line].substring(125,128),
          valorPesoNeto:linea[line].substring(129,134),
          aptitudGuarda:linea[line].substring(134,135),
          indicadorProceso:linea[line].substring(135,136),
          calibreRotulado:linea[line].substring(136,144),
          codZonaProdRotulado:linea[line].substring(144,146),
          codProductorRotulado:linea[line].substring(146,149),
          standardConsumo:linea[line].substring(149,158)
        }
        listaCajas.push(filas);
        
      }

      var restoPallet = pallet;
      // console.log(restoPallet);
      while (restoPallet.length > 0 ) {
        var fila = restoPallet.substring(1,indicePallet-1);
        var fila2 = fila.replace(/         /g,'*');
        var fila3 = fila2.replace(/       /g,'*');
        var fila4 = fila3.replace(/   /g,'');
        // var fila5 = fila4.replace(/ /g,'*');
        var fila1 = fila4.split('*');
        var folioPALLET = fila1[0].replace(/,/g,'');
        var cajAS = fila1[4].replace(/,/g,'');
        const pallet = {
          folioPallet: folioPALLET,
          cajas: cajAS 
        }
        listaPallet.push(pallet);
        var restoPallet = restoPallet.substr(97, terminoPallet-2);
      }
      listaPallet.forEach(element => {
        // console.log(element.folioPallet);
      });

      // console.log(filass);
      // console.log(listaCajas);
      // console.log('PALLET',listaPallet);
      console.log('FINALIZADO');
      };
      lector.readAsText(archivo);
    }
    document.getElementById('file-input')
      .addEventListener('change', leerArchivo, false);
      console.log(sicargar);
      this.cargando = true;
      
      
      setTimeout(() => {
        this.funcion();
        this.cargando = false;
        console.log('CAJAS',listaCajas);
        console.log('PALLET',listaPallet);
      }, 5000);
  }


  funcion(){
    var groupBy = function(xs, key) {
      return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    };
    var caja = groupBy(this.listaCajas, 'folioAsignado');
    this.listadoFinal.push(caja);

    // console.log(this.listaPallet);
    for (let i = 0; i < this.listaPallet.length; i++) {
      const element = this.listaPallet[i];
      // console.log(this.listaPallet[i].folioPallet);

      this.listadoFinal.forEach(element => {
        // console.log(element[this.listaPallet[i].folioPallet]);
        var detalle = element[this.listaPallet[i].folioPallet];
        const lista= {
          folio:this.listaPallet[i].folioPallet,
          detalle: detalle
        }
        this.listadoFinalFinal.push(lista);
        this.dataSourceTabla = new MatTableDataSource(this.listadoFinalFinal);
      });
    }
    console.log(this.listadoFinalFinal);
    console.log('FINALIZADO');
  }
  guardarDetalle(){
    // console.log(this.listadoFinalFinal);

  }
  detalleFolioPallet(item){
    console.log(item);
    var folioPalet = item.folioPallet;
    console.log(folioPalet.replace(/,/g,''));
  }
  cargarZonaProd(){
    console.log(this.rut, this.temporada);
    this.logisticaService.cargarZonaProductor(this.rut, this.temporada).subscribe((res) => {
      console.log(res);
    });
  }
  convertirFecha(fech: string) {
    var date = new Date(fech), mnth = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }
  selectProductoresInt(data: any) {
    console.log(data);
    const info = data.value.split('-');
    this.empresaSeleccionada = info[1];
    const temporada = localStorage.getItem('temporada');
    this.zonaSeleccionada = info[0];
    this.cargarHuertos(this.empresaSeleccionada, temporada, this.zonaSeleccionada);
  }
  cargarHuertos(empresa: any, temporada: any, zona: any) {
    this.huertos = [];
    this.commonService.cargarHuertos(empresa, temporada, zona)
      .subscribe((result) => {
        console.log('RESPONSE HUERTOS', result);
        this.huertos = result.items;
        this.cargarZonaProd();
        this.rescatarZona();
        this.cargarPakingSatelite();
      }, (error) => {
        console.error('ERROR CARGANDO HUERTOS', error);
      });
  }
  cargarPakingSatelite(){
    const req = {
      productor:this.rut,
      temporada:this.temporada,
      zona:this.zona_prod,
      zonaProductor:this.zonaSeleccionada
    }
    console.log(req);
    this.commonService.getPackingSatelite(req).subscribe((res) => {
      console.log(res);
    });
  }
  rescatarZona() {
    const temporada = localStorage.getItem('temporada');
    const planta = localStorage.getItem('planta');
    this.commonService.cargarZona(temporada, planta)
      .subscribe((result) => {
        console.log('RESPONSE ZONA', result);
        this.zona = result.items[0].id;
      }, (error) => {
        console.error('ERROR AL RESCATAR', error);
      });
  }
  finalizar(){
    let fechaM = this.convertirFecha(this.fecha) + 'T' + this.hora;    
    this.listadoFinalFinal.forEach(element => {
      console.log(element.folio);
      console.log(element.detalle.length);
      for (let i = 0; i < element.detalle.length; i++) {
        const elemento = element.detalle[i];
        const req = {
          encabezado: {id:3,zona:{id:this.zona_prod},exportadora:{id:this.exportadora},temporada:{id:this.temporada}},
          folioPallet: element.detalle[i].folioAsignado,
          nroguia: element.detalle[i].numGuiaProductor,
          fechaEmbalaje: element.detalle[i].fechaEmbalaje,
          fechaCosecha: element.detalle[i].fechaCosecha,
          esPreFrio: false,
          pesoNeto: element.detalle[i].codPeso,
          cajasPallet: element.detalle.length,
          peso: {id:element.detalle[i].valorPesoNeto},
          estadoPallet: {id:element.detalle[i].estadoPallet},
          basePallet: {id:element.detalle[i].codBasePallet},
          categoria: {id:element.detalle[i].codCategoriaReal},
          categoriaRotulada: {id:element.detalle[i].codCategoriaRotulada},
          tipoEnvase: {id:element.detalle[i].codTipoEnvase},
          pais: {id:element.detalle[i].codPais},
          aptitudCKU: {id:'  10'},
          tipoFrio: {id:element.detalle[i].codTipoFrio},
          dimension: {id:element.detalle[i].codDimension},
          etiqueta: {id:element.detalle[i].codEtiqueta},
          mercado: {id:element.detalle[i].codMercado},
          embalaje: {id:element.detalle[i].codEmbalaje},
          calibre: {id:element.detalle[i].codCalibre},
          calibreRotulado: {id:element.detalle[i].calibreRotulado},
          stdEmbalaje: {id:'element.detalle[i].folioAsignado'},
          productor : { empresa : {id:element.detalle[i].folioAsignado}, huerto : {id:element.detalle[i].folioAsignado} },
          productorRotulado : { exportadora : {id:this.exportadora}, zona : {id:element.detalle[i].codZonaProdRotulado}, empresa : {id:element.detalle[i].folioAsignado}, huerto : {id:element.detalle[i].folioAsignado} },
          variedad: {idsnap:element.detalle[i].codVariedadReal,especie:{idsnap:element.detalle[i].codEspecie}},
          variedadRotulada: {idsnap:element.detalle[i].codVariedadRotulada,especie:{idsnap:element.detalle[i].codEspecie}},
          packingSatelite: {id:'COD1'}
       }
      //  console.log(req);

       const req2 = 
       {
        zona: {id:this.zona_prod},
        temporada: {id:this.temporada},
        nroCaja: element.detalle[i].numCaja,
        folio: element.detalle[i].folioAsignado,
        estadoPallet: {id:element.detalle[i].estadoPallet},
        basePallet: {id:element.detalle[i].codBasePallet},
        packing: {id:'  22'},
        nroGuiaProductor: element.detalle[i].numGuiaProductor,
        productor: {zona:{id:element.detalle[i].codZonaProductor},huerto:{id:element.detalle[i].codHuerto},id:902},
        fechaEmbalaje: element.detalle[i].fechaEmbalaje,
        fechaCosecha: element.detalle[i].fechaCosecha,
        variedadReal: {id:element.detalle[i].codVariedadReal,especie:{id:element.detalle[i].codEspecie}},
        variedadRotulada: {id:element.detalle[i].codVariedadRotulada},
        categoria: {id:element.detalle[i].codCategoriaReal},
        categoriaRotulada: {id:element.detalle[i].codCategoriaRotulada},
        peso: {id:element.detalle[i].codPeso},
        tipoEnvase: {id:element.detalle[i].codTipoEnvase},
        dimension: {id:element.detalle[i].codDimension},
        etiqueta: {id:element.detalle[i].codEtiqueta},
        embalaje: {id:element.detalle[i].codEmbalaje},
        mercado: {id:element.detalle[i].codMercado},
        pais: {id:element.detalle[i].codPais},
        tipoFrio: {id:element.detalle[i].codTipoFrio},
        esPreFrio: false,
        calibre: {id:element.detalle[i].codCalibre},
        pesoNeto: element.detalle[i].valorPesoNeto,
        aptitudGuarda: {id:element.detalle[i].aptitudGuarda},
        indicadorProceso : element.detalle[i].indicadorProceso,
        calibreRotulado: {id:element.detalle[i].codCategoriaRotulada},
        productorRotulado: {zona:{id:element.detalle[i].codZonaProdRotulado},id:element.detalle[i].codProductorRotulado},
        stdConsumo: {id:element.detalle[i].standardConsumo}
     }
       console.log(req2);
      }
      console.log('FINALIZADO');
    });

    
  }
  formatoRut(rut: string) {
    this.commonService.cargarEmpresa(rut)
      .subscribe((result) => {
        console.log(result);
        this.productor = result.nombre;
        this.rescatarProductorIntencion(result.id);

      }, (error) => {
        console.error('ERROR AL CARGAR EMPRESA', error);
      });
  }
  rescatarProductorIntencion(rut: string) {
    const temporada = localStorage.getItem('temporada');
    this.logisticaService.cargarZonaProductor(rut, temporada)
      .subscribe((result) => {
        console.log(result);
        this.productorIntenciones = result.items;
        this.codProductor = result.items['codProductor'];
      }, (error) => {
        console.error('ERROR CARGAR ZONA PRODUCTOR', error);
      });
  }
  selectHuerto(data: any) {
    const h = this.huertos.find(element => {
      return element.id === data;
    });
    this.csg = h.codCSG;
  }
  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  agregarFavorito() {
    this.esFavorito = !this.esFavorito;
    if (this.esFavorito) {
      this.snackBar.open('✔  Añadido a favoritos', null, { duration: 800 })
    }
    else {
      this.snackBar.open('✖  Eliminado de favoritos', null, { duration: 800 })
    }
  }
  ver(){
    console.log(this.listaPallet);
    console.log(this.listaCajas);
    // this.dataSourceTabla = new MatTableDataSource(this.listaPallet);
    
    this.listaPallet.forEach(element => {
      for (let i = 0; i < element.length; i++) {
        // const element = element[i];
        
      }
      
    });
  }
  
  editar() {

  }
  anterior() { }

  siguiente() { }
}

