import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';

const ELEMENT_DATA = [
  {
    folio: 'falta', f_emision: 'nnnnn', forma_pago: '11', cliente: '11', tipo_moneda: '', monto:'', total:''
  }
];

@Component({
  selector: 'app-documentos_electronicos',
  templateUrl: './documentos_electronicos.component.html',
  styleUrls: ['./documentos_electronicos.component.css']
})
export class Documentos_electronicosComponent implements OnInit {
  displayedColumns: string[] = ['folio', 'f_emision', 'forma_pago', 'cliente', 'tipo_moneda', 'monto', 'total', 'impreso','icons'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource2 = new MatTableDataSource(ELEMENT_DATA);
  impreso:boolean;
  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { } 

  ngOnInit() {
    this.impreso=false;
  }

  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
  imprimir(){
    this.impreso=true;
  }
}
