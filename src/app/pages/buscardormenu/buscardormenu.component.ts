import { Component, OnInit, Inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiCommonService } from '../../services/apicommonservice.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-buscardormenu',
  templateUrl: './buscardormenu.component.html',
  styleUrls: ['./buscardormenu.component.css']
})

export class BuscardormenuComponent implements OnInit {
  menuBusqueda = [];
  buscando = false;
  subscription: Subscription;

  constructor(
    public apiCommon: ApiCommonService,
    public route: Router,
    private _location: Location,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.subscription = this.apiCommon.recibirMenusBusqueda().subscribe(mn => {
      this.menuBusqueda = mn.menus;
      if (this.menuBusqueda.length > 0 && mn.buscar) {
        this.buscando = true;
      } else {
        this.buscando = false;
      }
      if (!mn.buscar && this.menuBusqueda.length === 0) {
        this._location.back();
      }
    });
  }

  ir(url:any) {
    console.log('URL',url);
    this.route.navigate([url]);
    this.dialogRef.close();
  }

  salir() {
    this.dialogRef.close();
  }
}
