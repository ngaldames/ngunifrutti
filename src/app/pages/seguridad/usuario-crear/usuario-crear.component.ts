
import { Component, OnInit, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Router, ActivatedRoute } from '@angular/router';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { ApiseguridadService } from '../../../services/apiseguridad.service';
import { ApiCommonService } from '../../../services/apicommonservice.service';

export class TodoItemNode {
  children: TodoItemNode[];
  item: string;
}

export class TodoItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);
  get data(): TodoItemNode[] { return this.dataChange.value; }

  constructor(
    public wsSeguridad: ApiseguridadService
  ) {
    this.initialize();
  }

  initialize() {
    this.wsSeguridad.obtenerPantallas().subscribe(result => {
      console.log(result);
    });
    const user = localStorage.getItem('User');
  }
}

@Component({
  selector: 'app-usuario-crear',
  templateUrl: './usuario-crear.component.html',
  styleUrls: ['./usuario-crear.component.css'],
  providers: [ChecklistDatabase]
})

export class UsuarioCrearComponent implements OnInit {
  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();
  selectedParent: TodoItemFlatNode | null = null;
  newItemName = '';
  treeControl: FlatTreeControl<TodoItemFlatNode>;
  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;
  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);
  arr_parcial = [];
  arr_temporal = [];
  menus_principal = [];
  menus = [];
  menus_usuario_funcionalidad = [];
  ListUsuarios = [];
  ListRoles = [];
  user = '';
  exportadoras = [];
  plantas: [];
  exportadoraSeleccionada = '';
  planta = '';
  menuSeleccionado = '';

  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private database: ChecklistDatabase,
    public wsSeguridad: ApiseguridadService,
    public wsCommon: ApiCommonService
  ) {
    let co: any = this.activatedRouter.queryParams;
    this.user = co.value.id;
    // this.user = localStorage.getItem('User');
    // this.cargarfuncionalidadesUsuario(this.user);
    this.wsSeguridad.cargarListadoUsuarios('').subscribe(result => {
      this.ListUsuarios = result.items;
      console.log(this.ListUsuarios);
    })
    this.cargarListaRoles();
  }

  selectPlanta() {
    // this.exportadoraSeleccionada = '';
    this.selectMenu(this.menuSeleccionado)
  }

  selectExportadora() {
    // this.exportadoraSeleccionada = '';
    this.selectMenu(this.menuSeleccionado)
  }

  agregarTodosPermisos() {
    console.log(this.menuSeleccionado);
    const menuTotal = this.menus.filter(element => {
      return element.id.startsWith(this.menuSeleccionado) && !element.esMenu;
    });
    console.log(menuTotal);
    for (let index = 0; index < menuTotal.length; index++) {
      const element = menuTotal[index];
      this.wsSeguridad.obtenerFuncionalidadesMenus(element.id)
        .subscribe((result) => {
          if (result.items.length > 0) {
            console.log(result.items)
            result.items.forEach(element_funcionalidad => {
              const item = {
                id_padre: element.id,
                id: element_funcionalidad.id
              };
              this.agregarFuncionalidad(item);
            });
          }
        }, (error) => {
          console.error('ERROR CARGANDO FUNCIONALIDADES MENU', error);
        });
    }
  }

  obtenerExportadoras() {
    this.wsCommon.cargarTodasExportadoras().subscribe(result => {
      console.log(result);
      this.exportadoras = result.items;
    }, error => {
      this.obtenerExportadoras();
    });
  }

  obtenerPlantas() {
    this.wsCommon.cargarTodasPlantas().subscribe(result => {
      console.log(result);
      this.plantas = result.items;
    }, error => {
      this.obtenerPlantas();
    });
  }

  ngOnInit() {
    this.obtenerExportadoras();
    this.obtenerPlantas();
    this.wsSeguridad.obtenerPantallas().subscribe(result => {
      console.log(result);
      this.generarMenusPrincipales(result.items);
      this.menus = result.items;
    });
  }

  cambios(event: any) {
    console.log(event);
  }

  cargarListaRoles() {
    this.wsSeguridad.obtenerRoles()
      .subscribe((data) => {
        console.log(data);
        this.ListRoles = data.items;
      }, (error) => {
        this.snackBar.open('Error al cargar roles', null, { duration: 3000 });
      });
  }

  copiarDesdeUsuario(usuario) {
    console.log(usuario);
    const data = {
      UsuarioOrigen: usuario,
      UsuarioDestino: this.user,
    };
    console.log(data);
    this.wsSeguridad.copiarFuncionalidadesUsuarios(data).subscribe(result => {
      console.log(result);
      this.snackBar.open(' ✔ \xa0\xa0 Datos copiados Correctamente', null, { duration: 2000 });
    });
  }

  copiarDesdeRoles(rol: any) {
    console.log(rol);
  }

  guardar() {
    this.snackBar.open(' ✔ \xa0\xa0 Datos guardados correctamente', null, { duration: 2000 });
    this.router.navigate(['/usuarioslistado']);
  }

  eliminarFuncionalidad(item: any) {
    const usuario = this.user;
    const planta = this.planta;
    const exportadora = this.exportadoraSeleccionada;
    this.wsSeguridad.eliminarFuncionalidadesUsuarios(usuario, planta, exportadora, item.id, item.id_padre).subscribe(result => {
      console.log(result);
      this.snackBar.open(' ✔ \xa0\xa0 Funcionalidad Eliminada', null, { duration: 2000 });
    });
  }

  agregarFuncionalidad(item: any) {
    console.log(item);
    const data = {
      usuario: { id: this.user },
      planta: { id: this.planta },
      exportadora: { id: this.exportadoraSeleccionada },
      pantalla_funcionalidad: { pantalla: { id: item.id_padre }, funcionalidad: { id: item.id } }
    };
    console.log(JSON.stringify(data));
    this.wsSeguridad.agregarFuncionalidadesUsuarios(data)
      .subscribe((result) => {
        console.log('RESPONSE AGREGAR FUNCIONALIDAD', result)
        this.snackBar.open(' ✔ \xa0\xa0 Funcionalidad agregada correctamente', null, { duration: 2000 });
      }, (error) => {
        console.error('ERROR AGREGAR FUNCIONALIDAD', error);
      });
  }

  cargarfuncionalidadesUsuario(usuario: any) {
    const exportadora = this.exportadoraSeleccionada;
    const idPlanta = this.planta;
    this.wsSeguridad.obtenerFuncionalidadesUsuarios(usuario)
      .subscribe((result) => {
        this.menus_usuario_funcionalidad = result.items.filter(res => {
          return res.exportadora.id === exportadora && res.planta.id === idPlanta;
        });
      }, (error) => {
        console.error('ERROR CARGAR FUNCIONALIDADES', error);
      });
  }

  expandirMenuFuncionalidad(ind: any) {
    let todosSeleccionados = false;
    if (this.arr_parcial[ind].expandido) {
      return;
    }
    this.wsSeguridad.obtenerFuncionalidadesMenus(this.arr_parcial[ind].id)
      .subscribe((result) => {
        console.log(result);
        let element = {
          id: '',
          nombre: 'sin funcionalidades',
          left: this.arr_parcial[ind].left + 15,
          level: this.arr_parcial[ind].level + 1,
          ultimo_elemento: true,
          funcionalidad: true,
          id_padre: this.arr_parcial[ind].id
        };
        if (result.items.length > 0) {
          result.items.forEach(element => {
            element = {
              id: element.id,
              nombre: element.nombre,
              left: this.arr_parcial[ind].left + 15,
              level: this.arr_parcial[ind].level + 1,
              ultimo_elemento: true,
              funcionalidad: true,
              id_padre: this.arr_parcial[ind].id,
              check: false
            };
            const _verificarExisteFuncionalidad = this.verificarExisteFuncionalidad(element.id, this.arr_parcial[ind].id)
            element.check = _verificarExisteFuncionalidad;
            if (!element.check) {
              this.arr_parcial[ind].indeterminate = true;
            }
            this.arr_parcial.splice(ind + 1, 0, element);
          });
        } else {
          this.arr_parcial.splice(ind + 1, 0, element);
        }
        // if ( todosSeleccionados ) { this.arr_parcial[ind].check = true; }
        this.arr_parcial[ind].expandido = true;
      }, (error) => {
        console.error('ERROR EXPANDIR', error);
      });
  }

  verificarExisteFuncionalidad(idFuncionalidad: any, idMenu: any) {
    let existe = false;
    for (let index = 0; index < this.menus_usuario_funcionalidad.length; index++) {
      const element = this.menus_usuario_funcionalidad[index].pantalla_funcionalidad;
      if (element.pantalla.id == idMenu && element.funcionalidad.id == idFuncionalidad) {
        existe = true;
        break;
      }
    }
    return existe;
  }

  expandirMenu(ind: any) {
    if (ind < this.arr_parcial.length - 1) {
      // si el arreglo siguiente tiene un left mayor que el actual
      /// esto significa que los shuientes datos son hijos del item que seleccionamos
      if (this.arr_parcial[ind + 1].left > this.arr_parcial[ind].left) {
        let ultimoAeliminar = 0;
        /// for para eliminar un los elementos hijos cuando se hace click en un item
        for (let index = ind + 1; index < this.arr_parcial.length; index++) {
          const element = this.arr_parcial[index];
          if (element.left > this.arr_parcial[ind].left) {
            ultimoAeliminar++;
          } else {
            break;
          }
        }
        /// elimina los arreglos desde el que seleccionamos
        this.arr_parcial.splice(ind + 1, ultimoAeliminar);
        this.arr_parcial[ind].expandido = false;
        return;
      }
    }

    const menu_agregar = this.menus.filter(element => {
      return element.id_padre === this.arr_parcial[ind].id && this.arr_parcial[ind].expandido === false;
    });
    menu_agregar.forEach(element => {
      element.left = this.arr_parcial[ind].left + 15;
      element.level = this.arr_parcial[ind].level + 1;
      element.ultimo_elemento = false;
      const existeSubMenu = this.verificarExistenSubMenus(element.id);
      element.ultimo_elemento = !existeSubMenu;
      element['menuFinal'] = true;
      this.arr_parcial.splice(ind + 1, 0, element);
    });
    console.log(menu_agregar);
    this.arr_parcial[ind].expandido = true;
  }

  verificarExistenSubMenus(id_padre) {
    let existe = false;

    for (let index = 0; index < this.menus.length; index++) {
      const element = this.menus[index];
      if (element.id_padre === id_padre) {
        existe = true;
        break;
      }
    }
    return existe;
  }

  expandir(ind: any) {
    // si el item no tiene mas menus en su arreglo no se expandira
    if (!this.arr_parcial[ind].menus) {
      return;
    }
    ///////////////////////// eliminar ////////////////////////////////
    /// si el indice es menor al largo del arrego entro en el if
    if (ind < this.arr_parcial.length - 1) {
      // si el arreglo siguiente tiene un left mayor que el actual
      /// esto significa que los shuientes datos son hijos del item que seleccionamos
      if (this.arr_parcial[ind + 1].left > this.arr_parcial[ind].left) {
        let ultimoAeliminar = 0;
        /// for para eliminar un los elementos hijos cuando se hace click en un item
        for (let index = ind + 1; index < this.arr_parcial.length; index++) {
          const element = this.arr_parcial[index];
          if (element.left > this.arr_parcial[ind].left) {
            ultimoAeliminar++;
          } else {
            break;
          }
        }
        /// elimina los arreglos desde el que seleccionamos
        this.arr_parcial.splice(ind + 1, ultimoAeliminar);
        return;
      }
    }
    ///////////////////////// agregar //////////////////////////////////
    /// reconstruimos el arreglo desde donde hacemos click
    this.arr_parcial[ind].menus.forEach(element => {
      /// le damos un left de 10 mas que el arreglo padre
      /// para que los items hijos tengan un margen mayor
      element.left = this.arr_parcial[ind].left + 10;
      element.level = this.arr_parcial[ind].level + 1;
      if (element.menus.length === 0) {
        element.ultimo_elemento = true;
        console.log('no hay mas', element);
      }
      /// agregamos el arreglo al que ya existe
      this.arr_parcial.splice(ind + 1, 0, element);
    });

  }

  generarMenusPrincipales(menus) {
    this.menus_principal = menus.filter(element => {
      let cant_split = element.id.split(".");
      if (cant_split.length <= 2) {
        return element
      }
    });
    console.log(this.menus_principal);
  }

  selectMenu(idMenu) {
    this.cargarfuncionalidadesUsuario(this.user)
    this.arr_parcial = this.menus.filter(element => {
      element['left'] = 10;
      element['level'] = 0;
      element['check'] = false;
      element['expandido'] = false;
      let existeSubMenu = this.verificarExistenSubMenus(element.id);
      element.ultimo_elemento = !existeSubMenu;
      element['menuFinal'] = true;
      return element.id_padre === idMenu && element.id_padre != element.id;
    });
    console.log(this.arr_parcial);
  }

  recibirMenu(mn: any) {
    this.arr_parcial = [];
    // this.titulo = mn.nombre;
    console.log(mn);
    mn.forEach(element => {
      element['left'] = 10;
      element['level'] = 0;
      element['check'] = false;
      this.arr_parcial.push(element);
    });
  }

  check(estado, ind, level) {
    console.log(estado)
    if (this.arr_parcial[ind].id === '') {
      return;
    }
    if (this.arr_parcial[ind].funcionalidad) {
      console.log(this.arr_parcial[ind])
      console.log(this.arr_parcial[ind].check)
      if (estado) {
        console.log(" Agregar ", this.arr_parcial[ind].check);
        this.agregarFuncionalidad(this.arr_parcial[ind]);
      } else {
        console.log("Eliminar", this.arr_parcial[ind].check);
        this.eliminarFuncionalidad(this.arr_parcial[ind]);
      }
    } else {
      this.checkGrupal(ind, estado);
    }
  }

  checkGrupal(ind, estado) {
    console.log(this.arr_parcial[ind], estado);
    const id_padre = this.arr_parcial[ind].id;
    console.log(id_padre);
    for (let index = ind; index < this.arr_parcial.length; index++) {
      const element = this.arr_parcial[index];
      console.log(element);
      if (element.id_padre === id_padre) {
        if (element.funcionalidad) {
          const item = {
            id_padre: element.id_padre,
            id: element.id
          };
          if (estado) {
            this.agregarFuncionalidad(item);
          } else {
            this.eliminarFuncionalidad(item);
          }
        }
        element.check = estado;
      }
    }
  }

  norepetir(level) {
    console.log(this.arr_temporal)
    if (this.arr_temporal.indexOf(level) >= 0) {
      console.log("existe")
      return true;
    } else {
      this.arr_temporal.push(level);
      return false;
    }
  }
}