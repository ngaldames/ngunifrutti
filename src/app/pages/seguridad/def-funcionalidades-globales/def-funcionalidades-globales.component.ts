import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { AñadirPantallaComponent } from '../../../shared/dialogs/añadir-pantalla/añadir-pantalla.component';
import { MenuServiceService } from '../../../services/menu-service.service';
import { resource } from 'selenium-webdriver/http';


@Component({
  selector: 'app-def-funcionalidades-globales',
  templateUrl: './def-funcionalidades-globales.component.html',
  styleUrls: ['./def-funcionalidades-globales.component.css']
})

export class DefFuncionalidadesGlobalesComponent implements OnInit {

  codigo: any;
  pantalla: any;
  icons: any;
  displayedColumns: string[] = ['codigo', 'pantalla', 'icons'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public dialog: MatDialog,
    private menuServicios: MenuServiceService
  ) {
    this.menuServicios.listar().subscribe(resultado => {
      this.dataSource = new MatTableDataSource(resultado.items);
    });
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Items por página';
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog() {
    this.dialog.open(AñadirPantallaComponent, {
      width: '250px',
    });
  }
}