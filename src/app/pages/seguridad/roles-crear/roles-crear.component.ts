import { ApiseguridadService } from './../../../services/apiseguridad.service';
import { ApiCommonService } from './../../../services/apicommonservice.service';
import { MenuServiceService } from './../../../services/menu-service.service';
import { Component, OnInit, OnDestroy, Injectable } from '@angular/core';
import { MatTableDataSource, MatSnackBar, MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription, BehaviorSubject } from 'rxjs';
// Servicios
import { RolesService } from '../../../services/roles.service';
// Interfaz
import { ActivatedRoute } from '@angular/router';
import { FlatTreeControl } from '@angular/cdk/tree';

export class TodoItemNode {
  children: TodoItemNode[];
  id: string;
  nombre: string;
}
export class TodoItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

const TREE_DATA = {
  'Fruta Granel': {
    'Recepcion fruta granel': {
      Agregar: null,
      Editar: null,
      Borrar: null,
      Imprimir: null,
      Pesar: null,
    },
    'Recepcion y despacho envases vacios': {
      Agregar: null,
      Editar: null,
      Borrar: null,
      Imprimir: null,
      Pesar: null,
    },
  },
  'Fruta Embalada': {
    'CKU: Rechazo/Levantar': {
      Agregar: null,
      Editar: null,
      Borrar: null,
      Imprimir: null,
      Pesar: null,
    },
    'Reclasificaciones': {
      Agregar: null,
      Editar: null,
      Borrar: null,
      Imprimir: null,
      Pesar: null,
    },
  },
};

@Injectable()
export class ChecklistDatabase {
  listadoPantallas: any[] = [];
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);
  get data(): TodoItemNode[] { return this.dataChange.value; }

  constructor(
    public _pantallaService: ApiseguridadService,
    public snackBar: MatSnackBar,
  ) {
    this.obtenerDatos();
    this.initialize();
  }

  obtenerDatos() {
    this._pantallaService.obtenerPantallas()
      .subscribe((data) => {
        // console.log('PANTALLAS ' + JSON.stringify(data));
        this.listadoPantallas = data['items'];
        // console.log('LISTADO ' + JSON.stringify(this.listadoPantallas));
      }, (error) => {
        this.snackBar.open('Error listado menu', null, { duration: 3000 });
      });
  }

  initialize() {
    const data = this.buildFileTree(this.listadoPantallas, 0);
    // console.log('DATA ' + JSON.stringify(data));
    this.dataChange.next(data);
  }

  construirArbol() {

  }

  buildFileTree(obj: { [key: string]: any }, level: number): TodoItemNode[] {
    JSON.stringify('OBJETO ' + JSON.stringify(obj));
    return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new TodoItemNode();
      node.id = key;
      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.id = value;
        }
      }
      return accumulator.concat(node);
    }, []);
  }
}

@Component({
  selector: 'app-roles-crear',
  templateUrl: './roles-crear.component.html',
  styleUrls: ['./roles-crear.component.css'],
  providers: [ChecklistDatabase]
})

export class RolesCrearComponent implements OnInit, OnDestroy {
  public panelOpenState = false;
  public _Subscription: Subscription;
  public listaMenu: any[] = [];
  public listaExportadoras: any[] = [];
  public listaPlantas: any[] = [];
  public listaPantallas: any[] = [];

  _usr = localStorage.getItem('User');
  public ListFuncionalidad: any = [];
  public DataSourceListaFuncionalidadMenu = new MatTableDataSource<any>();
  // Flags
  public cargando = true;
  public textoMensaje: string;
  public error = false;
  // Bits
  public bitGuardar: boolean;
  public bitEditar: boolean;
  public bitOK: boolean;
  // Variables
  public _TipoMenu: string;
  // Viene
  public id_rol: string
  public nombre_rol: string;

  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();
  selectedParent: TodoItemFlatNode | null = null;
  newItemName = '';
  treeControl: FlatTreeControl<TodoItemFlatNode>;
  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;
  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

  getLevel = (node: TodoItemFlatNode) => node.level;
  isExpandable = (node: TodoItemFlatNode) => node.expandable;
  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;
  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;
  hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

  constructor(
    private route: ActivatedRoute,
    public snackBar: MatSnackBar,
    public _menuService: MenuServiceService,
    public _commonService: ApiCommonService,
    public _pantallaService: ApiseguridadService,
    private database: ChecklistDatabase
  ) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    database.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
  }

  ngOnInit() {
    // Traer datos desde el listado
    this._Subscription = this.route.queryParams
      .subscribe(params => {
        this.id_rol = params['rol_id'];
        this.nombre_rol = params['rol_des'];
      });
    this.cargarExportadoras();
    this.cargarPlantas();
    this.cargarMenus();
    // this.cargarPantallas();
    // this.cargarMenu();
  }

  ngOnDestroy() {
    this._Subscription.unsubscribe();
  }

  cargarPlantas() {
    this._commonService.cargarTodasPlantas()
      .subscribe((data) => {
        // console.log('PLANTAS '+JSON.stringify(data));
        let lista = data['items'];
        this.listaPlantas = lista;
      }, (error) => {
        this.snackBar.open('Error al cargar las plantas', null, { duration: 3000 });
      });
  }

  cargarExportadoras() {
    this._commonService.cargarTodasExportadoras()
      .subscribe((data) => {
        // console.log('TODAS LAS EXPORTADORAS ' + JSON.stringify(data));
        let lista = data['items'];
        if (lista.length > 0) {
          this.listaExportadoras = lista;
        }
      }, (error) => {
        this.snackBar.open('Error al cargar las exportadoras', null, { duration: 3000 });
      });
  }

  cargarMenus() {
    this._Subscription = this._menuService.obtenerMenus()
      .subscribe((data) => {
        let lista = data['items'];
        if (lista.length > 0) {
          this.listaMenu = lista;
        }
      }, (error) => {
        this.snackBar.open('Error cargar menu', null, { duration: 3000 });
      });
  }

  cargarPantallas() {
    this._pantallaService.obtenerPantallas()
      .subscribe((data) => {
        let lista = data['items'];
        this.listaPantallas = lista;
        console.log('Pantallas ' + JSON.stringify(this.listaPantallas));
      }, (error) => {
        this.snackBar.open('Error cargar pantalla', null, { duration: 3000 });
      })
  }

  guardar() {
    this.snackBar.open(' ✔ \xa0\xa0 Datos guardados correctamente', null, { duration: 2000 });
  }

  transformer = (node: TodoItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.item === node.id ? existingNode : new TodoItemFlatNode();
    flatNode.item = node.id;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  descendantsAllSelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  todoItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    this.checkAllParentsSelection(node);
  }

  todoLeafItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  checkAllParentsSelection(node: TodoItemFlatNode): void {
    let parent: TodoItemFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  checkRootNodeSelection(node: TodoItemFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
    const currentLevel = this.getLevel(node);
    if (currentLevel < 1) {
      return null;
    }
    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];
      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  // saveNode(node: TodoItemFlatNode, itemValue: string) {
  //   const nestedNode = this.flatNodeMap.get(node);
  //   this.database.updateItem(nestedNode!, itemValue);
  // }

  cargarMenu() {
    // this._Subscription = this.wsRoles.getListadoMenu()
    //   .subscribe((data: any) => {
    //     if (data['ListadoMenuResult']['lista'].length === 0) {
    //       this.textoMensaje = 'Sin datos para mostrar';
    //       this.error = true;
    //     } else {
    //       // Correcto
    //       this.DataSourceListaMenu = data['ListadoMenuResult']['lista'];
    //     }
    //   }, (error) => {

    //   });
  }

  // doSomething(value: any) {
  //   this.DataSourceListaFuncionalidadMenu.data = [];
  //   this.ListFuncionalidad = [];
  //   // ROL : ADMIN -> 1
  //   this._Subscription = this.wsRoles.getListadoFuncionalidadMenu('1', value)
  //     .subscribe((data: any) => {
  //       console.log(JSON.stringify(data));
  //       if (data['ListadoFuncionalidadMenuRolResult']['lista'] === null) {
  //         this.textoMensaje = 'Sin datos para mostrar';
  //         this.error = true;
  //         return;
  //       }
  //       if (data['ListadoFuncionalidadMenuRolResult']['lista'].length === 0) {
  //         this.textoMensaje = 'Sin datos para mostrar';
  //         this.error = true;
  //       } else {
  //         // Correcto
  //         data['ListadoFuncionalidadMenuRolResult']['lista'].forEach(element => {
  //           // tslint:disable-next-line:prefer-const
  //           let registro = {
  //             ch1: element.Asignado,
  //             codigo: element.des_funcionalidad_funcionalidadDepende,
  //             descripcion: element.des_funcionalidad_funcionalidad
  //           };
  //           this.ListFuncionalidad.push(registro);
  //         });
  //         this.DataSourceListaFuncionalidadMenu.data = this.ListFuncionalidad;
  //       }
  //     }, (error) => {

  //     });
  // }
}
