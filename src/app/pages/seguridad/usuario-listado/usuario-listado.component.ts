import { ApiCommonService } from './../../../services/apicommonservice.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { MatDialog, MatTableDataSource, MatSnackBar } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { UsuarioAgregarComponent } from '../../../shared/dialogs/usuario-agregar/usuario-agregar.component';
import { ApiseguridadService } from '../../../services/apiseguridad.service';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';

export interface user {
  codigo: string;
  descripcion: string;
  activo: any;
  icons: any;
}
@Component({
  selector: 'app-usuario-listado',
  templateUrl: './usuario-listado.component.html',
  styleUrls: ['./usuario-listado.component.css']
})

export class UsuarioListadoComponent implements AfterViewInit, OnInit, OnDestroy {
  usuarioLista: user[] = [];
  displayedColumns: string[] = ['id', 'nombre', 'activo', 'icons'];
  dataSource = new MatTableDataSource(this.usuarioLista);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  id: string;
  VFilaTabla: user;
  private sub: Subscription;
  public cargando = true;
  usuario = localStorage.getItem('User');
  temporada = localStorage.getItem('temporada');
  constructor(
    public dialog: MatDialog,
    private router: Router,
    private apiseguridadService: ApiseguridadService,
    private apiCommonService: ApiCommonService,
    private snackBar: MatSnackBar
  ) { }

  onRowClicked(row) {
    this.id = row.id;
    this.VFilaTabla = row;
    console.log('Usuario: ', this.id);
    console.log('Row: ', this.VFilaTabla);
  }

  ngOnInit() {
    // this.apiseguridadService.listaProductores().subscribe( result => {
    //   console.log(result)
    // })
    const data ={
      "bit_pendientes": false,
      "class_medio_ambiente": {
        "CodTemp": this.temporada,
        "CodPlanta": "  22",
        "CodExportadora": "100",
        "CodUsuario": this.usuario
      },
      "class_paginacion": {
        "CantiReg": 50,
        "Pagina": 1
      }
     }
    this.apiCommonService.listarGuiaRecepcion(data).subscribe(result => {
      console.log(result)
    })
    const time = new Date().getTime();
    // console.log(formatDate(Date.now(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530'));
    this.loadUsuarios();
    console.log(new Date().getTime() - time);
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.loadUsuarios)
    ).subscribe();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(UsuarioAgregarComponent, {
      width: '300px',
      data: {
        editar: false
      }
    });

    dialogRef.afterClosed().subscribe(result_modal => {
      if (result_modal.close) {
        return;
      }
      this.apiseguridadService
        .AddUsuarios(
          result_modal.data.id,
          result_modal.data.nombre,
          result_modal.data.password,
          result_modal.data.activo,
          result_modal.data.desde,
          result_modal.data.hasta)
        .subscribe(result_ => {
          console.log(result_modal.data);
          this.usuarioLista.push(result_modal.data);
          this.dataSource = new MatTableDataSource(this.usuarioLista);
          console.log(result_);
        });
    });
  }

  usuarioscrear(id) {
    this.router.navigate(['/usuarioscrear'],
      {
        queryParams: {
          id: id
        }
      });
    this.loadUsuarios();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  removeSelectedRows(id) {
    const data: any = this.dataSource;
    const position = this.usuarioLista.findIndex((element: any) => element.id === id);
    if (position >= 0) {
      this.usuarioLista.splice(position, 1);
    }
    this.apiseguridadService.DelUsuarios(id).subscribe(result => {
      console.log(result);
    });
    this.dataSource = new MatTableDataSource<user>(this.usuarioLista);
  }

  editar(data) {
    console.log(data)
    const position = this.usuarioLista.findIndex((element: any) => element.id === data.id);
    const dialogRef = this.dialog.open(UsuarioAgregarComponent, {
      width: '250px',
      data: {
        editar: true,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.close) {
        return;
      }
      if (result.editando === true) {
        this.usuarioLista[position]['nombre'] = result.data.nombre;
        this.usuarioLista[position]['activo'] = result.data.activo;
        this.usuarioLista[position]['desde'] = result.data.desde;
        this.usuarioLista[position]['hasta'] = result.data.hasta;
        this.usuarioLista[position]['id'] = result.data.id;
        this.usuarioLista[position]['password'] = result.data.password;
        console.log(result.data.id,result.data.password);
        this.dataSource = new MatTableDataSource<user>(this.usuarioLista);

        this.apiseguridadService.actualizarUsuario(result.data.id, data).subscribe(result_update => {
          console.log(result_update);
        });
      }
    });
  }

  loadUsuarios() {
    this.cargando = true;
    this.sub = this.apiseguridadService.cargarListadoUsuarios('')
      .subscribe((data: any) => {
        if (data.items != null) {
          this.dataSource = new MatTableDataSource(data.items);
          this.dataSource.paginator = this.paginator;
          this.paginator._intl.itemsPerPageLabel = 'Items por página';
          this.dataSource.sort = this.sort;
          this.usuarioLista = data.items;
        } else {
          this.snackBar.open('Sin usuarios cargados', null, { duration: 3000 });
        }
        this.cargando = false;
      }, (error) => {
        this.snackBar.open('Error al cargar roles', null, { duration: 3000 });
        // console.log('Error ' + JSON.stringify(error));
        this.cargando = false;
      });
  }
}
