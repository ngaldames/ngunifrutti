import { Subir_documentoComponent } from './../../shared/dialogs/subir_documento/subir_documento.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.css']
})
export class DocumentosComponent implements OnInit {
  @ViewChild('nav', { read: DragScrollComponent }) ds: DragScrollComponent;
  
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  subirDocumento(): void {
    const dialogRef = this.dialog.open(Subir_documentoComponent, {
      width: '400px',
    });
  }
}
