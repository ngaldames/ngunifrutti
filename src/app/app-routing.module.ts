import { Documentos_electronicosComponent } from './pages/general/documentos_electronicos/documentos_electronicos.component';
import { Folios_productor_a_recibidorComponent } from './pages/logistica/fruta_embalada/despacho/vistas/folios_productor_a_recibidor/folios_productor_a_recibidor.component';
import { Recepcion_camionComponent } from './pages/logistica/fruta_embalada/despacho/vistas/recepcion_camion/recepcion_camion.component';
import { Listado_despacho_puertoComponent } from './pages/logistica/fruta_embalada/despacho/vistas/listado_despacho_puerto/listado_despacho_puerto.component';
import { Listado_asignacion_prod_a_recibidorComponent } from './pages/logistica/fruta_embalada/despacho/vistas/listado_asignacion_prod_a_recibidor/listado_asignacion_prod_a_recibidor.component';
import { Nueva_asignacion_opComponent } from './pages/logistica/fruta_embalada/despacho/vistas/nueva_asignacion_op/nueva_asignacion_op.component';
import { Listado_principal_folios_opComponent } from './pages/logistica/fruta_embalada/despacho/vistas/listado_principal_folios_op/listado_principal_folios_op.component';
import { Nuevo_registro_de_cambioComponent } from './pages/logistica/fruta_embalada/control_de_calidad/vistas/nuevo_registro_de_cambio/nuevo_registro_de_cambio.component';
import { Control_de_calidadComponent } from './pages/logistica/fruta_embalada/control_de_calidad/vistas/control_de_calidad/control_de_calidad.component';
import { Despacho_terceros_por_otComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_terceros_por_ot/despacho_terceros_por_ot.component';
import { Despacho_planta_tercerosComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_planta_terceros/despacho_planta_terceros.component';
import { Listado_tratamientoComponent } from './pages/logistica/fruta_granel/tratamientos/vistas/listado_tratamiento/listado_tratamiento.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { RecFrutaGranelVariedadComponent } from './pages/logistica/fruta_granel/recepcion/rec-fruta-granel-variedad/rec-fruta-granel-variedad.component';
import { RecepcionFrutaGranelComponent } from './pages/logistica/fruta_granel/recepcion/recepcion-fruta-granel/recepcion-fruta-granel.component';
import { Consulta_cambio_condicionComponent } from './pages/logistica/fruta_granel/recepcion/consulta_cambio_condicion/consulta_cambio_condicion.component';
import { Despacho_interplantaComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_interplanta/despacho_interplanta.component';
import { Despacho_interplanta_por_OTComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_interplanta_por_OT/despacho_interplanta_por_OT.component';
import { Anular_despacho_packingComponent } from './pages/logistica/fruta_granel/despacho/vistas/anular_despacho_packing/anular_despacho_packing.component';
import { Devolver_packing_por_otComponent } from './pages/logistica/fruta_granel/despacho/vistas/devolver_packing_por_ot/devolver_packing_por_ot.component';
import { Despacho_packing_por_otComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_packing_por_ot/despacho_packing_por_ot.component';
import { Despacho_a_packingComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_a_packing/despacho_a_packing.component';
import { Lista_contenedoresComponent } from './pages/logistica/parametros/vistas/lista_contenedores/lista_contenedores.component';
import { Lista_parametrosComponent } from './pages/logistica/parametros/vistas/lista_parametros/lista_parametros.component';
import { DocumentosComponent } from './pages/documentos/documentos.component';
import { DefFuncionalidadesGlobalesComponent } from './pages/seguridad/def-funcionalidades-globales/def-funcionalidades-globales.component';
import { CkuConTablaComponent } from './pages/logistica/fruta_granel/recepcion/cku-con-tabla/cku-con-tabla.component';
import { DestareComponent } from './pages/logistica/fruta_granel/recepcion/destare/destare.component';
import { RecFrutaGranelComponent } from './pages/logistica/fruta_granel/recepcion/rec-fruta-granel/rec-fruta-granel.component';
import { FavoritosComponent } from './pages/favoritos/favoritos.component';
import { LoginComponent } from './login/login.component';
import { CtacteEnvcosComponent } from './pages/logistica/fruta_granel/recepcion/ctacte-envcos/ctacte-envcos.component';
import { DesEnvvaciosComponent } from './pages/logistica/fruta_granel/recepcion/des-envvacios/des-envvacios.component';
import { RecCkuComponent } from './pages/logistica/fruta_granel/recepcion/rec-cku/rec-cku.component';
import { RecFrioComponent } from './pages/logistica/fruta_granel/recepcion/rec-frio/rec-frio.component';
import { RecTrazabilidadComponent } from './pages/logistica/fruta_granel/recepcion/rec-trazabilidad/rec-trazabilidad.component';
import { RolesListadoComponent } from './pages/seguridad/roles-listado/roles-listado.component';
import { RolesCrearComponent } from './pages/seguridad/roles-crear/roles-crear.component';
import { UsuarioCrearComponent } from './pages/seguridad/usuario-crear/usuario-crear.component';
import { UsuarioListadoComponent } from './pages/seguridad/usuario-listado/usuario-listado.component';
import { PantallaCkuComponent } from './pages/logistica/fruta_granel/recepcion/pantalla-cku/pantalla-cku.component';
import { Env_salidaComponent } from './pages/logistica/fruta_granel/recepcion/env_salida/env_salida.component';
import { BuscardormenuComponent } from './pages/buscardormenu/buscardormenu.component';
import { AñadirGuiasComponent } from './pages/logistica/fruta_granel/despacho/vistas/añadir-guias/añadir-guias.component';
import { TratamientosComponent } from './pages/logistica/fruta_granel/tratamientos/vistas/tratamientos/tratamientos.component';
import { Despacho_a_productoresComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_a_productores/despacho_a_productores.component';
import { Despacho_productores_por_otComponent } from './pages/logistica/fruta_granel/despacho/vistas/despacho_productores_por_ot/despacho_productores_por_ot.component';
import { Listado_recepcionComponent } from './pages/logistica/fruta_embalada/recepcion/vistas/listado_recepcion/listado_recepcion.component';
import { Nueva_recepcion_fruta_embaladaComponent } from './pages/logistica/fruta_embalada/recepcion/vistas/nueva_recepcion_fruta_embalada/nueva_recepcion_fruta_embalada.component';

const routes: Routes = [
  { path: 'seguridad', loadChildren: 'app/pages/pages.module#PagesModule' },
  { path: 'logistica', loadChildren: 'app/pages/pages.module#PagesModule' },
  { path: 'login', component: LoginComponent },
  { path: 'favoritos', component: FavoritosComponent },
  { path: 'recfrutagranel', component: RecFrutaGranelComponent },
  { path: 'recfrutagranelvariedad', component: RecFrutaGranelVariedadComponent },
  { path: 'recepcionfrutagranel', component: RecepcionFrutaGranelComponent },
  { path: 'ctacte_envcos', component: CtacteEnvcosComponent },
  { path: 'Des_env_vacios', component: DesEnvvaciosComponent },
  { path: 'rec_cku', component: RecCkuComponent },
  { path: 'rec_frio', component: RecFrioComponent },
  { path: 'rec_trazabilidad', component: RecTrazabilidadComponent },
  { path: 'roleslistado', component: RolesListadoComponent },
  { path: 'rolescrear', component: RolesCrearComponent },
  { path: 'usuarioscrear', component: UsuarioCrearComponent },
  { path: 'usuarioslistado', component: UsuarioListadoComponent },
  { path: 'pantallacku', component: PantallaCkuComponent },
  { path: 'destare', component: DestareComponent },
  { path: 'env_salida', component: Env_salidaComponent },
  { path: 'cku-tabla', component: CkuConTablaComponent },
  { path: 'def-funcionalidades-globales', component: DefFuncionalidadesGlobalesComponent },
  { path: 'buscador', component: BuscardormenuComponent },
  { path: 'documentos_globales', component: DocumentosComponent },
  { path: 'parametros_logistica', component: Lista_parametrosComponent },
  { path: 'parametros_contenedores', component: Lista_contenedoresComponent },
  { path: 'despacho_packing', component: Despacho_a_packingComponent },
  { path: 'despacho_packing_por_ot', component: Despacho_packing_por_otComponent },
  { path: 'devolver_packing_por_ot', component: Devolver_packing_por_otComponent },
  { path: 'anular_packing_por_ot', component: Anular_despacho_packingComponent },
  { path: 'despacho_interplanta', component: Despacho_interplantaComponent },
  { path: 'despacho_interplanta_ot', component: Despacho_interplanta_por_OTComponent },
  { path: 'despacho_planta_terceros', component: Despacho_planta_tercerosComponent},
  { path: 'despacho_productores', component: Despacho_a_productoresComponent },
  { path: 'consulta_cambio_condicion', component: Consulta_cambio_condicionComponent },
  { path: 'anadir_guias', component: AñadirGuiasComponent },
  { path: 'tratamientos', component: TratamientosComponent },
  { path: 'listado_tratamientos', component: Listado_tratamientoComponent },
  { path: 'despacho_terceros_por_ot', component: Despacho_terceros_por_otComponent },
  { path: 'despacho_productores_por_ot', component: Despacho_productores_por_otComponent },
  { path: 'control_de_calidad_fruta_embalada', component: Control_de_calidadComponent },
  { path: 'nuevo_registro_cambio', component: Nuevo_registro_de_cambioComponent },
  { path: 'listado_recepcion_fruta_embalada', component: Listado_recepcionComponent },
  { path: 'nueva_recepcion_fruta_embalada', component: Nueva_recepcion_fruta_embaladaComponent },
  { path: 'listado_principal_folios_op', component: Listado_principal_folios_opComponent },
  { path: 'nueva_asignacion_op', component: Nueva_asignacion_opComponent },
  { path: 'listado_principal_asignacion_prod', component: Listado_asignacion_prod_a_recibidorComponent },
  { path: 'listado_despacho_puerto_aeropuerto', component: Listado_despacho_puertoComponent },
  { path: 'recepcion_camion_despacho_puerto', component: Recepcion_camionComponent },
  { path: 'folios_productor_a_recibidor', component: Folios_productor_a_recibidorComponent },
  { path: 'documentos_electronicos', component: Documentos_electronicosComponent },
  { path: '', redirectTo: '/favoritos', pathMatch: 'full' },
  { path: '**', component: FavoritosComponent }
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
