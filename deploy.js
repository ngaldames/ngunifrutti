var FtpDeploy = require('ftp-deploy');
var ftpDeploy = new FtpDeploy();

var config = {
    user: "unistgont/pall",                   // NOTE that this was username in 1.x 
    password: "Unidev.2019",           // optional, prompted if none given
    host: "172.18.8.139",
    port: 21,
    localRoot: __dirname + '/dist/Unifrutti/',
    remoteRoot: '/prueba/',
    include: ['*', '**/*'],      // this would upload everything except dot files
    // include: ['*.php', 'dist/*'],
    exclude: ['dist/**/*.map', 'assets'],     // e.g. exclude sourcemaps - ** exclude: [] if nothing to exclude **
    deleteRemote: false,              // delete ALL existing files at destination before uploading, if true
    forcePasv: true                 // Passive mode is forced (EPSV command is not sent)
}

// use with promises
ftpDeploy.deploy(config)
    .then(res => console.log('Terminado:', res))
    .catch(err => console.log(err))

// use with callback
ftpDeploy.deploy(config, function (err, res) {
    if (err) console.log(err)
    else console.log('Terminado callback:', res);
}); 